#include<iostream>
using namespace std;
#include<string>
#include<time.h>

class Info
{
public:
	string name;
	string author;
	string time;
};

class Seqlist
{
public:
	Seqlist(int space = 1)
		:_capacity(space)
		, _size(0)
	{
		_arr = new Info[space];
	}

	void Print();
	int find_by_name(string s1)
	{
		for (int i = 0; i < _size; i++)
		{
			if (s1 == _arr[i].name)
				return i;
		}
		return -1;
	}

	//��ɾ
	void Add();
	void Delete();
	void random_song();

	~Seqlist()
	{
		delete[] _arr;
		_capacity = 0;
		_size = 0;
	}
private:
	Info* _arr;
	int _capacity;
	int _size;
};

void Seqlist::Print()
{
	for (int i = 0; i < _size; i++)
	{
		cout << _arr[i].name << " " << endl;
	}
}

void Seqlist::Add()
{
	if (_size == _capacity)
	{
		Info* tmp = _arr;
		_arr = new Info[2 * _size];
		for (int i = 0; i < _size; i++)
			_arr[i] = tmp[i];
		delete[] tmp;
		_capacity *= 2;
	}
	Info s1;
	cout << "name:";
	getline(cin, s1.name);
	cout << "author:";
	getline(cin, s1.author);
	cout << "time:";
	getline(cin, s1.time);
	_arr[_size++] = s1;
}

void Seqlist::Delete()
{
	string s1;
	cout << "Enter the song's name which you want to delete:";
	getline(cin, s1);
	int pos = find_by_name(s1);
	if (pos < 0)
	{
		cout << "Can not find this song" << endl;
		return;
	}
	for (int i = pos + 1; i < _size; i++)
	{
		_arr[i - 1] = _arr[i];
	}
	_size--;
}

void Seqlist::random_song()
{
	int rand_num = rand() % _size;
	cout << "The random song is:" << _arr[rand_num].name << endl;
}


int main()
{
	srand((unsigned int)time(nullptr));
	Seqlist s1(1);
	s1.Add();//��
	s1.Add();
	s1.Add();
	s1.Print();
	s1.Delete();//ɾ
	s1.Print();
	s1.random_song();//�������
	return 0;
}