#include"BSTree.h"
#include<string>

namespace key
{
	void testkeybstree()
	{
		int a[] = { 1,2,3,4,5 };
		BSTree<int> bs;
		for (auto e : a)
		{
			bs.insert(e);
		}
		bs.InOrder();

		for (auto e : a)
		{
			bs.InOrder();
			bs.erase(e);
		}
	}
}

namespace key_value
{
	void test_key_value_bstree()
	{
		BSTree<string, string> bs;

		bs.insert("left", "���");
		bs.insert("right", "�ұ�");
		bs.insert("word", "����");
		bs.insert("I love you", "��ϲ����");
		bs.insert("I hate you", "��������");

		bs.InOrder();

		bs.erase("left");
		bs.InOrder();
		bs.erase("right");
		bs.InOrder();
		bs.erase("word");
		bs.InOrder();
		bs.erase("I love you");
		bs.InOrder();
		bs.erase("I hate you");

	}
}

int main()
{
	//key::testkeybstree();
	key_value::test_key_value_bstree();
	return 0;
}
