#pragma once
#include<iostream>
using namespace std;

namespace key
{
	template<class k>
	struct BSTNode
	{
		k _key;
		BSTNode<k>* _left;
		BSTNode<k>* _right;

		BSTNode(const k& key)
			:_key(key)
			,_left(nullptr)
			, _right(nullptr)
		{}
	};

	template<class k>
	class BSTree
	{
		typedef BSTNode<k> Node;
	public:
		bool insert(const k& key)
		{
			if (_root == nullptr)
			{
				_root = new Node(key);
				return true;
			}

			Node* parent = nullptr;
			Node* cur = _root;

			while (cur)
			{
				if (cur->_key < key)
				{
					parent = cur;
					cur = cur->_right;
				}
				else if(cur->_key > key)
				{
					parent = cur;
					cur = cur->_left;
				}
				else
				{
					return false;
				}
			}

			cur = new Node(key);
			if (parent->_key < key)
			{
				parent->_right = cur;
			}
			else
			{
				parent->_left = cur;
			}
			return true;

		}

		bool Find(const k& key)
		{
			Node* cur = _root;

			while (cur)
			{
				if (cur->_key < key)
					cur = cur->_right;
				else if (cur->_key > key)
					cur = cur->_left;
				else
					return true;
			}
			return false;
		}

		bool erase(const k& key)
		{
			Node* parent = nullptr;
			Node* cur = _root;

			while (cur)
			{
				if (cur->_key < key)
				{
					parent = cur;
					cur = cur->_right;
				}
				else if (cur->_key > key)
				{
					parent = cur;
					cur = cur->_left;
				}
				else
				{
					// 没有孩子 and 只有一个孩子
					// 有两个孩子
					if (cur->_left == nullptr)
					{
						if (parent == nullptr)
						{
							_root = cur->_right;
						}
						else
						{
							if (cur == parent->_left)
								parent->_left = cur->_right;
							else
								parent->_right = cur->_right;
						}
						delete cur;
						return true;
					}
					else if (cur->_right == nullptr)
					{
						if (parent == nullptr)
						{
							_root = cur->_left;
						}
						else
						{
							if (cur == parent->_left)
								parent->_left = cur->_left;
							else
								parent->_right = cur->_left;
						}
						delete cur;
						return true;
					}
					else
					{
						// 这是有两个孩子的情况
						// 我们就需要找左的最右 或 右的最左√
						Node* rightMinP = cur;
						Node* rightMin = cur->_right;

						while (rightMin->_left)
						{
							rightMinP = rightMin;
							rightMin = rightMin->_left;
						}

						cur->_key = rightMin->_key;

						if (rightMinP->_right == rightMin)
							rightMinP->_right = rightMin->_right;
						else
							rightMinP->_left = rightMin->_right;

						delete rightMin;
						return true;
					}
				}
			}
			return false;
		}

		void InOrder()
		{
			_Inorder(_root);
			cout << endl;
		}

	private:
		void _Inorder(Node* root)
		{
			if (root == nullptr)return;

			_Inorder(root->_left);
			cout << root->_key << " ";
			_Inorder(root->_right);
		}

		Node* _root = nullptr;
	};

}




namespace key_value
{
	template<class k, class v>
	struct BSTNode
	{
		k _key;
		v _value;
		BSTNode<k, v>* _left;
		BSTNode<k, v>* _right;

		BSTNode(const k& key, const v& value)
			:_key(key)
			,_value(value)
			, _left(nullptr)
			, _right(nullptr)
		{}
	};

	template<class k, class v>
	class BSTree
	{
		typedef BSTNode<k, v> Node;
	public:
		bool insert(const k& key, const v& value)
		{
			if (_root == nullptr)
			{
				_root = new Node(key, value);
				return true;
			}

			Node* parent = nullptr;
			Node* cur = _root;

			while (cur)
			{
				if (cur->_key < key)
				{
					parent = cur;
					cur = cur->_right;
				}
				else if (cur->_key > key)
				{
					parent = cur;
					cur = cur->_left;
				}
				else
				{
					return false;
				}
			}

			cur = new Node(key, value);
			if (parent->_key < key)
			{
				parent->_right = cur;
			}
			else
			{
				parent->_left = cur;
			}
			return true;

		}

		bool Find(const k& key)
		{
			Node* cur = _root;

			while (cur)
			{
				if (cur->_key < key)
					cur = cur->_right;
				else if (cur->_key > key)
					cur = cur->_left;
				else
					return true;
			}
			return false;
		}

		bool erase(const k& key)
		{
			Node* parent = nullptr;
			Node* cur = _root;

			while (cur)
			{
				if (cur->_key < key)
				{
					parent = cur;
					cur = cur->_right;
				}
				else if (cur->_key > key)
				{
					parent = cur;
					cur = cur->_left;
				}
				else
				{
					// 没有孩子 and 只有一个孩子
					// 有两个孩子
					if (cur->_left == nullptr)
					{
						if (parent == nullptr)
						{
							_root = cur->_right;
						}
						else
						{
							if (cur == parent->_left)
								parent->_left = cur->_right;
							else
								parent->_right = cur->_right;
						}
						delete cur;
						return true;
					}
					else if (cur->_right == nullptr)
					{
						if (parent == nullptr)
						{
							_root = cur->_left;
						}
						else
						{
							if (cur == parent->_left)
								parent->_left = cur->_left;
							else
								parent->_right = cur->_left;
						}
						delete cur;
						return true;
					}
					else
					{
						// 这是有两个孩子的情况
						// 我们就需要找左的最右 或 右的最左√
						Node* rightMinP = cur;
						Node* rightMin = cur->_right;

						while (rightMin->_left)
						{
							rightMinP = rightMin;
							rightMin = rightMin->_left;
						}

						cur->_key = rightMin->_key;
						cur->_value = rightMin->_value;

						if (rightMinP->_right == rightMin)
							rightMinP->_right = rightMin->_right;
						else
							rightMinP->_left = rightMin->_right;

						delete rightMin;
						return true;
					}
				}
			}
			return false;
		}

		void InOrder()
		{
			_Inorder(_root);
			cout << endl;
		}

	private:
		void _Inorder(Node* root)
		{
			if (root == nullptr)return;

			_Inorder(root->_left);
			cout << root->_key << " " << root->_value << endl;
			_Inorder(root->_right);
		}

		Node* _root = nullptr;
	};

}