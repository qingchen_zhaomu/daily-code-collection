#include<iostream>
using namespace std;
#include<vector>
#include<string>

class Solution {
public:
    vector<int> smallestK(vector<int>& nums, int k)
    {
        srand(time(NULL));
        qsort(nums, 0, nums.size() - 1, k);
        return { nums.begin(), nums.begin() + k };
    }

    void qsort(vector<int>& nums, int l, int r, int k)
    {
        if (l >= r)return;
        //随机生成 k
        int key = get_rand(nums, l, r);
        int left = l - 1, right = r + 1, i = l;
        //快排主逻辑
        while (i < right)
        {
            if (nums[i] < key)swap(nums[i++], nums[++left]);
            else if (nums[i] == key)i++;
            else swap(nums[i], nums[--right]);
        }
        //分类讨论
        int a = left - l + 1, b = right - left - 1, c = r - right;
        if (k < a)qsort(nums, l, left, k);
        else if (k <= a + b)return;
        else qsort(nums, right, r, k - a - b);
    }

    int get_rand(vector<int> nums, int left, int right)
    {
        return nums[rand() % (right - left + 1) + left];
    }
};

//class Solution {
//public:
//    vector<int> tmp;
//    vector<int> sortArray(vector<int>& nums)
//    {
//        tmp.resize(nums.size());
//        MergeSort(nums, 0, nums.size() - 1);
//        return nums;
//    }
//    void MergeSort(vector<int>& nums, int left, int right)
//    {
//        if (left >= right)return;
//
//        int mid = (right + left) >> 1;
//
//        MergeSort(nums, left, mid);
//        MergeSort(nums, mid + 1, right);
//
//        int cur1 = left, cur2 = mid + 1, i = 0;
//        while (cur1 <= mid && cur2 <= right)
//            tmp[i++] = nums[cur1] < nums[cur2] ? nums[cur1++] : nums[cur2++];
//
//        while (cur1 <= mid)tmp[i++] = nums[cur1++];
//        while (cur2 <= right)tmp[i++] = nums[cur2++];
//
//        for (int i = left; i <= right; i++)
//            nums[i] = tmp[i - left];
//    }
//};

//class Solution {
//public:
//    vector<int> tmp;
//    int reversePairs(vector<int>& nums)
//    {
//        tmp.reserve(nums.size());
//        return mergesort(nums, 0, nums.size() - 1);
//    }
//    int mergesort(vector<int>& nums, int left, int right)
//    {
//        if (left >= right)return 0;
//        int mid = (right + left) >> 1;
//        int ret = 0;
//        ret += mergesort(nums, left, mid);
//        ret += mergesort(nums, mid + 1, right);
//
//        int cur1 = left, cur2 = mid + 1, i = 0;
//        while (cur1 <= mid && cur2 <= right)
//        {
//            if (nums[cur1] <= nums[cur2])
//                tmp[i++] = nums[cur2++];
//            else
//            {
//                ret += right - cur2 + 1;
//                tmp[i++] = nums[cur1++];
//            }
//        }
//
//        while (cur1 <= mid) tmp[i++] = nums[cur1++];
//        while (cur2 <= right) tmp[i++] = nums[cur2++];
//        for (int j = left; j <= right; j++)
//            nums[j] = tmp[j - left];
//
//        return ret;
//    }
//};

//class Solution {
//public:
//    vector<int> tmp_nums;
//    vector<int> tmp_index;
//    vector<int> index;
//
//    vector<int> countSmaller(vector<int>& nums)
//    {
//        tmp_nums.reserve(nums.size());
//        tmp_index.reserve(nums.size());
//        vector<int> ret(nums.size());
//        for (int i = 0; i < nums.size(); i++) index.push_back(i);
//        MergeSort(nums, 0, nums.size() - 1, index, ret);
//        return ret;
//    }
//
//    void MergeSort(vector<int>& nums, int left, int right,
//        vector<int>& index, vector<int>& ret)
//    {
//        if (left >= right)return;
//        int mid = (right + left) >> 1;
//
//        MergeSort(nums, left, mid, index, ret);
//        MergeSort(nums, mid + 1, right, index, ret);
//
//        int cur1 = left, cur2 = mid + 1, i = 0;
//        while (cur1 <= mid && cur2 <= right)
//        {
//            if (nums[cur1] <= nums[cur2])
//            {
//                tmp_nums[i] = nums[cur2];
//                tmp_index[i] = index[cur2];
//                i++, cur2++;
//            }
//            else
//            {
//                ret[index[cur1]] += right - cur2 + 1;
//                tmp_nums[i] = nums[cur1];
//                tmp_index[i] = index[cur1];
//                i++, cur1++;
//            }
//        }
//        while (cur1 <= mid)
//        {
//            tmp_nums[i] = nums[cur1];
//            tmp_index[i] = index[cur1];
//            i++, cur1++;
//        }
//        while (cur2 <= right)
//        {
//            tmp_nums[i] = nums[cur2];
//            tmp_index[i] = index[cur2];
//            i++, cur2++;
//        }
//        for (int j = left; j <= right; j++)
//        {
//            nums[j] = tmp_nums[j - left];
//            index[j] = tmp_index[j - left];
//        }
//    }
//};

//class Solution {
//public:
//    vector<int> tmp;
//    int reversePairs(vector<int>& nums)
//    {
//        tmp.reserve(nums.size());
//        return MergeSort(nums, 0, nums.size() - 1);
//    }
//    int MergeSort(vector<int>& nums, int left, int right)
//    {
//        if (left >= right)return 0;
//        int mid = (right + left) >> 1;
//        int ret = 0;
//        ret += MergeSort(nums, left, mid);
//        ret += MergeSort(nums, mid + 1, right);
//
//        int cur1 = left, cur2 = mid + 1, i = left;
//        //计算翻转对个数
//        while (cur1 <= mid)
//        {
//            while (cur2 <= right && nums[cur1] / 2.0 <= nums[cur2])
//                cur2++;
//            if (cur2 > right)break;
//            ret += right - cur2 + 1;
//            cur1++;
//        }
//
//        cur1 = left, cur2 = mid + 1;
//        while (cur1 <= mid && cur2 <= right)
//        {
//            if (nums[cur1] <= nums[cur2]) tmp[i++] = nums[cur2++];
//            else tmp[i++] = nums[cur1++];
//        }
//        while (cur1 <= mid) tmp[i++] = nums[cur1++];
//        while (cur2 <= right) tmp[i++] = nums[cur2++];
//
//        for (int j = left; j <= right; j++)
//            nums[j] = tmp[j];
//        return ret;
//    }
//};

int main()
{

	return 0;
}
