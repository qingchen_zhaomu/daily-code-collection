#include<iostream>
using namespace std;
#include<vector>
#include<string>
#include<unordered_map>

class Solution 
{
public:
	void sortarray(vector<int>& nums, int left, int right)
	{
		if (left >= right) return;
		int key = left;
		int start = left, end = right;
		while (left < right)
		{
			while (left < right && nums[right] >= nums[key])right--;
			while (left < right && nums[left] <= nums[key])left++;
			swap(nums[right], nums[left]);
		}
		swap(nums[key], nums[left]);
		key = left;
		sortarray(nums, start, key - 1);
		sortarray(nums, key + 1, end);
	}

	vector<int> SortArray(vector<int>& nums)
	{
		sortarray(nums, 0, nums.size()-1);
		return nums;
	}
};

int main()
{
	Solution s1;
	vector<int> v1 = { 6,3,8,2,5,4,7 };
	s1.SortArray(v1);
	for (auto e : v1)
		cout << e << " ";
	cout << endl;

	return 0;
}

//class Solution {
//public:
//    void sortColors(vector<int>& nums)
//    {
//        int i = 0, left = -1, right = nums.size();
//        while (i < right)
//        {
//            if (nums[i] == 0) swap(nums[i++], nums[++left]);
//            else if (nums[i] == 2) swap(nums[i], nums[--right]);
//            else i++;
//        }
//    }
//};

//class Solution {
//public:
//    int minNumberOfFrogs(string croakOfFrogs)
//    {
//        string t = "croak";
//        int len = t.size();
//        vector<int> hash(len); //初始化默认为0
//        //            key : value
//        unordered_map<char, int> index;
//        for (int i = 0; i < len; i++)
//            index[t[i]] = i;//记录下标
//
//        for (auto e : croakOfFrogs)
//        {
//            if (e == 'c')
//            {
//                if (hash[len - 1]) hash[len - 1]--;
//                hash[index[e]]++;
//            }
//            else
//            {
//                int i = index[e];
//                if (hash[i - 1])hash[i - 1]--, hash[i]++;
//                else return -1;
//            }
//        }
//        for (int i = 0; i < len - 1; i++)
//            if (hash[i])return -1;
//        return hash[len - 1];
//    }
//};

//class Solution {
//public:
//    string countAndSay(int n)
//    {
//        string ret = "1";
//        for (int i = 1; i < n; i++)
//        {
//            string orstr;
//            int left = 0, right = 0;
//            for (; right < ret.size();)
//            {
//                right++;
//                if (right < ret.size() && ret[right] != ret[left])
//                {
//                    orstr += to_string(right - left) + ret[left];
//                    //orstr += (char)(right - left);
//                    //orstr += (char)(ret[left]);
//                    left = right;
//                }
//            }
//            orstr += to_string(right - left) + ret[left];
//            //orstr += (char)(right - left);
//            //orstr += (char)(ret[left]);
//            ret = orstr;
//        }
//        return ret;
//    }
//};

