#include<iostream>
using namespace std;
#include<vector>
#include<string>

class Stock 
{
private:
    string _company; // 公司名称
    int _shares; // 股票数量
    float _share_val; // 每股的价格
    float _total_val; // 股票总价

public:
    Stock(string company, int shares, float share_val)
        :_company(company)
        ,_shares(shares)
        ,_share_val(share_val)
    {}

    void buy(int num, float price)
    {
        if (num < 0)cout << "Number of shares purchased can't be negative " << endl;
        else
        {
            _shares += num;
            _share_val = price;
            set_tot();
        }
    }
    void sell(int num, float price)
    {
        if (num < 0) cout << "Number of shares sold can't be nagetive" << endl;
        else if (num > _shares) cout << "You can't sell more than you have" << endl;
        else
        {
            _shares -= num;
            _share_val = price;
            set_tot();
        }
    }
    void set_tot() { _total_val = _shares * _share_val; }

    void update(float price)
    {
        _share_val = price;
        set_tot();
    }
    void show()const
    {
        cout << "company :" << _company << endl;
        cout << "Number of shares :" << _shares << endl;
        cout << "Price per share :" << _share_val << endl;
        cout << "The total price of the stock :" << _total_val << endl;
        cout << "——————————————————————" << endl;
    }

    Stock topval(Stock& com)
    {
        com.set_tot();
        set_tot();
        if ((*this)._total_val < com._total_val)return com;
        return *this;
    }
};


int main()
{
    vector<Stock> v1 = { {"Tesla", 100, 178.08}, 
                         {"Apple", 50, 192.25}, 
                         {"Dis", 150, 103},
                         {"alibaba", 140, 183.31} };
    Stock c1 = v1[1].topval(v1[2]);
    c1.show(); cout << endl;

	return 0;
}


