#include<iostream>
using namespace std;
#include<vector>
#include<string>
#include<unordered_map>



//class Solution {
//public:
//    bool containsDuplicate(vector<int>& nums)
//    {
//        unordered_map<int, int> hash; // <nums[i], i>
//        for (auto e : nums)
//        {
//            if (hash.count(e))return true;
//            hash[e]++;
//        }
//        return false;
//    }
//};

//class Solution {
//public:
//    vector<int> twoSum(vector<int>& nums, int target)
//    {
//        unordered_map<int, int> hash; //<nums[i], i>
//        for (int i = 0; i < nums.size(); i++)
//        {
//            int x = target - nums[i];
//            if (hash.count(x))return { hash[x], i };
//            else hash[nums[i]] = i;
//        }
//        return { -1, -1 };
//    }
//};

//class Solution {
//public:
//    bool CheckPermutation(string s1, string s2)
//    {
//        if (s1.size() != s2.size())return 0;
//        int hash[26] = { 0 };
//        for (auto e : s1) hash[e - 'a']++;
//        for (auto e : s2) if (hash[e - 'a']-- == 0)return 0;
//        return true;
//    }
//};

//class Solution {
//public:
//    string longestCommonPrefix(vector<string>& nums)
//    {
//        if (nums.size() == 1) return "";
//        string tmp1 = nums[0], tmp2 = nums[1];
//        int left = 0;
//        for (; tmp1.size() - 1 > left && tmp2.size() - 1 > left && tmp1[left] == tmp2[left]; left++) { ; }
//        string tmp = tmp1.substr(0, left);
//        if (nums.size() == 2) return tmp;
//        for (int i = 2; i < nums.size(); i++)
//        {
//            string res = nums[i];
//            int l = 0;
//            for (; tmp.size() - 1 > l && res.size() - 1 > l && tmp[l] == res[l]; l++) { ; }
//            tmp = tmp.substr(0, left);
//        }
//        return tmp;
//    }
//};

//class Solution {
//public:
//    string findcommon(string s1, string s2)
//    {
//        if (s1 == "" || s2 == "")return "";
//        int index = 0;
//        while (index < s1.size() && index < s2.size() && s1[index] == s2[index])index++;
//        return s1.substr(0, index);
//    }
//
//    string longestCommonPrefix(vector<string>& nums)
//    {
//        if (nums.size() == 1)return nums[0];
//        string ret = nums[0];
//        for (int i = 1; i < nums.size(); i++)
//            ret = findcommon(ret, nums[i]);
//        return ret;
//    }
//};

//class Solution {
//public:
//    string longestPalindrome(string s)
//    {
//        if (s.size() == 2 || s.size() == 1)return s;
//        string tmp = s.substr(0, 1);
//        for (int i = 1; i < s.size() - 1; i++)
//        {
//            int left = i, right = i;
//            // 第一遍循环先遍历为奇数的情况
//            while (left >= 0 && right <= s.size()-1 && s[left] == s[right])left--, right++;
//            left++, right--;
//            if (right - left + 1 > tmp.size()) tmp = s.substr(left, right - left + 1);
//
//            //第二遍遍历为考虑偶数长度的情况
//            left = i, right = i + 1;
//            while (left >= 0 && right <= s.size() - 1 && s[left] == s[right])left--, right++;
//            left++, right--;
//            if (right - left + 1 > tmp.size()) tmp = s.substr(left, right - left + 1);
//        }
//        return tmp;
//    }
//};

//class Solution {
//public:
//    string longestPalindrome(string s)
//    {
//        if (s.size() == 1)return s;
//        //if (s.size() == 2 && s[0]==s[1])return s;
//        string tmp = s.substr(0, 1);
//        for (int i = 0; i < s.size() - 1; i++)
//        {
//            int left = i, right = i;
//            // 第一遍循环先遍历为奇数的情况
//            while (left >= 0 && right <= s.size() - 1 && s[left] == s[right])left--, right++;
//            left++, right--;
//            if (right - left + 1 > tmp.size()) tmp = s.substr(left, right - left + 1);
//
//            //第二遍遍历为考虑偶数长度的情况
//            left = i, right = i + 1;
//            while (left >= 0 && right <= s.size() - 1 && s[left] == s[right])left--, right++;
//            left++, right--;
//            if (right - left + 1 > tmp.size()) tmp = s.substr(left, right - left + 1);
//        }
//        return tmp;
//    }
//};

//class Solution {
//public:
//    string multiply(string nums1, string nums2)
//    {
//        if (nums1 == "0" || nums2 == "0") return "0";
//
//        int n1 = nums1.size(), n2 = nums2.size();
//        vector<int> AddNum(n1 + n2 - 1);
//        reverse(nums1.begin(), nums1.end());
//        reverse(nums2.begin(), nums2.end());
//
//        for (int i = 0; i < n1; i++)
//        {
//            for (int j = 0; j < n2; j++)
//            {
//                AddNum[i + j] += (nums1[i] - '0') * (nums2[j] - '0');
//            }
//        }
//        //处理进位
//        string ret;
//        int i = 0, k = 0;
//        for (; i < n1 + n2 - 1 || k; i++)
//        {
//            if (i < n1 + n2 - 1) k += AddNum[i];
//            ret += k % 10 + '0';
//            k /= 10;
//        }
//        reverse(ret.begin(), ret.end());
//        return ret;
//    }
//};

//class Solution {
//public:
//    bool backspaceCompare(string s, string t)
//    {
//        string s1;
//        for (auto ch : s)
//        {
//            if (s1.size() && ch == '#') s1.pop_back();
//            else if (ch != '#') s1 += ch;
//        }
//        string s2;
//        for (auto ch : t)
//        {
//            if (s2.size() && ch == '#') s2.pop_back();
//            else if (ch != '#') s2 += ch;
//        }
//        return s1 == s2;
//    }
//};



int main()
{
    /*Solution s1;
    vector<string> v1{ "dog", "racecar","car" };
    string r1 = "babad";
    string ret = s1.longestPalindrome(r1);
    cout << ret << endl;*/

    string s1 = "422";
    cout<<s1
    /*string s1 = "asdfghjkl";
    reverse(s1.begin(), s1.end());
    cout << s1 << endl;*/
    /*string s1 = "heeeeeeello";
    string s2 = to_string(s1[0]);*/
	return 0;
}