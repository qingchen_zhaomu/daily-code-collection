#include"vector.h"

namespace hjx
{
	void test_vector1()
	{
		vector<int> s1 = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		//vector<int> s2(3, 31);
		vector<int> s2(s1.begin(), s1.end());

		//vector<string> s3(3, 1);
		vector<int> s3 = s1;
		vector<int> s4;
		s4 = s1;

		for (auto e : s1)
			cout << e << " ";
		cout << endl;
		for (auto e : s4)
			cout << e << " ";
		cout << endl;

		for (size_t i = 0; i < s1.size(); i++)
		{
			cout << s2[i] << " ";
		}
		cout << endl;
	}


	void test_vector2()
	{
		vector<int> s1{ 3, 5, 7, 1, 5, 8, 2 };
		s1.push_back(20);
		s1.push_back(10);
		s1.push_back(50);

		hjx::vector<int>::iterator it1 = s1.begin();
		while (it1 != s1.end())
		{
			cout << *it1 << " ";
			it1++;
		}
		cout << endl;

		s1.pop_back();
		s1.pop_back();

		for (auto e : s1)
			cout << e << " ";
		cout << endl;

	}


	void test_vector3()
	{
		/*s1.insert(s1.begin() + 3, 1000);
		
		for (auto e : s1)
			cout << e << " ";
		cout << endl;*/

		/*vector<int>::iterator it1 = s1.begin();

		for (size_t i = 0; i<s1.size();i++)
		{
			if (s1[i] % 2 == 0)
			{
				it1 = s1.erase(s1.begin() + i);
			}
			else
				it1++;
		}

		for (auto e : s1)
			cout << e << " ";
		cout << endl; 

		vector<string> v1;
		v1.push_back("111111111111111111");
		v1.push_back("111111111111111111");
		v1.push_back("111111111111111111");
		v1.push_back("111111111111111111");
		v1.push_back("111111111111111111");
		for (auto e : v1)
		{
			cout << e << " ";
		}
		cout << endl;*/

		vector<int> s1{ 3, 5, 7, 1, 5, 8, 2 , 1};

		vector<int>::iterator it1 = s1.begin();
		while (it1 != s1.end())
		{
			if (*it1 % 2 == 0)
			{
				s1.erase(it1);
				//it1 = s1.erase(it1);
			}
			else
			{
				it1++;
			}
		}

		for (auto e : s1)
		{
			cout << e << " ";
		}
		cout << endl;

	}

}

int main()
{
	hjx::test_vector3();
	return 0;
}