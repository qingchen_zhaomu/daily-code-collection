#pragma once
#include<iostream>
using namespace std;
#include<assert.h>

namespace hjx
{
	template<class T>
	class vector
	{
	public:
		typedef T* iterator;
		typedef const T* const_iterator;

		vector() = default;

		iterator begin()
		{
			return _start;
		}
		iterator end()
		{
			return _finish;
		}
		const_iterator begin()const
		{
			return _start;
		}
		const_iterator end()const
		{
			return _finish;
		}


		//构造函数 重载 | 传一段迭代区间
		template<class InputIterator>
		vector(InputIterator first, InputIterator last)
		{
			while (first != last)
			{
				push_back(*first);
				++first;
			}
		}

		//构造函数 重载 | n个值为val
		vector(size_t n, const T& val = T())
		{
			reserve(n);
			for (size_t i = 0; i < n; i++)
			{
				push_back(val);
			}
		}

		//构造函数 重载 | vector<int> s1 = {1, 2, 3, 6, 8};
		vector(initializer_list<T> it)
		{
			for (auto e : it)
			{
				push_back(e);
			}
		}

		//为了防止调用到迭代器的版本，所以需要重载一个int类型的
		//       vector<int> s1 = ( 3, 1);
		vector(int n, const T& val = T())
		{
			reserve(n);
			for (int i = 0; i < n; i++)
			{
				push_back(val);
			}
		}

		//拷贝构造
		vector(vector<T>& s1)
		{
			//vector<T> tmp(s1.begin(), s1.end);
			//swap(tmp);
			reserve(capacity());;
			for (auto e : s1)
			{
				push_back(e);
			}
		}

		//赋值运算符重载
		vector<T>& operator=(vector<T> s1)
		{
			swap(s1);
			return *this;
		}

		~vector()
		{
			if (_start)
			{
				delete[]_start;
				_start = _finish = _end_of_storage = nullptr;
			}
		}

		void swap(vector<T>& v1)
		{
			std::swap(_start, v1._start);
			std::swap(_finish, v1._finish);
			std::swap(_end_of_storage, v1._end_of_storage);
		}


		size_t size()const
		{
			return _finish - _start;
		}
		size_t capacity()const
		{
			return _end_of_storage - _start;
		}

		T& operator[](size_t pos)
		{
			assert(pos <= size());
			return _start[pos];
		}
		const T& operator[](size_t pos)const
		{
			assert(pos <= size());
			return _start[pos];
		}

		void reserve(size_t len)
		{
			if (len > capacity())
			{
				size_t old_finish = _finish - _start;
				//size_t old_end_of_storage = _end_of_storage - _start;
				T* tmp = new T[len];

				//拷贝数据
				/*for (auto e : *this)
				{
					push_back(e);
				}*/
				for (size_t i = 0; i < old_finish; i++)
				{
					tmp[i] = _start[i];
				}

				delete[] _start;

				_start = tmp;
				_finish = _start + old_finish;
				_end_of_storage = _start + len;
			}
		}

		void push_back(const T& val)
		{
			////判断是否需要扩容
			//if (_finish == _end_of_storage)
			//{
			//	size_t newcapacity = capacity() == 0 ? 4 : 2 * capacity();
			//	reverse(newcapacity);
			//}
			////尾插
			//*_finish = val;
			//_finish++;

			//此处复用insert即可
			insert(end(), val);
		}

		void pop_back()
		{
			assert(size() > 0);
			--_finish;
		}

		iterator insert(iterator pos, const T& val)
		{
			assert(pos >= _start && pos <= _finish);

			//判断是否需要扩容
			if (_finish == _end_of_storage)
			{
				size_t old_pos = pos - _start;
				size_t newcapacity = capacity() == 0 ? 4 : 2 * capacity();
				reserve(newcapacity);
				pos = _start + old_pos;
			}
			//挪动数据
			iterator end = _finish - 1;
			while (end >= pos)
			{
				*(end + 1) = *end;
				--end;
			}
			*pos = val;
			++_finish;

			return pos;//由于迭代器失效，所以需要返回一个
		}

		iterator erase(iterator pos)
		{
			assert(pos >= _start && pos < _finish);

			//memcpy(pos, pos + 1, _finish - pos);
			iterator it = pos + 1;
			while (it < _finish)
			{
				*(it - 1) = *it;
				it++;
			}

			--_finish;

			if (_finish == pos)
				return _finish;
			else
				return pos;
		}

	private:
		iterator _start = nullptr;
		iterator _finish = nullptr;
		iterator _end_of_storage = nullptr;
	};

	template<class T>
	void swap(vector<T> s1, vector<T> s2)
	{
		s1.swap(s2);
	}
}

