#pragma once
#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<assert.h>

typedef int STDataType;
typedef struct Stack
{
	STDataType* a;
	int top;//下标
	int capacity;
}ST;

//初始化、销毁
void STInit(ST* ps);
void STDestroy(ST* ps);

//入栈、出栈
void STPush(ST* ps, STDataType x);
void STPop(ST* ps);

STDataType STTop(ST* ps);
int STSize(ST* ps);
bool STEmpty(ST* ps);
