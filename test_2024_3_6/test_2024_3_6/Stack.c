#include"Stack.h"

//初始化、销毁
void STInit(ST* ps)
{
	assert(ps);

	ps->a = NULL;
	ps->top = 0;//指向栈顶的下一位
	ps->capacity = 0;
}

void STDestroy(ST* ps)
{
	assert(ps);

	free(ps->a);
	ps->a = NULL;
	ps->capacity = 0;
	ps->top = 0;
}

//入栈、出栈 (尾插、尾删)
void STPush(ST* ps, STDataType x)
{
	assert(ps);
	//判断容量满了或为空，扩容
	if (ps->capacity == ps->top)
	{
		//进来代表满了或为空
		int newcapacity = ps->capacity == 0 ? 4 : ps->capacity * 2;
		STDataType* tmp = (STDataType*)realloc(ps->a, newcapacity * sizeof(STDataType));
		if (!tmp)
		{
			perror("realloc fail!");
			return;
		}
		ps->a = tmp;
		ps->capacity = newcapacity;
	}
	ps->a[ps->top++] = x;
}

void STPop(ST* ps)
{
	assert(ps);
	//判断是否为空
	assert(!STEmpty(ps));
	ps->top--;
}

STDataType STTop(ST* ps)
{
	assert(ps);
	//判断是否为空
	assert(!STEmpty(ps));
	return ps->a[ps->top - 1];
}

int STSize(ST* ps)
{
	assert(ps);
	return ps->top;
}

bool STEmpty(ST* ps)
{
	assert(ps);
	return ps->top == 0;
}
