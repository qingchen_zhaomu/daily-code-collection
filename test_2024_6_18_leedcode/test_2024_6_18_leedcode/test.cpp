#include<iostream>
using namespace std;
#include<vector>
#include<string>
#include<queue>
#include<functional>


  struct ListNode {
      int val;
      ListNode *next;
      ListNode() : val(0), next(nullptr) {}
      ListNode(int x) : val(x), next(nullptr) {}
      ListNode(int x, ListNode *next) : val(x), next(next) {}
  };
 
  class Solution {
  public:
      void reorderList(ListNode* head)
      {
          if (head == nullptr || head->next == nullptr || head->next->next == nullptr)return;
          ListNode* slow = head, * fast = head;
          while (fast && fast->next)
          {
              fast = fast->next->next;
              slow = slow->next;
          }
          //此时slow指针指向的是待逆置部分的前一个
          //将后半段逆置
          ListNode* cur = slow->next;
          while (cur->next)
          {
              ListNode* Next = cur->next;
              cur->next = Next->next;
              Next->next = slow->next;
              slow->next = Next;
          }

          //现在后半段已经逆置完成，将后半段的节点依次插入到前面
          cur = slow->next;
          ListNode* prev = head;
          while (cur)
          {
              ListNode* Next = cur->next;
              cur->next = prev->next;
              slow->next = Next;
              prev->next = cur;
              prev = cur->next;
              cur = Next;
          }
      }
  };

  class Solution {
  public:
      void my_reverse(ListNode* tmp, int k, ListNode* prev)
      {
          ListNode* cur = tmp;
          //逆序主逻辑
          for (int i = 0; i < k - 1; i++)
          {
              ListNode* Next = cur->next;
              cur->next = Next->next;
              Next->next = cur;
              prev->next = Next;
          }
      }
      ListNode* reverseKGroup(ListNode* head, int k)
      {
          if (k == 1)return head;
          //插入哨兵位
          ListNode* phead = new ListNode;
          phead->next = head;
          //计算执行次数
          int times = 0;
          ListNode* runtime = head;
          while (runtime) runtime = runtime->next, times++;
          times /= k;

          ListNode* Next = head, * prev = phead;
          for (int i = 0; i < times; i++)
          {
              for (int j = 0; j < k; j++) Next = Next->next;
              ListNode* con = prev->next;
              my_reverse(prev->next, k, prev);
              con->next = Next;
              prev = con;
          }
          return head;
      }
  };


int main()
{
    ListNode* head = new ListNode(1);
    ListNode* cur = head;
    cur->next = new ListNode(2); cur = cur->next;
    cur->next = new ListNode(3); cur = cur->next;
    cur->next = new ListNode(4); cur = cur->next;
    cur->next = new ListNode(5); cur = cur->next;

    Solution s1;
    s1.reverseKGroup(head, 2);
    return 0;
}

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
//class Solution {
//public:
//    ListNode* MergeSort(vector<ListNode*>& nums, int left, int right)
//    {
//        //处理边界情况
//        if (left > right) return nullptr;
//        if (left == right) return nums[left];
//
//        int mid = (right + left) >> 1;
//        ListNode* l1 = MergeSort(nums, left, mid);
//        ListNode* l2 = MergeSort(nums, mid + 1, right);
//
//        //比较大小，小的先插入
//        return merge_two_list(l1, l2);
//    }
//    ListNode* merge_two_list(ListNode* l1, ListNode* l2)
//    {
//        if (!l1)return l2;
//        if (!l2)return l1;
//        ListNode* head = new ListNode; ListNode* prev = head;
//        ListNode* cur1 = l1, * cur2 = l2;
//        while (cur1 && cur2)
//        {
//            if (cur1->val < cur2->val)
//            {
//                prev->next = cur1;
//                prev = cur1;
//                cur1 = cur1->next;
//            }
//            else
//            {
//                prev->next = cur2;
//                prev = cur2;
//                cur2 = cur2->next;
//            }
//        }
//        while (cur1)
//        {
//            prev->next = cur1;
//            prev = cur1;
//            cur1 = cur1->next;
//        }
//        while (cur2)
//        {
//            prev->next = cur2;
//            prev = cur2;
//            cur2 = cur2->next;
//        }
//        return head->next;
//    }
//    ListNode* mergeKLists(vector<ListNode*>& lists)
//    {
//        return MergeSort(lists, 0, lists.size() - 1);
//    }
//};

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
//class Solution {
//public:
//    struct cmp
//    {
//        bool operator()(ListNode* l1, ListNode* l2)
//        {
//            return l1->val > l2->val;
//        }
//    };
//
//    ListNode* mergeKLists(vector<ListNode*>& lists)
//    {
//        //先建一个小根堆
//        priority_queue<ListNode*, vector<ListNode*>, cmp> heap;
//        //将头节点 k 个指针全部放进堆里面
//        for (auto e : lists) if (e)heap.push(e);
//        //创建一个返回链表的哨兵位
//        ListNode* head = new ListNode;
//        ListNode* prev = head;
//        //合并链表主逻辑
//        while (!heap.empty()) //堆不为空就一直循环
//        {
//            ListNode* tmp = heap.top();
//            heap.pop();
//            prev->next = tmp;
//            prev = tmp;
//            if (tmp->next) heap.push(tmp->next);
//        }
//        return head->next;
//    }
//};

//int main()
//{
//	vector<int> v1 = { 3,6,3,6,2,6,5,7,9,2,1,5,3 };
//	priority_queue<int, vector<int>, greater<int>> p1(v1.begin(), v1.end());
//	while (!p1.empty())
//	{
//		cout << p1.top() << " ";
//		p1.pop();
//	}
//	return 0;
//}

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
//class Solution {
//public:
//    void reorderList(ListNode* head)
//    {
//        if (head == nullptr || head->next == nullptr || head->next->next == nullptr)return;
//        ListNode* slow = head, * fast = head;
//        while (fast && fast->next)
//        {
//            fast = fast->next->next;
//            slow = slow->next;
//        }
//        //此时slow指针指向的是待逆置部分的前一个
//        //将后半段逆置
//        ListNode* cur = slow->next;
//        while (cur->next)
//        {
//            ListNode* Next = cur->next;
//            cur->next = Next->next;
//            Next->next = slow->next;
//            slow->next = Next;
//        }
//
//        //现在后半段已经逆置完成，将后半段的节点依次插入到前面
//        cur = slow->next;
//        ListNode* prev = head;
//        while (cur)
//        {
//            ListNode* Next = cur->next;
//            cur->next = prev->next;
//            slow->next = Next;
//            prev->next = cur;
//            prev = cur->next;
//            cur = Next;
//        }
//    }
//};

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
//class Solution {
//public:
//    ListNode* swapPairs(ListNode* head)
//    {
//        //处理链表为空的情况
//        if (!head)return head;
//        //建立哨兵位
//        ListNode* newhead = new ListNode;
//        newhead->next = head;
//        ListNode* prev = newhead;
//        ListNode* cur = head, * tmp = head->next;
//
//        while (tmp && cur)
//        {
//            cur->next = tmp->next;
//            prev->next = tmp;
//            tmp->next = cur;
//            cur = tmp; tmp = tmp->next;
//            prev = tmp;
//            cur = tmp->next;
//            if (cur) tmp = cur->next;
//        }
//        //我就不删哨兵位，反正泄露的是力扣的内存(doge)
//        return newhead->next;
//    }
//};

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
//class Solution {
//public:
//    ListNode* addTwoNumbers(ListNode* l1, ListNode* l2)
//    {
//        ListNode* cur1 = l1, * cur2 = l2;
//        ListNode* head = new ListNode;
//        ListNode* tail = head;
//        int tmp = 0;
//        while (cur1 || cur2)
//        {
//            if (cur1)tmp += cur1->val;
//            if (cur2)tmp += cur2->val;
//            tail->next = new ListNode;
//            tail->next->val = tmp % 10;
//            tmp /= 10;
//            if (cur1)cur1 = cur1->next;
//            if (cur2)cur2 = cur2->next;
//            tail = tail->next;
//        }
//        if (tmp)
//        {
//            tail->next = new ListNode;
//            tail->next->val = tmp;
//        }
//        return head->next;
//    }
//};

//void quicksort(vector<int>& nums, int left, int right)
//{
//	if (left >= right)return;
//	int randi = rand() % (right - left + 1) + left;
//	int key = left, start = left, end = right;
//	swap(nums[randi], nums[key]);
//	while (left < right)
//	{
//		while (left < right && nums[right] >= nums[key])right--;
//		while (left < right && nums[left] <= nums[key])left++;
//		swap(nums[left], nums[right]);
//	}
//	swap(nums[key], nums[left]);
//	key = left;
//	quicksort(nums, start, key - 1);
//	quicksort(nums, key + 1, end);
//}
//
//vector<int> tmp(500);
//void my_MergeSort(vector<int>& nums, int left, int right)
//{
//	if (left >= right)return;
//	int mid = (right + left) >> 1;
//
//	my_MergeSort(nums, left, mid);
//	my_MergeSort(nums, mid + 1, right);
//
//	int cur1 = left, cur2 = mid + 1, i = 0;
//	while (cur1 <= mid && cur2 <= right)
//		tmp[i++] = nums[cur1] > nums[cur2] ? nums[cur1++] : nums[cur2++];
//	while (cur1 <= mid) tmp[i++] = nums[cur1++];
//	while (cur2 <= right) tmp[i++] = nums[cur2++];
//
//	for (int j = left; j <= right; j++) nums[j] = tmp[j - left];
//}

//int main()
//{
//	srand(time(NULL));
//	vector<int> v1 = { 3,6,1,4,9,5,7,3,7,5,2,8,3 };
//	/*quicksort(v1, 0, v1.size() - 1);
//	for (auto e : v1)cout << e << " ";
//	cout << endl;*/
//
//	//tmp.reserve(v1.size() + 1);
//	/*my_MergeSort(v1, 0, v1.size() - 1);
//	for (auto e : v1)cout << e << " ";
//	cout << endl;*/
//
//	return 0;
//}

