#include<iostream>
using namespace std;
#include<vector>
#include<algorithm>
#include<assert.h>
#include<unordered_map>

//class Solution {
//public:
//    int totalFruit(vector<int>& fruits)
//    {
//        unordered_map<int, int> hash;
//        int ret = 0;
//        for (int left = 0, right = 0; right < fruits.size(); right++)
//        {
//            hash[fruits[right]]++;
//            while (hash.size() > 2)
//            {
//                hash[fruits[left]]--;
//                if (hash[fruits[left]] == 0)hash.erase(fruits[left]);
//                left++;
//            }
//            ret = max(ret, right - left + 1);
//        }
//        return ret;
//    }
//};

//class Solution {
//public:
//    vector<int> findAnagrams(string s, string p)
//    {
//        vector<int> ret;//要返回的数组
//        int hash1[26] = { 0 };//p里面的值存一份
//        for (auto e : p)hash1[e - 'a']++;
//        int hash2[26] = { 0 };//统计s里面的进窗口的字母
//        for (int left = 0, right = 0, count = 0; right < s.size(); right++)
//            //count是有效字符个数
//        {
//            if (++hash2[s[right] - 'a'] <= hash1[s[right] - 'a'])count++;//进窗口
//            if (right - left + 1 > p.size())//判断
//            {
//                //出窗口
//                if (hash2[s[left] - 'a']-- <= hash1[s[left] - 'a'])count--;
//                left++;
//            }
//            //更新结果
//            if (count == p.size())ret.push_back(left);
//        }
//        return ret;
//    }
//};

//class Solution {
//public:
//    vector<int> findPeaks(vector<int>& mountain)
//    {
//        vector<int> ret;
//        for (int right = 1; right < mountain.size() - 1; right++)
//            if (mountain[right] > mountain[right - 1] 
//                && mountain[right] > mountain[right + 1])ret.push_back(right);
//        return ret;
//    }
//};

//class Solution {
//public:
//    vector<int> findSubstring(string s, vector<string>& words)
//    {
//        vector<int> ret;//返回数组
//        unordered_map<string, int> hash1;//存的是words里面出现过的频次
//        for (auto& e : words)hash1[e]++;
//        int len = words[0].size(), num = words.size();
//        for (int i = 0; i < len; i++)//滑动len次
//        {
//            unordered_map<string, int> hash2;//存的是要遍历的字符串
//            //滑动窗口主逻辑
//            for (int left = i, right = i, count = 0/*count 是有效字符串个数*/;
//                right + len <= s.size(); right += len)
//            {
//                //进窗口 + 维护count
//                string s1 = s.substr(right, len);
//                //成立则代表存入的是有效字符串
//                if (hash1[s1] && ++hash2[s1] <= hash1[s1])count++;
//                //判断
//                if (right - left + 1 > len * num)
//                {
//                    //出窗口 + 维护count
//                    string s2 = s.substr(left, len);
//                    if (hash1[s2] && hash2[s2]-- <= hash1[s2])count--;
//                    left += len;
//                }
//                //更新结果
//                if (count == num)ret.push_back(left);
//            }
//        }
//        return ret;
//    }
//};

int main()
{
    vector<int> v1 = { 1,1,4,3,7,3,2,5,4 };
    Solution s1;
    int n = s1.findSubstring(v1);

    cout << n << endl;

    return 0;
}