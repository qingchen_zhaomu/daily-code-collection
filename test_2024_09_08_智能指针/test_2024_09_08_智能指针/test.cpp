#include<iostream>
using namespace std;
#include"share_ptr.h"

namespace hjx
{
	struct Date
	{
		int _year;
		int _month;
		int _day;

		Date(int year = 1, int month = 1, int day = 1)
			:_year(year)
			,_month(month)
			,_day(day)
		{}

	};

	void test_share_ptr1()
	{
		share_ptr<Date> sp1(new Date);
		share_ptr<Date> sp2(new Date[5], [](Date* ptr) {delete[] ptr; });
		//shared_ptr<Date[]> sp2(new Date[5], [](Date* ptr) {delete[] ptr; });
	}
}


int main()
{
	hjx::test_share_ptr1();
	return 0;
}


