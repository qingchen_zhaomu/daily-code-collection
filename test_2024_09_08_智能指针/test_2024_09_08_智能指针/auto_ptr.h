#pragma once

namespace hjx
{
	template<class T>
	class auto_ptr
	{
	public:
		auto_ptr(T* ptr)
			:_ptr(ptr)
		{}

		auto_ptr(const auto_ptr<T>& sp)
		{
			_ptr = sp._ptr;
			sp._ptr = nullptr;
		}

		auto_ptr<T>& operator=(const auto_ptr<T>& aa)
		{
			// 不能给自己赋值
			if (this != &aa)
			{
				if (_ptr)
				{
					// 如果原空间有资源，则直接释放
					delete _ptr;
				}
				_ptr = aa._ptr;
				aa._ptr = nullptr;
			}

			return *this;
		}

		~auto_ptr()
		{
			if (_ptr)
			{
				cout << "delete:" << _ptr << endl;
				delete _ptr;
			}
		}

		T& operator*()
		{
			return *_ptr;
		}

		T* operator->()
		{
			return _ptr;
		}

	private:
		T* _ptr;
	};


}

