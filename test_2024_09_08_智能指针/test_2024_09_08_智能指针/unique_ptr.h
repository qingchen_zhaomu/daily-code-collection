#pragma once

namespace hjx
{
	// unique_ptr 有一个作用就是防止拷贝
	// 所以，这里面的赋值和拷贝构造都是被delete给限制起来的

	template<class T>
	class unique_ptr
	{
	public:
		unique_ptr(T* ptr)
			:_ptr(ptr)
		{}

		unique_ptr(auto_ptr<T>& sp) = delete;
		unique_ptr<T>& operator=(unique_ptr<T>& aa) = delete;

		~unique_ptr()
		{
			if (_ptr)
			{
				cout << "delete:" << _ptr << endl;
				delete _ptr;
			}
		}

		T& operator*()
		{
			return *_ptr;
		}

		T* operator->()
		{
			return _ptr;
		}

	private:
		T* _ptr;
	};



}


