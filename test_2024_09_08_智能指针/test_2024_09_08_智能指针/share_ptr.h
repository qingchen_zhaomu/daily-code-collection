#pragma once
#include<functional>

namespace hjx
{
	template<class T>
	class share_ptr
	{
	public:
		share_ptr(T* ptr = nullptr)
			:_ptr(ptr)
			,_pcount(new int(1))
		{}

		template<class D>
		share_ptr(T* ptr, D del)
			:_ptr(ptr)
			, _pcount(new int(1))
			,_del(del)
		{}

		share_ptr(const share_ptr<T>& sp)
			:_ptr(sp._ptr)
			,_pcount(sp._pcount)
		{
			++(*_pcount);
		}

		void release()
		{
			if (--(*_pcount) == 0)
			{
				//delete _ptr;
				// 这里就可以直接改成用删除器
				_del(_ptr);

				delete _pcount;
				_ptr = nullptr;
				_pcount = nullptr;
			}
		}

		// 赋值重载
		share_ptr<T>& operator=(const share_ptr<T>& sp)
		{
			if (_ptr != sp._ptr)
			{
				release();

				_ptr = sp._ptr;
				_pcount = sp._pcount;
				++(*_pcount);
			}

			return *this;
		}

		~share_ptr()
		{
			release();
		}

		T& operator*()
		{
			return *_ptr;
		}

		T* operator->()
		{
			return _ptr;
		}

		T* get()const
		{
			return _ptr;
		}

		int use_count()
		{
			return *_pcount;
		}

	private:
		T* _ptr;
		int* _pcount;

		// 由于底层默认是delete删除，所以遇到数组等其他情况的时候
		// 就处理不了，所以我们就需要引入删除器
		function<void(T* ptr)> _del = [](T* ptr) { delete ptr; };
	};

	// 但是我们的share_ptr还会遇到循环引用的问题
	// 所以我们还需要引入弱指针来打破循环
	template<class T>
	class weak_ptr
	{
	public:
		weak_ptr(){}// 无参构造

		weak_ptr(const share_ptr<T>& sp)
			:_ptr(sp.get())
		{}

		weak_ptr<T>& operator=(const share_ptr<T>& sp)
		{
			_ptr = sp.get();
			return *this;
		}

	private:
		T* _ptr = nullptr;
	};



	template<class T>
	class share_ptr<T[]>
	{
	public:
		share_ptr(T* ptr = nullptr)
			:_ptr(ptr)
			, _pcount(new int(1))
		{}

		template<class D>
		share_ptr(T* ptr, D del)
			: _ptr(ptr)
			, _pcount(new int(1))
			, _del(del)
		{}

		share_ptr(const share_ptr<T[]>& sp)
			:_ptr(sp._ptr)
			, _pcount(sp._pcount)
		{
			++(*_pcount);
		}

		void release()
		{
			if (--(*_pcount) == 0)
			{
				//delete _ptr;
				// 这里就可以直接改成用删除器
				_del(_ptr);

				delete _pcount;
				_ptr = nullptr;
				_pcount = nullptr;
			}
		}

		// 赋值重载
		share_ptr<T[]>& operator=(const share_ptr<T[]>& sp)
		{
			if (_ptr != sp._ptr)
			{
				release();

				_ptr = sp._ptr;
				_pcount = sp._pcount;
				++(*_pcount);
			}

			return *this;
		}

		~share_ptr()
		{
			release();
		}

		T& operator*()
		{
			return *_ptr;
		}

		T* operator->()
		{
			return _ptr;
		}

		T* get()const
		{
			return _ptr;
		}

		int use_count()
		{
			return *_pcount;
		}

	private:
		T* _ptr;
		int* _pcount;

		// 由于底层默认是delete删除，所以遇到数组等其他情况的时候
		// 就处理不了，所以我们就需要引入删除器
		function<void(T* ptr)> _del = [](T* ptr) { delete[] ptr; };
	};



}

