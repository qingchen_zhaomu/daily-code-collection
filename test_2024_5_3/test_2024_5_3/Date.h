#pragma once
#include<iostream>
using namespace std;

#include<assert.h>

class Date
{
	friend ostream& operator<<(ostream& out, const Date& d);
	friend istream& operator>>(istream& in, Date& d);

public:
	Date(int year = 1, int month = 1, int day = 1);

	void Print();

	int GetMonthDay(int year, int month)
	{
		assert(month < 13 && month > 0);
		int MonthDayArray[13] =
		{ -1, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

		if (month == 2 && ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0))
			return 29;
		else
			return MonthDayArray[month];
	}

	bool operator<(const Date& d)const;
	bool operator>(const Date& d)const;
	bool operator<=(const Date& d)const;
	bool operator>=(const Date& d)const;
	bool operator==(const Date& d)const;
	bool operator!=(const Date& d)const;

	Date& operator+=(int day);//*this要变，故不加const
	Date operator+(int day)const;
	Date& operator-=(int day);
	Date operator-(int day)const;

	int operator-(const Date& d);

	Date& operator--();
	Date operator--(int);
	Date& operator++();
	Date operator++(int);

	//日期类无需析构函数，编译器默认生成的就够用
private:
	int _year;
	int _month;
	int _day;
};

ostream& operator<<(ostream& out, const Date& d);
istream& operator>>(istream& in, Date& d);