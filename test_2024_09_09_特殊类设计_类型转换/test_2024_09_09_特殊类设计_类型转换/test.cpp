#include<iostream>
using namespace std;
#include<string>

// 1 设计一个类 不能被拷贝
class copyban
{
public:
	copyban() = default;

	// 要想不被拷贝 只需要将拷贝构造和赋值重载用delete限定起来即可
	copyban(const copyban& cb) = delete;
	copyban& operator=(const copyban& cb) = delete;

private:
	int _a = 0;
};


// 2 设计一个类 只能在堆上创建空间
class heaponly
{
public:
	// 返回指向堆空间的指针
	static heaponly* create_on_heap()
	{
		return new heaponly;
	}

	// 为了防止在外面使用拷贝构造和赋值重载
	// 绕过规则，在栈上创建对象，所以这两个需要delete掉
	heaponly(const heaponly& ho) = delete;
	heaponly& operator=(const heaponly& ho) = delete;

private:
	// 方法一，将构造函数私有，这样就可以限制类的生成
	// 接着开放一个static接口函数，该函数里面有在对上面开空间
	heaponly(){}
};

class Heap_Only_2
{
public:
	void Destroy()
	{
		delete this;
	}

	// 注意，我们这里是不需要写拷贝构造和赋值的delete
	// 因为我们除了堆上的之外，其他地方上对象都创建不出来
	// 也就没有拷贝和赋值的说法

private:
	// 方法二是将析构函数私有化，这样我们对象创建完之后，无法调用私有的析构
	// 也就创建不了对象。而我们堆上的空间是不会自动调用析构的，需要我们自己手动释放空间
	// 所以我们可以给一个destroy函数，这样就可以在外面调用析构，也就实现了只在堆上开空间
	~Heap_Only_2(){}
};

//int main()
//{
//	copyban cb;
//
//	// 方法一
//	heaponly* hp = heaponly::create_on_heap();
//	delete hp;
//	//heaponly hp1;// 栈上无法开空间
//	//static heaponly hp2;// 静态区上也无法开空间
//
//	// 方法二
//	Heap_Only_2* ho = new Heap_Only_2;
//	ho->Destroy();
//	//Heap_Only_2 ho1;// 栈上无法开空间
//	//static Heap_Only_2 ho2;// 静态区上也无法开空间
//
//	return 0;
//}


/////////////////////////////////////////////////////////////

// 3 设计一个类 只能在栈上创建空间
// 方法一
//class stackonly
//{
//public:
//	// 开放一个接口
//	static stackonly create_obj()
//	{
//		return stackonly();
//	}
//
//	// 但是这里我们需要将拷贝构造、赋值、operator_new、operator_delete给ban掉
//	// 这样子的化有一个缺点，就是，我们还可以在静态区上开空间
//	// 好的一点就是我们已经在堆上开不了空间了，静态区封不住
//	stackonly(const stackonly& so) = delete;
//	stackonly& operator=(const stackonly& so) = delete;
//
//	void* operator new(size_t size) = delete;
//	void operator delete(void* p) = delete;
//
//private:
//	stackonly():_a(0){}
//
//private:
//	int _a = 0;
//};

// 方法二
class Stack_Only
{
public:
	static Stack_Only create_obj()
	{
		// 返回的是一个右值
		// 这里直接写一个移动构造，就不会调用拷贝，赋值同理
		return Stack_Only();
	}


	Stack_Only(Stack_Only&& so)
	{}

	// 拷贝构造需要delete掉，赋值同理
	Stack_Only(Stack_Only& so) = delete;

private:
	// 这里是不能私有析构的，因为我们栈上开的对象是会自动调用析构的，不像堆上的空间
	// 所以这里还是私有构造，不同的是，我们返回的是一个匿名对象
	// 所以我们可以直接用移动构造，这样就不会去调用拷贝而是直接移动
	Stack_Only(){}

	// 但是这个可以直接创建类型，然后在前面加上一个move就可以打破了
	// 就相当于没有封住，没什么用，了解即可
};


//int main()
//{
//	//static StackOnly s1;
//	/*StackOnly s2;
//	StackOnly* s3 = new StackOnly;*/
//
//	Stack_Only s4 = Stack_Only::create_obj();
//
//	static Stack_Only s5(move(s4));
//	Stack_Only* s6 = new Stack_Only(move(s5));
//
//	return 0;
//}



/////////////////////////////////////////////////
// 单例，全场只有一个实例化对象

// 饿汉模式
class Info_Mgr
{
public:
	static Info_Mgr& create_obj()
	{
		return _ins;
	}

	void Print()
	{
		cout << _ip << endl;
		cout << _port << endl;
	}

private:
	// 首先，还是需要将构造函数私有化，这样就能禁止在外构造出多个对象
	// 然后，我们饿汉模式就是上来就实例化出一个静态对象，然后通过一个静态函数返回
	Info_Mgr(){}

	// 拷贝构造和赋值delete掉
	Info_Mgr(const Info_Mgr& ifm) = delete;
	Info_Mgr& operator=(const Info_Mgr& ifm) = delete;

private:
	string _ip = "127.0.0.1";
	int _port = 80;

	static Info_Mgr _ins;
};

Info_Mgr Info_Mgr::_ins;


//int main()
//{
//	Info_Mgr::create_obj().Print();
//	//Info_Mgr cpy(Info_Mgr::create_obj());
//	//Info_Mgr cp;
//	return 0;
//}



// 懒汉模式
// 由于饿汉是上来就要实例化，有时候就会导致开启的时间过长
// 所以就有了懒汉模式
// 懒汉模式的本质就是，等你需要了我在实例化，并且是静态的，因为静态的只会初始化一次

// 懒汉一、指针版本
class InFo
{
public:
	static InFo& getinh()
	{
		if (_inh == nullptr)
		{
			_inh = new InFo();
		}
		return *_inh;
	}

	void Print()
	{
		cout << _ip << endl;
		cout << _port << endl;
	}

private:
	InFo(){}

	InFo(const InFo& ii) = delete;
	InFo& operator=(const InFo& ii) = delete;

private:
	string _ip = "127.0.0.1";
	int _port = 80;

	static InFo* _inh;
};

InFo* InFo::_inh = nullptr;


// 懒汉二、静态版本
class Info_sec
{
public:
	static Info_sec& getinfo()
	{
		static Info_sec ins;
		return ins;
	}

	void Print()
	{
		cout << _ip << endl;
		cout << _port << endl;
	}

private:
	Info_sec() {}

	Info_sec(const Info_sec& ii) = delete;
	Info_sec& operator=(const Info_sec& ii) = delete;

private:
	string _ip = "127.0.0.1";
	int _port = 80;
};



