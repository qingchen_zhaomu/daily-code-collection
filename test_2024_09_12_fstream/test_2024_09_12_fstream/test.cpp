#include<iostream>
using namespace std;
#include<fstream>

//int main()
//{
//	ofstream ofs("test.txt");
//	//ofs.open("test.txt");
//	ofs.put('x');
//	ofs.write("hello\n", 6);
//
//	ofs << "2222222222" << endl;
//	ofs << 111 << endl;
//	ofs << 1.11 << endl;
//
//	ofs.close();
//
//	return 0;
//}

//int main()
//{
//	ifstream ifs("test.cpp");
//	while (!ifs.eof())
//	{
//		char ch = ifs.get();
//		cout << ch;
//	}
//
//	return 0;
//}

//int main()
//{
//	ifstream ifs("C:\\Users\\30464\\Desktop\\picture\\xzy.jpg", ios_base::in | ios_base::binary);
//
//	int n = 0;
//	while (!ifs.eof())
//	{
//		char ch = ifs.get();
//		++n;
//	}
//	cout << n << endl;
//
//	return 0;
//}

//#include<string>
//#include<vector>
//#include<algorithm>
//
//int main()
//{
//	vector<string> v1({ "20z", "20m", "10q", "18a" });
//	sort(v1.begin(), v1.end());
//	return 0;
//}


int Add(int x, int y)
{
	return x + y;
}

float Sub(float x, float y)
{
	return x - y;
}

double Mul(double x, double y)
{
	return x * y;
}

int Div(int x, int y)
{
	if (y == 0)
	{
		cout << "被除数不能为0" << endl;
		return 0;
	}
	return x / y;
}


//int main()
//{
//	int a = 10;
//	float b = 3.14;
//	double c = 3.1415926;
//	char d = 'd';
//
//	cout << Add(5, 7) << endl;
//	cout << Sub(5.5, 7.3) << endl;
//	cout << Mul(5.66, 7.88) << endl;
//	cout << Div(14, 7) << endl;
//
//	return 0;
//}



//int main()
//{
//	// float 转 int
//	float a = 10.1;
//	int res1 = static_cast<int>(a);
//
//	// double 转 int
//	double b = 3.14159;
//	int res2 = static_cast<int>(b);
//
//	cout << res1 << "  " << res2 << endl;
//
//	return 0;
//}

//int main()
//{
//	int src;
//	cout << "请输入成绩:";
//	cin >> src;
//
//	if (src > 95)cout << "特别优秀" << endl;
//	else if (src >= 90)cout << "优秀" << endl;
//	else if (src >= 80)cout << "好" << endl;
//	else if (src >= 70)cout << "中等" << endl;
//	else if (src >= 60)cout << "通过" << endl;
//	else cout << "孩子你也是废了" << endl;
//
//	return 0;
//}


//int main()
//{
//	int sum = 0;
//	for (int i = 1; i <= 10; i++)sum += i;
//	cout << sum * sum << endl;
//
//	return 0;
//}

//int main()
//{
//	int i = 1, sum = 0;
//	while (i <= 1000)
//	{
//		sum += i;
//		i++;
//	}
//	cout << sum << endl;
//
//	return 0;
//}


//int main()
//{
//	for (int i = 0; i < 6; i++)
//	{
//		for (int j = 0; j < 8; j++)
//		{
//			cout << "*";
//		}
//		cout << endl;
//	}
//	return 0;
//}


class Date
{
	friend ostream& operator << (ostream& out, const Date& d);
	friend istream& operator >> (istream& in, Date& d);
public:
	Date(int year = 1, int month = 1, int day = 1)
		:_year(year)
		, _month(month)
		, _day(day)
	{}
	operator bool()
	{
		// 这里是随意写的，假设输入_year为0，则结束
		if (_year == 0)
			return false;
		else
			return true;
	}
private:
	int _year;
	int _month;
	int _day;
};
istream& operator >> (istream& in, Date& d)
{
	in >> d._year >> d._month >> d._day;
	return in;
}
ostream& operator << (ostream& out, const Date& d)
{
	out << d._year << " " << d._month << " " << d._day;
	return out;
}


#include<sstream>

int main()
{
	int i = 3;
	Date d1{ 2024, 3 ,31 };
	ostringstream oss;
	// 这里是 ostringstream 里面有一个string存着数据
	// 我们这里的oss<<是将数据存进去那个string里面
	// 下面就需要用一个string接收并打印
	oss << i << endl;
	oss << d1 << endl;

	string s = oss.str();
	cout << s << endl;

	//istringstream iss(s);
	/*istringstream iss;
	iss.str("207 2023 9 6");*/
	istringstream iss("207 2024 1 13");

	int num;
	Date d2;
	iss >> num >> d2;


	return 0;
}








