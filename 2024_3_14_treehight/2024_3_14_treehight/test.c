#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

typedef int TDataType;
typedef struct TreeNoode
{
	TDataType val;
	struct TreeNoode* leftnode;
	struct TreeNoode* rightnode;
}TN;

int TreeHight(TN* root)
{
	if (!root)return 0;
	int Left = TreeHight(root->leftnode);
	int Right = TreeHight(root->rightnode);
	return Left > Right ? Left + 1 : Right + 1;
}

TN* TreeFind(TN* root, int x)
{
	//找到尾
	if (root == NULL)return NULL;
	//找到了
	if (root->val == x)return root;
	//没找到
	TN* ret1 = TreeFind(root->leftnode, x);
	if (ret1)return ret1;
	return TreeFind(root->rightnode, x);

	return NULL;
}

int main()
{
	
	return 0;
}