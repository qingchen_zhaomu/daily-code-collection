#include<iostream>
using namespace std;
#include<vector>
#include<string>
#include<queue>
#include<functional>



//class Solution {
//public:
//    typedef pair<int, int> PII;
//    int dx[4] = { 0, 0, 1, -1 };
//    int dy[4] = { 1, -1, 0, 0 };
//    bool vis[301][301] = { false };
//    int m, n;
//
//    int numIslands(vector<vector<char>>& nums)
//    {
//        m = nums.size(), n = nums[0].size();
//        int gtime = 0;
//        for (int i = 0; i < m; i++)
//        {
//            for (int j = 0; j < n; j++)
//            {
//                if (vis[i][j] == false && nums[i][j] == '1')
//                {
//                    gtime++;
//                    // BFS主逻辑
//                    queue<PII> q;
//                    q.push({ i, j });
//                    while (q.size())
//                    {
//                        auto [a, b] = q.front();
//                        q.pop();
//                        for (int i = 0; i < 4; i++)
//                        {
//                            int x = a + dx[i], y = b + dy[i];
//                            if (x >= 0 && x < m && y >= 0 && y < n && nums[x][y] == '1' && vis[x][y] == false)
//                            {
//                                q.push({ x, y });
//                                vis[x][y] = true;
//                            }
//                        }
//                    }
//                }
//            }
//        }
//        return gtime;
//    }
//};

int main()
{
	vector<vector<int>> v1(3, vector<int>(3));
 	return 0;
}

//class Solution {
//public:
//    typedef pair<int, int> PII;
//    int dx[4] = { 0, 0, 1, -1 };
//    int dy[4] = { 1, -1, 0, 0 };
//
//    vector<vector<int>> floodFill(vector<vector<int>>& image, int sr, int sc, int color)
//    {
//        int m = image.size(), n = image[0].size();
//        int prev = image[sr][sc];
//        if (prev == color) return image;
//
//        queue<PII> q1;
//        q1.push({ sr, sc });
//        while (q1.size())
//        {
//            auto [a, b] = q1.front();
//            q1.pop();
//            image[a][b] = color;
//            for (int i = 0; i < 4; i++)
//            {
//                int x = a + dx[i];
//                int y = b + dy[i];
//                if (x >= 0 && x < m && y >= 0 && y < n && image[x][y] == prev)
//                {
//                    q1.push({ x, y });
//                }
//            }
//        }
//        return image;
//    }
//};

//class KthLargest {
//public:
//    int _k;
//    priority_queue<int, vector<int>, greater<int>> _nums;
//
//    KthLargest(int k, vector<int>& nums)
//    {
//        _k = k;
//        for (auto e : nums)
//        {
//            _nums.push(e);
//            if (_nums.size() > _k)_nums.pop();
//        }
//    }
//
//    int add(int val)
//    {
//        _nums.push(val);
//        if (_nums.size() > _k)_nums.pop();
//        return _nums.top();
//    }
//};

/**
 * Your KthLargest object will be instantiated and called as such:
 * KthLargest* obj = new KthLargest(k, nums);
 * int param_1 = obj->add(val);
 */

//class MedianFinder {
//public:
//    //大小堆维护
//    priority_queue<int> left;
//    priority_queue<int, vector<int>, greater<int>> right;
//
//    MedianFinder() {
//        ;
//    }
//
//    void addNum(int val)
//    {
//        if (left.size() == right.size())
//        {
//            if (left.size() == 0 || val <= left.top()) left.push(val);
//            else
//            {
//                right.push(val);
//                left.push(right.top());
//                right.pop();
//            }
//        }
//        else
//        {
//            if (val <= left.top())
//            {
//                left.push(val);
//                right.push(left.top());
//                left.pop();
//            }
//            else right.push(val);
//        }
//    }
//
//    double findMedian()
//    {
//        if (left.size() == 0) return NULL;
//        else if (left.size() == right.size()) return (left.top() + right.top()) / 2.0;
//        else return left.top();
//    }
//};

/**
 * Your MedianFinder object will be instantiated and called as such:
 * MedianFinder* obj = new MedianFinder();
 * obj->addNum(num);
 * double param_2 = obj->findMedian();
 */

//class Solution {
//public:
//    //           words  times
//    typedef pair<string, int> PSI;
//
//    struct cmp
//    {
//        bool operator()(PSI p1, PSI p2)
//        {
//            if (p1.second == p2.second)
//            {
//                return p1.first < p2.first;
//            }
//            return p1.second > p2.second;
//        }
//    };
//
//    vector<string> topKFrequent(vector<string>& words, int k)
//    {
//        //统计每个单词出现的频次
//        unordered_map<string, int> hash;
//        for (auto& e : words)hash[e]++;
//        //将所有元素进堆
//        priority_queue<PSI, vector<PSI>, cmp> p1;
//        for (auto& e : hash)
//        {
//            p1.push(e);
//            if (p1.size() > k) p1.pop();
//        }
//        //提取结果
//        vector<string> ret(k);
//        for (int i = p1.size() - 1; i >= 0; i--)
//        {
//            ret[i] = p1.top().first;
//            p1.pop();
//        }
//        return ret;
//    }
//};

//class KthLargest {
//public:
//    int _k;
//    priority_queue<int, deque<int>, greater<int>> _nums;
//
//    KthLargest(int k, vector<int>& nums)
//    {
//        _k = k;
//        for (auto e : nums)
//        {
//            _nums.push(e);
//            if (_nums.size() > _k)_nums.pop();
//        }
//    }
//
//    int add(int val)
//    {
//        _nums.push(val);
//        if (_nums.size() > _k)_nums.pop();
//        return _nums.top();
//    }
//};

//class Solution {
//public:
//    int lastStoneWeight(vector<int>& stones)
//    {
//        priority_queue<int> p1(stones.begin(), stones.end());
//        int ret = 0;
//        while (p1.size() != 1)
//        {
//            int ret = p1.top();
//            p1.pop();
//            ret -= p1.top();
//            p1.pop();
//            p1.push(ret);
//        }
//        return p1.top();
//    }
//};

//int main()
//{
//    //默认为大根堆
//	vector<int> v1 = { 6,4,6,18,2,5,7,3,7,2 };
//    //priority_queue<int, vector<int>, less<int>> p2;
//	priority_queue<int> p1(v1.begin(), v1.end());
//	while (!p1.empty())
//	{
//		cout << p1.top() << " ";
//		p1.pop();
//	}
//	return 0;
//}

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
//class Solution {
//public:
//    vector<int> largestValues(TreeNode* root)
//    {
//        vector<int> ret;
//        if (root == nullptr) return ret;
//        vector<TreeNode*> q;
//        int comp = 0; //该变量用于记录每一层的最大值
//        q.push_back(root);
//        while (q.size())
//        {
//            comp = INT_MIN;
//            vector<TreeNode*> tmp;
//            //先记录这一层的最大值
//            for (auto& e : q)
//                comp = max(comp, e->val);
//            ret.push_back(comp);
//            //将子节点全部push进tmp数组中
//            for (auto& e : q)
//            {
//                if (e->left)tmp.push_back(e->left);
//                if (e->right)tmp.push_back(e->right);
//            }
//            q = tmp;
//        }
//        return ret;
//    }
//};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
//class Solution {
//public:
//    int widthOfBinaryTree(TreeNode* root)
//    {
//        vector<pair<TreeNode*, unsigned int>> q;
//        q.push_back({ root, 1 });
//        unsigned int ret = 0;
//        while (q.size())
//        {
//            vector<pair<TreeNode*, unsigned int>> tmp;
//            //先更新结果
//            auto& [x1, y1] = q[0];
//            auto& [x2, y2] = q.back();
//            ret = max(ret, y2 - y1 + 1);
//            //将下一层的节点都插入到tmp数组中，最后再直接替换掉q数组
//            for (auto& [x, y] : q)
//            {
//                if (x->left)tmp.push_back({ x->left, 2 * y });
//                if (x->right)tmp.push_back({ x->right, 2 * y + 1 });
//            }
//            q = tmp;
//        }
//        return ret;
//    }
//};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
//class Solution {
//public:
//    vector<vector<int>> zigzagLevelOrder(TreeNode* root)
//    {
//        int time = -1; // 记录是否要逆序的变量
//        vector<vector<int>> ret;
//        if (root == nullptr) return ret;
//        queue<TreeNode*> q1;
//        q1.push(root);
//        while (q1.size())
//        {
//            int sz = q1.size();
//            vector<int> tmp;
//            for (int i = 0; i < sz; i++)
//            {
//                if (q1.front()->left != nullptr) q1.push(q1.front()->left);
//                if (q1.front()->right != nullptr) q1.push(q1.front()->right);
//                tmp.push_back(q1.front()->val);
//                q1.pop();
//            }
//            time++;
//            if (time % 2 != 0) reverse(tmp.begin(), tmp.end());
//            ret.push_back(tmp);
//        }
//        return ret;
//    }
//};

//int main()
//{
//
//	return 0;
//}
