#include"Snake.h"


void test()
{

	PlaySound(TEXT("zaoan.wav"), NULL, SND_FILENAME | SND_ASYNC | SND_LOOP);

	srand((unsigned int)time(NULL));

	int ch = 0;
	do
	{
		Snake snake = { 0 };

		GameStart(&snake);//游戏开始前的初始化
		GameRun(&snake);//玩游戏的过程
		GameEnd(&snake);//善后的工作

		SetPos(20, 15);
		printf("再来一局吗？(Y/N):");
		ch = getchar();
		getchar();//清理\n
	} while (ch == 'Y' || ch == 'y');
	SetPos(0, 27);
}

int main()
{
	//修改适配本地中文环境
	setlocale(LC_ALL, "");

	test();
	return 0;
}