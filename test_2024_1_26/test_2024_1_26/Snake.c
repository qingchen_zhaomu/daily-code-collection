#include"Snake.h"

void SetPos(int x, int y)
{
	//获得设备句柄
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	//根据句柄设置光标位置
	COORD pos = { x,y };
	SetConsoleCursorPosition(handle, pos);
}

//打印欢迎界面
void WelcomeToGame()
{
	SetPos(35, 12);
	printf("欢迎来到贪吃蛇小游戏\n");
	SetPos(38, 20);
	system("pause");
	system("cls");

	SetPos(28, 10);
	printf("用↑↓←→来控制蛇的移动，A键是加速，D键是减速");
	SetPos(28, 11);
	printf("加速能得到更高的分数");
	SetPos(28, 12);
	printf("按空格键可以暂停,按Esc键可以退出游戏");
	SetPos(38, 20);
	system("pause");
	system("cls");
}


//绘制地图
void CreatMap()
{
	int i = 0;
	//上
	SetPos(0, 0);
	for (i = 0; i <= 56; i+=2)
	{
		wprintf(L"%lc", WALL);
	}
	//下
	SetPos(0, 26);
	for (i = 0; i <= 56; i += 2)
	{
		wprintf(L"%lc", WALL);
	}
	//左
	for (i = 1; i <= 25; i++)
	{
		SetPos(0, i);
		wprintf(L"%lc", WALL);
	}
	//右
	for (i = 1; i <= 25; i++)
	{
		SetPos(56, i);
		wprintf(L"%lc", WALL);
	}

}


void InitSnake(pSnake ps)
{
	//创建5个蛇身的结点
	ps->pSnake = NULL;
	int i = 0;
	pSnakeNode cur = NULL;
	for (i = 0; i < 5; i++)
	{
		cur = (pSnakeNode)malloc(sizeof(SnakeNode));
		if (cur == NULL)
		{
			perror("malloc fail!");
			return;
		}
		cur->x = POS_X + 2 * i;
		cur->y = POS_Y;
		cur->next = NULL;

		//头插法
		if (ps->pSnake == NULL)
		{
			ps->pSnake = cur;
		}
		else
		{
			cur->next = ps->pSnake;
			ps->pSnake = cur;
		}
	}

	//打印蛇身
	cur = ps->pSnake;
	while (cur)
	{
		SetPos(cur->x, cur->y);
		wprintf(L"%lc", BODY);
		cur = cur->next;
	}

	
	//贪吃蛇其他信息初始化
	ps->dir = RIGHT;
	ps->FoodWeight = 10;
	ps->pFood = NULL;
	ps->score = 0;
	ps->SleepTime = 200;
	ps->status = OK;
}


void CreateFood(pSnake ps)
{
	int x = 0, y = 0;
again:
	do
	{
		x = rand() % 53 + 2;
		y = rand() % 24 + 1;
	} while (x % 2 != 0);

	//判断坐标在不在蛇身上，与每个节点作比较
	pSnakeNode cur = ps->pSnake;
	while (cur)
	{
		if (x == cur->x && y == cur->y)
		{
			goto again;
		}
		cur = cur->next;
	}

	//创建食物
	pSnakeNode pFood = (pSnakeNode)malloc(sizeof(SnakeNode));
	if (pFood == NULL)
	{
		perror("malloc fail!");
		return;
	}

	pFood->x = x;
	pFood->y = y;

	ps->pFood = pFood;
	SetPos(x, y);
	wprintf(L"%lc", FOOD);

}


//游戏开始前的准备
void GameStart(pSnake ps)
{
	//控制控制台的信息，窗口大小，窗口名
	system("mode con cols=100 lines=30");
	system("title 贪吃蛇");

	//隐藏光标
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);//获得句柄
	CONSOLE_CURSOR_INFO CursorInfo;
	GetConsoleCursorInfo(handle, &CursorInfo);//获取控制台光标信息
	CursorInfo.bVisible = false;//隐藏光标
	SetConsoleCursorInfo(handle, &CursorInfo);

	//打印欢迎信息
	WelcomeToGame();

	//绘制地图
	CreatMap();

	//初始化蛇
	InitSnake(ps);

	//创建食物
	CreateFood(ps);
}


void PrintHelpInfo()
{
	SetPos(60, 12);
	printf("1.不能穿墙，不能咬到自己");
	SetPos(60, 14);
	printf("2.用 ↑.↓.←.→ 来控制蛇的移动");
	SetPos(60, 16);
	printf("3.A键是加速，D键是减速");

	SetPos(60, 18);
	printf("4.按空格键可以暂停,按Esc键可以退出游戏");
	SetPos(60, 20);
	printf("嘉鑫版");
}


int NextIsFood(pSnake ps, pSnakeNode pNext)
{
	if (ps->pFood->x == pNext->x && ps->pFood->y == pNext->y)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}


void EatFood(pSnake ps, pSnakeNode pNext)
{
	//头插
	pNext->next = ps->pSnake;
	ps->pSnake = pNext;

	pSnakeNode cur = ps->pSnake;
	
	//打印蛇
	while (cur)
	{
		SetPos(cur->x, cur->y);
		wprintf(L"%lc", BODY);
		cur = cur->next;
	}

	ps->score += ps->FoodWeight;

	//释放旧的食物
	free(ps->pFood);
	//新建食物
	CreateFood(ps);

}


void NotEatFood(pSnake ps, pSnakeNode pNext)
{
	//头插
	pNext->next = ps->pSnake;
	ps->pSnake = pNext;

	//释放尾结点
	//顺便打印尾节点
	pSnakeNode cur = ps->pSnake;

	SetPos(cur->x, cur->y);
	wprintf(L"%lc", BODY);

	while (cur->next->next)
	{
		cur = cur->next;
	}

	//将尾节点置空 打印 '  ' 
	SetPos(cur->next->x, cur->next->y);
	printf("  ");

	free(cur->next);
	cur->next = NULL;
}


void KillByWall(pSnake ps)
{
	if (ps->pSnake->x == 0 ||
		ps->pSnake->x == 56 ||
		ps->pSnake->y == 0 ||
		ps->pSnake->y == 26)
	{
		ps->status = KILL_BY_WALL;
	}
}

void KillBySelf(pSnake ps)
{
	pSnakeNode cur = ps->pSnake->next->next->next;

	while (cur)
	{
		if (cur->x == ps->pSnake->x && cur->y == ps->pSnake->y)
		{
			ps->status = KILL_BY_SELF;
			return;
		}
		cur = cur->next;
	}
}


void SnakeMove(pSnake ps)
{
	pSnakeNode pNext = (pSnakeNode)malloc(sizeof(SnakeNode));
	if (pNext == NULL)
	{
		perror("malloc fail!::294");
		return;
	}

	pNext->next = NULL;
	switch (ps->dir)
	{
	case UP:
		pNext->x = ps->pSnake->x;
		pNext->y = ps->pSnake->y - 1;
		break;
	case DOWN:
		pNext->x = ps->pSnake->x;
		pNext->y = ps->pSnake->y + 1;
		break;
	case LEFT:
		pNext->x = ps->pSnake->x - 2;
		pNext->y = ps->pSnake->y;
		break;
	case RIGHT:
		pNext->x = ps->pSnake->x + 2;
		pNext->y = ps->pSnake->y;
		break;
	}

	if (NextIsFood(ps, pNext))
	{
		//是食物就吃掉
		EatFood(ps, pNext);
	}
	else
	{
		//不是食物就正常走一步
		NotEatFood(ps, pNext);
	}

	//判断撞墙
	KillByWall(ps);
	
	//判断撞自己
	KillBySelf(ps);

}


void pause(pSnake ps)
{
	while (1)
	{
		Sleep(1000);
		if (KEY_PRESS(VK_SPACE))
		{
			break;
		}
		else if (KEY_PRESS(VK_ESCAPE))
		{
			ps->status = ESC;
			break;
		}
	}
}


void GameRun(pSnake ps)
{
	PrintHelpInfo();

	do
	{
		SetPos(62, 9);
		printf("总分:%5d\n", ps->score);
		SetPos(62, 10);
		printf("食物的分值:%02d\n", ps->FoodWeight);

		//检测按键
		//上、下、左、右、ESC、空格、F3、F4
		if (KEY_PRESS(VK_UP) && ps->dir != DOWN)
		{
			ps->dir = UP;
		}
		else if (KEY_PRESS(VK_DOWN) && ps->dir != UP)
		{
			ps->dir = DOWN;
		}
		else if (KEY_PRESS(VK_LEFT) && ps->dir != RIGHT)
		{
			ps->dir = LEFT;
		}
		else if (KEY_PRESS(VK_RIGHT) && ps->dir != LEFT)
		{
			ps->dir = RIGHT;
		}
		else if (KEY_PRESS(VK_ESCAPE))
		{
			ps->status = ESC;
			break;
		}
		else if (KEY_PRESS(VK_SPACE))
		{
			pause(ps);
		}
		else if (KEY_PRESS(0X41))
		{
			if (ps->SleepTime >= 80)
			{
				ps->SleepTime -= 30;
				ps->FoodWeight += 2;
			}
		}
		else if (KEY_PRESS(0X44))
		{
			if (ps->FoodWeight > 2)
			{
				ps->SleepTime += 30;
				ps->FoodWeight -= 2;
			}
		}

		//走一步
		SnakeMove(ps);

		//睡眠一下
		Sleep(ps->SleepTime);

	} while (ps->status == OK);
	//system("start https://www.yuanshen.com/#/");
}

void GameEnd(pSnake ps)
{
	SetPos(15, 12);
	switch (ps->status)
	{
	case ESC:
		printf("别啊，怎么就不玩了，不会是太菜了吧(滑稽)");
		break;
	case KILL_BY_SELF:
		printf("即将为您打开您的最爱！！！");
		SetPos(15, 13);
		printf("即将为您打开您的最爱！！！");
		SetPos(15, 14);
		printf("即将为您打开您的最爱！！！");

		
		for (int i = 3; i >=0; i--)
		{
			SetPos(28, 15);
			printf("%d", i);
			Sleep(1000);
		}
		
		system("start https://ys.mihoyo.com/");
		break;
	case KILL_BY_WALL:
		printf("即将为您打开您的最爱！！！");
		SetPos(15, 13);
		printf("即将为您打开您的最爱！！！");
		SetPos(15, 14);
		printf("即将为您打开您的最爱！！！");

		for (int i = 3; i > 0; i--)
		{
			SetPos(28, 15);
			printf("%d", i);
			Sleep(1000);
		}
		system("start https://ys.mihoyo.com/");
		break;
	}

	//释放资源
	pSnakeNode cur = ps->pSnake;
	pSnakeNode del = NULL;

	while (cur)
	{
		del = cur;
		cur = cur->next;
		free(del);
	}
	SetPos(0, 28);
	free(ps->pFood);
	ps = NULL;
}

