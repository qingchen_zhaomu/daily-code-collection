#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<math.h>
#include<string.h>
#include<assert.h>
#include<errno.h>

int main()
{
	FILE* pf = fopen("test.txt", "r"); 
	if (pf == NULL)
	{
		printf("%s\n", strerror(errno));
	}
	else
		printf("open file successful");
	return 0;
}

////strtok函数的使用
//int main()
//{
//	char arr[] = "zpw@bitedu.tech";
//	char* p = "@.";
//	char buf[1024];
//	char* ret;
//	strcpy(buf, arr);
//	for (ret = strtok(buf, p); ret; ret = strtok(NULL, p))
//	{
//		printf("%s\n", ret);
//	}
//	return 0;
//}

//int main()
//{
//	char arr[] = "zpw@bitedu.tech";
//	char* p = "@.";
//
//	char buf[1024] = { 0 };
//	strcpy(buf, arr);//拷贝一份之后可以切割buf
//	char* ret;
//
//	for (ret = strtok(buf, p); ret != NULL; ret = strtok(NULL, p))
//	{
//		printf("%s\n", ret);
//	}
//
//	return 0;
//}

////strstr函数的实现
//char* my_strstr(const char* p1, const char* p2)
//{
//	assert(p1 && p2);
//	char* s1, *s2;
//	char* cur = (char*)p1;
//	if (!*p1)
//		return  (char*)p1;
//	while (*cur)
//	{
//		s1 = cur;
//		s2 = (char*)p2;
//		while (*s1 && *s2 && !(*s1 - *s2))
//			s1++, s2++;
//		if (!*s2)
//			return cur;
//		cur++;
//	}
//	return NULL;
//}

//char* my_strstr(const char* p1, const char* p2)
//{
//	assert(p1 && p2);
//	char* s1 = NULL;
//	char* s2 = NULL;
//	char* cur = (char*)p1;
//	if (*p2 == '\0')
//	{
//		return ((char*)p1);
//		//因为p1是受到const的保护的，如果直接返回的话就说明p1不再受到保护
//		//所以需要一个强制类型转换将p1转换成char*
//	}
//	while (*cur)
//	{
//		s1 = cur;
//		s2 = (char*)p2;
//		while ((*s1 != '\0') && (*s2 != '\0') && (*s1 == *s2))
//		{
//			s1++;
//			s2++;
//		}
//		if (*s2 == '\0')
//		{
//			return cur;//找到子串
//		}
//		cur++;
//	}
//	return NULL;//找不到子串
//}
//int main()
//{
//	const char* arr1 = "abbbcdef";
//	const char* arr2 = "bbc";
//	char* ret = my_strstr(arr1, arr2);
//	if (ret == NULL)
//	{
//		printf("子串不存在\n");
//	}
//	else
//	{
//		printf("%s\n", ret);
//	}
//	return 0;
//}

////strstr - 查找字符串
//int main()
//{
//	const char* p1 = "abcdef";
//	const char* p2 = "abcdefghi";
//	const char* p3 = "def";
//	const char* p4 = "defq";
//	char* ret = strstr(p1, p3);// def
//	char* ret = strstr(p2, p3);// defghi
//	char* ret = strstr(p1, p4);// 不存在
//	if (ret == NULL)
//	{
//		printf("字串不存在\n");
//	}
//	else
//	{
//		printf("%s\n", ret);
//	}
//	return 0;
//}

////strncmp && strcmp
//int main()
//{
//	const char* arr1 = "abcdef";
//	const char* arr2 = "abcqwer";
//	//int ret = strcmp(arr1, arr2);
//	int ret = strncmp(arr1, arr2, 3);
//	printf("%d\n", ret);
//	return 0;
//}

//int main()
//{
//	int i = 0;
//	int n = 0;
//	scanf("%d", &n);
//	int sum = 0;
//	int ret = 1;
//	for (i = 1; i <= n; i++)
//	{
//		ret *= i;
//		sum += ret;
//	}
//	printf("%d", sum);
//	return 0;
//}

//int main()
//{
//	int i = 0;
//	int n = 0;
//	int sum = 0;
//	int j = 0;
//	scanf("%d", &n);
//	for (i = 0; i < n; i++)
//	{
//		printf("输入: ");
//		scanf("%d", &j);
//		sum += j;
//	}
//	printf("%d\n", sum);
//	return 0;
//}

//int main()
//{
//	int i = 0;
//	int sum = 0;
//	int n = 0;
//	scanf("%d", &n);
//	for (i = 1; i <= n; i++)
//	{
//		sum += i;
//	}
//	printf("%d", sum);
//	return 0;
//}

//int main()
//{
//	int i = 1;
//	/*for (i = 1; i <= 100; i++)
//	{
//		printf("%d ", i);
//	}*/
//	/*while (i <= 50)
//	{
//		printf("hello world  %d\n",i);
//		i++;
//	}*/
//	/*for (i = 0; i < 50; i++)
//	{
//		printf("hello world\n");
//		i++;
//	}*/
//	return 0;
//}

//int main()
//{
//	int i = 0;
//	int count = 0;
//	for (i = 101; i <= 200; i+=2)
//	{
//		int flag = 1;
//		int j = 0;
//		for (j = 2; j <= sqrt(i); j++)
//		{
//			if (i % j == 0)
//			{
//				flag = 0;
//				break;
//			}
//		}
//		if (flag == 1)
//		{
//			printf("%d ", i);
//			count++;
//		}
//	}
//	printf("\n%d\n", count);
//	return 0;
//}