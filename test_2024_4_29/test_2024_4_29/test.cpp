#include<iostream>
using namespace std;


class A
{
public:
	A(int a = 1)
		:_a(a)
	{}
private:
	int _a;
};

int main()
{
	A s1;
	const A s2;
	cout << &s1 << endl;
	cout << &s2 << endl;
	return 0;
}

//class A
//{
//public:
//	A(int a = 1)
//		:_a(a)
//	{}
//	
//	A* operator&()
//	{
//		cout << "A* operator&()" << endl;
//		return this;
//	}
//
//	const A* operator&()const
//	{
//		cout << "const A* operator&()const" << endl;
//		return this;
//	}
//private:
//	int _a;
//};
//
//int main()
//{
//	A s1;
//	const A s2;
//	cout << &s1 << endl;
//	cout << &s2 << endl;
//	return 0;
//}

//class Date
//{
//public:
//	Date(int year = 1, int month = 1, int day = 1)
//		:_year(year)
//		,_month(month)
//		,_day(day)
//	{}
//	void Print()
//	{
//		cout << _year << " " << _month << " " << _day << endl;
//	}
//
//private:
//	int _year;
//	int _month;
//	int _day;
//};

//int main()
//{
//	Date s1(2024, 10, 28);
//	Date s2;
//	s2 = s1;
//	s1.Print();
//	s2.Print();
//	return 0;
//}

//class Date
//{
//public:
//	Date(int year = 1, int month = 1, int day = 1)
//		:_year(year)
//		, _month(month)
//		, _day(day)
//	{}
//	Date(const Date& d)
//	{
//		_year = d._year;
//		_month = d._month;
//		_day = d._day;
//		cout << "const Date& d" << endl;
//	}
//	bool operator<(const Date& d) const
//	{
//		if (_year < d._year)
//			return true;
//		else if (_year == d._year)
//		{
//			if (_month < d._month)
//				return true;
//			else if (_month == d._month)
//			{
//				if (_day < d._day)
//					return true;
//			}
//		}
//		return false;
//	}
//	bool operator==(const Date& d)
//	{
//		return _year == d._year
//			&& _month == d._month
//			&& _day == d._day;
//	}
//	bool operator>(const Date& d)
//	{
//		return !(*this < d && *this == d);
//	}
//	Date& operator=(const Date& d)
//	{
//		_year = d._year;
//		_month = d._month;
//		_day = d._day;
//		return *this;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//int main()
//{
//	Date s1(2024, 1, 16);
//	Date s2(2023, 11, 18);
//	//s1 < s2;
//	Date s3 = s1;
//	s3 = s1;
//	return 0;
//}

//class Date
//{
//public:
//	Date(int year = 1, int month = 1, int day = 1)
//		:_year(year)
//		,_month(month)
//		,_day(day)
//	{}
//
////private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//bool Compare(const Date& s1, const Date& s2)
//{
//	if (s1._year > s2._year)
//		return true;
//	else if (s1._year == s2._year)
//	{
//		if (s1._month > s2._month)
//			return true;
//		else if (s1._month == s2._month)
//		{
//			if (s1._day > s2._day)
//				return true;
//		}
//	}
//	return false;
//}
//
//int main()
//{
//	Date s1(2024, 1, 1);
//	Date s2(2024, 3, 31);
//	return 0;
//}
//class stack
//{
//public:
//	stack(int capacity = 10)
//	{
//		_a = (int*)malloc(sizeof(int) * capacity);
//		if (_a == nullptr)
//		{
//			perror("malloc fail");
//			return;
//		}
//		_size = 0;
//		_capacity = capacity;
//	}
//
//	stack(const stack& s1)
//	{
//		_a = (int*)malloc(sizeof(int) * s1._capacity);
//		if (_a == nullptr)
//		{
//			perror("malloc fail");
//			return;
//		}
//		memcpy(_a, s1._a, sizeof(int) * s1._size);
//		_capacity = s1._capacity;
//		_size = s1._size;
//	}
//
//	void Push(const int& data)
//	{
//		_a[_size] = data;
//		_size++;
//	}
//	~stack()
//	{
//		if (_a)
//		{
//			free(_a);
//			_a = nullptr;
//			_size = 0;
//			_capacity = 0;
//		}
//	}
//private:
//	int* _a;
//	int _size;
//	int _capacity;
//};
//
//int main()
//{
//	stack s1;
//	s1.Push(1);
//	s1.Push(2);
//	s1.Push(3);
//	s1.Push(4);
//	stack s2 = s1;
//	return 0;
//}

//class Date
//{
//public:
//	Date(int year = 1, int month = 1)
//		:_year(year)
//		,_month(month)
//	{}
//	void Print()
//	{
//		cout << _year << " " << _month << " " << endl;
//	}
//private:
//	int _year;
//	int _month;
//};
//
//int main()
//{
//	Date s1(2024, 1);
//	Date s2(s1);
//	s1.Print();
//	s2.Print();
//	return 0;
//}

//class Date
//{
//public:
//	Date(int year = 1, int month = 2)
//		:_year(year)
//		, _month(month)
//	{}
//	Date(const Date& s1)
//	{
//		_year = s1._year;
//		_month = s1._month;
//	}
//private:
//	int _year;
//	int _month;
//};
//
//int main()
//{
//	Date s1;
//	Date s2(s1);
//	return 0;
//}

//class Date
//{
//public:
//	Date(int year = 1,int month = 2)
//		:_year(year)
//		,_month(month)
//	{}
//	void Print(const Date& d1)
//	{
//		;
//	}
//private:
//	int _year;
//	int _month;
//};
//
//int main()
//{
//	Date s1;
//	Date s2;
//	s1.Print(s2);
//	return 0;
//}

//int Swap(int a, int b)
//{
//	int tmp = a;
//	a = b;
//	b = tmp;
//}
//
//int Swap(int* a, int* b)
//{
//	int tmp = *a;
//	*a = *b;
//	*b = tmp;
//}
//
//int main()
//{
//	int n = 1, m = 2;
//	return 0;
//}

//class Date
//{
//public:
//	Date(int year=1,int month=1,int day=1)
//		:_year(year)
//		,_month(month)
//		,_day(day)
//	{}
//	//Date(const Date& d1)//拷贝构造函数
//	//{
//	//	this->_year = d1._year;
//	//	this->_month = d1._month;
//	//	this->_day = d1._day;
//	//}
//
//	Date(const Date& d1)//拷贝构造函数
//	{
//		_year = d1._year;
//		_month = d1._month;
//		_day = d1._day;
//	}
//	void Print()
//	{
//		cout << _year << " " << _month << " " << _day << endl;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//int main()
//{
//	Date s1(2024, 1, 16);
//	Date s2(s1);
//	s1.Print();
//	s2.Print();
//	return 0;
//}

//class Time
//{
//public:
//	Time()
//		:c(0)
//	{}
//	~Time()
//	{
//		cout << "Time" << endl;
//	}
//private:
//	int c;
//};
//
//class Date
//{
//public:
//	Date(int year = 1, int month = 1, int day = 1)
//		:_year(year)
//		,_month(month)
//		,_day(day)
//	{}
//private:
//	int _year;
//	int _month;
//	int _day;
//	Time _b;
//};
//
//int main()
//{
//	Date s1;
//	return 0;
//}

//class stack
//{
//public:
//	stack(int* a = nullptr)
//		:_a((int*)malloc(sizeof(int)*4))
//		,_capacity(0)
//		,_top(0)
//	{
//		cout << "stack" << endl;
//	}
//
//	~stack()
//	{
//		free(_a);
//		_a = nullptr;
//		cout << "~stack" << endl;
//	}
//
//private:
//	int* _a;
//	int _capacity;
//	int _top;
//};
//
//int main()
//{
//	stack s1;
//	return 0;
//}

//class Date
//{
//public:
//	Date()
//		:_month(20)
//		,_year(_month)
//	{}
//private:
//	int _year;
//	int _month;
//};
//
//int main()
//{
//	Date s1;
//	return 0;
//}

//class Time
//{
//public:
//	Time(int b)
//	{
//		_hour = b;
//	}
//private:
//	int _hour;
//};
//
//class Date
//{
//public:
//	Date(int year = 2024, int month = 1, int day = 30)
//		:_year(year)
//		,_month(month)
//		,_day(day)
//		,a(1231)
//	{}
//	void Print()
//	{
//		cout << _year << endl;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//	Time a;
//};
//
//int main()
//{
//	Date s1;
//	return 0;
//}


//class Date
//{
//public:
//	Date(int year, int month, int day)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	void Print()
//	{
//		cout << _year << " " << _month << " " << _day << endl;
//	}
//private:
//	int _year = 1;
//	int _month = 1;
//	int _day = 1;
//};
//
//int main()
//{
//	Date s1(2024, 1, 16);
//	s1.Print();
//	return 0;
//}

//class Date
//{
//public:
	//Date(int year, int month, int day)
	//{
	//	_year = year;
	//	_month = month;
	//	_day = day;
	//}
//	void Print()
//	{
//		cout << _year << " " << _month << " " << _day << endl;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//int main()
//{
//	Date s1(2024, 1, 16);
//	s1.Print();
//	return 0;
//}

//class Date
//{
//public:
//	void Print()
//	{
//		cout << _year << " " << _month << " " << _day << endl;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//int main()
//{
//	Date s1;
//	s1.Print();
//	return 0;
//}

//class Date
//{
//public:
//	Date()
//	{
//		;//空类
//	}
//	Date(int year=2024, int month, int day)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//int main()
//{
//	Date s1;
//	return 0;
//}

//class Date
//{
//public:
//	Date(int year, int month, int day)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//int main()
//{
//	Date s1;
//	return 0;
//}

//class Date
//{
//public:
//	Date()
//	{
//		cout << "构成函数重载" << endl;
//	}
//	Date(int year = 1, int month = 1, int day = 1)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};

//class Stack
//{
//public:
//	Stack()
//	{
//		_a = (int*)malloc(sizeof(int) * 4);
//		_capacity = 0;
//		_top = 0;
//	}
//private:
//	int* _a;
//	int _capacity;
//	int _top;
//};
//
//int main()
//{
//	Stack s1;
//	return 0;
//}

//typedef struct Stack
//{
//	int* a;
//	int capacity;
//	int top;
//}ST;
//
//void StackInit(ST* plist)
//{
//	int* tmp = (int*)malloc(sizeof(int) * 4);
//	if (tmp == NULL)
//	{
//		perror("malloc fail");
//		return;
//	}
//	plist->a = tmp;
//	plist->capacity = 0;
//	plist->top = 0;
//}
//
//int main()
//{
//	ST s1;
//	StackInit(&s1);
//	return 0;
//}

//class Date
//{
//public:
//	Date(int year = 2024, int month = 1, int day = 19)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//		cout << _year << _month << _day << endl;
//	}
//	~Date()
//	{
//		;//日期类没有要清理的资源，无需析构
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//
//
//int main()
//{
//	Date s1;
//	
//	return 0;
//}