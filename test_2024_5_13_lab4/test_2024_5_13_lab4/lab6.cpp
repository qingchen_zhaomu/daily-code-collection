#include<iostream>
using namespace std;
#include<assert.h>
#include<string>

char* find_substring(char* str1, char* str2)
{
	char* cur = str1;
	char* s1, * s2;

	assert(*str1 && *str2);

	if (*str2 == '\0')
		return str1;

	while (*cur)
	{
		s1 = cur; s2 = str2;

		while (*s1 == *s2)
		{
			++s1, ++s2;
			if (*s2 == '\0')
				return cur;
		}
		++cur;
	}
	return nullptr;
}

char* string_connect(char* destination, char* start)
{
	char* ret = destination;

	assert(*destination && *start);

	while (*destination)
		++destination;

	while ((*destination++ = *start++))
		;
	return ret;
}

void reverse_string(char* str, int len)
{
	for (int i = 0, j = len - 1; i < j; i++, j--) {
		char c = str[i];
		str[i] = str[j];
		str[j] = c;
	}
}

int main()
{
	char arr[30] = "hello world";
	char ar[10] = "wor";
	char* pos1 = find_substring(arr, ar);//�ַ�������
	printf("%p\n", pos1);

	
	int count = 0;
	char* pos2 = string_connect(arr, ar);
	while (*pos2++ != '\0')
		count++;
	for (int i = 0; i < count; i++)
		cout << arr[i];
	cout << endl;

	int sz = 0; char* p = arr;
	while (*p++ != '\0')
		sz++;
	reverse_string(arr, sz);
	for (int i = 0; i < sz; i++)
		cout << arr[i];
	cout << endl;
	return 0;
}





