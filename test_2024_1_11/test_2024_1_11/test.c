﻿#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>



//int add(int a, int b)
//{
//	return a + b;
//}
//int sub(int a, int b)
//{
//	return a - b;
//}
//int mul(int a, int b)
//{
//	return a * b;
//}
//int div(int a, int b)
//{
//	return a / b;
//}
//int main()
//{
//	int x, y;
//	int input = 1;
//	int ret = 0;
//	int(*p[5])(int x, int y) = { 0, add, sub, mul, div }; //转移表
//	do
//	{
//		printf("*************************\n");
//		printf(" 1:add 2:sub \n");
//		printf(" 3:mul 4:div \n");
//		printf(" 0:exit \n");
//		printf("*************************\n");
//		printf("请选择：");
//		scanf("%d", &input);
//		if ((input <= 4 && input >= 1))
//		{
//			printf("输入操作数：");
//			scanf("%d %d", &x, &y);
//			ret = (*p[input])(x, y);
//			printf("ret = %d\n", ret);
//		}
//		else if (input == 0)
//		{
//			printf("退出计算器\n");
//		}
//		else
//		{
//			printf("输⼊有误\n");
//		}
//	} while (input);
//	return 0;
//}
 
//void print(char* p)
//{
//	printf("%s\n", p);
//}
//
//int main()
//{
//	void(*p)(char*) = print;
//	(*p)(" love xzy!!!");
//	return 0;
//}

//void ptr()
//{
//	printf("hello world\n");
//}
//
//int main()
//{
//	ptr();
//	printf("%p\n", ptr);
//	printf("%p\n", &ptr);
//	return 0;
//}

//void array(int(*p)[4], int i, int j)
//{
//	for (int a = 0; a < i; a++)
//	{
//		for (int b = 0; b < j; b++)
//		{
//			printf("%d ", *(*(p + a) + b));
//		}
//		printf("\n");
//	}
//}
//
//int main()
//{
//	int arr[3][4] = { 1,2,3,4,2,3,4,5,3,4,5,6 };
//	array(arr, 3, 4);
//	return 0;
//}

//int main()
//{
//	int arr[5] = { 1,2,3,4,5 };
//	int(*p)[5] = &arr;
//	return 0;
//}

//int main()
//{
//	char arr1[] = "hello world";
//	char arr2[] = "hello world";
//	const char* str3 = "hello world";
//	const char* str4 = "hello world";
//	if (arr1 == arr2)
//		printf("hehe\n");
//	else
//		printf("haha\n");
//
//	if (str3 == str4)
//		printf("hihi\n");
//	else
//		printf("hoho\n");
//
//	return 0;
//}

//int main()
//{
//	char* a = "hello world";
//	char* p = a;
//	for (int i = 0; i < 11; i++)
//	{
//		printf("%c", *(p+i));
//	}
//	return 0;
//}

//int main()
//{
//	char a = 'w';
//	char* p = &a;
//	printf("%c\n", *p);
//	return 0;
//}

//int main()
//{
//	int arr1[4] = { 1,2,3,4 };
//	int arr2[4] = { 2,3,4,5 };
//	int arr3[4] = { 3,4,5,6 };
//	int* ptr[3] = { arr1,arr2,arr3 };
//	for (int i = 0; i < 3; i++)
//	{
//		for (int j = 0; j < 4; j++)
//		{
//			printf("%d ", *(*(ptr + i) + j));
//		}
//		printf("\n");
//	}
//	printf("\n");
//	for (int i = 0; i < 3; i++)
//	{
//		for (int j = 0; j < 4; j++)
//		{
//			printf("%d ", ptr[i][j]);
//		}
//		printf("\n");
//	}
//	return 0;
//}

//int main()
//{
//	int arr[3][4] = { 1,2,3,4,2,3,4,5,3,4,5,6 };
//	return 0;
//}

//int main()
//{
//	int a = 1, b = 2, c = 3;
//	int* p1 = &a; 
//	int* p2 = &b;
//	int* p3 = &c;
//	int* arr[3] = { p1,p2,p3 };
//	*arr[1] = 6;
//	printf("%d\n", b);
//	*(*(arr + 2)) = 9;
//	printf("%d\n", c);
//	return 0;
//}

//int my_strlen(const char* str)
//{
//	int count = 0;
//	assert(str);
//	while (*str)
//	{
//		count++;
//		str++;
//	}
//	return count;
//}
//int main()
//{
//	int len = my_strlen("abcdef");
//	printf("%d\n", len);
//	return 0;
//}

//int main()
//{
//	int a = 0;
//	int* p = &a;
//	int** pa = &p;
//	**pa = 10;
//	printf("%d\n", a);
//	return 0;
//}

//void Swap(int* x, int* y)
//{
//	int tmp = *x;
//	*x = *y;
//	*y = tmp;
//}
//int main()
//{
//	int a = 12;
//	int b = 30;
//	printf("%d %d\n", a, b);
//	Swap(&a, &b);
//	printf("%d %d\n", a, b);
//	return 0;
//}


//#define NDEBUG
//#include<assert.h>

//int main()
//{
//	int a = 10;
//	int* p = &a;
//	//......
//	p = NULL;
//	//......
//	assert(p!=NULL);
//
//	return 0;
//}


//int main()
//{
//	int a = 10;
//	int* p = NULL;
//	*p = 20;
//	return 0;
//}


//int* test()
//{
//	int a = 10;
//	return &a;
//}
//
//int main()
//{
//	int* p = test();
//	printf("hello world\n");
//	printf("%d\n", *p);
//	return 0;
//}

//int main()
//{
//	int arr[5] = { 0 };
//	int* p = arr;
//	for (int i = 0; i < 10; i++)
//	{
//		*p = 1;
//		p++;
//	}
//	return 0;
//}


//int main()
//{
//	int* p;
//	*p = 20;
//	return 0;
//}


//int main()
//{
//	int arr[] = { 1,2,3,4,5,6 };
//	int* p = &arr[0];
//	int i = 0;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	while (p < arr + sz)
//	{
//		printf("%d ", *p);
//		p++;
//	}
//	return 0;
//}

//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	printf("%d\n", &arr[9] - &arr[0]);
//	return 0;
//}

//int main()
//{
//	int i = 0;
//	int arr[5] = { 1,2,3,4,5 };
//	for (i = 0; i < 5; i++)
//	{
//		printf("%d ", arr[i]);
//	}
//	printf("\n");
//	for (i = 0; i < 5; i++)
//	{
//		printf("%d ", *(arr + i));
//	}
//	printf("\n");
//	for (i = 0; i < 5; i++)
//	{
//		printf("%d ", i[arr]);
//	}
//	return 0;
//}

//int main()
//{
//	int a = 10;
//	int* p = &a;
//	printf("%p\n", &a);
//	printf("%p\n", p);
//	printf("%p\n", p+1);
//	printf("%p\n", p + 2);
//	return 0;
//}

//int main()
//{
//	int n = 10;
//	int m = 20;
//	const int* p = &n;
//	*p = 20;
//	p = &m; 
//	return 0;
//}
//
//
//
//
//
//int main()
//{
//	int n = 10;
//	int m = 20;
//	int* const p = &n;
//	*p = 20;
//	p = &m;
//	return 0;
//}
//
//
//
//
//int main()
//{
//	int n = 10;
//	int m = 20;
//	const int* const p = &n;
//	*p = 20;
//	p = &m;
//	return 0;
//}




//int main()
//{
//	int a = 0;
//	a = 20;
//	const int b = 5;
//	const int* p = &b;
//	*p = 0;
//	printf("%d\n", b);
//	return 0;
//}


//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int* p = &arr[0];
//	int i = 0;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	while (p < arr + sz) //指针的⼤⼩⽐较
//	{
//		printf("%d ", *p);
//		p++;
//	}
//	return 0;
//}


//int main()
//{
//	int a = 0;
//	void* p = &a;
//	++(int*)p;
//	return 0;
//}


//int main()
//{
//	int i = 10;
//	char* a = (char*)&i;
//	int* b = &i;
//
//	printf("%p\n", &i);
//	printf("%p\n", a);
//	printf("%p\n", a + 1);
//	printf("%p\n", b);
//	printf("%p\n", b + 1);
//	return 0;
//}


//int main()
//{
//	int arr[5] = { 1, 2, 3, 4, 5 };
//	*(arr + 1) = 0;
//	for (int i = 0; i < 5; i++)
//	{
//		printf("%d ", *(arr + i));
//	}
//	return 0;
//}

//int main()
//{
//	int n = 0x44332211;
//	int* pc = &n;
//	*pc = 0;
//	return 0;
//}

//int main()
//{
//	int i = 6;
//	int* p = &i;
//	printf("i=%d\n", *p);
//	*p = 3;
//	printf("i=%d\n", *p);
//	return 0;
//}


