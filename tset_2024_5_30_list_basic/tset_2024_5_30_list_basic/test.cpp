#include"list.h"

namespace hjx
{
	template<class T>
	void Print(list<T> s1)
	{
		for (auto e : s1)
			cout << e << " ";
		cout << endl;
	}

	void test_mylist1()
	{
		list<int> s1;
		s1.push_back(1);
		s1.push_back(2);
		s1.push_back(3);
		s1.push_back(4);

		Print(s1);

		s1.push_front(20);
		s1.push_front(30);

		Print(s1);

		s1.pop_back();
		s1.pop_back();
		s1.pop_back();

		Print(s1);

		s1.pop_front();

		Print(s1);

	}
}


int main()
{
	hjx::test_mylist1();
	return 0;
}