#include<iostream>
using namespace std;
#include<vector>
#include<algorithm>

//class Solution {
//public:
//    Solution() = default;
//    void duplicateZeros(vector<int>& arr)
//    {
//        int start = 0, pos_last = arr.size() - 1, end = arr.size() - 1;
//        int count = 0;
//        while (start < pos_last)
//        {
//            if (arr[start++] == 0)
//                --pos_last, ++count;
//        }
//        if (arr[pos_last] == 0)count++;
//        if (arr.size() - count < pos_last + 1)
//        {
//            arr[end--] = arr[pos_last--];
//        }
//        while (end > 0)
//        {
//            if (arr[pos_last] == 0)
//            {
//                arr[end] = arr[end - 1] = 0;
//                end -= 2;
//                pos_last--;
//            }
//            else
//            {
//                arr[end--] = arr[pos_last--];
//            }
//        }
//    }
//};

//class Solution {
//public:
//    int triangleNumber(vector<int>& nums)
//    {
//        sort(nums.begin(), nums.end());
//        int end = nums.size() - 1;
//        int left = 0, right = end - 1;
//        int count = 0;
//        while (end >= 2)
//        {
//            right = end - 1, left = 0;
//            while (right > left)
//            {
//                if (nums[left] + nums[right] > nums[end])
//                {
//                    count += right - left;
//                    --right;
//                }
//                else
//                    ++left;
//            }
//            end--;
//        }
//        return count;
//    }
//};

class Solution {
public:
    
    bool isHappy(int n)
    {
        int slow = n, fast = n;

    }
};

int after_square(int n)
{
    int return_num = 0;
    while (n / 10 != 0)
    {
        int num = n % 10;
        return_num += num * num;
        n /= 10;
    }
    return_num += n * n;
    return return_num;
}

int main()
{
    vector<int> s1 = { 1,5,2,0,6,8,0,6,0 };
    vector<int> s3 = { 8,4,5,0,0,0,0,7 };
    //Solution s2;
    //int n = after_square(19);
    //int n = after_square(37);
    //int n = min(4, 6);
    //cout << n << endl;
    sort(s1.begin(), s1.end(), greater<int>());
    for (auto e : s1)
    {
        cout << e << " ";
    }
    cout << endl;
    return 0;
}