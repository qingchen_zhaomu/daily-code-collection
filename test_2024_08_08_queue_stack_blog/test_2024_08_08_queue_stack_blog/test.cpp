#include"stack.h"
#include"queue.h"

namespace hjx
{
	void test_stack1()
	{
		stack<int> s1;
		s1.push(1);
		s1.push(2);
		s1.push(3);
		s1.push(4);
		while (!s1.empty())
		{
			cout << s1.top() << " ";
			s1.pop();
		}
	}

	void test_queue1()
	{
		queue<int> q1;
		q1.push(1);
		q1.push(2);
		q1.push(3);
		q1.push(4);
		q1.push(5);

		while (!q1.empty())
		{
			cout << q1.front() << " ";
			q1.pop();
		}
	}
}

int main()
{
	//hjx::test_stack1();
	hjx::test_queue1();
	return 0;
}