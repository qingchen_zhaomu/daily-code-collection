#pragma once
#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;
#include<assert.h>

namespace hjx
{
	class string
	{
		const static size_t npos;

		typedef char* iterator;
		typedef const char* const_iterator;
	public:
		string(const char* str = "")
			:_size(strlen(str))
		{
			_str = new char[_size + 1];
			strcpy(_str, str);
			_capacity = _size;
		}

		string(const string& s1)
		{
			string tmp(s1._str);
			swap(tmp);
		}

		string& operator=(string s1)
		{
			swap(s1);
			return *this;
		}

		~string()
		{
			delete[] _str;
			_str = nullptr;
			_size = _capacity = 0;
		}

		const char* c_str()const
		{
			return _str;
		}

		size_t size()const
		{
			return _size;
		}

		char& operator[](int pos)
		{
			assert(pos < _size && pos >= 0);
			return _str[pos];
		}
		const char& operator[](int pos)const
		{
			assert(pos < _size && pos >= 0);
			return _str[pos];
		}

		iterator begin()
		{
			return _str;
		}

		iterator end()
		{
			return _str + _size;
		}

		const_iterator begin()const
		{
			return _str;
		}

		const_iterator end()const
		{
			return _str + _size;
		}

		void swap(string& s1)
		{
			std::swap(_str, s1._str);
			std::swap(_size, s1._size);
			std::swap(_capacity, s1._capacity);
		}

		void reserve(size_t len)
		{
			if (len > _capacity)
			{
				char* tmp = new char[len + 1];
				strcpy(tmp, _str);
				delete[] _str;
				_str = tmp;
				_capacity = len;
			}
		}

		void insert(size_t pos, const char s)
		{
			if (_size == _capacity)
			{
				size_t newcapacity = _capacity == 0 ? 4 : _capacity * 2;
				reserve(newcapacity);
			}

			for (size_t i = _size + 1; i > pos; i--)
				_str[i] = _str[i - 1];
			_str[pos] = s;
			_size++;
		}

		void insert(size_t pos, const char* s)
		{
			size_t len = strlen(s);
			if (len + _size > _capacity)
				reserve(len + _size);

			for (size_t i = _size + len; i > pos + len - 1; i--)
				_str[i] = _str[i - len];

			memcpy(_str + pos, s, len);
			_size += len;
		}

		void push_back(const char s)
		{
			insert(_size, s);
		}

		void append(const char* s)
		{
			insert(_size, s);
		}

		string& operator+=(const char s)
		{
			push_back(s);
			return *this;
		}

		string& operator+=(const char* s)
		{
			append(s);
			return *this;
		}

		void erase(size_t pos, size_t len = npos)
		{
			if (len > _size - 1 - pos)
			{
				_str[pos] = '\0';
				_size = pos;
			}
			else
			{
				strcpy(_str + pos, _str + pos + len);
				_size -= len;
			}
		}

		size_t find(char s, size_t pos)
		{
			for (size_t i = pos; i < _size; i++)
			{
				if (_str[i] == s)
				{
					return i;
				}
			}

			return npos;
		}

		size_t find(char* s, size_t pos)
		{
			char* p = strstr(_str + pos, s);
			return p - _str;
		}

		bool operator<(const string& s) const
		{
			return strcmp(_str, s._str) < 0;
		}

		bool operator>(const string& s) const
		{
			return !(*this <= s);
		}

		bool operator<=(const string& s) const
		{
			return *this < s || *this == s;
		}

		bool operator>=(const string& s) const
		{
			return !(*this < s);
		}

		bool operator==(const string& s) const
		{
			return strcmp(_str, s._str) == 0;
		}

		bool operator!=(const string& s) const
		{
			return !(*this == s);
		}

		string substr(size_t pos, size_t len = npos)
		{
			if (len > _size - pos)
			{
				string tmp(_str + pos);
				return tmp;
			}
			else
			{
				string tmp;
				tmp.reserve(len + 1);
				memcpy(tmp._str, _str + pos, len);
				tmp._str[len + 1] = '\0';
				tmp._size = len;
				return tmp;
			}
		}

		void clean()
		{
			_str[0] = '\0';
			_size = 0;
		}

	private:
		char* _str;
		size_t _size;
		size_t _capacity;
	};

	ostream& operator<<(ostream& out, const string& s1)
	{
		for (int i = 0; i < s1.size(); i++)
			out << s1[i];
		return out;
	}

	istream& operator>>(istream& in, string& s1)
	{
		s1.clean();
		char ch = in.get();
		while (ch != ' ' && ch != '\n')
		{
			s1 += ch;
			ch = in.get();
		}
		return in;
	}

	

	const size_t string::npos = -1;

	void swap(string& s1, string& s2)
	{
		s1.swap(s2);
	}
}

