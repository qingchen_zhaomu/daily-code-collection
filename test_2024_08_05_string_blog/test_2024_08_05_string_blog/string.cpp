#include"string.h"

namespace hjx
{
	void test_string1()
	{
		string s1("hello world");
		for (int i = 0; i < s1.size(); i++)
		{
			cout << s1[i] << " ";
		}
		cout << endl;

		for (auto e : s1)
		{
			cout << e << " ";
		}
		cout << endl;

		//s1.insert(3, 'x');
		s1.push_back('x');
		cout << s1.c_str() << endl;
		
		//s1.insert(3, "hahahahaha");
		s1.append("hahahahaha");
		cout << s1.c_str() << endl;
	}

	void test_string2()
	{
		string s1("hello world");
		/*s1.erase(3, 5);
		cout << s1.c_str() << endl;
		s1.erase(3);
		cout << s1.c_str() << endl;*/
		/*string s2;
		cout << s1 << endl;
		cin >> s2;
		cout << s2 << endl;*/

		string s2 = s1.substr(3);
		cout << s2 << endl;
	}

}
//#include<string>
int main()
{
	//hjx::test_string1();
	hjx::test_string2();
	/*string s2("hello world");
	s2.insert(3, "x");
	cout << s2.c_str();*/
	return 0;
}