#define _CRT_SECURE_NO_WARNINGS 1

#include<iostream>
#include<vector>
#include<list>
#include<map>
#include<string>
#include<array>
using namespace std;

#include<assert.h>

//int main()
//{
//	// 常见左值
//
//	int* p = new int(10);
//	int b = 1;
//	const int c = b;
//	*p = 10;
//	string s("1111111111");
//	s[0];
//
//	double x = 1.1, y = 2.2;
//	// 常见右值(如下)
//	10;
//	x + y;
//	fmin(x, y);
//	string("1111");
//
//	////////////////////////////////////
//	// 左值引用
//	int& r1 = b;
//	int*& r2 = p;
//	int& r3 = *p;
//	string& r4 = s;
//
//	////////////////////////////////////
//	// 右值引用给右值取别名
//	int&& rr1 = 10;
//	double&& rr2 = x+y;
//	double&& rr3 = fmin(x,y);
//
//
//
//	/////////////////////////////////////
//	//cout << &p << endl;
//	//cout << &b << endl;
//	//cout << &c << endl;
//	//cout << &s << endl;
//
//	/////////////////////////////////////
//	//cout << &10 << endl;
//	//cout << &(x + y) << endl;
//	//cout << &(string("11111111")) << endl;
//
//	const int& a = 10;
//
//
//	/////////////////////////////////////////////////
//	// 左值引用引用给右值取别名：不能直接引用，但是const 左值引用可以
//	const int& rx1 = 10;
//	const double& rx2 = x+y;
//	const double& rx3 = fmin(x, y);
//	const string& rx4 = string("11111");
//
//
//	// 右值引用引用给左值取别名：不能直接引用，但是move(左值)以后右值引用可以引用
//	int&& rrx1 = move(b);
//	int*&& rrx2 = move(p);
//	int&& rrx3 = move(*p);
//	string&& rrx4 = move(s);
//	string&& rrx5 = (string&&)s;
//
//
//	/*int&& rrx1 = b;
//	int*&& rrx2 = p;
//	int&& rrx3 = *p;
//	string&& rrx4 = s;
//	string&& rrx5 = s;*/
//
//
//	vector<vector<int>> vv(100, vector<int>(0));
//	//return vv;
//
//	return 0;
//}



//vector<vector<int>> test1()
//{
//	vector<vector<int>> vv(100, vector<int>(0));
//	return vv;
//}
//
//int main()
//{
//	vector<vector<int>> vvv;
//	vvv = test1();
//	return 0;
//}


//int main()
//{
//	vector<vector<int>> vvv = test1();
//	return 0;
//}



void data(int& t)
{
	cout << "左值" << endl;
}

void data(int&& t)
{
	cout << "右值" << endl;
}

template<class T>
void func(T&& t)
{
	data(forward<T>(t));
}

int main()
{
	int a = 10;
	func(a);
	func(move(10));
	return 0;
}






