//#include<iostream>
//using namespace std;
//
// //                    1
////struct Date
////{
////	int year;
////	int month;
////	int day;
////
////	bool operator<(Date& d1)
////	{
////		return d1.year < year;
////	}
////};
////
////template<class T>
////bool myless(const T& left, const T& right)
////{
////	return left < right;
////}
////
////// 函数模板不要写特化，因为可以直接写函数
////template<>
////bool myless<Date*>(Date* const & left, Date* const& right)
////{
////	return *left < *right;
////}
//
//
//
//
//
//
// //                    2
//
//// 全特化
//template<class T1, class T2>
//struct Date
//{
//	Date()
//	{
//		cout << "Date<T1, T2>" << endl;
//	}
//};
//
//template<>
//struct Date<int, char>
//{
//	Date()
//	{
//		cout << "Date<int, char>" << endl;
//	}
//};
//
//
////int main()
////{
////	Date<int, int> d1;
////	Date<int, char> d2;
////	return 0;
////}
//
//
//
////                    3
//// 半特化      偏特化
//
//// 原模版
//template<class T1, class T2>
//struct myless
//{
//	myless() { cout << "原模版" << endl; }
//	bool operator()(T1& x, T2& y)
//	{
//		return x < y;
//	}
//};
//
//// 全特化
//template<>
//struct myless<char, double>
//{
//	myless() { cout << "全特化" << endl; }
//
//	bool operator()(char& x, double& y)
//	{
//		return x < y;
//	}
//};
//
//// 半特化
//template<class T>
//struct myless<T, int>
//{
//	myless() { cout << "半特化" << endl; }
//
//	bool operator()(T& x, int& y)
//	{
//		return x < y;
//	}
//};
//
//// 偏特化
//template<class T1, class T2>
//struct myless<T1*, T2*>
//{
//	myless() { cout << "偏特化" << endl; }
//
//	// 此处的T类型不为T*，而是T
//	// 如果我此时传过来的是int*，则此时T的类型为int
//	bool operator()(T1* x, T2* y)
//	{
//		return *x < *y;
//	}
//};
//
//int main()
//{
//	myless<int, char> ml;//原模版
//	myless<char, double> m2;//全特化
//	myless<int, int> m3;//半特化
//	myless<int*, int*> m4;//偏特化
//	myless<int**, int**> m5;//偏特化
//	return 0;
//}
//
////template<typename T1, typename T2>
////void func(T1 i, T2 j)
////{
////	cout << "hello world" << endl;
////}
////
////template<class T>
////void Add(const T& left, const T& right)
////{
////	cout << "T" << endl;
////}
////
////void Add(const int& left, const double& right)
////{
////	cout << "int  double" << endl;
////}
////
////
////int main()
////{
////	
////	Add(1, 1);
////	Add(1, 1.1);
////
////	return 0;
////}
//
//
////int Add(int left, int right)
////{
////	cout << "1" << endl;
////	return left + right;
////}
////
////template<class T>
////T Add(T left, T right)
////{
////	cout << "2" << endl;
////	return left + right;
////}
////
////int main()
////{
////	Add(1, 1);
////	Add<int>(1, 1);
////	return 0;
////}
//
//template<class T>
//class date
//{
//public:
//	date(T year, T month, T day, size_t n )
//		:_year(year)
//		, _month(month)
//		, _day(day)
//	{
//		cout << "类模板" << endl;
//	}
//
//	~date();
//
//private:
//	T _year;
//	T _month;
//	T _day;
//};
//
//template<class T>
//date<T>::~date()
//{
//	cout << "~date()" << endl;
//}
//
////int main()
////{
////	date<int> d1(1, 1, 1,10);
////	return 0;
////}


#include"func.h"

int main()
{
	//Add();
	func();
	return 0;
}

