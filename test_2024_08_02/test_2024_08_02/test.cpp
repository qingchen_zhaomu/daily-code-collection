#include<iostream>
using namespace std;

struct A
{
	~A()
	{
		cout << "~A()" << endl;
	}
};

class smartptr
{
public:
	smartptr(A* ret)
		:_ret(ret)
	{}

	~smartptr()
	{
		delete _ret;
	}

private:
	A* _ret;
};

void sendmsg(int a)
{
	A* arr1 = new A;
	A* arr2 = new A;
	smartptr up1(arr1);
	smartptr up2(arr2);

	if (a == 0)throw("a==0");
	
	cout << "hahahahahahahaha" << endl;
}

int main()
{
	try
	{
		sendmsg(0);
	}
	catch(...)
	{
		cout << "hehe" << endl;
	}

	return 0;
}
