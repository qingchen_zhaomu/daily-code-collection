#pragma once
#pragma once
#include<iostream>
using namespace std;
#include<assert.h>
#include<list>

namespace hjx
{
	template<class T, class Container = list<T>>
	class queue
	{
	public:

		void push(const T& x)
		{
			_con.push_back(x);
		}
		void pop()
		{
			_con.pop_front();
		}

		const T& top()
		{
			return _con.front();
		}
		bool empty()
		{
			return _con.empty();
		}

		size_t size()
		{
			return _con.size();
		}
	private:
		Container _con;
	};
}