#define _CRT_SECURE_NO_WARNINGS 1
#include"sort.h"

void PrintArray(int* a, int n)
{
	for (int i = 0; i < n; i++)
		printf("%d ", a[i]);
	printf("\n");
}

void Swap(int* p1, int* p2)
{
	int tmp = *p1;
	*p1 = *p2;
	*p2 = tmp;
}


//直接插入排序
void InsertSort(int* a, int n)
{
	for (int i = 0; i < n - 1; i++)
	{
		int end = i;
		int tmp = a[end + 1];
		while (end >= 0)
		{
			if (a[end] > tmp)
			{
				a[end + 1] = a[end];
				end--;
			}
			else
				break;
		}
		a[end + 1] = tmp;
	}
}

//希尔排序
void ShellSort(int* a, int n)
{
	int gap = n;
	while (gap > 1)
	{
		gap = gap / 3 + 1;

		for (int i = 0; i < n - gap; i++)
		{
			int end = i;
			int tmp = a[end + gap];
			while (end >= 0)
			{
				if (a[end] > tmp)
				{
					a[end + gap] = a[end];
					end -= gap;
				}
				else
					break;
			}
			a[end + gap] = tmp;
		}
	}
}

//冒泡排序
void BubbleSort(int* a, int n)
{
	for (int i = 0; i < n - 1; i++)
	{
		int change = 0;
		for (int j = 0; j < n - 1 - i; j++)
		{
			if (a[j] > a[j + 1])
			{
				Swap(&a[j], &a[j + 1]);
				change = 1;
			}
		}
		if (change == 0)
			break;
	}
}

//堆排序
//向下调整
void AdjustDown(int* a, int n, int parent)
{
	int child = parent * 2 + 1;

	while (child < n)
	{
		//升序建大堆
		//假设法找到大的那个孩子
		if (child + 1 < n && a[child] < a[child + 1])child++;

		if (a[child] > a[parent])
		{
			Swap(&a[child], &a[parent]);
			parent = child;
			child = child * 2 + 1;
		}
		else
			break;
	}
}

void HeapSort(int* a, int n)
{
	//直接用原数组建堆，升序，向下建大堆
	for (int i = (n - 2) / 2; i >= 0; i--)AdjustDown(a, n, i);

	//首尾交换，排序
	int end = n - 1;
	while (end)
	{
		Swap(&a[0], &a[end]);
		AdjustDown(a, end--, 0);
	}
}

//选择排序
void SelectSort(int* a, int n)
{
	int begin = 0, end = n - 1;

	while (begin < end)
	{
		int mini = begin, maxi = begin;
		for (int i = begin + 1; i <= end; i++)
		{
			if (a[i] < a[mini])mini = i;
			if (a[i] > a[maxi])maxi = i;
		}
		Swap(&a[begin], &a[mini]);
		if (begin == maxi)maxi = mini;
		Swap(&a[end], &a[maxi]);
		begin++; end--;
	}
}


//三数取中
int GetMid(int* a, int left, int right)
{
	int mid = (left + right) / 2;

	if (a[left] < a[mid])
	{
		if (a[mid] < a[right])return mid;
		else if (a[left] > a[right])return left;
		else 
			return right;
	}
	else
	{
		if (a[left] < a[right])return left;
		else if (a[right] > a[mid])return right;
		else
			return mid;
	}
}

//快排
void QuickSort(int* a, int left, int right)
{
	if (left >= right)return;

	int mid = GetMid(a, left, right);
	Swap(&a[left], &a[mid]);

	int begin = left, end = right;
	int key = left;

	while (left < right)
	{
		while (left < right && a[right] >= a[key]) right--;
		while (left < right && a[left] <= a[key]) left++;

		Swap(&a[left], &a[right]);
	}
	Swap(&a[left], &a[key]);
	key = left;

	QuickSort(a, begin, key - 1);
	QuickSort(a, key+1, end);
}


//归并排序
void _MergeSort(int* a, int begin, int end, int* tmp)
{
	if (begin == end)return;

	int mid = (begin + end) / 2;
	_MergeSort(a, begin, mid, tmp);
	_MergeSort(a, mid+1, end, tmp);

	//归并
	int begin1 = begin, end1 = mid;
	int begin2 = mid + 1, end2 = end;
	int i = begin;

	while (begin1 <= end1 && begin2 <= end2)
	{
		if (a[begin1] <= a[begin2])tmp[i++] = a[begin1++];
		else
			tmp[i++] = a[begin2++];
	}

	while (begin1 <= end1)tmp[i++] = a[begin1++];
	while (begin2 <= end2)tmp[i++] = a[begin2++];

	memcpy(a + begin, tmp + begin, sizeof(int) * (end - begin + 1));
}

void MergeSort(int* a, int n)
{
	int* tmp = (int*)malloc(sizeof(int) * n);
	if (tmp == NULL)
	{
		perror("malloc fail");
		return;
	}

	_MergeSort(a, 0, n - 1, tmp);

	free(tmp);
	tmp = NULL;
}

void MergeSortNonR(int* a, int n)
{
	int* tmp = (int*)malloc(sizeof(int) * n);
	if (tmp == NULL)
	{
		perror("malloc fail!");
		return;
	}

	int gap = 1;

	while (gap < n)
	{
		for (int j = 0; j < n; j += 2 * gap)
		{
			//归并
			int begin1 = j, end1 = begin1+gap-1;
			int begin2 = end1 + 1, end2 = begin2 + gap - 1;

			//越界问题处理
			if (begin2 >= n || end1 >= n)break;
			if (end2 >= n)end2 = n - 1;

			int i = j;

			while (begin1 <= end1 && begin2 <= end2)
			{
				if (a[begin1] <= a[begin2])tmp[i++] = a[begin1++];
				else
					tmp[i++] = a[begin2++];
			}

			while (begin1 <= end1)tmp[i++] = a[begin1++];
			while (begin2 <= end2)tmp[i++] = a[begin2++];

			memcpy(a + j, tmp + j, sizeof(int) * (end2 - j + 1));
		}

		gap *= 2;
	}
	free(tmp);
	tmp = NULL;
}


void CountSort(int* a, int n)
{
	int min = a[0], max = a[0];
	for (int i = 0; i < n; i++)
	{
		if (a[i] < min)
			min = a[i];
		if (a[i] > max)
			max = a[i];
	}

	int range = max - min + 1;
	int* count = (int*)malloc(sizeof(int) * range);
	if (!count)
	{
		perror("malloc faail!");
		return;
	}

	memset(count, 0, sizeof(int) * range);

	for (int i = 0; i < n; i++)
	{
		count[a[i] - min]++;
	}

	int j = 0;
	for (int i = 0; i < range; i++)
	{
		while (count[i]--)
		{
			a[j++] = min + i;
		}
	}
}


//int main()
//{
//	int a[] = { 4,6,3,5,7,1,2,9,8 };
//	HeapSort(a, sizeof(a) / sizeof(int));
//	PrintArray(a, sizeof(a) / sizeof(int));
//	return 0;
//}


