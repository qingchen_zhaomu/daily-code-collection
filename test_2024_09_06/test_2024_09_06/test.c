#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
#include<math.h>

typedef struct
{
	double x;
	double y;
}Point;

double p1x = 0;
double p2x = 0;
double p1y = 0;
double p2y = 0;

double f1(Point a, Point b)
{
	double c = p2x - p1x;
	double d = p2y - p1y;

	return sqrt(c * c + d * d);
}

int main()
{
	Point p1, p2;
	scanf("%lf %lf", &(p1.x), &(p1.y));
	scanf("%lf %lf", &(p2.x), &(p2.y));

	double e = f1(p1, p2);

	return 0;
}

