#include<iostream>
using std::cout;
using std::endl;

namespace zk
{
	int n = 3;
	int j = 4;
}
//using namespace zk;
using zk::n;

int main()
{
	cout << n << endl;
	cout << n << endl;
	cout << n << endl;
	cout << n << endl;
	cout << n << endl;
	cout << n << endl;
	cout << zk::j << endl;
	return 0;
}

//namespace Boy
//{
//	namespace zk
//	{
//		void Add()
//		{
//			std::cout << 1 << std::endl;
//		}
//	}
//}
//
//namespace Girl
//{
//	namespace zk
//	{
//		void Add()
//		{
//			std::cout << 2 << std::endl;
//		}
//	}
//}
//
//int a = 2;
//
//int main()
//{
//	int a = 1;
//	std::cout << a << std::endl;
//	std::cout << ::a << std::endl;
//	return 0;
//}