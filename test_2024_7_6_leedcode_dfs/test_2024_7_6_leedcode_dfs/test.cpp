#include<iostream>
using namespace std;
#include<vector>
#include<string>
#include<queue>
#include<functional>



//class Solution {
//public:
//    long prev = LONG_MIN;
//    bool isValidBST(TreeNode* root)
//    {
//        if (!root)return true;
//        bool left = isValidBST(root->left);
//        if (left == false)return false;
//        if (prev < root->val)
//        {
//            prev = root->val;
//        }
//        else return false;
//        bool right = isValidBST(root->right);
//        return left && right;
//    }
//};

//class Solution {
//public:
//    TreeNode* pruneTree(TreeNode* root)
//    {
//        //�ݹ����
//        if (!root)return nullptr;
//        //�ݹ������߼�
//        root->left = pruneTree(root->left);
//        root->right = pruneTree(root->right);
//        if (root->left == nullptr && root->right == nullptr && root->val == 0)root = nullptr;
//        return root;
//    }
//};

//class Solution {
//public:
//    int dfs(TreeNode* root, int sumnum)
//    {
//        int sum = root->val + 10 * sumnum;
//        if (!root->left && !root->right)return sum;
//        int ret = 0;
//        if (root->left)ret += dfs(root->left, sum);
//        if (root->right)ret += dfs(root->right, sum);
//        return ret;
//    }
//
//    int sumNumbers(TreeNode* root)
//    {
//        return dfs(root, 0);
//    }
//};

//class Solution {
//public:
//    bool evaluateTree(TreeNode* root)
//    {
//        if (!root->left && !root->right)return root->val;
//        bool left = evaluateTree(root->left);
//        bool right = evaluateTree(root->right);
//        return root->val == 2 ? left | right : left & right;
//    }
//};

//class Solution {
//public:
//    double myPow(double x, int n)
//    {
//        if (n < 0)
//        {
//            return 1 / pow(x, -(long long)n);
//        }
//        else return pow(x, n);
//    }
//    double pow(double x, long long n)
//    {
//        if (n == 0)return 1;
//        double a = myPow(x, n / 2);
//        return n % 2 == 0 ? a * a : a * a * x;
//    }
//};

//class Solution {
//public:
//    ListNode* swapPairs(ListNode* head)
//    {
//        if (!head || !head->next)return head;
//        ListNode* ret = head->next;
//        ListNode* tmp = ret->next;
//        //����˳��
//        ret->next = head;
//        head->next = swapPairs(tmp);
//        return ret;
//    }
//};

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
//class Solution {
//public:
//    ListNode* reverseList(ListNode* head)
//    {
//        if (head == nullptr || head->next == nullptr)return head;
//        ListNode* ret = reverseList(head->next);
//        head->next->next = head;
//        head->next = nullptr;
//        return ret;
//    }
//};

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
//class Solution {
//public:
//    ListNode* mergeTwoLists(ListNode* l1, ListNode* l2)
//    {
//        if (!l1) return l2;
//        if (!l2) return l1;
//        if (l1->val < l2->val) l1->next = mergeTwoLists(l1->next, l2);
//        else l2->next = mergeTwoLists(l1, l2->next);
//        return l1->val < l2->val ? l1 : l2;
//    }
//};

//class Solution {
//public:
//    void dfs(vector<int>& sou, vector<int>& bro, vector<int>& tar, int num)
//    {
//        if (num == 1)
//        {
//            tar.push_back(sou.back());
//            sou.pop_back();
//            return;
//        }
//        dfs(sou, tar, bro, num - 1);
//        tar.push_back(sou.back());
//        sou.pop_back();
//        dfs(bro, sou, tar, num - 1);
//    }
//
//    void hanota(vector<int>& A, vector<int>& B, vector<int>& C)
//    {
//        dfs(A, B, C, A.size());
//    }
//};

int main()
{

	return 0;
}