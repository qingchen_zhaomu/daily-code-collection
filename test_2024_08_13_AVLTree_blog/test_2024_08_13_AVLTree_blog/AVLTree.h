#pragma once
#include<iostream>
using namespace std;
#include<assert.h>

template<class k, class v>
struct AVLTreeNode
{
	AVLTreeNode<k, v>* _left;
	AVLTreeNode<k, v>* _right;
	AVLTreeNode<k, v>* _parent;

	int _bf;
	pair<k, v> _kv;

	AVLTreeNode(const pair<k, v>& kv)
		:_left(nullptr)
		, _right(nullptr)
		, _parent(nullptr)
		,_bf(0)
		,_kv(kv)
	{}
};


template<class k, class v>
class AVLTree
{
	typedef AVLTreeNode<k, v> Node;
public:
	AVLTree() = default;

	AVLTree(const AVLTree<k, v>& avl)
	{
		_root = copy(avl._root);
	}

	~AVLTree()
	{
		Destroy(_root);
		_root = nullptr;
	}

	bool Insert(const pair<k, v>& kv)
	{
		if (!_root)
		{
			_root = new Node(kv);
			return true;
		}

		Node* parent = nullptr;
		Node* cur = _root;

		while (cur)
		{
			if (cur->_kv.first < kv.first)
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (cur->_kv.first > kv.first)
			{
				parent = cur;
				cur = cur->_left;
			}
			else
			{
				return false;
			}
		}

		cur = new Node(kv);
		if (cur->_kv.first < parent->_kv.first)
			parent->_left = cur;
		else
			parent->_right = cur;
		cur->_parent = parent;

		while (parent)
		{
			if (cur == parent->_left)
				parent->_bf--;
			else
				parent->_bf++;

			if (parent->_bf == 0)
				break;
			else if (parent->_bf == 1 || parent->_bf == -1)
			{
				cur = parent;
				parent = parent->_parent;
			}
			else if (parent->_bf == 2 || parent->_bf == -2)
			{
				if (parent->_bf == 2 && cur->_bf == 1)
				{
					//左单旋
					RotateL(parent);
				}
				else if (parent->_bf == -2 && cur->_bf == -1)
				{
					//右单旋
					RotateR(parent);
				}
				else if (parent->_bf == 2 && cur->_bf == -1)
				{
					//右左双旋
					RotateRL(parent);
				}
				else
				{
					//左右双旋
					RotateLR(parent);
				}
				break;
			}
			else
			{
				assert(false);
			}
		}
		return true;
	}


	//////////////////////////////////////////////////// AVL删除算法

	bool Erase(const k& key)
	{
		if (!_root)return false;

		Node* parent = nullptr;
		Node* cur = _root;

		while (cur)
		{
			if (cur->_kv.first < key)
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (cur->_kv.first > key)
			{
				parent = cur;
				cur = cur->_left;
			}
			else
			{
				// 删除主逻辑
				// 判断三种情况，没孩子，一个孩子，两个孩子
				if (cur->_left == nullptr)
				{
					// 假如删除的是根节点
					if (!parent)
					{
						_root = cur->_right;
						delete cur;
						return true;
					}
					else
					{
						if (parent->_left == cur)
						{
							// 如果待删除节点在parent的左
							// 平衡因子 ++
							parent->_bf++;
							parent->_left = cur->_right;
						}
						else
						{
							// 如果待删除节点在parent的右
							// 平衡因子 --
							parent->_bf--;
							parent->_right = cur->_right;
						}
					}
				}
				else if (cur->_right == nullptr)
				{
					// 假如删除的是根节点
					if (!parent)
					{
						_root = cur->_left;
						delete cur;
						return true;
					}
					else
					{
						if (parent->_left == cur)
						{
							// 如果待删除节点在parent的左
							// 平衡因子 ++
							parent->_bf++;
							parent->_left = cur->_left;
						}
						else
						{
							// 如果待删除节点在parent的右
							// 平衡因子 --
							parent->_bf--;
							parent->_right = cur->_left;
						}
					}
				}
				else
				{
					// 有两个孩子的情况
					// 找右子树的最左节点
					Node* rightMinP = cur;
					Node* rightMin = cur->_right;

					// 循环找最左节点
					while (rightMin->_left)
					{
						rightMinP = rightMin;
						rightMin = rightMin->_left;
					}

					// 交换右子树的最左节点与待删除节点的值
					cur->_kv = rightMin->_kv;

					// 删除右子树的最左节点
					if (rightMin == rightMinP->_left)
					{
						rightMinP->_bf++;
						rightMinP->_left = rightMin->_right;
					}
					else
					{
						rightMinP->_bf--;
						rightMinP->_right = rightMin->_right;
					}

					// 重置parent和cur
					parent = rightMinP;
					cur = rightMin;

				}
				delete cur;
				/////////////////////////////////////////////////////


				bool is_or_not = false;
				while (parent)
				{
					// 更新平衡因子
					if (is_or_not)
					{
						if (parent->_left == cur)
							parent->_bf++;
						else
							parent->_bf--;
					}
					is_or_not = true;

					if (parent->_bf == 1 || parent->_bf == -1)
						return true;
					else if (parent->_bf == 0)
					{
						cur = parent;
						parent = parent->_parent;
					}
					else if(parent->_bf == 2 || parent->_bf == -2)
					{
						//旋转逻辑
						Node* highernode;
						int sign;
						if (parent->_bf > 0)
						{
							sign = 1;
							highernode = parent->_right;
						}
						else
						{
							sign = -1;
							highernode = parent->_left;
						}

						if (highernode->_bf == 0)
						{
							if (highernode == parent->_right)
							{
								RotateL(parent);
								parent->_bf = 1;
								highernode->_bf = -1;
							}
							else
							{
								RotateR(parent);
								parent->_bf = -1;
								highernode->_bf = 1;
							}
						}
						else if (sign == highernode->_bf)
						{
							// 单旋
							if (sign > 0)
								RotateL(parent);
							else
								RotateR(parent);
						}
						else
						{
							// 多旋
							if (sign > 0)
								RotateRL(parent);
							else
								RotateLR(parent);
						}
						cur = parent;
						parent = parent->_parent;
					}
					else
					{
						assert(false);
					}
				}


			}
		}
		return false;
	}





	void InOrder()
	{
		_InOrder(_root);
		cout << endl;
	}

private:
	//左单旋
	void RotateL(Node* parent)
	{
		Node* curR = parent->_right;
		Node* curRL = curR->_left;
		Node* Parentparent = parent->_parent;

		//旋转逻辑
		parent->_right = curRL;
		curR->_left = parent;

		//处理parent
		if (curRL)curRL->_parent = parent;
		parent->_parent = curR;

		if (!Parentparent)
		{
			_root = curR;
			curR->_parent = nullptr;
		}
		else
		{
			if (Parentparent->_left == parent)
				Parentparent->_left = curR;
			else
				Parentparent->_right = curR;

			curR->_parent = Parentparent;
		}
		curR->_bf = parent->_bf = 0;
	}

	//右单旋
	void RotateR(Node* parent)
	{
		Node* curL = parent->_left;
		Node* curLR = curL->_right;
		Node* Parentparent = parent->_parent;

		//旋转逻辑
		parent->_left = curLR;
		curL->_right = parent;

		//处理parent
		if (curLR)curLR->_parent = parent;
		parent->_parent = curL;

		if (!Parentparent)
		{
			_root = curL;
			curL->_parent = nullptr;
		}
		else
		{
			if (Parentparent->_left == parent)
				Parentparent->_left = curL;
			else
				Parentparent->_right = curL;

			curL->_parent = Parentparent;
		}
		curL->_bf = parent->_bf = 0;
	}

	//右左双旋
	void RotateRL(Node* parent)
	{
		Node* curR = parent->_right;
		Node* curRL = curR->_left;
		int bf = curRL->_bf;

		RotateR(curR);
		RotateL(parent);

		if (bf == 0)
		{
			parent->_bf = 0;
			curR->_bf = 0;
			curRL->_bf = 0;
		}
		else if (bf == 1)
		{
			parent->_bf = -1;
			curR->_bf = 0;
			curRL->_bf = 0;
		}
		else if(bf == -1)
		{
			parent->_bf = 0;
			curR->_bf = 1;
			curRL->_bf = 0;
		}
		else
		{
			assert(false);
		}
	}

	//左右双旋
	void RotateLR(Node* parent)
	{
		Node* curL = parent->_left;
		Node* curLR = curL->_right;
		int bf = curLR->_bf;

		RotateL(curL);
		RotateR(parent);

		if (bf == 0)
		{
			parent->_bf = 0;
			curL->_bf = 0;
			curLR->_bf = 0;
		}
		else if (bf == 1)
		{
			parent->_bf = 0;
			curL->_bf = -1;
			curLR->_bf = 0;
		}
		else if (bf == -1)
		{
			parent->_bf = 1;
			curL->_bf = 0;
			curLR->_bf = 0;
		}
		else
		{
			assert(false);
		}
	}


	int _Height(Node* root)
	{
		if (!root)return 0;

		int left = _Height(root->_left);
		int right = _Height(root->_right);

		return max(left, right) + 1;
	}

	void _InOrder(Node* root)
	{
		if (!root)return;

		_InOrder(root->_left);
		cout << root->_kv.first << ":" << root->_kv.second << endl;
		_InOrder(root->_right);
	}

	void Destroy(Node* root)
	{
		if (root == nullptr)return;

		Destroy(root->_left);
		Destroy(root->_right);
		delete root;
	}

	Node* copy(Node* root)
	{
		if (root == nullptr)return nullptr;

		Node* newnode = new Node(root->_kv);

		newnode->_right = copy(root->_right);
		newnode->_left = copy(root->_left);

		return newnode;
	}

private:
	Node* _root;
};


