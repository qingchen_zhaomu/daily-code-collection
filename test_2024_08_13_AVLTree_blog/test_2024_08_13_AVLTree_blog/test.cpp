#include"AVLTree.h"
#include<string>

void testAVLTree1()
{
	int a[] = { 1,2,3,4,5,6 };
	AVLTree<int, int> avl;
	for (auto e : a)
	{
		avl.Insert({ e,e });
	}
	avl.InOrder();
}

void testAVLTree2()
{
	AVLTree<string, string> avl;
	avl.Insert({ "left","���"});
	avl.Insert({ "right","�ұ�"});
	avl.Insert({ "word","����"});
	avl.Insert({ "love","����"});
	avl.Insert({ "hate","����"});
	avl.InOrder();

	avl.Erase("word");
	avl.InOrder();
	avl.Erase("left");
	avl.InOrder();
	avl.Erase("right");
	avl.InOrder();
	avl.Erase("love");
	avl.InOrder();
	avl.Erase("hate");
	avl.InOrder();

}

int main()
{
	//testAVLTree1();
	testAVLTree2();
	return 0;
}