﻿#pragma once
#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
#include<stdlib.h>

//静态顺序表
// #define N 100
//struct SeqList
//{
//	SLDataType arr[N];
//	int size;
//};
//typedef struct SeqList SL;


//动态顺序表
typedef int SLDataType;

typedef struct SeqList
{
	SLDataType* arr; //动态管理
	int capacity;    //顺序表总容量
	int size;        //有效数据个数
}SL;

//初始化和销毁
void SLInit(SL* ps);
void SLDestroy(SL* ps);
void SLPrint(SL* ps);

//扩容
void SLCheckCapacity(SL* ps);

//头部插⼊删除/尾部插⼊删除
void SLPushBack(SL * ps, SLDataType x);
void SLPopBack(SL* ps);
void SLPushFront(SL* ps, SLDataType x);
void SLPopFront(SL* ps);

//指定位置之前插入数据
//删除指定位置数据
void SLInsert(SL* ps, int pos, SLDataType x);
void SLErase(SL* ps, int pos);
int SLFind(SL* ps, SLDataType x);

