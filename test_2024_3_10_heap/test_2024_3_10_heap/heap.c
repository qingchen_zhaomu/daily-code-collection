#include"heap.h"

//初始化与销毁
void HPInit(HP* php)
{
	assert(php);
	php->a = NULL;
	php->capacity = 0;
	php->size = 0;
}

void HPDestroy(HP* php)
{
	assert(php);
	free(php->a);
	php->a = NULL;
	php->capacity = 0; 
	php->size = 0;
}

void Swap(HPDatatype* p1, HPDatatype* p2)
{
	HPDatatype tmp = *p1;
	*p1 = *p2;
	*p2 = tmp;
}

void AdjustUp(HPDatatype* a,int child)
{
	int parent = (child - 1) / 2;
	while (child > 0)
	{
		if (a[child] < a[parent])
		{
			Swap(&a[child], &a[parent]);
			child = parent;
			parent = (parent - 1) / 2;
		}
		else
		{
			break;
		}
	}
}

void HPPush(HP* php, HPDatatype x)
{
	//开辟动态空间，老生常谈
	assert(php);

	if (php->capacity == php->size)
	{
		int newcapacity = php->capacity == 0 ? 4 : 2 * php->capacity;
		HPDatatype* tmp = (HPDatatype*)realloc(php->a, sizeof(HPDatatype)*newcapacity);
		if (tmp==NULL)
		{
			perror("realloc fail!");
			return;
		}
		php->a = tmp;
		php->capacity = newcapacity;
	}
	//开辟成功
	php->a[php->size++] = x;
	//php->size++;

	//向上调整
	AdjustUp(php->a,php->size-1);
}

void AdjustDown(HPDatatype* a, int n, int parent)
{
	int child = parent * 2 + 1;
	while (child < n)
	{
		//假设法找到小的孩子
		if (child + 1 < n && a[child + 1] < a[child])
			child++;

		if (a[child] < a[parent])
		{
			Swap(&a[child], &a[parent]);
			parent = child;
			child = child * 2 + 1;
		}
		else
		{
			break;
		}
	}
}

void HPPop(HP* php)
{
	assert(php);
	assert(php->size > 0);
	Swap(&php->a[0], &php->a[php->size - 1]);
	php->size--;

	AdjustDown(php->a, php->size, 0);
}

HPDatatype HPTop(HP* php)
{
	assert(php);
	return php->a[0];
}

bool HPEmpty(HP* php)
{
	assert(php);
	return php->size == 0;
}

void HPInitArray(HP* php, HPDatatype* a, int n)
{
	assert(php);
	HPDatatype* tmp = (HPDatatype*)malloc(sizeof(HPDatatype) * n);
	if (!tmp)
	{
		perror("malloc fail");
		return;
	}
	php->a = tmp;
	memcpy(php->a, a, sizeof(HPDatatype) * n);
	php->capacity = n; php->size = n;

	for (int i = (php->size - 2) / 2; i >= 0; i--)
	{
		AdjustDown(php->a, php->size, i);
	}
}
