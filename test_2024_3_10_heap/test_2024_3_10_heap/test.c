#include"heap.h"

void heapsort(int* a,int n)
{
	//建小堆，从大到小排序
	for (int i = (n - 2) / 2; i >= 0; i--)
	{
		AdjustDown(a, n, i);
	}
	int end = n - 1;

	while (end)
	{
		Swap(&a[0], &a[end]);
		AdjustDown(a, end--, 0);
	}
}

void CreateNData()
{
	int n = 10;
	srand((unsigned int)time(0));
	const char* file = "data.txt";
	FILE* fin = fopen(file, "w");
	if (fin == NULL)
	{
		perror("fopen fail");
		return;
	}

	for (int i = 0; i < n; i++)
	{
		int x = rand() % 1000000;
		fprintf(fin, "%d\n", x);
	}
	fclose(fin);
}

void topk()
{
	printf("请输入要找top前几个数:");
	int k = 0;
	scanf("%d", &k);

	const char* file = "data.txt";
	FILE* fout = fopen(file, "r");
	if (fout == NULL)
	{
		perror("fopen fail");
		return;
	}

	int* minheap = (int*)malloc(sizeof(int) * k);
	if (minheap == NULL)
	{
		perror("malloc fail");
		return;
	}
	//读数据进堆
	for (int i = 0; i < k; i++)
	{
		fscanf(fout, "%d", &minheap[i]);
	}
	//建堆
	for (int i = (k - 2) / 2; i >= 0; i--)
	{
		AdjustDown(minheap, k, i);
	}
	int x = 0;
	while (fscanf(fout, "%d", &x) != EOF)
	{
		if (x > minheap[0])
		{
			minheap[0] = x;
			AdjustDown(minheap, k, 0);
		}
	}
	for (int i = 0; i < k; i++)
	{
		printf("%d ", minheap[i]);
	}
	fclose(fout);
}

int main()
{
	//CreateNData();
	topk();
	return 0;
}

//int main()
//{
//	int a[] = { 6,8,5,7,9,3,2,4,1,0 };
//	heapsort(a,sizeof(a)/sizeof(int));
//	return 0;
//}

//int main()
//{
//	int arr[] = { 60, 35, 22, 56, 12, 77, 99 };
//	HP php;
//	HPInit(&php);
//
//	for (int i = 0; i < sizeof(arr) / sizeof(int); i++)
//	{
//		HPPush(&php, arr[i]);
//	}
//
//	/*for (int i = 0; i < php.size - 1; i++)
//	{
//		printf("%d ", php.a[i]);
//	}*/
//
//	while (!HPEmpty(&php))
//	{
//		printf("%d\n", HPTop(&php));
//		HPPop(&php);
//	}
//
//	HPDestroy(&php);
//	return 0;
//}

