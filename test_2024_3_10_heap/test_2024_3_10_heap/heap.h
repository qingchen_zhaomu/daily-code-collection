#define _CRT_SECURE_NO_WARNINGS 1
#pragma once

#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<string.h>
#include<assert.h>
#include<time.h>

typedef int HPDatatype;

typedef struct Heap
{
	HPDatatype* a;
	int size;
	int capacity;
}HP;

//初始化与销毁
void HPInit(HP* php);
void HPDestroy(HP* php);

void HPInitArray(HP* php, HPDatatype* a, int n);

//插入、删除
void HPPush(HP* php, HPDatatype x);
void HPPop(HP* php);

HPDatatype HPTop(HP* php);
bool HPEmpty(HP* php);

void AdjustDown(HPDatatype* a, int n, int parent);
void Swap(HPDatatype* p1, HPDatatype* p2);
void AdjustUp(HPDatatype* a, int child);
