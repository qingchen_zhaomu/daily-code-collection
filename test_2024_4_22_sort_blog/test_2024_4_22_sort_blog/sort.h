#define _CRT_SECURE_NO_WARNINGS 1

#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<string.h>
#include<time.h>

//��ӡ
void PrintArray(int* a, int n);
void Swap(int* a, int* b);

//��������
void InsertSort(int* a, int n);

//ϣ������
void ShellSort(int* a, int n);

//ð������
void BubbleSort(int* a, int n);

//������(���򽨴�ѣ�����С��)
void AdjustDown(int* a, int n, int parent);
void HeapSort(int* a, int n);

//ѡ������
void SelectSort(int* a, int n);

//����
void QuickSort(int* a, int left, int right);
void QuickSort1(int* a, int left, int right);

//�鲢����
void MergeSort(int* a, int n);
void MergeSortNonR(int* a, int n);
