#include<iostream>
using namespace std;
#include<string>
#include<vector>

//class person
//{
//public:
//	virtual void buyticket() { cout << "��Ʊ-ȫƱ" << endl; }
//	int num;
//};
//
//class stu : public person
//{
//public:
//	virtual void buyticket() { cout << "��Ʊ-���" << endl; }
//	int tel;
//};
//
//void func(person* p)
//{
//	p->buyticket();
//}
//
//int main()
//{
//	person* p1 = new stu;
//	stu st;
//	func(p1);
//	func(&st);
//	return 0;
//}

class animal
{
public:
	virtual void sound()const = 0;
};

class cat : public animal
{
public:
	virtual void sound()const
	{
		cout << "����" << endl;
	}
};

class dog : public animal
{
public:
	virtual void sound()const
	{
		cout << "����" << endl;
	}
};

void animalsound(const animal& anm)
{
	anm.sound();
}

int main()
{
	animalsound(cat());
	animalsound(dog());
	return 0;
}