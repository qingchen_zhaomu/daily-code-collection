#include<iostream>
using namespace std;

//class A {};
//class B : public A {};
//class Person 
//{
//public:
//	virtual A* f() { return new A; }
//};
//class Student : public Person 
//{
//public:
//	virtual B* f() { return new B; }
//};



//////////////////////////

//class Person
//{
//public:
//	virtual void buyticket()
//	{
//		cout << "普通人——全票" << endl;
//	}
//};
//
//class Student : public Person
//{
//public:
//	virtual void buyticket()
//	{
//		cout << "学生——打折" << endl;
//	}
//};
//
//class soldier : public Person
//{
//public:
//	virtual void buyticket()
//	{
//		cout << "军人——优先" << endl;
//	}
//};
//
//void func(Person p)
//{
//	p.buyticket();
//}
//
//int main()
//{
//	Person per;
//	Student stu;
//	soldier sol;
//	func(per);
//	func(stu);
//	func(sol);
//	return 0;
//}



//////////////////////////



//////////////////////////
//   析构函数的重写

//class A
//{
//public:
//	A()
//	{
//		cout << "A()" << endl;
//	}
//	virtual ~A()
//	{
//		cout << "~A()" << endl;
//	}
//	int _a;
//};
//
//class B : public A
//{
//public:
//	B()
//	{
//		cout << "B()" << endl;
//	}
//	~B()
//	{
//		cout << "~B()" << endl;
//	}
//	int _b;
//};
//
//
//int main()
//{
//	A* ptr = new B;
//	delete ptr;
//	return 0;
//}


//////////////////////////


//class A 
//{
//public:
//	virtual void Print()
//	{
//		cout << "支持博主" << endl;
//	}
//};
//
//class B : public A 
//{
//public:
//	virtual void Print()
//	{
//		cout << "一键三连" << endl;
//	}
//};
//
//void func(A& ref)
//{
//	ref.Print();
//}
//
//int main()
//{
//	A a;
//	B b;
//	func(a);
//	func(b);
//	return 0;
//}




//class car
//{
//public:
//	virtual void comment() = 0;
//};
//
//class benz : public car
//{
//public:
//	virtual void comment()
//	{
//		cout << "奔驰" << endl;
//	}
//};
//
//class ferrari : public car
//{
//public:
//	virtual void comment()
//	{
//		cout << "法拉利" << endl;
//	}
//};
//
//void func(car& c)
//{
//	c.comment();
//}
//
//int main()
//{
//	benz be;
//	ferrari fe;
//	func(be);
//	func(fe);
//	return 0;
//}



//class A
//{
//public:
//	virtual void func1()
//	{
//		cout << "A::func1()" << endl;
//	}
//
//	virtual void func2()
//	{
//		cout << "A::func2()" << endl;
//	}
//
//	void func3()
//	{
//		cout << "A::func3()" << endl;
//	}
//};
//
//class B : public A
//{
//public:
//	virtual void func1()
//	{
//		cout << "B::func1()" << endl;
//	}
//};
//
//void show(A& _a)
//{
//	_a.func1();
//	_a.func2();
//	_a.func3();
//}
//
//int main()
//{
//	A a;
//	B b;
//	show(a);
//	cout << endl << endl;
//	show(b);
//	return 0;
//}


int main()
{
	int a = 0, b = 1;
	char x = 'x', y = 'y';
	swap(a, b);
	swap(x, y);
	return 0;
}


