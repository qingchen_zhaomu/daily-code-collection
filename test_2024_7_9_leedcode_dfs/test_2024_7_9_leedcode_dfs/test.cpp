#include<iostream>
using namespace std;
#include<vector>
#include<string>
#include<queue>
#include<functional>



//class Solution {
//public:
//    bool row[10][10];
//    bool col[10][10];
//    bool tmp[3][3][10];
//    void solveSudoku(vector<vector<char>>& nums)
//    {
//        for (int i = 0; i < 9; i++)
//            for (int j = 0; j < 9; j++)
//            {
//                if (nums[i][j] != '.')
//                {
//                    int a = nums[i][j] - '0';
//                    row[i][a] = col[j][a] = tmp[i / 3][j / 3][a] = true;
//                }
//            }
//        dfs(nums);
//    }
//    bool dfs(vector<vector<char>>& nums)
//    {
//        for (int i = 0; i < 9; i++)
//            for (int j = 0; j < 9; j++)
//                if (nums[i][j] == '.')
//                {
//                    for (int a = 1; a <= 9; a++)
//                    {
//                        if (!row[i][a] && !col[j][a] && !tmp[i / 3][j / 3][a])
//                        {
//                            nums[i][j] = a + '0';
//                            row[i][a] = col[j][a] = tmp[i / 3][j / 3][a] = true;
//                            bool check = dfs(nums);
//                            if (check)return true;
//                            else
//                            {
//                                nums[i][j] = '.';
//                                row[i][a] = col[j][a] = tmp[i / 3][j / 3][a] = false;
//                            }
//                        }
//                    }
//                    return false;
//                }
//        return true;;
//    }
//};

//class Solution {
//public:
//    bool row[10][10];
//    bool col[10][10];
//    bool tmp[3][3][10];
//    bool isValidSudoku(vector<vector<char>>& nums)
//    {
//        for (int i = 0; i < 9; i++)
//            for (int j = 0; j < 9; j++)
//            {
//                if (nums[i][j] != '.')
//                {
//                    int a = nums[i][j] - '0';
//                    if (row[i][a] || col[j][a] || tmp[i / 3][j / 3][a])
//                        return false;
//                    row[i][a] = col[j][a] = tmp[i / 3][j / 3][a] = true;
//                }
//            }
//        return true;
//    }
//};

//class Solution {
//public:
//    vector<vector<string>> ret;
//    int n;
//    vector<vector<string>> solveNQueens(int _n)
//    {
//        n = _n;
//        string tmp;
//        for (int i = 0; i < n; i++)tmp += ".";
//        vector<string> cb(n, tmp);
//        dfs(cb, 0);
//        return ret;
//    }
//    void dfs(vector<string> cb, int pos)
//    {
//        if (pos == n)
//        {
//            ret.push_back(cb);
//            return;
//        }
//        for (int i = 0; i < n; i++)
//        {
//            bool check = true;
//            //向上判断是否有皇后
//            for (int x = pos; x >= 0; x--)
//                if (cb[x][i] == 'Q')check = false;
//            for (int x = pos - 1, y = i - 1; x >= 0 && y < n && y >= 0; x--, y--)
//                if (cb[x][y] == 'Q')check = false;
//            for (int x = pos - 1, y = i + 1; x >= 0 && y < n && y >= 0; x--, y++)
//                if (cb[x][y] == 'Q')check = false;
//            if (check == true)
//            {
//                cb[pos][i] = 'Q';
//                dfs(cb, pos + 1);
//            }
//            cb[pos][i] = '.';
//        }
//    }
//};

//class Solution {
//public:
//    vector<vector<string>> ret;
//    bool row[10], d1[20], d2[20];
//    vector<string> cb;
//    int n;
//    vector<vector<string>> solveNQueens(int _n)
//    {
//        n = _n;
//        cb.resize(n);
//        for (int i = 0; i < n; i++)cb[i].append(n, '.');
//        dfs(0);
//        return ret;
//    }
//    void dfs(int pos)
//    {
//        if (pos == n)
//        {
//            ret.push_back(cb);
//            return;
//        }
//        for (int i = 0; i < n; i++)
//        {
//            if (!row[i] && !d1[pos + i] && !d2[pos - i + n])
//            {
//                cb[pos][i] = 'Q';
//                row[i] = d1[pos + i] = d2[pos - i + n] = true;
//                dfs(pos + 1);
//                cb[pos][i] = '.';
//                row[i] = d1[pos + i] = d2[pos - i + n] = false;
//            }
//        }
//    }
//};

//class Solution {
//public:
//    int ret;
//    bool check[17];
//    int n;
//    int countArrangement(int _n)
//    {
//        n = _n;
//        dfs(1);
//        return ret;
//    }
//    void dfs(int pos)
//    {
//        if (pos - 1 == n)
//        {
//            ret++;
//            return;
//        }
//        for (int i = 1; i <= n; i++)
//        {
//            if (check[i] == false && (i % pos == 0 || pos % i == 0))
//            {
//                check[i] = true;
//                dfs(pos + 1);
//                check[i] = false;
//            }
//        }
//    }
//};

//class Solution {
//public:
//    string path;
//    vector<string> ret;
//    vector<string> letterCasePermutation(string s)
//    {
//        dfs(s, 0);
//        return ret;
//    }
//    void dfs(string& s, int pos)
//    {
//        if (pos == s.size())
//        {
//            ret.push_back(path);
//            return;
//        }
//        char ch = s[pos];
//        //不变
//        path.push_back(ch);
//        dfs(s, pos + 1);
//        path.pop_back();
//        //变
//        if (ch < '0' || ch > '9')
//        {
//            ch = change(ch);
//            path.push_back(ch);
//            dfs(s, pos + 1);
//            path.pop_back();
//        }
//    }
//    char change(char ch)
//    {
//        if (ch >= 'a' && ch <= 'z')
//            ch -= 32;
//        else ch += 32;
//        return ch;
//    }
//};

int main()
{
	/*string a = "..........";
	a[3] = 'Q';
	cout << a;*/
	int n = 4;
	string tmp;
	for (int i = 0; i < n; i++)tmp += ".";
	vector<string> cb(n, tmp);
	for (auto e : cb)cout << e << endl;
	return 0;
}
