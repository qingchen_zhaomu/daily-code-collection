#include<iostream>
using namespace std;
#include<vector>
#include<string>
#include<queue>
#include<functional>



//class Solution {
//public:
//    vector<int> path;
//    vector<vector<int>> ret;
//    int tar;
//    vector<vector<int>> combinationSum(vector<int>& nums, int target)
//    {
//        tar = target;
//        dfs(nums, 0, 0);
//        return ret;
//    }
//    void dfs(vector<int>& nums, int sum, int pos)
//    {
//        if (sum >= tar)
//        {
//            if (sum == tar)ret.push_back(path);
//            return;
//        }
//        if (pos == nums.size())return;
//        for (int k = 0; k * nums[pos] <= tar; k++)
//        {
//            if (k) path.push_back(nums[pos]);
//            dfs(nums, sum + k * nums[pos], pos + 1);
//        }
//        for (int k = 1; k * nums[pos] <= tar; k++)
//            path.pop_back();
//    }
//};

//class Solution {
//public:
//    vector<int> path;
//    vector<vector<int>> ret;
//    int tar;
//    vector<vector<int>> combinationSum(vector<int>& nums, int target)
//    {
//        tar = target;
//        dfs(nums, 0, 0);
//        return ret;
//    }
//    void dfs(vector<int>& nums, int sum, int k)
//    {
//        if (sum >= tar)
//        {
//            if (sum == tar)ret.push_back(path);
//            return;
//        }
//        for (int i = k; i < nums.size(); i++)
//        {
//            path.push_back(nums[i]);
//            dfs(nums, sum + nums[i], i);
//            path.pop_back();
//        }
//    }
//};

//class Solution {
//public:
//    int ret = 0, tar;
//    int findTargetSumWays(vector<int>& nums, int target)
//    {
//        tar = target;
//        dfs(nums, 0, 0);
//        return ret;
//    }
//    void dfs(vector<int>& nums, int sum, int pos)
//    {
//        if (pos == nums.size())
//        {
//            if (sum == tar)
//                ret++;
//            return;
//        }
//        dfs(nums, sum + nums[pos], pos + 1);
//        dfs(nums, sum - nums[pos], pos + 1);
//    }
//};

//class Solution {
//public:
//    vector<vector<int>> ret;
//    vector<int> path;
//    int k, n;
//    vector<vector<int>> combine(int _n, int _k)
//    {
//        k = _k, n = _n;
//        dfs(0);
//        return ret;
//    }
//    void dfs(int pos)
//    {
//        if (path.size() == k)
//        {
//            ret.push_back(path);
//            return;
//        }
//        for (int i = pos; i < n; i++)
//        {
//            path.push_back(i + 1);
//            dfs(i + 1);
//            path.pop_back();
//        }
//    }
//};

//class Solution {
//public:
//    vector<string> ret;
//    int check[2];
//    string path;
//    vector<string> generateParenthesis(int n)
//    {
//        dfs(n, 0);
//        return ret;
//    }
//    void dfs(int n, int k)
//    {
//        if (k == 2 * n)
//        {
//            ret.push_back(path);
//            return;
//        }
//        if (check[0] != n)
//        {
//            //左括号匹配
//            check[0]++;
//            path += "(";
//            dfs(n, k + 1);
//            path.pop_back();
//            check[0]--;
//        }
//        if (check[0] > check[1])
//        {
//            check[1]++;
//            path += ")";
//            dfs(n, k + 1);
//            path.pop_back();
//            check[1]--;
//        }
//    }
//};

//class Solution {
//public:
//    //字符串数组
//    string tmp[10] = { "", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz" };
//    string path;
//    vector<string> ret;
//    vector<string> letterCombinations(string nums)
//    {
//        if (nums.size() == 0)return ret;
//        dfs(nums, 0);
//        return ret;
//    }
//    void dfs(string nums, int k)
//    {
//        if (k == nums.size())
//        {
//            ret.push_back(path);
//            return;
//        }
//        for (auto e : tmp[nums[k] - '0'])
//        {
//            path += e;
//            dfs(nums, k + 1);
//            path.pop_back();
//        }
//    }
//};

//string tmp[10] = { "", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz" };
//string tmp[10];
//string tmp[10] = ["", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"];

//全排列 关注合法

//class Solution {
//public:
//    bool check[9];
//    vector<vector<int>> ret;
//    vector<int> path;
//    vector<vector<int>> permuteUnique(vector<int>& nums)
//    {
//        sort(nums.begin(), nums.end());
//        dfs(nums, 0);
//        return ret;
//    }
//    void dfs(vector<int>& nums, int k)
//    {
//        if (k == nums.size())
//        {
//            ret.push_back(path);
//            return;
//        }
//        for (int i = 0; i < nums.size(); i++)
//        {
//            if (check[i] == false && (i == 0 || nums[i] != nums[i - 1] || check[i - 1] != false))
//            {
//                path.push_back(nums[i]);
//                check[i] = true;
//                dfs(nums, k + 1);
//                path.pop_back();
//                check[i] = false;
//            }
//        }
//    }
//};

//全排列 关注不合法

//class Solution {
//public:
//    bool check[9];
//    vector<vector<int>> ret;
//    vector<int> path;
//    vector<vector<int>> permuteUnique(vector<int>& nums)
//    {
//        sort(nums.begin(), nums.end());
//        dfs(nums, 0);
//        return ret;
//    }
//    void dfs(vector<int>& nums, int k)
//    {
//        if (k == nums.size())
//        {
//            ret.push_back(path);
//            return;
//        }
//        for (int i = 0; i < nums.size(); i++)
//        {
//            if (check[i] != false || (i != 0 && nums[i] == nums[i - 1] && check[i - 1] == false))
//            {
//                continue;
//            }
//            else
//            {
//                path.push_back(nums[i]);
//                check[i] = true;
//                dfs(nums, k + 1);
//                path.pop_back();
//                check[i] = false;
//            }
//        }
//    }
//};

int main()
{
	//tmp = ["", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"];

	return 0;
}