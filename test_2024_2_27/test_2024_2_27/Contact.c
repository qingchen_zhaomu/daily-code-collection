#include"Seqlist.h"
#include"Contact.h"

//通讯录的初始化和销
void ContactInit(Contact* pcon)
{
	SLInit(pcon);
}

void ContactDesTroy(Contact* pcon)
{
	SLDestroy(pcon);
}

//增加、删除、修改、查找、查看通讯录
// 
//增加
void ContactAdd(Contact* pcon)
{
	SLDataType info;

	printf("请输入联系人姓名:");
	scanf("%s", info.name);
	printf("请输入联系人年龄:");
	scanf("%d", &info.age);
	printf("请输入联系人性别:");
	scanf("%s", info.gender);
	printf("请输入联系人电话:");
	scanf("%s", info.tel);
	printf("请输入联系人住址:");
	scanf("%s", info.addr);

	//保存数据到通讯录(顺序表)
	SLPushBack(pcon, info);
}


int FindByName(Contact* pcon, char name[])
{
	for (int i = 0; i < pcon->size; i++)
	{
		if (strcmp(pcon->arr[i].name, name) == 0)
		{
			return i;
		}
	}
	return -1;
}

//删除
void ContactDel(Contact* pcon)
{
	//删除之前需要找有没有这么个人
	//找到了，可以删除
	//找不到，不能执行删除
	printf("请输入要删除的人的名字:");
	char name[NAME_MAX];
	scanf("%s", name);

	int findIndex = FindByName(pcon, name);

	//找不到该联系人
	if (findIndex < 0)
	{
		printf("找不到该联系人！\n");
		return;
	}
	//有该联系人
	SLErase(pcon, findIndex);
	printf("删除成功！\n");
}

//修改
void ContactModify(Contact* pcon)
{
	char name[NAME_MAX];
	printf("请输入要查找的联系人姓名:");
	scanf("%s", name);
	int findIndex = FindByName(pcon, name);

	//找不到要修改的人
	if (findIndex < 0)
	{
		printf("要修改的联系人不存在！\n");
		return;
	}
	//要修改的联系人存在
	printf("请输入联系人姓名\n:");
	scanf("%s", pcon->arr[findIndex].name);
	printf("请输入联系人年龄\n:");
	scanf("%d", &pcon->arr[findIndex].age);
	printf("请输入联系人性别\n:");
	scanf("%s", pcon->arr[findIndex].gender);
	printf("请输入联系人电话\n:");
	scanf("%s", pcon->arr[findIndex].tel);
	printf("请输入联系人住址\n:");
	scanf("%s", pcon->arr[findIndex].addr);

	printf("修改成功！\n");
}

//查看通讯录
void ContactShow(Contact* pcon)
{
	printf("%s %s %s %s %s\n", "姓名", "性别", "年龄", "电话", "住址");
	for (int i = 0; i < pcon->size; i++)
	{
		printf("%s %s %d %s %s\n",
			pcon->arr[i].name,
			pcon->arr[i].gender,
			pcon->arr[i].age,
			pcon->arr[i].tel,
			pcon->arr[i].addr);
	}
}

//查找联系人
void ContactFind(Contact* pcon)
{
	char name[NAME_MAX];
	printf("请输入要查找的联系人姓名:");
	scanf("%s", name);
	int findIndex = FindByName(pcon, name);

	//找不到要找的人
	if (findIndex < 0)
	{
		printf("要修改的联系人不存在！\n");
		return;
	}
	//要找的联系人存在
	printf("%s %s %s %s %s\n", "姓名", "性别", "年龄", "电话", "住址");
	printf("%s %s %d %s %s\n",
		pcon->arr[findIndex].name,
		pcon->arr[findIndex].gender,
		pcon->arr[findIndex].age,
		pcon->arr[findIndex].tel,
		pcon->arr[findIndex].addr);
}
