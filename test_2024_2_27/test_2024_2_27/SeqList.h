﻿#pragma once
#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<assert.h>
#include"Contact.h"

//动态顺序表
typedef Info SLDataType;

typedef struct SeqList
{
	SLDataType* arr; //动态管理
	int capacity;    //顺序表总容量
	int size;        //有效数据个数
}SL;

//初始化和销毁
void SLInit(SL* ps);
void SLDestroy(SL* ps);

//扩容
void SLCheckCapacity(SL* ps);

//尾部插⼊删除
void SLPushBack(SL* ps, SLDataType x);

//删除指定位置数据
void SLErase(SL* ps, int pos);


