#pragma once
#include<iostream>
using namespace std;
#include<vector>
#include<string>
#include<assert.h>

template<class k>
struct HashFunc
{
	size_t operator()(const k& key)
	{
		return (size_t)key;
	}
};

template<>
struct HashFunc<string>
{
	size_t operator()(const string& key)
	{
		size_t hashi = 0;
		for (const auto& e : key)
		{
			hashi *= 31;
			hashi += e;
		}
		return hashi;
	}
};



template<class k, class v>
struct hashNode
{
	hashNode<k, v>* _next;
	pair<k, v> _data;

	hashNode(const pair<k, v>& data)
		:_data(data)
		, _next(nullptr)
	{}
};

// 前置声明，因为后续的operator++需要用到hashtables
// 但是我们的hashtables也要用到迭代器
// 这就造成了一个先有鸡还是先有蛋的问题
// 所以我们在这里的解决方法是前置声明
template<class k, class v, class Hash>
class HashTable;

// 迭代器
template<class k, class v, class Hash, class Ptr, class Ref>
struct HTIterator
{
	typedef hashNode<k, v> Node;
	typedef HTIterator<k, v, Hash, Ptr, Ref> Self;

	const HashTable<k, v, Hash>* _ht;
	Node* _node;

	HTIterator(Node* node, const HashTable<k, v, Hash>* ht)
		:_node(node)
		, _ht(ht)
	{}

	Ref operator*()
	{
		return _node->_data;
	}

	Ptr operator->()
	{
		return &_node->_data;
	}

	bool operator!=(const Self& s)
	{
		return _node != s._node;
	}

	bool operator==(const Self& s)
	{
		return _node == s._node;
	}

	Self& operator++()
	{
		if (_node->_next)
		{
			_node = _node->_next;
		}
		else
		{
			Hash hs;

			size_t hashi = hs(_node->_data.first) % _ht->_tables.size();
			++hashi;
			while (hashi < _ht->_tables.size())
			{
				if (_ht->_tables[hashi]) { break; }
				++hashi;
			}
			if (_ht->_tables.size() == hashi)
				_node = nullptr;
			else
				_node = _ht->_tables[hashi];
		}
		return *this;
	}

};




template<class k, class v, class Hash = HashFunc<k>>
class HashTable
{
	typedef hashNode<k, v> Node;

	template<class k, class v, class Hash, class Ptr, class Ref>
	friend struct HTIterator;
public:

	typedef HTIterator<k, v, Hash, pair<k, v>*, pair<k, v>&> iterator;
	typedef HTIterator<k, v, Hash, const pair<k, v>*, const pair<k, v>&> const_iterator;

	iterator begin()
	{
		if (_n == 0)return end();

		for (size_t i = 0; i < _tables.size(); i++)
		{
			if (_tables[i])return iterator(_tables[i], this);
		}
		return end();
	}

	iterator end()
	{
		return iterator(nullptr, this);
	}

	const_iterator begin()const
	{
		if (_n == 0)return end();

		for (size_t i = 0; i < _tables.size(); i++)
		{
			if (_tables[i])return const_iterator(_tables[i], this);
		}
	}

	const_iterator end()const
	{
		return const_iterator(nullptr, this);
	}


	HashTable()
	{
		_tables.resize(50, nullptr);
	}

	~HashTable()
	{
		for (size_t i = 0; i < _tables.size(); i++)
		{
			Node* cur = _tables[i];
			while (cur)
			{
				Node* next = cur->_next;
				delete cur;
				cur = next;
			}
			_tables[i] = nullptr;
		}
	}

	pair<iterator, bool> Insert(const pair<k, v>& data)
	{
		Hash hs;
		size_t hashi = hs(data.first) % _tables.size();

		// 用find找这个节点有没有出现过
		iterator it = Find(data.first);
		if (it != end())return { it, false };

		// 判断扩容
		if (_n == _tables.size())
		{
			// 进入判断逻辑
			vector<Node*> newtab(_tables.size() * 2, nullptr);
			// 拷贝数据
			for (size_t i = 0; i < _tables.size(); i++)
			{
				Node* cur = _tables[i];
				while (cur)
				{
					Node* next = cur->_next;
					size_t hasii = hs(cur->_data.first) % newtab.size();

					// 头插
					cur->_next = newtab[hasii];
					newtab[hasii] = cur;

					cur = next;
				}
				_tables[i] = nullptr;
			}
			_tables.swap(newtab);
		}

		// 插入逻辑，头插
		Node* newnode = new Node(data);
		newnode->_next = _tables[hashi];
		_tables[hashi] = newnode;
		++_n;

		return { iterator(newnode, this), true };;
	}

	iterator Find(const k& key)
	{
		Hash hs;
		size_t hashi = hs(key) % _tables.size();
		Node* cur = _tables[hashi];

		while (cur)
		{
			if (cur->_data.first == key)
			{
				return iterator(cur, this);
			}

			cur = cur->_next;
		}
		return end();
	}

	bool Erase(const k& key)
	{
		Hash hs;

		size_t hashi = hs(key) % _tables.size();
		Node* cur = _tables[hashi];
		Node* prev = nullptr;

		while (cur)
		{
			if (cur->_data.first == key)
			{
				if (!prev)
				{
					_tables[hashi] = cur->_next;
				}
				else
				{
					prev->_next = cur->_next;
				}
				delete cur;
				--_n;
				return true;
			}
			prev = cur;
			cur = cur->_next;
		}
		return false;
	}



private:
	vector<Node*> _tables;
	size_t _n = 0;
};
