#include"hash.h"

void testhashfunc1()
{
	HashTable<int, int> ht;
	ht.Insert({ 1,1 });
	ht.Insert({ 2,2 });
	ht.Insert({ 3,3 });
	ht.Insert({ 4,4 });

	for (auto e : ht)
		cout << e.first << ":" << e.second << endl;

	ht.Erase(1);
	ht.Erase(2);
	ht.Erase(3);
	ht.Erase(4);

	auto it = ht.Find(1);
}


int main()
{
	testhashfunc1();
	return 0;
}
