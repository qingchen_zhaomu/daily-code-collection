#include<iostream>
using namespace std;


//class Person
//{
//public:
//    Person(const char* name = "peter")
//        : _name(name)
//    {
//        cout << "Person()" << endl;
//    }
//
//    Person(const Person& p)
//        : _name(p._name)
//    {
//        cout << "Person(const Person& p)" << endl;
//    }
//
//    Person& operator=(const Person& p)
//    {
//        cout << "Person operator=(const Person& p)" << endl;
//        if (this != &p)
//            _name = p._name;
//
//        return *this;
//    }
//
//    ~Person()
//    {
//        cout << "~Person()" << endl;
//    }
//protected:
//    string _name; // 姓名
//};
//
//class Student : public Person
//{
//public:
//    //   构造
//    Student(const char* name = "Peter", int id = 0)
//        :Person(name)
//        ,_id(id)
//    {}
//
//    //   拷贝构造
//    Student(const Student& s)
//        :Person(s)
//        ,_id(s._id)
//    {}
//
//    //   赋值重载
//    Student& operator=(Student& s)
//    {
//        //  确保不是我赋值给我自己
//        if (this != &s)
//        {
//            Person::operator=(s);
//            _id = s._id;
//        }
//        return *this;
//    }
//private:
//    int _id;
//};
//
//
//int main()
//{
//    Student s1("Peter", 331);
//    Student s2;
//    s2 = s1;
//
//    return 0;
//}


//class A
//{
//public:
//	void fun()
//	{
//		cout << "func()" << endl;
//	}
//};
//class B : public A
//{
//public:
//	void fun(int i)
//	{
//		A::fun();
//		cout << "func(int i)->" << i << endl;
//	}
//};




//class person
//{
//public:
//	void print()
//	{
//		cout << _num << endl;
//	}
//
//	int _num = 0;
//};
//
//class student : public person
//{
//public:
//	void print(int i)
//	{
//		cout << _id << endl;
//	}
//
//	int _id = 3;
//};
//
//int main()
//{
//	student s;
//	
//	s.print(1);// √
//	s.print();//  ×
//
//	return 0;
//}





//class Person
//{
//public:
//	void Print()
//	{
//		cout << _num << endl;
//	}
//
//	int _num = 0;
//};
//
//class Student : public Person
//{
//private:
//	int _id = 3;
//};
//
//int main()
//{
//	Student s;
//	Person p;
//	//继承下来的变量
//	s._num += 1000;
//	p._num += 500;
//	cout << s._num << endl;
//	cout << p._num << endl;
//
//	cout << endl << endl;
//	//继承下来的函数
//	s.Print();
//	p.Print();
//	return 0;
//}


///////////// 友元关系不能继承 父的友元子不能用  子友元不能用父的成员

//class Person
//{
//protected:
//	int _num = 0;
//};
//
//class Student : public Person
//{
//	friend class Show;
//private:
//	int _id = 3;
//};
//
//class Show
//{
//	Show(Student& s, Person& p)
//	{
//		cout << s._id << endl;
//		cout << p._num << endl;
//	}
//};
//
//int main()
//{
//
//	return 0;
//}




////////////////////     静态成员变量属于整个继承类

//class Person
//{
//public:
//	static char x;
//protected:
//	int _num = 0;
//};
//
//char Person::x = 'x';
//
//class Student : public Person
//{
//private:
//	int _id = 3;
//};
//
//int main()
//{
//	Student s;
//	Person p;
//	printf("%p\n", &Student::x);
//	printf("%p\n", &Person::x);
//	return 0;
//}


//class job
//{
//public:
//	int _name = 100;
//};
//
//class teacher : public job
//{
//
//};
//
//class student : public job
//{
//
//};
//
//class assistant : public student, public teacher
//{
//
//};
//
//int main()
//{
//	assistant a;
//	cout << a._name << endl;
//	return 0;
//}



class job
{
public:
	int _name = 100; // 姓名
};
class Student : virtual public job
{
protected:
	int _num; //学号
};
class Teacher : virtual public job
{
protected:
	int _id; // 职工编号
};
class Assistant : public Student, public Teacher
{
protected:
	string _majorCourse; // 主修课程
};

int main()
{
	Assistant a;
	cout << a._name << endl;
	return 0;
}

