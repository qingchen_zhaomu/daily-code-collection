#include<iostream>
using namespace std;
#include<vector>
#include<algorithm>
#include<assert.h>

//class Solution {
//public:
//    int minSubArrayLen(int target, vector<int>& nums)
//    {
//        int count = 0, sum = 100000;
//        int left = 0, right = left + 1;
//        while (right < nums.size())
//        {
//            count = nums[left] + nums[right];
//            if (count >= target)
//            {
//                if (sum > right - left + 1)
//                {
//                    sum = right - left + 1;
//                }
//            }
//            count -= nums[left];
//            if (count >= target)
//            {
//                left++;
//            }
//            else
//            {
//                right++;
//            }
//        }
//        return sum;
//    }
//};

//class Solution {
//public:
//    int minSubArrayLen(int target, vector<int>& nums)
//    {
//        int count = 0, sum = 100000;
//        int left = 0, right = left + 1;
//        count = nums[left];
//        while (right >= left)
//        {
//            while (right < nums.size())
//            {
//                count += nums[right];
//                if (count >= target)
//                {
//                    if (sum > right - left + 1)
//                    {
//                        sum = right - left + 1;
//                    }
//                }
//                right++;
//            }
//            count -= nums[left++];
//            if (count >= target)
//            {
//                left++;
//            }
//            else
//            {
//                right++;
//            }
//            right--;
//        }
//        if (sum == 100000)
//        {
//            return 0;
//        }
//        return sum;
//    }
//};

//class Solution {
//public:
//    int minSubArrayLen(int target, vector<int>& nums)
//    {
//        int count = 0, sum = 100000;
//        int left = 0, right = 0;
//        //count = nums[left] + nums[right++];
//        for (; right < nums.size(); right++)
//        {
//            count += nums[right];
//            while (count >= target)
//            {
//                sum = min(sum, right - left + 1);
//                count -= nums[left++];
//            }
//        }
//        if (sum == 100000)
//        {
//            return 0;
//        }
//        return sum;
//    }
//};

//class Solution {
//public:
//    int longestOnes(vector<int>& nums, int k)
//    {
//        int sumzero = 0, count = 0;
//        for (int left = 0, right = 0; right < nums.size(); right++)
//        {
//            if (nums[right] == 0)sumzero++;
//            while (sumzero > k)
//                if (nums[left++] == 0)sumzero--;
//            count = max(count, right - left + 1);
//        }
//        return count;
//    }
//};

//class Solution {
//public:
//    int minOperations(vector<int>& nums, int x)
//    {
//        for (int right = nums.size() - 1, left = 0; left < right;)
//        {
//            int n = max(nums[left], nums[right]);
//            x -= n;
//            if (n == nums[left])left++;
//            else right--;
//            if (x < 0)
//            {
//
//            }
//        }
//    }
//};

//class Solution {
//public:
//    int minOperations(vector<int>& nums, int x)
//    {
//        int sum = 0, count = 0, len = 0;
//        for (int i = 0; i < nums.size(); i++)
//            count += nums[i];
//        for (int right = 0, left = 0; right < nums.size(); right++)
//        {
//            sum += nums[right];
//            while (sum > count - x)
//            {
//                sum -= nums[left++];
//            }
//            if(sum == count - x)
//                len = max(len, right - left + 1);
//        }
//        if (len == 0)return -1;
//        return nums.size() - len;
//    }
//};

class Solution {
public:
    int minOperations(vector<int>& nums, int x)
    {
        int sum = 0, count = 0, len = -1;
        for (int i = 0; i < nums.size(); i++)
            count += nums[i];
        for (int right = 0, left = 0; right < nums.size(); right++)
        {
            sum += nums[right];
            while (sum > count - x)
                sum -= nums[left++];
            if (sum == count - x)
                len = max(len, right - left + 1);
        }
        if (len == -1)return len;
        return nums.size() - len;
    }
};

int main()
{
    vector<int> v1 = { 1,1 };
    Solution s1;
    int n = s1.minOperations(v1, 3);

    cout << n << endl;

    return 0;
}