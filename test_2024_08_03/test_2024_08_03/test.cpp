#include<iostream>
using namespace std;

//class A
//{
//public:
//	A()
//	{
//		_b = 0;
//	}
//
//	~A()
//	{
//		_b--;
//	}
//
//	static int get_b()
//	{
//		return _b;
//	}
//
//private:
//	int _a;
//	static int _b;
//};
//
//int A::_b = 0;
//
//int main()
//{
//	A aa1;
//	A aa2;
//	A aa3;
//	A aa4;
//	//cout << A::get_b() << endl;
//	return 0;
//}


//class A
//{
//	friend class B;
//	int _a = 1;
//};
//
//class B
//{
//public:
//	int getnum()
//	{
//		return _t._a;
//	}
//private:
//	A _t;
//};

//class Date
//{
//	friend ostream& operator<<(ostream& _cout, const Date& d);
//	friend istream& operator>>(istream& _cin, Date& d);
//public:
//	Date(int year = 1900, int month = 1, int day = 1)
//		: _year(year)
//		, _month(month)
//		, _day(day)
//	{}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//ostream& operator<<(ostream& _cout, const Date& d)
//{
//	_cout << d._year << "-" << d._month << "-" << d._day;
//	return _cout;
//}
//istream& operator>>(istream& _cin, Date& d)
//{
//	_cin >> d._year;
//	_cin >> d._month;
//	_cin >> d._day;
//	return _cin;
//}

//class A
//{
//	int _a = 0;
//	static int _s;
//
//public:
//	class B // 内部类
//	{
//		int _k;
//
//		void getAnum(const A& a)
//		{
//			cout << _s << endl;
//			cout << a._a << endl;
//		}
//	};
//
//};
//int A::_s = 0;
//
//int main()
//{
//	A a1;
//	A::B _b;
//	cout << sizeof(a1) << endl;
//	//B b1;
//	//cout << b1.getnum() << endl;
//	return 0;
//}


//class A
//{
//public:
//	A()
//	{
//		cout << "A()" << endl;
//	}
//	~A()
//	{
//		cout << "~A()" << endl;
//	}
//};
//
//int main()
//{
//	A a1;
//	A();
//	A a2;
//	return 0;
//}

//#include<map>
//
//int main()
//{
//	map<int, int> m1;
//
//	//使用有名对象的插入方式
//	pair< int, int> s1(1, 2);
//	m1.insert(s1);
//
//	//使用匿名对象的插入方式
//	m1.insert(pair<int, int>(1, 2));
//
//	return 0;
//}


//int main()
//{
//	//动态开辟一块空间
//	int* ptr1 = new int;
//	delete ptr1;
//
//	//动态开辟一块空间并初始化
//	int* ptr2 = new int(5);
//	delete ptr2;
//
//	//动态开辟一段空间
//	int* ptr3 = new int[10];
//	delete[] ptr3;
//
//	//动态开辟一段空间并初始化
//	int* ptr4 = new int[10] {1, 2, 3, 4, 5, 6};
//	delete[] ptr4;
//
//	return 0;
//}

struct A
{
	A()
	{
		cout << "A()" << " ";
	}

	~A()
	{
		cout << "~A()" << " " ;
	}
};

int main()
{
	A* a1 = new A;
	delete a1;

	cout << endl << endl;

	A* a2 = new A[10];
	cout << endl;
	delete[] a2;
	return 0;
}