#define _CRT_SECURE_NO_WARNINGS 1
#include"sort.h"

void printarray(int* a, int n)
{
	for (int i = 0; i < n; i++)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
}

void swap(int* a, int* b)
{
	int tmp = *a;
	*a = *b;
	*b = tmp;
}

//选择排序
void selectsort(int* a, int n)
{
	int begin = 0, end = n - 1;
	while (begin<end)
	{
		int mini = begin, maxi = begin;
		for (int i = begin + 1; i <= end; i++)
		{
			if (a[i] < a[mini])
			{
				mini = i;
			}
			if (a[i] > a[maxi])
			{
				maxi = i;
			}
		}
		swap(&a[begin], &a[mini]);
		if (maxi == begin)
		{
			maxi = mini;
		}
		swap(&a[end], &a[maxi]);
		begin++;
		end--;
	}
}


//向下调整(左小右大)
//小堆
void adjustdown(int* a, int n, int parent)
{
	int child = parent * 2 + 1;
	while (child < n)
	{
		//假设法找到小的那个孩子
		if (child + 1 < n && a[child] > a[child + 1])
		{
			child++;
		}
		
		if (a[parent] > a[child])
		{
			swap(&a[parent], &a[child]);
			parent = child;
			child = child * 2 + 1;
		}
		else
			break;
	}
}
//堆排序
void heapsort(int* a, int n)
{
	for (int i = (n - 2) / 2; i >= 0; i--)
	{
		adjustdown(a, n, i);
	}
	int end = n - 1;
	while (end)
	{
		swap(&a[0], &a[end]);
		adjustdown(a, end--, 0);
	}
}


//希尔排序
void shell(int* a, int n)
{
	int gap = n;
	while (gap > 1)
	{
		gap = gap / 3 + 1;
		for (int i = 0; i < n - gap; i++)
		{
			int end = i;
			int tmp = a[end + gap];
			while (end >= 0)
			{
				if (a[end] > tmp)
				{
					a[end + gap] = a[end];
					end -= gap;
				}
				else
					break;
			}
			a[end + gap] = tmp;
		}
	}
}

//快排
void quicksort(int* a, int left, int right)
{
	//返回条件
	if (left >= right)return;

	int key = left;
	int begin = left, end = right;
	while (left < right)
	{
		while (left < right && a[right] >= a[key])
		{
			right--;
		}
		while (left < right && a[left] <= a[key])
		{
			left++;
		}
		swap(&a[left], &a[right]);
	}
	swap(&a[key], &a[left]);
	key = left;

	quicksort(a, begin, key - 1);
	quicksort(a, key + 1, end);
}


int main()
{
	int a[] = { 2,3,5,6,4,7,1 };
	//selectsort(a, sizeof(a) / sizeof(int));
	//printarray(a, sizeof(a) / sizeof(int));
	//shell(a, sizeof(a) / sizeof(int));
	//printarray(a, sizeof(a) / sizeof(int));
	//quicksort(a, 0, sizeof(a) / sizeof(int) - 1);
	//printarray(a, sizeof(a) / sizeof(int));
	heapsort(a, sizeof(a) / sizeof(int));
	printarray(a, sizeof(a) / sizeof(int));
	return 0;
}


//void shell(int* a, int n)
//{
//	int gap = n;
//	while (gap > 1)
//	{
//		gap = gap / 3 + 1;
//
//		for (int i = 0; i < n - gap; i++)
//		{
//			int end = i;
//			int tmp = a[end + gap];
//			while (end >= 0)
//			{
//				if (tmp < a[end])
//				{
//					a[end + gap] = a[end];
//					end -= gap;
//				}
//				else
//					break;
//			}
//			a[end + gap] = tmp;
//		}
//	}
//}


//void quicksort(int* a, int left, int right)
//{
//	//返回条件
//	if (left >= right)
//		return;
//
//	int begin = left, end = right;
//	int key = begin;
//	while (left < right)
//	{
//		while (left < right && a[right] >= a[key])
//		{
//			right--;
//		}
//		while (left < right && a[left] <= a[key])
//		{
//			left++;
//		}
//		swap(&a[left], &a[right]);
//	}
//	swap(&a[left], &a[key]);
//	key = left;
//
//	quicksort(a, begin, key - 1);
//	quicksort(a, key + 1, end);
//}