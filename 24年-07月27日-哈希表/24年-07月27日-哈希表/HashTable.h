#pragma once
#include<iostream>
using namespace std;
#include<vector>
#include<string>
#include<assert.h>


template<class k>
struct HashFunc
{
	// 用于将key转换成整形(int)
	size_t operator()(const k& key)
	{
		return (size_t)key;
	}
};

// 特化一份string的
template<>
struct HashFunc<string>
{
	// 用于将key转换成整形(int)
	size_t operator()(const string& key)
	{
		size_t hash = 0;
		for (auto e : key)
		{
			hash *= 31;
			hash += e;
		}
		return hash;
	}
};


namespace hash_bucket
{
	template<class T>
	struct HashNode
	{
		T _data;
		HashNode<T>* _next;

		HashNode(const T& data)
			:_data(data)
			,_next(nullptr)
		{}
	};


	template<class k, class T, class KeyOfT, class Hash>
	class HashTable;

	template<class k, class T, class Ptr, class Ref, class KeyOfT, class Hash>
	struct HTIterator
	{
		typedef HashNode<T> Node;
		typedef HTIterator<k, T, Ptr, Ref, KeyOfT, Hash> Self;

		Node* _node;
		const HashTable<k, T, KeyOfT, Hash>* _ht;

		HTIterator(Node* node, const HashTable<k, T, KeyOfT, Hash>* ht)
			:_node(node)
			,_ht(ht)
		{}

		Ref operator*()
		{
			return _node->_data;
		}

		Ptr operator->()
		{
			return &_node->_data;
		}

		bool operator!=(const Self& s)
		{
			return _node != s._node;
		}

		bool operator==(const Self& s)
		{
			return _node == s._node;
		}

		Self& operator++()
		{
			if (_node->_next)
			{
				_node = _node->_next;
			}
			else
			{
				Hash hs;
				KeyOfT kot;
				size_t hashi = hs(kot(_node->_data)) % _ht->_tables.size();
				++hashi;

				while (hashi < _ht->_tables.size())
				{
					if (_ht->_tables[hashi])
					{
						break;
					}
					++hashi;
				}

				if (hashi == _ht->_tables.size())
				{
					_node = nullptr;
				}
				else
				{
					_node = _ht->_tables[hashi];
				}
			}

			return *this;
		}

	};


	template<class k, class T, class KeyOfT, class Hash>
	class HashTable
	{
		template<class k, class T, class Ptr, class Ref, class KeyOfT, class Hash>
		friend struct HTIterator;

		typedef HashNode<T> Node;
	public:
		typedef HTIterator<k, T, T*, T&, KeyOfT, Hash> Iterator;
		typedef HTIterator<k, T, const T*, const T&, KeyOfT, Hash> ConstIterator;

		Iterator Begin()
		{
			if (_n == 0)
				return End();

			for (size_t i = 0; i < _tables.size(); i++)
			{
				Node* cur = _tables[i];
				if (cur)
				{
					return Iterator(cur, this);
				}
			}

			return End();
		}

		Iterator End()
		{
			return Iterator(nullptr, this);
		}

		ConstIterator Begin()const
		{
			if (_n == 0)
				return End();

			for (size_t i = 0; i < _tables.size(); i++)
			{
				Node* cur = _tables[i];
				if (cur)
				{
					return Iterator(cur, this);
				}
			}

			return End();
		}

		ConstIterator End()const
		{
			return Iterator(nullptr, this);
		}

		HashTable()
		{
			_tables.resize(50, nullptr);
		}

		~HashTable()
		{
			for (size_t i = 0; i < _tables.size(); i++)
			{
				Node* cur = _tables[i];
				while (cur)
				{
					Node* next = cur->_next;
					delete cur;
					cur = next;
				}
				_tables[i] = nullptr;
			}
		}

		pair<Iterator, bool> Insert(const T& data)
		{
			KeyOfT kot;
			Iterator it = Find(kot(data));
			if (it != End())
				return { it, false };

			Hash hs;
			size_t hashi = hs(kot(data)) % _tables.size();

			// 负载因子 == 1 扩容
			if (_n == _tables.size())
			{
				vector<Node*> newtables(_tables.size() * 2, nullptr);
				for (size_t i = 0; i < _tables.size(); i++)
				{
					Node* cur = _tables[i];
					while (cur)
					{
						Node* next = cur->_next;

						size_t hasi = hs(kot(cur->_data)) % newtables.size();
						cur->_next = newtables[hasi];
						newtables[hasi] = cur;

						cur = next;
					}
					_tables[i] = nullptr;
				}
				_tables.swap(newtables);
			}


			// 头插
			Node* newnode = new Node(data);
			newnode->_next = _tables[hashi];
			_tables[hashi] = newnode;
			++_n;

			return {Iterator(newnode, this), true};
		}

		Iterator Find(const k& key)
		{
			Hash hs;
			KeyOfT kot;
			size_t hashi = hs(key) % _tables.size();
			Node* cur = _tables[hashi];

			while (cur)
			{
				if (kot(cur->_data) == key)
				{
					return Iterator(cur, this);
				}

				cur = cur->_next;
			}
			return End();
		}

		bool Erase(const k& key)
		{
			Hash hs;
			KeyOfT kot;

			size_t hashi = hs(key) % _tables.size();
			Node* prev = nullptr;
			Node* cur = _tables[hashi];

			while (cur)
			{
				if (kot(cur->_data) == key)
				{
					if (prev == nullptr)
					{
						_tables[hashi] = cur->_next;
					}
					else
					{
						prev->_next = cur->_next;
					}
					delete cur;
					--_n;
					return true;
				}

				prev = cur;
				cur = cur->_next;
			}
			return false;
		}

	private:
		vector<Node*> _tables;  // 指针数组
		size_t _n = 0;   // 表中存储数据个数
	};



	/*void TestHT1()
	{
		HashTable<int, int> ht;
		int a[] = { 11,21,4,14,24,15,9,19,29,39 };
		for (auto e : a)
		{
			ht.Insert({ e,e });
		}

		ht.Insert({ -6, 6 });

		for (auto e : a)
		{
			ht.Erase(e);
		}
	}

	void TestHT2()
	{
		HashTable<string, string> ht;
		ht.Insert({ "sort", "排序" });
		ht.Insert({ "left", "左边" });
	}*/
}