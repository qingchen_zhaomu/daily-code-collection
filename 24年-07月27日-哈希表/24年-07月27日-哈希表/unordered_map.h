#pragma once
#include"HashTable.h"


namespace hjx
{
	template<class k, class v, class Hash = HashFunc<k>>
	class unordered_map
	{
		struct KeyOfT
		{
			const k& operator()(const pair<k, v>& kv)
			{
				return kv.first;
			}
		};
	public:
		typedef typename hash_bucket::HashTable<k, pair<const k, v>, KeyOfT, Hash>::Iterator iterator;
		typedef typename hash_bucket::HashTable<k, pair<const k, v>, KeyOfT, Hash>::ConstIterator const_iterator;

		iterator begin()
		{
			return _ht.Begin();
		}

		iterator end()
		{
			return _ht.End();
		}

		const_iterator begin()const
		{
			return _ht.Begin();
		}

		const_iterator end()const
		{
			return _ht.End();
		}


		pair<iterator, bool> insert(const pair<k, v>& kv)
		{
			return _ht.Insert(kv);
		}

		hash_bucket::HashNode<pair<k, v>> Find(const k& key)
		{
			return _ht.Find(key);
		}

		bool Erase(const k& key)
		{
			return _ht.Erase(key);
		}

		v& operator[](const k& key)
		{
			pair<iterator, bool> ret = _ht.Insert({ key, v() });
			return ret.first->second;
		}

	private:
		hash_bucket::HashTable<k, pair<const k, v>, KeyOfT, Hash> _ht;
	};



	void test_map()
	{
		unordered_map<string, string> dict;
		dict.insert({ "sort", "����" });
		dict.insert({ "left", "���" });
		dict.insert({ "right", "�ұ�" });

		dict["left"] = "��ߣ�ʣ��";
		dict["insert"] = "����";
		dict["string"];

		//unordered_map<string, string>::iterator it = dict.begin();
		//while (it != dict.end())
		//{
		//	// �����޸�first�������޸�second
		//	//it->first += 'x';
		//	it->second += 'x';

		//	cout << it->first << ":" << it->second << endl;
		//	++it;
		//}
		for (auto e : dict)
		{
			cout << e.first <<" "<< e.second << endl;
		}

		cout << endl;
	}

}
