#pragma once
#include"HashTable.h"


namespace hjx
{
	template<class k , class Hash = HashFunc<k>>
	class unordered_set
	{
		struct KeyOfT
		{
			const k& operator()(const k& kv)
			{
				return kv;
			}
		};
	public:
		typedef typename hash_bucket::HashTable<k, const k, KeyOfT, Hash>::Iterator iterator;
		typedef typename hash_bucket::HashTable<k, const k, KeyOfT, Hash>::ConstIterator const_iterator;

		iterator begin()
		{
			return _ht.Begin();
		}

		iterator end()
		{
			return _ht.End();
		}

		const_iterator begin()const
		{
			return _ht.Begin();
		}

		const_iterator end()const
		{
			return _ht.End();
		}


		pair<iterator, bool> insert(const k& kv)
		{
			return _ht.Insert(kv);
		}

		iterator Find(const k& key)
		{
			return _ht.Find(key);
		}

		bool Erase(const k& key)
		{
			return _ht.Erase(key);
		}

	private:
		hash_bucket::HashTable<k, const k, KeyOfT, Hash> _ht;
	};



	void test_set()
	{
		unordered_set<int> dict;
		
	}

}