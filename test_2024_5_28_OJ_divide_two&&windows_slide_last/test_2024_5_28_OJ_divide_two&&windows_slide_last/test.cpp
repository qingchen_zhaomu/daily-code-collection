#include<iostream>
using namespace std;
#include<vector>
#include<algorithm>
#include<assert.h>
#include<unordered_map>

//class Solution {
//public:
//    string minWindow(string s, string t)
//    {
//        int hash1[128];//计算t中有的字母
//        for (auto& e : t)hash1[e]++;
//        int hash2[128];
//        int begin = -1, len = INT_MAX;
//        //滑动窗口主逻辑
//        for (int left = 0, right = 0, count = 0; right < s.size(); right++)
//        {
//            //进窗口 + 维护count
//            if (++hash2[s[right]] <= hash1[s[right]])count++;
//            //判断
//            while (count == t.size())
//            {
//                //更新结果
//                if (right - left + 1 < len)
//                    begin = left, len = right - left + 1;
//                //出窗口 + 维护count
//                if (hash2[s[left]]-- <= hash1[s[left]])count--;
//                left++;
//            }
//        }
//        if (begin == -1)return "";
//        else return s.substr(begin, len);
//    }
//};

//class Solution {
//public:
//    int search(vector<int>& nums, int target)
//    {
//        for (int left = 0, right = nums.size() - 1; left <= right;)
//        {
//            int mid = (left + right) / 2;
//            if (nums[mid] < target)left = mid + 1;
//            else if (nums[mid] > target)right = mid - 1;
//            else return mid;
//        }
//        return -1;
//    }
//};

//class Solution {
//public:
//    int takeAttendance(vector<int>& nums)
//    {
//        int left = 0, right = nums.size() - 1;
//        while (left < right)
//        {
//            int mid = left + (right - left) / 2;
//            if (nums[mid] != mid) right = mid;
//            else left = mid + 1;
//        }
//        if (nums[left] == left)return left + 1;
//        return left;
//    }
//};
//
//int main()
//{
//    vector<int> v1 = { 1,1,4,3,7,3,2,5,4 };
//    string s2 = "ABC";
//    string s3 = "ADOBECODEBANC";
//    Solution s1;
//    string n = s1.takeAttendance(v1);
//
//    cout << n << endl;
//
//    return 0;
//}