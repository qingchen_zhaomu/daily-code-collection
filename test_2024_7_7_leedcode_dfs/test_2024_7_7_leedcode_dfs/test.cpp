#include<iostream>
using namespace std;
#include<vector>
#include<string>
#include<queue>
#include<functional>



//class Solution {
//public:
//    bool check[9];
//    vector<vector<int>> ret;
//    vector<int> path;
//    vector<vector<int>> permuteUnique(vector<int>& nums)
//    {
//        sort(nums.begin(), nums.end());
//        dfs(nums, 0);
//        return ret;
//    }
//    void dfs(vector<int>& nums, int k)
//    {
//        //递归出口
//        if (k == nums.size())
//        {
//            ret.push_back(path);
//            return;
//        }
//        for (int i = 0; i < nums.size(); i++)
//        {
//            //剪枝(重复的 和 前面已经有过的)
//            if (check[i] == false && (i == 0 || nums[i] != nums[i - 1] || check[i - 1] == true))
//            {
//                check[i] = true;
//                path.push_back(nums[i]);
//                dfs(nums, k + 1);
//                path.pop_back();
//                check[i] = false;
//            }
//        }
//    }
//};

//class Solution {
//public:
//    int ret;
//    int sum;
//    int subsetXORSum(vector<int>& nums)
//    {
//        dfs(nums, 0);
//        return ret;
//    }
//    void dfs(vector<int>& nums, int k)
//    {
//        ret += sum;
//        for (int i = k; i < nums.size(); i++)
//        {
//            sum ^= nums[i];
//            dfs(nums, i + 1);
//            sum ^= nums[i];
//        }
//    }
//};

//class Solution {
//public:
//    int ret;
//    vector<int> path;
//    int subsetXORSum(vector<int>& nums)
//    {
//        ret = 0;
//        dfs(nums, 0);
//        return ret;
//    }
//    void dfs(vector<int>& nums, int k)
//    {
//        for (int i = k; i < nums.size(); i++)
//        {
//            path.push_back(nums[i]);
//            if (path.size() == 1)
//                ret += path[0];
//            else
//            {
//                int tmp = path[0];
//                for (int j = 1; j < path.size(); j++)
//                    tmp ^= path[j];
//                ret += tmp;
//            }
//            dfs(nums, i + 1);
//            path.pop_back();
//        }
//    }
//};

//class Solution {
//public:
//    vector<vector<int>> ret;
//    vector<int> path;
//    vector<vector<int>> subsets(vector<int>& nums)
//    {
//        dfs(nums, 0);
//        ret.push_back(path);
//        return ret;
//    }
//    void dfs(vector<int>& nums, int k)
//    {
//        for (int i = k; i < nums.size(); i++)
//        {
//            path.push_back(nums[i]);
//            ret.push_back(path);
//            dfs(nums, i + 1);
//            path.pop_back();
//        }
//    }
//};

//class Solution {
//public:
//    vector<vector<int>> ret;
//    vector<int> path;
//    vector<vector<int>> subsets(vector<int>& nums)
//    {
//        dfs(nums, nums.size());
//        return ret;
//    }
//    void dfs(vector<int>& nums, int i)
//    {
//        if (i == 0)//证明已经到叶子节点了
//        {
//            ret.push_back(path);
//            return;
//        }
//        //不选该节点
//        dfs(nums, i - 1);
//        //选该节点
//        path.push_back(nums[i - 1]);
//        dfs(nums, i - 1);
//        path.pop_back();
//    }
//};

//class Solution {
//public:
//    vector<vector<int>> ret;
//    vector<int> path;
//    bool check[7];
//
//    vector<vector<int>> permute(vector<int>& nums)
//    {
//        dfs(nums);
//        return ret;
//    }
//    void dfs(vector<int>& nums)
//    {
//        if (path.size() == nums.size())
//        {
//            ret.push_back(path);
//            return;
//        }
//        for (int i = 0; i < nums.size(); i++)
//        {
//            if (check[i] == false)
//            {
//                path.push_back(nums[i]);
//                check[i] = true;
//                dfs(nums);
//                path.pop_back();
//                check[i] = false;
//            }
//        }
//    }
//};

//class Solution {
//public:
//    vector<string> ret;
//    vector<string> binaryTreePaths(TreeNode* root)
//    {
//        dfs(root, "");
//        return ret;
//    }
//    void dfs(TreeNode* root, string path)
//    {
//        if (!root)return;
//        //前序遍历
//        path += (to_string(root->val));
//        if (!root->left && !root->right)
//        {
//            ret.push_back(path);
//            return;
//        }
//        path += "->";
//        dfs(root->left, path);
//        dfs(root->right, path);
//    }
//};

/**
 * Definition for a binary tree node.
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode() : val(0), left(nullptr), right(nullptr) {}
 *     TreeNode(int x) : val(x), left(nullptr), right(nullptr) {}
 *     TreeNode(int x, TreeNode *left, TreeNode *right) : val(x), left(left), right(right) {}
 * };
 */
//class Solution {
//public:
//    int time;
//    int ret = 0;
//    int kthSmallest(TreeNode* root, int k)
//    {
//        time = k;
//        dfs(root);
//        return ret;
//    }
//    void dfs(TreeNode* root)
//    {
//        //边界情况
//        if (!root || !time)return;
//        dfs(root->left);
//        if (!(--time))
//        {
//            ret = root->val;
//            return;
//        }
//        dfs(root->right);
//    }
//};

int main()
{

	return 0;
}