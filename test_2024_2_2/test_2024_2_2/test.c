#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>

struct S
{
	char a : 1;
	char b : 4;
};


int main()
{
	struct S s = { 0 };
	//错误示范
	//scanf("%d", % s.b);


	//正确示范1
	s.a = 10;

	//正确示范2
	int d = 0;
	scanf("%d", &d);
	s.b = d;

	return 0;
}


//struct S
//{
//	char a : 3;
//	char b : 4;
//	char c : 5;
//	char d : 4;
//};
//
//
//int main()
//{
//	struct S s = { 0 };
//	s.a = 10;
//	s.b = 12;
//	s.c = 3;
//	s.d = 4;
//	return 0;
//}


//struct A
//{
//	int _a : 2;
//	int _b : 5;
//	int _c : 10;
//};
//
//struct B
//{
//	int _a : 2;
//	int _b : 5;
//};
//
//struct C
//{
//	int _a : 2;
//	int _b : 5;
//	int _c : 10;
//	int _d : 30;
//};
//
//int main()
//{
//	printf("%d\n", sizeof(struct A));
//	printf("%d\n", sizeof(struct B));
//	printf("%d\n", sizeof(struct C));
//	return 0;
//}


//struct A
//{
//	int _a : 2;
//	int _b : 5;
//};
//int _d : 30;

//struct Stu
//{
//	char a;
//	int b;
//};
//
//void Print1(struct Stu* test)
//{
//	printf("%d\n", test->b);
//}
//
//void Print2(struct Stu test)
//{
//	printf("%d\n", test.b);
//}
//
//int main()
//{
//	struct Stu test = { 'a',1 };
//
//	Print1(&test);
//	Print2(test);
//	return 0;
//}



//struct Stu1
//{
//	char a;
//	int b;
//};
//
//struct Stu2
//{
//	char a;
//	char b;
//	int c;
//	struct Stu1 d;
//};
//
//int main()
//{
//	printf("%d\n", sizeof(struct Stu2));
//	return 0;
//}


//int main()
//{
//	int i = 0;
//	scanf("%d", &i);
//	printf("hehe\n");
//}