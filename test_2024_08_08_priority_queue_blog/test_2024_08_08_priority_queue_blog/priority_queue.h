#pragma once
#include<iostream>
using namespace std;
#include<vector>

namespace hjx
{
	template<class T>
	struct myless
	{
		bool operator()(const T& x, const T& y)
		{
			return x < y;
		}
	};

	template<class T>
	struct mygreater
	{
		bool operator()(const T& x, const T& y)
		{
			return x > y;
		}
	};

	template<class T, class container = vector<T>, class compare = myless<T>>
	class priority_queue
	{
	public:
		priority_queue() = default;

		template<class InputIterator>
		priority_queue(InputIterator first, InputIterator last)
		{
			while (first != last)
			{
				_con.push_back(*first);
				first++;
			}
			//����
			for (int i = ((int)(_con.size()) - 2) / 2; i >= 0; i--)
			{
				AdjustDown(i);
			}
		}


		void AdjustUp(int child)
		{
			compare comf;
			int parent = (child - 1) / 2;
			while (child > 0)
			{
				if (comf(_con[child], _con[parent]))
				{
					swap(_con[child], _con[parent]);
					child = parent;
					parent = (parent - 1) / 2;
				}
				else
					break;
			}
		}

		void AdjustDown(int parent)
		{
			compare comf;
			int child = parent * 2 + 1;
			while (child < _con.size())
			{
				if (child + 1 < _con.size() && comf(_con[child + 1], _con[child]))child++;

				if (comf(_con[child], _con[parent]))
				{
					swap(_con[child], _con[parent]);
					parent = child;
					child = child * 2 + 1;
				}
				else break;
			}
		}

		void push(const T& x)
		{
			_con.push_back(x);
			AdjustUp((int)(_con.size()) - 1);
		}

		void pop()
		{
			swap(_con[0], _con[_con.size() - 1]);
			_con.pop_back();
			AdjustDown(0);
		}

		const T& top()
		{
			return _con[0];
		}

		size_t size()
		{
			return _con.size();
		}

		bool empty()
		{
			return _con.empty();
		}

	private:
		container _con;
	};


}



