#include"priority_queue.h"

namespace hjx
{
	void test_priority_queue1()
	{
		priority_queue<int> pq;
		pq.push(1);
		pq.push(2);
		pq.push(3);
		pq.push(4);
		pq.push(5);

		while (!pq.empty())
		{
			cout << pq.top() << " ";
			pq.pop();
		}
	}

	void test_priority_queue2()
	{
		vector<int> v1{ 1,2,3,5,7,4,6,3 };
		priority_queue<int> pq(v1.begin(), v1.end());
		while (!pq.empty())
		{
			cout << pq.top() << " ";
			pq.pop();
		}
	}

}

int main()
{
	//hjx::test_priority_queue1();
	hjx::test_priority_queue2();
	return 0;
}