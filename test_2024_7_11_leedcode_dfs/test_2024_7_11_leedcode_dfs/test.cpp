#include<iostream>
using namespace std;
#include<vector>
#include<string>
#include<queue>
#include<functional>



//class Solution {
//public:
//    int n, m;
//    int dx[4] = { 0,0,1,-1 };
//    int dy[4] = { 1,-1,0,0 };
//    int memo[201][201];
//    int longestIncreasingPath(vector<vector<int>>& nums)
//    {
//        n = nums.size(), m = nums[0].size();
//        vector<vector<int>> vis(n, vector<int>(m));
//        int ret = -1;
//        for (int i = 0; i < n; i++)
//            for (int j = 0; j < m; j++)
//                ret = max(ret, dfs(nums, i, j, vis));
//        return ret;
//    }
//    int dfs(vector<vector<int>>& nums, int i, int j, vector<vector<int>>& vis)
//    {
//        int ret = 1;
//        if (memo[i][j])return memo[i][j];
//        for (int k = 0; k < 4; k++)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < n && y >= 0 && y<m && !vis[x][y] && nums[x][y]>nums[i][j])
//            {
//                vis[x][y] = true;
//                ret = max(ret, dfs(nums, x, y, vis) + 1);
//                vis[x][y] = false;
//            }
//        }
//        memo[i][j] = ret;
//        return ret;
//    }
//};

//class Solution {
//public:
//    int vis[201][201];
//    int getMoneyAmount(int n)
//    {
//        return dfs(1, n);
//    }
//    int dfs(int left, int right)
//    {
//        if (left >= right) return 0;
//        if (vis[left][right])return vis[left][right];
//        int ret = INT_MAX;
//        for (int i = left; i <= right; i++)
//        {
//            int x = dfs(left, i - 1);
//            int y = dfs(i + 1, right);
//            ret = min(ret, i + max(x, y));
//        }
//        vis[left][right] = ret;
//        return ret;
//    }
//};

//class Solution {
//public:
//    int lengthOfLIS(vector<int>& nums)
//    {
//        vector<int> vis(nums.size());
//        int ret = 0;
//        for (int i = 0; i < nums.size(); i++)
//        {
//            ret = max(ret, dfs(nums, i, vis));
//        }
//        return ret;
//    }
//    int dfs(vector<int>& nums, int pos, vector<int>& vis)
//    {
//        int ret = 1;
//        if (vis[pos] != 0)return vis[pos];
//        for (int i = pos + 1; i < nums.size(); i++)
//        {
//            if (nums[i] > nums[pos])
//            {
//                ret = max(ret, dfs(nums, i, vis) + 1);
//            }
//        }
//        vis[pos] = ret;
//        return ret;
//    }
//};

//class Solution {
//public:
//    int uniquePaths(int m, int n)
//    {
//        //记忆化搜索
//        vector<vector<int>> vis(m + 1, vector<int>(n + 1));
//        vis[1][1] = 1;
//        return dfs(m, n, vis);
//    }
//    int dfs(int m, int n, vector<vector<int>>& vis)
//    {
//        if (vis[m][n])return vis[m][n];
//        if (m == 0 || n == 0)return 0;
//        if (m == 1 && n == 1)return 1;
//        vis[m][n] = dfs(m - 1, n, vis) + dfs(m, n - 1, vis);
//        return vis[m][n];
//    }
//};

//class Solution {
//public:
//    // 记忆化搜索
//    // 建一个备忘录
//    int memo[101];
//    int fib(int n)
//    {
//        memset(memo, -1, sizeof(memo));
//        memo[0] = 0, memo[1] = 1;
//        return dfs(n);
//    }
//    int dfs(int n)
//    {
//        if (memo[n] != -1)return memo[n];
//        memo[n] = dfs(n - 1) + dfs(n - 2);
//        return memo[n];
//    }
//};

//class Solution {
//public:
//    int dx[2] = { 0,1 };
//    int dy[2] = { 1,0 };
//    bool vis[101][101];
//    int ret, cnt;
//    int wardrobeFinishing(int m, int n, int _cnt)
//    {
//        cnt = _cnt;
//        dfs(m, n, 0, 0);
//        return ret;
//    }
//    void dfs(int m, int n, int i, int j)
//    {
//        ret++;
//        vis[i][j] = true;
//        for (int k = 0; k < 2; k++)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            int sum = 0, nx = x, ny = y;
//            while (nx)
//            {
//                sum += nx % 10;
//                nx /= 10;
//            }
//            while (ny)
//            {
//                sum += ny % 10;
//                ny /= 10;
//            }
//            if (x >= 0 && x < m && y >= 0 && y < n && sum <= cnt && !vis[x][y])
//                dfs(m, n, x, y);
//        }
//    }
//};

int main()
{
	
	return 0;
}