#pragma once
#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<windows.h>
#include<stdbool.h>
#include<locale.h>
#include<time.h>
#include<math.h>

#include <mmsystem.h>//导入声音头文件
#pragma comment(lib,"Winmm.lib")


#define WALL L'□'
#define BODY L'●'
#define FOOD L'★'

#define POS_X 24
#define POS_Y 5

#define KEY_PRESS(VK) ( GetAsyncKeyState(VK) &0x1 ? 1 : 0 )

enum GAME_STATUS
{
	OK = 1,
	ESC,
	KILL_BY_WALL,
	KILL_BY_SELF
};

enum DIRECTION
{
	UP = 1,
	DOWN,
	LEFT,
	RIGHT
};


//蛇身结点的定义
typedef struct SnakeNode
{
	int x;
	int y;
	struct SnakeNode* next;
}SnakeNode, * pSnakeNode;


//贪吃蛇
typedef struct Snake
{
	pSnakeNode pSnake;//维护整条蛇的指针
	pSnakeNode pFood;//指向食物的指针
	int score;//当前积累的分数
	int FoodWeight;//一个食物的分数
	int SleepTime;//蛇休眠的时间，时间越短，蛇的速度越快
	enum GAME_STATUS status;//游戏当前的状态
	enum DIRECTION dir;//蛇当前走的方向

}Snake, * pSnake;


void SetPos(int x, int y);

//游戏开始前的准备
void GameStart(pSnake ps);

//打印欢迎界面
void WelcomeToGame();

//绘制地图
void CreatMap();

//初始化蛇
void InitSnake(pSnake ps);

//创建食物
void CreateFood(pSnake ps);

//游戏运行
void GameRun(pSnake ps);

//走一步
void SnakeMove(pSnake ps);

//判断下一个位置是否是食物
int NextIsFood(pSnake ps, pSnakeNode pNext);

//下一个是食物，就吃掉食物
void EatFood(pSnake ps, pSnakeNode pNext);

//下一个不是食物，就正常走一步
void NotEatFood(pSnake ps, pSnakeNode pNext);

//判断撞墙
void KillByWall(pSnake ps);

//判断撞自己
void KillBySelf(pSnake ps);

//游戏结束之后的善后
void GameEnd(pSnake ps);