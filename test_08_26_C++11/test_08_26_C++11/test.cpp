#define _CRT_SECURE_NO_WARNINGS 1

#include<iostream>
#include<vector>
#include<list>
#include<map>
#include<string>
#include<array>
using namespace std;
#include<assert.h>
#include<functional>

//void Print()
//{
//	cout << endl;
//}
//
//template<class T, class ...Args>
//void Print(T t, Args... args)
//{
//	cout << t << " ";
//	Print(args...);
//}
//
//template <class ...Args>
//void showlist(Args... args)
//{
//	Print(args...);
//}
//
//int main()
//{
//	showlist(1);
//	showlist(1, "xxx");
//	showlist(1, "xxx", 2.2);
//	return 0;
//}

//
//template<class T>
//void Print(T t)
//{
//	cout << t << " ";
//}
//
//template <class ...Args>
//void showlist(Args... args)
//{
//	int arr[] = { (Print(args),0)... };
//	cout << endl;
//}


//template<class T>
//int Print(T t)
//{
//	cout << t << " ";
//	return 0;
//}
//
//template <class ...Args>
//void showlist(Args... args)
//{
//	int arr[] = { Print(args)... };
//	cout << endl;
//}
//
//int main()
//{
//	showlist(1);
//	showlist(1, "xxx");
//	showlist(1, "xxx", 2.2);
//	return 0;
//}



//template <class ...Args>
//void showlist(Args... args)
//{
//	int arr[] = { (cout << args << " ", 0)... };
//	cout << endl;
//}
//
//int main()
//{
//	showlist(1);
//	showlist(1, "xxx");
//	showlist(1, "xxx", 2.2);
//	return 0;
//}


//int main()
//{
//	list<pair<int, char>> ls;
//	pair<int, char> p1(1, 'x');
//	ls.emplace_back(p1);
//
//	ls.emplace_back(1, 'x');
//	return 0;
//}


//int main()
//{
//	/*auto add1 = [](int x, int y)->int {return x + y; };
//	auto add2 = [](int x, int y) {return x + y; };
//
//	auto func1 = []
//		{
//			cout << "hello world" << endl;
//		};
//
//	func1();*/
//
//
//	//int a = 0, b = 1;
//
//	/*auto swap2 = [a, b]()mutable
//		{
//			int tmp = a;
//			a = b;
//			b = tmp;
//		};
//
//	swap2();
//	cout << a << b;*/
//
//	int a = 0, b = 1;
//
//	auto swap3 = [](int& a,int& b)mutable->void{
//			int tmp = a;
//			a = b;
//			b = tmp;
//		};
//
//	swap3(a, b);
//	cout << a << b;
//
//	return 0;
//}


//int f(int a, char b)
//{
//	return a + b;
//}
//
//struct Functor
//{
//public:
//	int operator() (int a, int b)
//	{
//		return a + b;
//	}
//};
//
//int main()
//{
//	int a = 2;
//	function<int(int, char)> f1 = f;
//	f(a, 'x');
//
//	function<int(int, int)> f2 = Functor();
//	return 0;
//}



//class Plus
//{
//public:/*
//	static int plusi(int a, int b)
//	{
//		return a + b;
//	}*/
//
//	int plusd(int a, int b)
//	{
//		return a + b;
//	}
//};
//
//int main()
//{
//	//function<int(int, int)> f4 = &Plus::plusi;
//	Plus pd;
//
//	function<int(Plus*, int, int)> f5 = &Plus::plusd;
//	function<int(Plus, int, int)> f6 = &Plus::plusd;
//
//	cout << f5(&pd, 1, 2) << endl;
//	cout << f6(pd, 1, 2) << endl;
//	return 0;
//}



//using placeholders::_1;
//using placeholders::_2;
//using placeholders::_3;

//int sub(int a, int b)
//{
//	return (a - b) * 10;
//}
//
//int main()
//{
//	/*auto f1 = bind(sub, _1, _2);
//	cout << f1(10, 5) << endl;
//
//	auto f2 = bind(sub, _2, _1);
//	cout << f2(10, 5) << endl;*/
//
//	auto f3 = bind(sub, _1, 100);
//	cout << f3(5) << endl;
//
//	auto f4 = bind(sub, 100, _1);
//	cout << f4(5) << endl;
//
//	return 0;
//}

using placeholders::_1;
using placeholders::_2;
using placeholders::_3;

class Plus
{
public:
	int plusd(int a, int b)
	{
		return a + b;
	}
};

int main()
{
	function<int(int, int)> f1 = bind(&Plus::plusd, Plus(), _1, _2);
	cout << f1(3, 31) << endl;
	return 0;
}









