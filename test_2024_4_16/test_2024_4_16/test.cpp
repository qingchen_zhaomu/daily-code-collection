#include<iostream>
using namespace std;

class Date
{
public:
	Date(int year = 1, int month = 2, int day = 1)
	{
		_year = year;
		_month = month;
		_day = day;
	}
	
	void Print()
	{
		cout << _year << "-" << _month << "-" << _day << endl;
	}

	Date(const Date& d)
	{
		_day = d._day;
		_year = d._year;
		_month = d._month;
	}

	bool operator<(const Date & d)
	{
		return _year < d._year;
	}

	Date operator=(const Date& d)
	{
		_year = d._year;
		_month = d._month;
		_day = d._day;

		return *this;
	}

private:
	int _year = 1;
	int _month = 1;
	int _day = 1;
};


int main()
{ 
	Date d1(2023, 9, 6);//初始化
	//Date d2 = d1;//拷贝构造函数
	Date d2(2024, 3, 31);
	Date d3;
	Date d4;
	d3 = d4 = d1;

	d3.Print();
	d4.Print();
	//cout << d1.operator<(d2) << endl;

	return 0;
}

//class Date
//{
//public:
//	Date(int year = 1, int month = 2, int day = 1)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	void Print()
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//private:
//	int _year=1;
//	int _month=1;
//	int _day=1;
//};
//
//
//int main()
//{
//	Date d1;
//	d1.Print();
//	return 0;
//}