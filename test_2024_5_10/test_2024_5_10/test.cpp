#include<iostream>
using namespace std;
#include<string>
#include<list>
#include<algorithm>

void test_string1()
{
	string s = "hello world";

	string utl = "https://legacy.cplusplus.com/reference/string/string/?kw=string";
	size_t pos1 = utl.find(':');
	string s1 = utl.substr(0, pos1 - 0);
	cout << s1 << endl;

	size_t pos2 = utl.find('/', pos1 + 3);
	string s2 = utl.substr(pos1 + 3, pos2 - pos1 - 3);
	cout << s2 << endl;

	string s3 = utl.substr(pos2 + 1);
	cout << s3 << endl;
}

int main()
{
	test_string1();
	return 0;
}

//void test_string1()
//{
//	string s1 = "hello world";
//	s1.assign("111111");
//	cout << s1 << endl;
//
//	string s2("hello world");
//	s2.insert(0, "xxxx");
//	cout << s2 << endl;
//
//	s2.insert(0, 1, 'y');
//	cout << s2 << endl;
//
//	s2.insert(s2.begin(), 'y');
//	cout << s2 << endl;
//
//	s2.insert(s2.begin(), s1.begin(), s1.end());
//	cout << s2 << endl;
//
//}
//
//void test_string2()
//{
//	//string s1 = "hello world";
//	//cout << s1 << endl;
//	/*s1.erase(3, 2);
//	cout << s1 << endl;*/
//
//	//s1.replace(5, 1, "%20");
//	//cout << s1 << endl;
//
//	string s2("hello     world hello bit");
//	cout << s2 << endl;
//	for (int i = 0; i < s2.size();)
//	{
//		if (s2[i] == ' ')
//		{
//			s2.replace(i, 1, "%20");
//			i += 3;
//		}
//		else
//			i++;
//	}
//	cout << s2 << endl;
//
//
//	string s3("hello     world hello bit");
//	cout << s3 << endl;
//	string s4;
//	for (auto ch : s3)
//	{
//		if (ch == ' ')
//		{
//			s4 += "%20";
//		}
//		else
//		{
//			s4 += ch;
//		}
//	}
//	cout << s4 << endl;
//
//}
//
//void test_string3()
//{
//	/*string s1 = "hello world aaaaa gbbbb rrrrrrrr";
//	string s2 = "hello worldaaaaa";
//	cout << s1.size() << endl;
//	cout << s1.capacity() << endl;
//	cout << s2.size() << endl;
//	cout << s2.capacity() << endl;*/
//
//	string s1 = "test.cpp";
//	string s2 = "test.cpp.zip";
//
//	size_t pos1 = s1.rfind('.');
//	size_t pos2 = s2.rfind('.');
//	string s3 = s1.substr(pos1);
//	string s4 = s2.substr(pos2);
//
//	cout << s3 << endl;
//	cout << s4 << endl;
//
//}
//
//int main()
//{
//	test_string3();
//	return 0;
//}

//void test_string1()
//{
//	string s1("hello world");
//	cout << s1 << endl;
//	sort(s1.begin(), s1.end());
//	cout << s1 << endl;
//}
//
//void test_string2()
//{
//	string s1("hello world");
//	cout << s1 << endl;
//
//	s1.push_back('x');
//	cout << s1 << endl;
//
//	s1.append("yyyyyyyyyy");
//	cout << s1 << endl;
//
//	string s2 = "hahahaha";
//	s1 += s2;
//	cout << s1 << endl;
//}
//
//
//int main()
//{
//	test_string2();
//	return 0;
//}

//void test_string1()
//{
//	/*list<int> s1;
//	s1.push_back(1);
//	s1.push_back(2);
//	s1.push_back(3);
//
//	list<int>::iterator it1 = s1.begin();
//	while (it1 != s1.end())
//	{
//		cout << *it1 << " ";
//		++it1;
//	}
//	cout << endl;*/
//
//	string s1("hello world");
//	//遍历方法1
//	for (int i = 0; i < s1.size(); i++)
//	{
//		cout << s1[i] << " ";
//	}
//	cout << endl;
//
//	//遍历方法2
//	string::iterator it1 = s1.begin();
//	while (it1 != s1.end())
//	{
//		cout << *it1 << " ";
//		it1++;
//	}
//	cout << endl;
//
//	//遍历方法3
//	for (auto e : s1)
//	{
//		cout << e << " ";
//	}
//	cout << endl;
//}

//int main()
//{
//	test_string1();
//	return 0;
//}

//class Date
//{
//public:
//	Date(int year = 1)
//		:_year(year)
//	{
//		cout << "Date(int year = 1)" << endl;
//	}
//
//	~Date()
//	{
//		cout << "~Date()" << endl;
//	}
//
//private:
//	int _year;
//};

//void test_string1()
//{
//	//最重要的三个
//	string s1;//无参构造
//	string s2("hello world");//带参构造
//	string s3 = s2;//拷贝构造
//
//
//	//相对没那么重要
//	string s4(s3, 3, 5);//部份拷贝，从3位置开始往后拷贝5个
//	string s5(s3, 3);//没有标注要拷贝多少个的话，就是从3位置开始一直拷贝到结尾
//	string s6("hello world", 5);//拷贝前5个作为初始化
//	string s7(10, 'z');//用10个 z 来初始化
//
//
//	cout << s1 << endl;
//	cout << s2 << endl;
//	cout << s3 << endl;
//	cout << s4 << endl;
//	cout << s5 << endl;
//	cout << s6 << endl;
//	cout << s7 << endl;
//
//	/*cin >> s1;
//	cout << s1 << endl;*/
//
//}
//
//void test_string2()
//{
//	string s1("hello world");
//	string s2 = "hello world";
//	const string& s3 = "hello world";
//}
//
//void test_string3()
//{
//	string s1("hello world");
//	cout << s1.size() << endl;
//	cout << s1.length() << endl;
//}
//
////#include<assert.h>
////class Date
////{
////public:
////	char& operator[](int i)
////	{
////		assert(i < _size);
////		return _str[i];
////	}
////private:
////	char* _str;
////	size_t _capacity;
////	size_t _size;
////};
//
//void test_string4()
//{
//	string s1 = "hello world";
//	for (int i = 0; i < s1.size(); i++)
//	{
//		cout << s1[i] <<" ";
//	}
//	cout << endl;
//
//	for (int i = 0; i < s1.size(); i++)
//	{
//		cout << ++s1[i] << " ";
//	}
//	cout << endl;
//
//	/*string::iterator it1 = s1.begin();
//	while (it1 != s1.end())
//	{
//		cout << *it1 << " ";
//		++it1;
//	}
//	cout << endl;*/
//
//	string::iterator it1 = s1.begin();
//	while (it1 != s1.end())
//	{
//		cout << *it1 << " ";
//		++it1;
//	}
//	cout << endl;
//}


//int main()
//{
//	test_string4();
//	/*int* tmp = (int*)malloc(sizeof(int));
//	if (tmp == NULL)
//	{
//		perror("malloc fail");
//		return;
//	}
//	int* tmp = (int*)malloc(sizeof(int)*10);
//	Date s1;
//	Date* s2 = new Date(1);
//	delete s2;*/
//	//Date* s3 = new Date[10]{ 1, 3, 4};
//	return 0;
//}

//template<class T>
//class Date
//{
//public:
//	Date(T year = 1)
//		:_year(year)
//	{}
//
//	void Push(T data);
//
//private:
//	T _year;
//};
//
//template<class T>
//void Date<T>::Push(T data)
//{
//	;
//}
//
//int main()
//{
//	Date<int> s1;
//
//	return 0;
//}