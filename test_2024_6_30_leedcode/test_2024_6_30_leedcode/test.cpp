#include<iostream>
using namespace std;
#include<vector>
#include<string>
#include<queue>
#include<functional>

//class Solution {
//public:
//    unordered_map<char, unordered_set<char>> edges;
//    unordered_map<char, int> in; // 表示入度
//    bool cheak = false;
//
//    string alienOrder(vector<string>& words)
//    {
//        //初始化 + 建图
//        for (auto e : words)
//            for (auto ch : e)
//                in[ch] = 0;
//
//        int n = words.size();
//        for (int i = 0; i < n; i++)
//        {
//            for (int j = i + 1; j < n; j++)
//            {
//                add(words[i], words[j]);
//                if (cheak == true) return "";
//            }
//        }
//
//        // 拓扑排序
//        queue<char> q;
//        // 将入度为 0 的元素全部push到队列中
//        for (auto [a, b] : in)
//            if (b == 0) q.push(a);
//
//        string ret;
//        while (q.size())
//        {
//            char tmp = q.front(); q.pop();
//            ret += tmp;
//            for (auto t : edges[tmp])
//            {
//                if (--in[t] == 0) q.push(t);
//            }
//        }
//
//        // 判断是否有环，有则返回空
//        for (auto [a, b] : in)if (b != 0) return "";
//        return ret;
//    }
//
//    void add(string& s1, string& s2)
//    {
//        int n = min(s1.size(), s2.size());
//        int i = 0;
//        for (i = 0; i < n; i++)
//        {
//            if (s1[i] != s2[i])
//            {
//                char a = s1[i], b = s2[i];
//                if (edges.count(a) == 0 || edges[a].count(b) == 0)
//                {
//                    edges[a].insert(b);
//                    in[b]++;
//                }
//                break;
//            }
//        }
//        if (i == s2.size() && i < s1.size()) cheak = true;
//    }
//};

//class Solution {
//public:
//    vector<int> findOrder(int n, vector<vector<int>>& prerequisites)
//    {
//        if (n == 1) return { 0 };
//        unordered_map<int, vector<int>> hash;
//        vector<int> in(n);
//        vector<int> ret;
//
//        // 建图
//        for (auto e : prerequisites)
//        {
//            int a = e[0], b = e[1];
//            hash[b].push_back(a);
//            in[a]++;
//        }
//
//        // 将入度为 0 的元素全部都存入到队列中
//        queue<int> q;
//        for (int i = 0; i < n; i++)
//            if (in[i] == 0)q.push(i);
//
//        // BFS
//        while (q.size())
//        {
//            int t = q.front(); q.pop();
//            ret.push_back(t);
//            for (auto e : hash[t])
//            {
//                in[e]--;
//                if (in[e] == 0) q.push(e);
//            }
//        }
//
//        // end
//        for (auto e : in) if (e)return {};
//        return ret;
//    }
//};

//class Solution {
//public:
//    bool canFinish(int n, vector<vector<int>>& prerequisites)
//    {
//        unordered_map<int, vector<int>> edges;
//        vector<int> in(n);
//
//        // 建图
//        for (auto e : prerequisites)
//        {
//            int a = e[0], b = e[1];
//            edges[b].push_back(a);
//            in[a]++;
//        }
//
//        // 将入度为0的元素插入到队列中
//        queue<int> q;
//        for (int i = 0; i < n; i++)
//            if (in[i] == 0)q.push(i);
//
//        // BFS
//        while (q.size())
//        {
//            int t = q.front();
//            q.pop();
//            for (auto e : edges[t])
//            {
//                in[e]--;
//                if (in[e] == 0)q.push(e);
//            }
//        }
//
//        // 判断是否有环
//        for (auto e : in)if (e != 0)return false;
//        return true;
//    }
//};

//class Solution {
//public:
//    int dx[4] = { 0, 0, 1, -1 };
//    int dy[4] = { 1, -1, 0, 0 };
//
//    int maxDistance(vector<vector<int>>& nums)
//    {
//        int m = nums.size(), n = nums[0].size();
//        vector<vector<int>> vis(m, vector<int>(n, -1));
//        queue<pair<int, int>> q;
//        for (int i = 0; i < m; i++)
//            for (int j = 0; j < n; j++)
//                if (nums[i][j] == 1)
//                {
//                    q.push({ i, j });
//                    vis[i][j] = 0;
//                }
//        int ret = 0;
//        while (q.size())
//        {
//            auto [a, b] = q.front(); q.pop();
//            for (int i = 0; i < 4; i++)
//            {
//                int x = a + dx[i], y = b + dy[i];
//                if (x >= 0 && x < m && y >= 0 && y < n && vis[x][y] == -1 && nums[x][y] == 0)
//                {
//                    q.push({ x, y });
//                    vis[x][y] = vis[a][b] + 1;
//                    ret = max(ret, vis[x][y]);
//                }
//            }
//        }
//        if (ret == 0)return -1;
//        return ret;
//    }
//};

//class Solution {
//public:
//    int dx[4] = { 0, 0, 1, -1 };
//    int dy[4] = { 1, -1, 0, 0 };
//
//    vector<vector<int>> highestPeak(vector<vector<int>>& nums)
//    {
//        int m = nums.size(), n = nums[0].size();
//        vector<vector<int>> ret(m, vector<int>(n, -1));
//        queue<pair<int, int>> q;
//        for (int i = 0; i < m; i++)
//            for (int j = 0; j < n; j++)
//                if (nums[i][j] == 1)
//                {
//                    ret[i][j] = 0;
//                    q.push({ i, j });
//                }
//        while (q.size())
//        {
//            auto [a, b] = q.front();
//            q.pop();
//            for (int i = 0; i < 4; i++)
//            {
//                int x = a + dx[i], y = b + dy[i];
//                if (x >= 0 && x < m && y >= 0 && y < n && ret[x][y] == -1)
//                {
//                    ret[x][y] = ret[a][b] + 1;
//                    q.push({ x, y });
//                }
//            }
//        }
//        return ret;
//    }
//};

//class Solution {
//public:
//    bool vis[501][501] = { false };
//    int dx[4] = { 0, 0, 1, -1 };
//    int dy[4] = { 1, -1, 0, 0 };
//
//    int numEnclaves(vector<vector<int>>& nums)
//    {
//        int m = nums.size(), n = nums[0].size();
//        //找四条边上的0, 并插入到queue中
//        queue<pair<int, int>> q;
//        for (int i = 0; i < n; i++)
//        {
//            if (nums[0][i] == 1) q.push({ 0, i });
//            if (nums[m - 1][i] == 1) q.push({ m - 1, i });
//        }
//        for (int i = 0; i < m; i++)
//        {
//            if (nums[i][0] == 1) q.push({ i, 0 });
//            if (nums[i][n - 1] == 1) q.push({ i, n - 1 });
//        }
//        //将所有边上的数据全部 bfs，并将该位置的vis修改为true
//        while (q.size())
//        {
//            auto [a, b] = q.front();
//            q.pop();
//            for (int i = 0; i < 4; i++)
//            {
//                int x = a + dx[i], y = b + dy[i];
//                if (x >= 0 && x < m && y >= 0 && y < n && nums[x][y] == 1 && vis[x][y] == false)
//                {
//                    q.push({ x, y });
//                    vis[x][y] = true;
//                }
//            }
//        }
//        //统计不能走出单元格数据的数量
//        int ret = 0;
//        for (int i = 1; i < m - 1; i++)
//        {
//            for (int j = 1; j < n - 1; j++)
//            {
//                if (nums[i][j] == 1 && vis[i][j] == false)
//                {
//                    ret++;
//                }
//            }
//        }
//        return ret;
//    }
//};

//class Solution {
//public:
//    int dx[4] = { 0, 0, 1, -1 };
//    int dy[4] = { 1, -1, 0, 0 };
//
//    vector<vector<int>> updateMatrix(vector<vector<int>>& mat)
//    {
//        int m = mat.size(), n = mat[0].size();
//        vector<vector<int>> ret(m, vector<int>(n, -1));
//        queue<pair<int, int>> q;
//        for (int i = 0; i < m; i++)
//            for (int j = 0; j < n; j++)
//            {
//                if (mat[i][j] == 0)
//                {
//                    ret[i][j] = 0;
//                    q.push({ i, j });
//                }
//            }
//
//        while (q.size())
//        {
//            auto [a, b] = q.front();
//            q.pop();
//            for (int i = 0; i < 4; i++)
//            {
//                int x = a + dx[i];
//                int y = b + dy[i];
//                if (x >= 0 && x < m && y >= 0 && y < n && ret[x][y] == (-1))
//                {
//                    ret[x][y] = ret[a][b] + 1;
//                    q.push({ x, y });
//                }
//            }
//        }
//        return ret;
//    }
//};

int main()
{

	return 0;
}
