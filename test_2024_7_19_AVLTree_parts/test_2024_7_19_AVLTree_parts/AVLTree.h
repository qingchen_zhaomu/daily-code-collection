#pragma once
#include<iostream>
using namespace std;
#include<string>
#include<vector>
#include<assert.h>

template<class k, class v>
struct AVLTreeNode
{
	pair<k, v> _kv;
	AVLTreeNode<k, v>* _left;
	AVLTreeNode<k, v>* _right;
	AVLTreeNode<k, v>* _parent;
	int _bf;

	AVLTreeNode(const pair<k, v>& kv)
		:_kv(kv)
		,_left(nullptr)
		,_right(nullptr)
		,_parent(nullptr)
		,int _bf(0)
	{}
};

template<class k, class v>
class AVLTree
{
	typedef AVLTreeNode<k, v> Node;
public:
	AVLTree() = default;
	AVLTree(const AVLTree<k, v>& avl)
	{
		_root = copy(avl._root);
	}

	~AVLTree()
	{
		Destroy(_root);
		_root = nullptr;
	}

	bool insert(const pair<k, v>& kv)
	{
		if (_root == nullptr)
		{
			_root = new Node(kv);
			return true;
		}

		Node* cur = _root;
		Node* parent = nullptr;
		while (cur)
		{
			if (cur->_kv.first > kv.first)
			{
				parent = cur;
				cur = cur->_left;
			}
			else if (cur->_kv.first < kv.first)
			{
				parent = cur;
				cur = cur->_right;
			}
			else
			{
				return false;
			}
		}
		// 此时cur已经到了要插入的位置了
		cur = new Node(kv);
		if (parent->_left == cur)
		{
			parent->_left = cur;
		}
		else
		{
			parent->_right = cur;
		}
		// 更新平衡因子
		//
		while (parent)
		{
			if (parent->_left == cur)
				parent->_bf--;
			else
				parent->_bf++;

			if (parent->_bf == 0)
			{
				break;
			}
			else if (parent->_bf == 1 || parent->_bf == -1)
			{
				cur = parent;
				parent = parent->_parent;
			}
			else if (parent->_bf == 2 || parent->_bf == -2)
			{
				// 旋转
				if (parent->_bf == 2 && cur->_bf == 1)
				{
					// 左单旋
					RotateL(parent);
				}
				else if (parent->_bf == -2 && cur->_bf == -1)
				{
					// 右单旋
					RotateR(parent);
				}
				else if (parent->_bf == 2 && cur->_bf == -1)
				{
					// 右左双旋
					RotateRL(parent);
				}
				else
				{
					// 左右双旋
					RotateLR(parent);
				}

				break;
			}
			else
			{
				assert(false);
			}
		}

		return true;
	}

private:
	void RotateL(Node* parent)
	{
		Node* curR = parent->_right;
		Node* curRL = curR->_left;
		Node* parentParent = parent->_parent;

		parent->_right = curRL;
		curR->_left = parent;
		if (curRL)
			curRL->_parent = parent;
		parent->_parent = curR;

		if (parent == nullptr)
		{
			_root = curR;
		}
		else
		{
			if (parentParent->_left == parent)
			{
				parentParent->_left = curR;
			}
			else
			{
				parentParent->_right = curR;
			}
			curR->_parent = parentParent;
		}
		
		curR->_bf = parent->_bf = 0;
	}

	void RotateR(Node* parent)
	{
		Node* curL = parent->_left;
		Node* curLR = curL->_right;
		Node* parentParent = parent->_parent;

		parent->_left = curLR;
		curL->_right = parent;
		if (curLR)
			curLR->_parent = parent;
		parent->_parent = curL;
		
		if (parent == nullptr)
		{
			_root = curL;
		}
		else
		{
			if (parentParent->_left == parent)
			{
				parentParent->_left = curL;
			}
			else
			{
				parentParent->_right = curL;
			}
			curL->_parent = parentParent;
		}

		curL->_bf = parent->_bf = 0;
	}

	void RotateRL(Node* parent)
	{
		Node* curR = parent->_right;
		Node* curRL = curR->_left;
		int bf = curRL->_bf;

		RotateR(curR);
		RotateL(parent);

		// 更新平衡因子
		if (bf == 0)
		{
			parent->_bf = 0;
			curR->_bf = 0;
			curRL->_bf = 0;
		}
		else if (bf == 1)
		{
			parent->_bf = -1;
			curR->_bf = 0;
			curRL->_bf = 0;
		}
		else if (bf == -1)
		{
			parent->_bf = 0;
			curR->_bf = 1;
			curRL->_bf = 0;
		}
		else
		{
			assert(false);
		}
	}

	void RotateLR(Node* parent)
	{
		Node* curL = parent->_left;
		Node* curLR = curL->_right;
		int bf = curLR->_bf;

		RotateL(curL);
		RotateR(parent);

		// 更新平衡因子
		if (bf == 0)
		{
			parent->_bf = 0;
			curL->_bf = 0;
			curLR->_bf = 0;
		}
		else if (bf == 1)
		{
			parent->_bf = 0;
			curL->_bf = -1;
			curLR->_bf = 0;
		}
		else if (bf == -1)
		{
			parent->_bf = 1;
			curL->_bf = 0;
			curLR->_bf = 0;
		}
		else
		{
			assert(false);
		}
	}

	Node* copy(Node* root)
	{
		if (!root)return nullptr;
		root->_left = copy(root->_left);
		root->_right = copy(root->_right);
		return new Node(root->_kv);
	}
	void Destroy(Node& root)
	{
		if (!root)return;
		Destroy(root->_left);
		Destroy(root->_right);
		delete root;
	}

private:
	Node* _root = nullptr;
};

