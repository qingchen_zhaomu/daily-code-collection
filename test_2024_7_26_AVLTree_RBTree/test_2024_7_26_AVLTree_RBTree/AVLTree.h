#pragma once
#include<iostream>
using namespace std;
#include<vector>
#include<string>
#include<assert.h>

template<class k, class v>
struct AVLTreeNode
{
	AVLTreeNode<k, v>* _left;
	AVLTreeNode<k, v>* _right;
	AVLTreeNode<k, v>* _parent;
	int _bf;
	pair<k, v> _kv;

	AVLTreeNode(const pair<k, v>& kv)
		:_kv(kv)
		, _left(nullptr)
		, _right(nullptr)
		, _parent(nullptr)
		, _bf(0)
	{}
};

template<class k, class v>
class AVLTree
{
	typedef AVLTreeNode<k, v> Node;
public:
	AVLTree() = default;
	AVLTree(const AVLTree<k, v>& avl)
	{
		_root = copy(avl._root);
	}

	AVLTree<k, v>& operator=(AVLTree<k, v> kv)
	{
		swap(_root, kv._root);
		return *this;
	}

	~AVLTree()
	{
		Destroy(_root);
		_root = nullptr;
	}

	bool Insert(const pair<k, v>& kv)
	{
		if (_root == nullptr)
		{
			_root = new Node(kv);
			return true;
		}
		Node* cur = _root;
		Node* parent = nullptr;

		while (cur)
		{
			if (cur->_kv.first < kv.first)
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (cur->_kv.first > kv.first)
			{
				parent = cur;
				cur = cur->_left;
			}
			else
			{
				return false;
			}
		}
		cur = new Node(kv);
		if (cur->_kv.first < parent->_kv.first)
			parent->_left = cur;
		else
			parent->_right = cur;
		cur->_parent = parent;

		//更新平衡因子
		while (parent)
		{
			if (parent->_left == cur)
				parent->_bf--;
			else
				parent->_bf++;

			if (parent->_bf == 0)
				break;
			else if (parent->_bf == 1 || parent->_bf == -1)
			{
				cur = parent;
				parent = parent->_parent;
			}
			else if (parent->_bf == 2 || parent->_bf == -2)
			{
				if (parent->_bf == 2 && cur->_bf == 1)
				{
					//左单旋
					RotateL(parent);
				}
				else if (parent->_bf == -2 && cur->_bf == -1)
				{
					//右单旋
					RotateR(parent);
				}
				else if (parent->_bf == 2 && cur->_bf == -1)
				{
					//右左双旋
					RotateRL(parent);
				}
				else
				{
					//左右双旋
					RotateLR(parent);
				}
				break;
			}
			else
			{
				assert(false);
			}
		}
		return true;
	}

	void InOrder()
	{
		_InOrder(_root);
		cout << endl;
	}

	bool IsBalanceTree()
	{
		return _IsBalanceTree(_root);
	}

private:
	void RotateL(Node* parent)
	{
		Node* curR = parent->_right;
		Node* curRL = curR->_left;
		Node* parentParent = parent->_parent;

		parent->_right = curRL;
		curR->_left = parent;

		if (curRL)curRL->_parent = parent;
		parent->_parent = curR;

		if (parentParent == nullptr)
		{
			_root = curR;
			curR->_parent = nullptr;
		}
		else
		{
			if (parentParent->_left == parent)
			{
				parentParent->_left = curR;
			}
			else
			{
				parentParent->_right = curR;
			}
			curR->_parent = parentParent;
		}

		parent->_bf = curR->_bf = 0;
	}

	void  RotateR(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;

		parent->_left = subLR;
		if (subLR)
			subLR->_parent = parent;

		Node* parentParent = parent->_parent;

		subL->_right = parent;
		parent->_parent = subL;

		if (parentParent == nullptr)
		{
			_root = subL;
			subL->_parent = nullptr;
		}
		else
		{
			if (parent == parentParent->_left)
			{
				parentParent->_left = subL;
			}
			else
			{
				parentParent->_right = subL;
			}

			subL->_parent = parentParent;
		}
		parent->_bf = subL->_bf = 0;
	}

	void RotateRL(Node* parent)
	{
		Node* curR = parent->_right;
		Node* curRL = curR->_left;
		int bf = curRL->_bf;

		RotateR(curR);
		RotateL(parent);

		if (bf == 0)
		{
			curR->_bf = 0;
			curRL->_bf = 0;
			parent->_bf = 0;
		}
		else if (bf == 1)
		{
			curR->_bf = 0;
			curRL->_bf = 0;
			parent->_bf = -1;
		}
		else if (bf == -1)
		{
			curR->_bf = 1;
			curRL->_bf = 0;
			parent->_bf = 0;
		}
		else
		{
			assert(false);
		}
	}

	void RotateLR(Node* parent)
	{
		Node* curL = parent->_left;
		Node* curLR = curL->_right;
		int bf = curLR->_bf;

		RotateL(curL);
		RotateR(parent);

		// 更新平衡因子
		if (bf == 0)
		{
			parent->_bf = 0;
			curL->_bf = 0;
			curLR->_bf = 0;
		}
		else if (bf == 1)
		{
			parent->_bf = 0;
			curL->_bf = -1;
			curLR->_bf = 0;
		}
		else if (bf == -1)
		{
			parent->_bf = 1;
			curL->_bf = 0;
			curLR->_bf = 0;
		}
		else
		{
			assert(false);
		}
	}

	int _Height(Node* root)
	{
		if (root == nullptr)
			return 0;

		int leftHeight = _Height(root->_left);
		int rightHeight = _Height(root->_right);

		return leftHeight > rightHeight ? leftHeight + 1 : rightHeight + 1;
	}

	bool _IsBalanceTree(Node* root)
	{
		if (root == nullptr)return true;
		int lefth = _Height(root->_left);
		int righth = _Height(root->_right);
		int dif = righth - lefth;

		if (abs(dif) > 2)
		{
			cout << root->_kv.first << ":" << "高度不平衡" << endl;
			return false;
		}
		if (root->_bf != dif)
		{
			cout << root->_kv.first << ":" << "平衡因子" << endl;
			return false;
		}
		return _IsBalanceTree(root->_left) &&
			_IsBalanceTree(root->_right);
	}

	void _InOrder(Node* root)
	{
		if (root == nullptr)
			return;
		_InOrder(root->_left);
		cout << root->_kv.first << ":" << root->_kv.second << endl;
		_InOrder(root->_right);
	}

	void Destroy(Node* root)
	{
		if (!root)return;

		Destroy(root->_left);
		Destroy(root->_right);
		delete root;
	}

	Node* copy(Node* root)
	{
		if (root == nullptr)return nullptr;

		Node* newnode = new Node(root->_kv);

		newnode->_left = copy(root->_left);
		newnode->_right = copy(root->_right);
		return newnode;
	}

private:
	Node* _root;
};

void TestAVLTree()
{
	AVLTree<int, int> t;
	int a[] = { 16, 3, 7, 11, 9 };
	for (auto e : a)
	{
		t.Insert({ e, e });
	}

	t.InOrder();
	t.IsBalanceTree();
}