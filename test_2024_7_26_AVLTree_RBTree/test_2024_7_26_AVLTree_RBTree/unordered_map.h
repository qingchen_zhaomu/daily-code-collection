#pragma once
#include"RBTree.h"

namespace hjx
{
	template<class k, class v>
	class map
	{
		struct KeyOfT
		{
			const k& operator()(const pair<k, v>& kv)
			{
				return kv.first;
			}
		};
	public:
		typedef typename RBTree<k, pair<const k, v>, KeyOfT>::Iterator iterator;
		typedef typename RBTree<k, pair<const k, v>, KeyOfT>::ConstIterator const_iterator;
		
		iterator begin()
		{
			return _t.Begin();
		}
		iterator end()
		{
			return _t.End();
		}

		const_iterator begin()const
		{
			return _t.Begin();
		}
		const_iterator end()const
		{
			return _t.End();
		}

		pair<iterator, bool> insert(const pair<k, v>& kv)
		{
			return _t.Insert(kv);
		}

		v& operator[](const k& key)
		{
			pair<iterator, bool> ret = insert({key, v()});
			return ret.first->second;
		}

	private:
		RBTree<k, pair<const k, v>, KeyOfT> _t;
	};

	void test_map()
	{
		hjx::map<string, string> dict;
		dict.insert({ "sort", "����" });
		dict.insert({ "left", "���" });
		dict.insert({ "right", "�ұ�" });

		dict["left"] = "��ߣ�ʣ��";
		dict["insert"] = "����";
		dict["string"];

		auto it = dict.begin();
		while (it != dict.end())
		{
			// �����޸�first�������޸�second
			//it->first += 'x';
			it->second += 'x';

			cout << it->first << ":" << it->second << endl;
			++it;
		}
		cout << endl;
	}
}