#pragma once
#include"RBTree.h"

namespace hjx
{
	template<class k>
	class set
	{
		struct KeyOfT
		{
			const k& operator()(const k& kv)
			{
				return kv;
			}
		};
	public:
		typedef typename RBTree<k, const k, KeyOfT>::Iterator iterator;
		typedef typename RBTree<k, const k, KeyOfT>::ConstIterator const_iterator;

		iterator begin()
		{
			return _t.Begin();
		}
		iterator end()
		{
			return _t.End();
		}

		const_iterator begin()const
		{
			return _t.Begin();
		}
		const_iterator end()const
		{
			return _t.End();
		}

		pair<iterator, bool> insert(const k& kv)
		{
			return _t.Insert(kv);
		}

	private:
		RBTree<k, const k, KeyOfT> _t;
	};

	void test_set()
	{
		set<int> s;
		int a[] = { 4, 2, 6, 1, 3, 5, 15, 7, 16, 14 };
		for (auto e : a)
		{
			s.insert(e);
		}

		for (auto e : s)
		{
			cout << e << " ";
		}
		cout << endl;

		set<int>::iterator it = s.end();
		while (it != s.begin())
		{
			--it;

			cout << *it << " ";
		}
		cout << endl;

		it = s.begin();
		//*it += 10;
		while (it != s.end())
		{
			cout << *it << " ";
			++it;
		}
		cout << endl;

	}
}