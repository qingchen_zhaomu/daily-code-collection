#pragma once
#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
using namespace std;

namespace hjx
{
	class string
	{
	public:
		typedef char* iterator;
		typedef const char* const_iterator;

		string(const char* str = "");
		string(const string& s1);

		~string();

		size_t size()const;

		void swap(string& s1);

		iterator begin();
		iterator end();
		const_iterator begin()const;
		const_iterator end()const;

		string& operator=(string s1);

		void reserve(size_t len);
		void push_back(const char s);
		void append(const char* s1);

		string& operator+=(const char* s1);
		string& operator+=(const char s);

		void insert(size_t pos , const char s);
		void insert(size_t pos, const char* s1);
		void erase(size_t pos, size_t len = npos);

		const char* c_str()const;

		char& operator[](int pos);
		const char& operator[](int pos)const;

		size_t find(const char s, size_t pos = 0);
		size_t find(const char* s1, size_t pos = 0);

		bool operator<(const string& s) const;
		bool operator>(const string& s) const;
		bool operator<=(const string& s) const;
		bool operator>=(const string& s) const;
		bool operator==(const string& s) const;
		bool operator!=(const string& s) const;

		string substr(size_t pos = 0, size_t len = npos)const;

		void clean();

	private:
		char* _str = nullptr;
		size_t _size = 0;
		size_t _capacity = 0;

		static size_t npos;
	};

	void swap(string& s1, string& s2);
	ostream& operator<<(ostream& out, const string& s1);
	istream& operator>>(istream& in, string& s1);

}
