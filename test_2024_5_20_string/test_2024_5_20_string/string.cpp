#include"string.h"

namespace hjx
{
	size_t string::npos = -1;

	string::string(const char* str)
		:_size(strlen(str))
	{
		_str = new char[_size + 1];
		_capacity = _size;

		strcpy(_str, str);
	}

	string::string(const string& s1)
	{
		//老实人做法
		/*_str = new char[s1._capacity + 1];
		_capacity = s1._capacity;
		_size = s1._size;

		strcpy(_str, s1._str);*/

		string tmp(s1._str);
		swap(tmp);
	}

	size_t string::size()const
	{
		return _size;
	}

	void string::swap(string& s1)
	{
		std::swap(_str, s1._str);
		std::swap(_size, s1._size);
		std::swap(_capacity, s1._capacity);
	}

	void swap(string& s1, string& s2)
	{
		s1.swap(s2);
	}

	string::iterator string::begin()
	{
		return _str;
	}
	string::iterator string::end()
	{
		return _str + size();
	}

	string::const_iterator string::begin()const
	{
		return _str;
	}
	string::const_iterator string::end()const
	{
		return _str + size();
	}

	string::~string()
	{
		if (_str != nullptr)
		{
			delete[] _str;
			_capacity = _size = 0;
			_str = nullptr;
		}
	}


	string& string::operator=(string s1)
	{
		swap(s1);
		return *this;
	}

	const char* string::c_str()const
	{
		return _str;
	}


	void string::reserve(size_t len)
	{
		if (len > _capacity)
		{
			char* tmp = new char[len + 1];
			strcpy(tmp, _str);
			delete[]_str;
			_str = tmp;
			_capacity = len;
		}
	}

	void string::push_back(const char s)
	{
		insert(size(), s);
	}
	void string::append(const char* s1)
	{
		insert(size(), s1);
	}

	string& string::operator+=(const char s)
	{
		push_back(s);
		return *this;
	}
	string& string::operator+=(const char* s1)
	{
		append(s1);
		return *this;
	}

	void string::insert(size_t pos, const char s)
	{
		if (_size == _capacity)
		{
			size_t new_capacity = _capacity == 0 ? 4 : 2 * _capacity;
			reserve(new_capacity);
		}

		for (size_t i = _size + 1; i > pos; i--)
			_str[i] = _str[i - 1];
	
		_str[pos] = s;
		_size++;
	}

	void string::insert(size_t pos, const char* s1)
	{
		size_t len = strlen(s1);
		if (len + _size > _capacity)
			reserve(len + _size);

		for (size_t i = _size + len - 1; i > pos + len - 1; i--)
		{
			_str[i] = _str[i - len];
		}

		memcpy(_str + pos, s1, len);
		_size += len;
	}

	void string::erase(size_t pos, size_t len)
	{
		/*for (size_t i = pos + len; i < _size; i++)
		{
			_str[i - len] = _str[i];
		}
		_size -= len;*/

		if (len > _size - pos - 1)
		{
			_str[pos] = '\0';
			_size = pos;
		}
		else
		{
			strcpy(_str + pos, _str + pos + len);
			_size -= len;
		}
	}

	char& string::operator[](int pos)
	{
		return _str[pos];
	}
	const char& string::operator[](int pos)const
	{
		return _str[pos];
	}

	size_t string::find(const char s, size_t pos)
	{
		size_t end = size();
		for (size_t i = pos; i < end; i++)
		{
			if (_str[i] == s)
				return i;
		}
		return npos;
	}
	size_t string::find(const char* s1, size_t pos)
	{
		char* position = strstr(_str + pos, s1);
		return position - _str;
	}

	bool string::operator<(const string& s) const
	{
		return strcmp(_str, s._str) < 0;
	}

	bool string::operator>(const string& s) const
	{
		return !(*this <= s);
	}

	bool string::operator<=(const string& s) const
	{
		return *this < s || *this == s;
	}

	bool string::operator>=(const string& s) const
	{
		return !(*this < s);
	}

	bool string::operator==(const string& s) const
	{
		return strcmp(_str, s._str) == 0;
	}

	bool string::operator!=(const string& s) const
	{
		return !(*this == s);
	}


	string string::substr(size_t pos, size_t len)const
	{
		if (len > _size - pos)
		{
			string tmp(_str + pos);
			return tmp;
		}
		else
		{
			string tmp;
			tmp.reserve(len+1);
			memcpy(tmp._str, _str + pos, len);
			tmp._str[len + 1] = '\0';
			tmp._size =  len;
			/*for (size_t i = pos; i < pos + len; i++)
			{
				tmp += _str[i];
			}*/
			return tmp;
		}
	}

	void string::clean()
	{
		_str[0] = '\0';
		_size = 0;
	}

	ostream& operator<<(ostream& out, const string& s1)
	{
		for (int i = 0; i < s1.size(); i++)
		{
			out << s1[i];
		}
		return out;
	}
	/*istream& operator>>(istream& in, string& s1)
	{
		s1.clean();
		char ch = in.get();
		while (ch != ' ' && ch != '\0')
		{
			s1 += ch;
			ch = in.get();
		}
		return in;
	}*/

	istream& operator>>(istream& in, string& s1)
	{
		s1.clean();
		char buff[128];
		int i = 0;

		char ch = in.get();
		while (ch != ' ' && ch != '\0')
		{
			buff[i++] = ch;
			if (i == 127)
			{
				buff[i] = '\0';
				s1 += buff;
				i = 0;
			}

			ch = in.get();

		}
		if (i != 0)
		{
			buff[i] = '\0';
			s1 += buff;
		}

		return in;
	}

}
