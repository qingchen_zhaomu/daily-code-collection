//#pragma once
//#include<iostream>
//using namespace std;
//#include<assert.h>
//
//namespace hjx
//{
//	template<class T>
//	struct ListNode
//	{
//		ListNode(const T& x)
//			:_next(nullptr)
//			, _prev(nullptr)
//			, _data(x)
//		{}
//
//		ListNode<T>* _next;
//		ListNode<T>* _prev;
//		T _data;
//	};
//
//	template<class T, class Ref, class Ptr>
//	struct ListIterator
//	{
//		typedef ListNode<T> Node;
//		typedef ListIterator<T, Ref, Ptr> Self;
//
//		Node* _node;
//
//		ListIterator(Node* node)
//			:_node(node)
//		{}
//
//		Self& operator++()
//		{
//			_node = _node->_next;
//			return *this;
//		}
//
//		Self& operator--()
//		{
//			_node = _node->_prev;
//			return *this;
//		}
//
//		Self& operator++(int)
//		{
//			Self tmp(*this);
//			_node = _node->_next;
//			return tmp;
//		}
//
//		Self& operator--(int)
//		{
//			Self tmp(*this);
//			_node = _node->_prev;
//			return tmp;
//		}
//
//		Ref operator*()
//		{
//			return _node->_data;
//		}
//
//		Ptr operator->()
//		{
//			return &_node->_data;
//		}
//
//		bool operator!=(const Self& it)
//		{
//			return _node != it._node;
//		}
//
//		bool operator==(const Self& it)
//		{
//			return _node == it._node;
//		}
//
//	};
//
//	template<class T>
//	class list
//	{
//		typedef ListNode<T> Node;
//
//	public:
//		typedef ListIterator<T, T&, T*> iterator;
//		typedef ListIterator<T, const T&, const T*> const_iterator;
//
//		iterator begin()
//		{
//			return iterator(_head->_next);
//		}
//
//		iterator end()
//		{
//			return iterator(_head);
//		}
//
//		const_iterator begin()const
//		{
//			return const_iterator(_head->_next);
//		}
//
//		const_iterator end()const
//		{
//			return const_iterator(_head);
//		}
//
//
//		// 初始化函数（创建哨兵位）
//
//		void empty_init()
//		{
//			_head = new Node(0);
//			_head->_next = _head;
//			_head->_prev = _head;
//		}
//
//		list()
//		{
//			empty_init();
//		}
//
//		list(initializer_list<T> il)
//		{
//			empty_init();
//
//			for (auto& e : il)
//			{
//				push_back(e);
//			}
//		}
//
//		//拷贝构造
//		list(const list<T>& lt)
//		{
//			empty_init();
//
//			for (auto e : lt)
//			{
//				push_back(e);
//			}
//		}
//
//		list<T>& operator=(list<T> lt)
//		{
//			swap(_head, lt._head);
//			return *this;
//		}
//
//		~list()
//		{
//			auto it = begin();
//			while (it != end())
//			{
//				it = erase(it);
//			}
//
//			delete _head;
//			_head = nullptr;
//		}
//
//		void push_back(const T& x)
//		{
//			/*Node* newnode = new Node(T());
//			Node* tail = _head->_prev;
//
//			//插入主逻辑
//			newnode->_next = _head;
//			newnode->_prev = tail;
//			tail->_next = newnode;
//			_head->_prev = newnode;*/
//
//			insert(end(), x);
//		}
//
//		void pop_back()
//		{
//			erase(--end());
//		}
//
//		void push_front(const T& x)
//		{
//			insert(begin(), x);
//		}
//
//		void pop_front()
//		{
//			erase(begin());
//		}
//
//		//insert没有迭代器失效
//		iterator insert(iterator pos, const T& x)
//		{
//			Node* cur = pos._node;
//			Node* prev = cur->_prev;
//			Node* newnode = new Node(x);
//
//			newnode->_next = cur;
//			newnode->_prev = prev;
//			prev->_next = newnode;
//			cur->_prev = newnode;
//
//			return iterator(newnode);
//		}
//
//		//erase有迭代器失效
//		iterator erase(iterator pos)
//		{
//			assert(pos != end());
//
//			Node* cur = pos._node;
//			Node* prev = cur->_prev;
//			Node* next = cur->_next;
//
//			prev->_next = next;
//			next->_prev = prev;
//
//			delete cur;
//
//			return iterator(next);
//		}
//
//
//	private:
//		Node* _head;
//	};
//}
//
//
//
//int main()
//{
//	hjx::list<int> mylist = { 1,2,3,4,5 };
//	for (auto e : mylist)
//		cout << e << " ";
//	cout << endl;
//
//	mylist.push_back(10);
//	mylist.push_back(110);
//	mylist.push_front(20);
//	mylist.pop_front();
//	mylist.pop_back();
//
//	for (auto e : mylist)
//		cout << e << " ";
//	cout << endl;
//
//	return 0;
//}


#include <iostream>
#define SIZE 5  

class Queue {
private:
    int arr[SIZE];
    int front, rear;

public:
    Queue() {
        front = -1;
        rear = -1;
    }

    void EnQueue(int value) {
        if (rear == SIZE - 1) {
            std::cout << "Queue is full\n";
        }
        else {
            if (front == -1) front = 0;
            arr[++rear] = value;
            std::cout << "Enqueued: " << value << std::endl;
        }
    }

    void DeQueue() {
        if (front == -1) {
            std::cout << "Queue is empty\n";
        }
        else {
            std::cout << "Dequeued: " << arr[front++] << std::endl;
            if (front > rear) {
                front = rear = -1; // Reset queue if empty
            }
        }
    }
    void Display() {
        if (front == -1) {
            std::cout << "Queue is empty\n";
        }
        else {
            std::cout << "Queue elements: ";
            for (int i = front; i <= rear; i++) {
                std::cout << arr[i] << " ";
            }
            std::cout << std::endl;
        }
    }
};

int main() {
    Queue q;
    q.EnQueue(10);
    q.EnQueue(20);
    q.EnQueue(30);
    q.Display();
    q.DeQueue();
    q.Display();
    q.DeQueue();
    q.DeQueue();
    q.DeQueue();
    return 0;
}
