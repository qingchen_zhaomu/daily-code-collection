#include"priority-queue.h"
#include<algorithm>

//int main()
//{
//	//priority_queue<int, vector<int>, less<int>> q1;
//	/*bit::priority_queue<int, vector<int>, bit::myless<int>> q1;
//	q1.push(1);
//	q1.push(2);
//	q1.push(3);
//	q1.push(4);
//	q1.push(5);
//	q1.push(6);
//
//	while (!q1.empty())
//	{
//		cout << q1.top();
//		q1.pop();
//	}*/
//
//
//	vector<int> v1({ 6,3,7,8,2,6,9,4,7,5 });
//	sort(v1.begin(), v1.end(), greater<int>());
//	for (auto e : v1)cout << e << " ";
//	cout << endl;
//
//	return 0;
//}


int f(int x, int y)
{
	return x + y;
}

struct F
{
	int operator()(int x, int y)
	{
		return x + y;
	}
};

struct test1
{
	test1(int rate):_rate(rate){}

	static int func1(int x, int y)
	{
		return x + y;
	}

	int func2(int x, int y)
	{
		return x + y;
	}

private:
	int _rate;
};

int main()
{
	function<int(int, int)> f1 = f;
	function<int(int, int)> f2 = F();
	function<int(int, int)> f3 = [](int x, int y) {return x + y; };

	cout << f1(1, 1) << endl;
	cout << f2(1, 1) << endl;
	cout << f3(1, 1) << endl;

	function<int(int, int)> f4 = &test1::func1;
	function<int(test1, int, int)> f5 = &test1::func2;
	function<int(test1*, int, int)> f6 = &test1::func2;

	cout << f4(1, 1) << endl;
	cout << f5(test1(1), 1, 1) << endl;
	cout << f6(&test1(1) ,1, 1) << endl;

	return 0;
}

