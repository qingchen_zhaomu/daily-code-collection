#include<iostream>
using namespace std;


template<class T>
class SmartPtr
{
public:
	SmartPtr(T* ptr = nullptr)
		:_ptr(ptr)
	{}

	~SmartPtr()
	{
		if (_ptr)delete _ptr;
	}

	T& operator*() { return *_ptr; }
	T* operator->() { return _ptr; }

private:
	T* _ptr;
};

struct Date
{
	int _year;
	int _month;
	int _day;

	Date(int year = 1, int month = 1, int day = 1)
		:_year(year)
		,_month(month)
		,_day(day)
	{}

	~Date()
	{
		cout << "~Date" << endl;
	}
};

//int main()
//{
//	auto_ptr<Date> p1(new Date);
//	auto_ptr<Date> p2(p1);
//	// 拷贝时，管理权限转移
//	// 此处的p1已经为空
//	p1->_month++;
//
//	return 0;
//}


int main()
{
	unique_ptr<Date> up1(new Date);
	// unique_ptr 不允许拷贝
	//unique_ptr<Date> up2(up1);

	unique_ptr<Date[]> up2(new Date[5]);

	// unique_ptr 不允许拷贝
	shared_ptr<Date> sp1(new Date);
	shared_ptr<Date> sp2(sp1);
	shared_ptr<Date> sp3(sp2);

	shared_ptr<Date[]> sp4(new Date[5]);


	shared_ptr<Date> sp5 = make_shared<Date>(2023, 9, 6);

	return 0;
}






