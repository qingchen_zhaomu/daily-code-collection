#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>

struct Stu1
{
	char a;
	int b;
	char c;
};

struct Stu2
{
	char a;
	char b;
	int c;
};

int main()
{
	printf("%d\n", sizeof(struct Stu1));
	printf("%d\n", sizeof(struct Stu2));
	return 0;
}

////正确做法
//typedef struct Stu
//{
//	int age;
//	struct Stu* next;
//}Stu;
//
////错误做法
//typedef struct Stu
//{
//	int age;
//	Stu* next;
//}Stu;


////情况1
//typedef struct
//{
//	char name[20];
//	char gender[6];
//	int age;
//}Stu;
//
//
////情况2
//struct Stu
//{
//	char name[20];
//	char gender[6];
//	int age;
//};

//int main()
//{
//	return 0;
//}


//struct student
//{
//	char name[20];
//	char gender[6];//male 或 female
//	int age;
//};
//
//int main()
//{
//	struct student a = { "张三","male",18 };
//	struct student b = { "王五","male",30 };
//
//	struct student c = { .gender = "female",.age = 12,.name = "静香" };
//	return 0;
//}