#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

int main()
{
	FILE* pf = fopen("data.txt", "r");
	if (pf==NULL)
	{
		perror("fopen");
		return 1;
	}
	int ch = fgetc(pf); printf("%c\n", ch);
	ch = fgetc(pf); printf("%c\n", ch);
	ch = fgetc(pf); printf("%c\n", ch);
	ch = fgetc(pf); printf("%c\n", ch);
	int n = ftell(pf);
	printf("%d\n", n);

	fseek(pf, 0, SEEK_SET);

	ch = fgetc(pf); printf("%c\n", ch);

	fclose(pf);
	pf = NULL;
	return 0;
}

//typedef struct S
//{
//	char name[20];
//	int age;
//	float score;
//}S;
//
//int main()
//{
//	S s = { "zhangsan",20,85.5f };
//	S tmp = { 0 };
//
//	char arr[100] = { 0 };
//	sprintf(arr, "%s %d %.1f", s.name, s.age, s.score);
//	printf(arr);
//	//sprintf就相当于把东西放到arr里去了
//
//	sscanf(arr, "%s %d %f", tmp.name, &(tmp.age), &(tmp.score));
//	printf("\n%s %d %.1f", tmp.name, tmp.age, tmp.score);
//	return 0;
//}