#include<iostream>
using namespace std;
#include<vector>
#include<algorithm>

//class Solution {
//public:
//    Solution() = default;
//    vector<int> twoSum(vector<int>& nums, int target)
//    {
//        int start = 0, end = nums.size() - 1;
//        vector<int> s1(2);
//        while (start < end)
//        {
//            if (nums[start] + nums[end] > target)
//                end--;
//            else if (nums[start] + nums[end] < target)
//                start++;
//            if (nums[start] + nums[end] == target)
//                break;
//        }
//        s1[0] = nums[start], s1[1] = nums[end];
//        return s1;
//    }
//};

//class Solution {
//public:
//    vector<int> twoSum(vector<int>& nums, int target)
//    {
//        vector<int> s2(nums);
//        int left, right;
//        sort(nums.begin(), nums.end());
//        int start = 0, end = nums.size() - 1;
//        vector<int> s1 = { 1, 1 };
//        while (start < end)
//        {
//            if (nums[start] + nums[end] > target)
//                end--;
//            else if (nums[start] + nums[end] < target)
//                start++;
//            if (nums[start] + nums[end] == target)
//                break;
//        }
//        for (int i = 0; i < nums.size(); i++)
//        {
//            if (s2[i] == nums[start])
//                left = i;
//            else if (s2[i] == nums[end])
//                right = i;
//        }
//        s1[0] = left, s1[1] = right;
//        return s1;
//    }
//};

//class Solution {
//public:
//    vector<int> twoSum(vector<int>& nums, int target)
//    {
//        vector<int> s2(nums);
//        int left, right;
//        sort(nums.begin(), nums.end());
//        int start = 0, end = nums.size() - 1;
//        vector<int> s1 = { 1, 1 };
//        while (start < end)
//        {
//            if (nums[start] + nums[end] > target)
//                end--;
//            else if (nums[start] + nums[end] < target)
//                start++;
//            if (nums[start] + nums[end] == target)
//                break;
//        }
//        for (int i = 0; i < nums.size(); i++)
//        {
//            if (s2[i] == nums[start])
//            {
//                left = i;
//                break;
//            }
//        }
//        for (int i = 0; i < nums.size(); i++)
//        {
//            if (s2[i] == nums[end] && left != i)
//            {
//                right = i;
//                break;
//            }
//        }
//        s1[0] = left, s1[1] = right;
//        return s1;
//    }
//};

//class Solution {
//public:
//    void Deduplication(vector<int>& nums, int& dir, int jud)
//    {
//        int before = dir;
//        if (jud == 1)
//            dir++;
//        else
//            dir--;
//        while (nums[dir] == nums[before])
//        {
//            if (jud == 1)
//                dir++;
//            else
//                dir--;
//        }
//    }
//    vector<vector<int>> threeSum(vector<int>& nums)
//    {
//        sort(nums.begin(), nums.end());
//        vector<vector<int>> s1;
//        for (int cur = 0; cur < nums.size() - 2;)
//        {
//            int left = cur + 1, right = nums.size() - 1;
//            while (left < right)
//            {
//                if (left != right && nums[left] + nums[right] < (-1) * nums[cur])
//                {
//                    Deduplication(nums, left, 1);
//                }
//                else if (left != right && nums[left] + nums[right] > (-1) * nums[cur])
//                {
//                    Deduplication(nums, right, 0);
//                }
//                if (left != right && nums[left] + nums[right] == (-1) * nums[cur])
//                {
//                    //vector<int> v1 = ;
//                    s1.push_back({ nums[cur], nums[left], nums[right] });
//                    Deduplication(nums, left, 1);
//                    Deduplication(nums, right, 0);
//                }
//            }
//            Deduplication(nums, cur, 1);
//        }
//        return s1;
//    }
//};
//
//class Solution {
//public:
//    vector<vector<int>> threeSum(vector<int>& nums)
//    {
//        vector<vector<int>> s1;
//
//        sort(nums.begin(), nums.end());
//
//        for (int cur = 0; cur < nums.size() - 2;)
//        {
//            if (nums[cur] > 0)break;
//            int left = cur + 1, right = nums.size() - 1;
//            while (left < right)
//            {
//                if ((nums[left] + nums[right]) < ((-1) * nums[cur]))
//                {
//                    left++;
//                }
//                else if ((nums[left] + nums[right]) > ((-1) * nums[cur]))
//                {
//                    right--;
//                }
//                else
//                {
//                    s1.push_back({ nums[cur], nums[left], nums[right] });
//                    left++, right--;
//                    while ((left < right) && nums[left] == nums[left - 1])left++;
//                    while ((left < right) && nums[right] == nums[right + 1])right--;
//                }
//            }
//            cur++;
//            while ((cur < nums.size() - 1) && nums[cur] == nums[cur - 1])cur++;
//        }
//        return s1;
//    }
//};

//class Solution {
//public:
//    vector<vector<int>> fourSum(vector<int>& nums, int target)
//    {
//        sort(nums.begin(), nums.end());
//        vector<vector<int>> s1;
//        if (nums.size() < 4)
//            return s1;
//        if (nums.size() == 4 && nums[0] + nums[1] + nums[2] + nums[3] == target)
//        {
//            s1.push_back({ nums[0], nums[1], nums[2], nums[3] });
//            return s1;
//        }
//        for (int OutsideNum = 0; OutsideNum < nums.size() - 4;)
//        {
//            for (int cur = OutsideNum + 1; cur < nums.size() - 2; )
//            {
//                int left = cur + 1, right = nums.size() - 1;
//                while (left < right)
//                {
//                    int sum = nums[left] + nums[right] + nums[cur];
//                    if (sum < target - nums[OutsideNum])left++;
//                    else if (sum > target - nums[OutsideNum])right--;
//                    else
//                    {
//                        s1.push_back({ nums[OutsideNum], nums[cur], nums[left], nums[right] });
//                        left++, right--;
//                        while (left < right && nums[left] == nums[left - 1])left++;
//                        while (left < right && nums[right] == nums[right + 1])right--;
//                    }
//                }
//                cur++;
//                while (cur < nums.size() - 2 && nums[cur] == nums[cur - 1])cur++;
//            }
//            OutsideNum++;
//            while (OutsideNum < nums.size() - 3 && nums[OutsideNum] == nums[OutsideNum - 1])OutsideNum++;
//        }
//        return s1;
//    }
//};

//class Solution {
//public:
//    vector<vector<int>> fourSum(vector<int>& nums, int target)
//    {
//        sort(nums.begin(), nums.end());
//        vector<vector<int>> s1;
//        if (nums.size() < 4)
//            return s1;
//        if (nums.size() == 4 && (long long)nums[0] + (long long)nums[1] + (long long)nums[2] + (long long)nums[3] == (long long)target)
//        {
//            s1.push_back({ nums[0], nums[1], nums[2], nums[3] });
//            return s1;
//        }
//        else if (nums.size() == 4)
//            return s1;
//
//        for (int OutsideNum = 0; OutsideNum < nums.size() - 3;)
//        {
//            for (int cur = OutsideNum + 1; cur < nums.size() - 2; )
//            {
//                int left = cur + 1, right = nums.size() - 1;
//                while (left < right)
//                {
//                    long long sum = (long long)nums[left] + (long long)nums[right] + (long long)nums[cur];
//                    if (sum < target - nums[OutsideNum])left++;
//                    else if (sum > target - nums[OutsideNum])right--;
//                    else
//                    {
//                        s1.push_back({ nums[OutsideNum], nums[cur], nums[left], nums[right] });
//                        left++, right--;
//                        while (left < right && nums[left] == nums[left - 1])left++;
//                        while (left < right && nums[right] == nums[right + 1])right--;
//                    }
//                }
//                cur++;
//                while (cur < nums.size() - 2 && nums[cur] == nums[cur - 1])cur++;
//            }
//            OutsideNum++;
//            while (OutsideNum < nums.size() - 3 && nums[OutsideNum] == nums[OutsideNum - 1])OutsideNum++;
//        }
//        return s1;
//    }
//};
//
//int main()
//{
//    Solution s1;
//    vector<int> s2 = { 1,0,-1,0,-2,2 };
//    vector<vector<int>> s3 = s1.fourSum(s2, 0);
//    for (int i = 0; i < s3.size(); i++)
//    {
//        for (auto e : s3[i])
//        {
//            cout << e << " ";
//        }
//        cout << endl;
//    }
//    cout << endl;
//	return 0;
//}

//class Solution {
//public:
//    int minSubArrayLen(int target, vector<int>& nums)
//    {
//        int count = 0, sum = 100000;
//        int left = 0, right = left + 1;
//        while (right < nums.size())
//        {
//            left = right - 1, count = 0;
//            count += nums[left];
//            while (right < nums.size())
//            {
//                count += nums[right];
//                
//                if (count >= target)
//                {
//                    if (right - left < sum)
//                        sum = right - left;
//                    right++;
//                    break;
//                }
//                right++;
//            }
//        }
//        return sum;
//    }
//};

class Solution {
public:
    int minSubArrayLen(int target, vector<int>& nums)
    {
        int count = 0, sum = 100000;
        int left = 0, right = left + 1;
        while (right < nums.size())
        {
            count = nums[left] + nums[right];
            if (count >= target)
            {
                if (sum < right - left - 1)
                {
                    sum = right - left - 1;
                }
            }
            count -= nums[left];
            if (count >= target)
            {
                left++;
            }
            else
            {
                right++;
            }
        }
        return sum;
    }
};

int main()
{
    vector<int> v1 = { 1,2,3,4,5 };
    Solution s1;
    int n = s1.minSubArrayLen(11, v1);

    cout << n << endl;

    return 0;
}