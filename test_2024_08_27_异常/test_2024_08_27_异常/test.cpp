#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<thread>
#include <ctime>
#include <ratio>
#include <chrono>
using namespace std;

// C语言的报错

//int main()
//{
//	FILE* fout = fopen("Test.txt", "r");
//	cout << fout << endl;
//	cout << errno << endl;
//	perror("open fail");
//	return 0;
//}



// C++的抛异常

//int Division(int a, int b)
//{
//	if (b == 0)
//	{
//		string s("Division by 0");
//		throw s;
//	}
//	else
//	{
//		return (a / b);
//	}
//}
//
//void func()
//{
//	int a, b;
//	cin >> a >> b;
//
//	try
//	{
//		cout << Division(a, b) << endl;
//	}
//	catch(const char* s)
//	{
//		cout << s << endl;
//	}
//	catch (int sz)
//	{
//		cout << sz << endl;
//	}
//
//	cout << "xxxxxxxxxxxxxxxxxxxxx" << endl;
//
//}
//
//int main()
//{
//	while (1)
//	{
//		try
//		{
//			func();
//		}
//		catch (const string& s)
//		{
//			cout << s << endl;
//		}
//		catch (...)
//		{
//			cout << "unknown exception" << endl;
//		}
//	}
//
//	return 0;
//}





// 常用异常继承体系

class Exception
{
public:
	Exception(const string& errmsg, int id)
		:_errmsg(errmsg)
		,_id(id)
	{}

	virtual string what() const
	{
		return _errmsg;
	}

	int getid()
	{
		return _id;
	}

protected:
	string _errmsg;
	int _id;
};


class SqlException : public Exception
{
public:
	SqlException(const string& errmsg, int id, const string& sql)
		:Exception(errmsg, id)
		,_sql(sql)
	{}

	virtual string what()
	{
		string str = "SqlException:";
		str += _errmsg;
		str += "->";
		str += _sql;

		return str;
	}
private:
	const string _sql;
};

class CacheException : public Exception
{
public:
	CacheException(const string& errmsg, int id)
		:Exception(errmsg, id)
	{}

	virtual string what() const
	{
		string str = "CacheException:";
		str += _errmsg;
		return str;
	}
};

class HttpServerException : public Exception
{
public:
	HttpServerException(const string& errmsg, int id, const string& type)
		:Exception(errmsg, id)
		, _type(type)
	{}

	virtual string what() const
	{
		string str = "HttpServerException:";
		str += _type;
		str += ":";
		str += _errmsg;
		return str;
	}

private:
	const string _type;
};

void SQLMgr()
{
	if (rand() % 7 == 0)
	{
		throw SqlException("权限不足", 100, "select * from name = '张三'");
	}

	cout << "调用成功" << endl;
}

void CacheMgr()
{
	if (rand() % 5 == 0)
	{
		throw CacheException("权限不足", 100);
	}
	else if (rand() % 6 == 0)
	{
		throw CacheException("数据不存在", 101);
	}
	SQLMgr();
}

//void seedmsg(const string& s)
//{
//	//cout << "void seedmsg(const string& s)" << endl;
//
//	//throw HttpServerException("网络不稳定，发送失败", 102, "put");
//
//	if (rand() % 2 == 0)
//	{
//		throw HttpServerException("网络不稳定，发送失败", 102, "put");
//	}
//	else if (rand() % 3 == 0)
//	{
//		throw HttpServerException("你已经不是对象的好友，发送失败", 103, "put");
//	}
//	else
//	{
//		cout << "发送成功" << endl;
//	}
//}

void HttpServer()
{
	if (rand() % 3 == 0)
	{
		throw HttpServerException("请求资源不存在", 100, "get");
	}
	else if (rand() % 4 == 0)
	{
		throw HttpServerException("权限不足", 101, "post");
	}

	// 失败以后，再重试3次
	/*for (size_t i = 0; i < 4; i++)
	{
		try
		{
			seedmsg("今天一起看电影吧");
			break;
		}
		catch (const Exception& e)
		{
			if (e.getid() == 102)
			{
				if (i == 3)
					throw e;

				cout << "开始第" << i + 1 << "重试" << endl;
			}
			else
			{
				throw e;
			}
		}
	}*/


	CacheMgr();
}


int main()
{
	srand(time(0));

	while (1)
	{
		// 线程相关的知识，意思是休息一秒再执行
		this_thread::sleep_for(chrono::seconds(1));

		try 
		{
			HttpServer();
		}
		catch (const Exception& e) // 这里捕获父类对象就可以
		{
			//using std::chrono::system_clock;
			//// 多态
			//system_clock::time_point today = system_clock::now();
			//std::time_t tt = system_clock::to_time_t(today);
			//cout << ctime(&tt) << e.what() << endl << endl;
			cout << e.what() << endl << endl;
		}
		catch (...)
		{
			cout << "Unkown Exception" << endl;
		}
	}

	return 0;
}




















