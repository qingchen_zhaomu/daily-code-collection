#define _CRT_SECURE_NO_WARNINGS 1
#pragma once
#include<iostream>
#include<assert.h>
using namespace std;

namespace hjx
{
	class string
	{
	public:

		typedef char* iterator;//这里typedef是为了不止用char*，还可以是其他类型
		iterator begin();
		iterator end();

		typedef const char* const_iterator;
		const_iterator begin()const;
		const_iterator end()const;

		//string();
		//全缺省写在.h文件里面
		string(const char* str = "");//构造
		string(const string& s);//拷贝构造
		string& operator=(string s);//赋值
		~string();//析构

		const char* c_str()const;

		size_t size()const;
		char& operator[](int pos);
		const char& operator[](int pos) const;

		void reserve(size_t n);
		void push_back(char s);
		void append(const char* s);

		string& operator+=(char s);
		string& operator+=(const char* s);

		void insert(int pos, char s);
		void insert(int pos, const char* s);
		void erase(size_t pos = 0, size_t len = npos);

		const static size_t npos;
		//const static size_t npos = -1;//特例

		size_t find(char s, size_t pos = 0);
		size_t find(const char* s, size_t pos = 0);

		void swap(string& s);

		string substr(size_t pos = 0, size_t len = npos);

		bool operator<(string& s)const;
		bool operator>(string& s)const;
		bool operator<=(string& s)const;
		bool operator>=(string& s)const;
		bool operator==(string& s)const;
		bool operator!=(string& s)const;

		void clear();

	private:
		char* _str = nullptr;
		size_t _capacity = 0;
		size_t _size = 0;
	};


	void swap(string& s1, string& s2);
	ostream& operator<<(ostream& out, const string& s);
	istream& operator>>(istream& in, string& s);
}
