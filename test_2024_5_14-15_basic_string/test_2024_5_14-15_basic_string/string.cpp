#include"string.h"

namespace hjx
{
	const size_t string::npos = -1;//定义

	//其实不用单独写无参，可以写全缺省
	//string::string()//默认的无参构造
	//{
	//	_str = new char[1] {'0'};
	//	_capacity = 0;
	//	_size = 0;
	//}

	//单写一个'\0'过不了，因为是char*，需要是字符串
	string::string(const char* str)//单写一个""，里面默认会有一个\0
		:_size(strlen(str))
	{
		_str = new char[_size + 1];
		_capacity = _size;

		strcpy(_str, str);
	}

	//string s3 = s1;
	string::string(const string& s)//拷贝构造
	{
		string tmp(s._str);
		swap(tmp);
	}

	string& string::operator=(string s)//赋值重载
	{
		swap(s);
		return *this;
	}

	string::~string()
	{
		delete[] _str;
		_str = nullptr;
		_size = _capacity = 0;
	}

	const char* string::c_str()const
	{
		return _str;
	}

	size_t string::size()const
	{
		return _size;
	}

	char& string::operator[](int pos)
	{
		assert(pos < _size);
		return _str[pos];
	}


	string::iterator string::begin()
	{
		return _str;
	}

	string::iterator string::end()
	{
		return _str + _size;
	}

	string::const_iterator string::begin()const
	{
		return _str;
	}
	string::const_iterator string::end()const
	{
		return _str + _size;
	}
	
	const char& string::operator[](int pos)const
	{
		assert(pos < _size);
		return _str[pos];
	}

	void string::reserve(size_t n)
	{
		if (_capacity < n)
		{
			char* tmp = new char[n + 1];
			strcpy(tmp, _str);
			delete[] _str;
			_str = tmp;
			_capacity = n;
		}
	}

	void string::push_back(char s)
	{
		if (_capacity == _size)
		{
			size_t new_capacity = _capacity == 0 ? 4 : 2 * _capacity;
			reserve(new_capacity);
		}
		_str[_size++] = s;
		_str[_size] = '\0';
	}

	void string::append(const char* str)
	{
		size_t n = strlen(str);
		if (n + _size > _capacity)
		{
			reserve(n + _size);
		}
		strcpy(_str + _size, str);
		_size += n;
	}

	string& string::operator+=(char s)
	{
		push_back(s);
		return *this;
	}

	string& string::operator+=(const char* s)
	{
		append(s);
		return *this;
	}


	void string::insert(int pos, char s)
	{
		if (_capacity == _size)
		{
			size_t new_capacity = _capacity == 0 ? 4 : 2 * _capacity;
			reserve(new_capacity);
		}

		size_t end = _size + 1;
		while (pos < end)//此处不能用=, 因为有size_t类型，所以不存在<0的情况
		{
			_str[end] = _str[end - 1];
			--end;
		}

		_str[pos] = s;
		_size++;

	}

	void string::insert(int pos, const char* s)
	{
		size_t len = strlen(s);
		if (len + _size > _capacity)
		{
			reserve(len + _capacity);
		}

		size_t end = _size + len;
		while (end > pos + len - 1)
		{
			_str[end] = _str[end - len];
			--end;
		}

		memcpy(_str + pos, s, len);
		_size += len;
	}

	void string::erase(size_t pos, size_t len )
	{
		assert(pos < _size);

		if (len >= _size - pos)
		{
			_str[pos] = '\0';
			_size = pos;
		}
		else
		{
			strcpy(_str + pos, _str + pos + len);
			_size -= pos;
		}
	}

	size_t string::find(char s, size_t pos)
	{
		for (size_t i = pos; i < _size; i++)
		{
			if (_str[i] == s)
				return i;
		}
		return npos;
	}

	size_t string::find(const char* s, size_t pos)
	{
		char* char_pos = strstr(_str + pos, s);
		return char_pos - _str;
	}

	//s1.swap(s2);
	void string::swap(string& s)
	{
		std::swap(_str, s._str);
		std::swap(_capacity, s._capacity);
		std::swap(_size, s._size);
	}

	string string::substr(size_t pos, size_t len)
	{
		if (len > _size - pos)
		{
			string ret(_str + pos);
			return ret;
		}
		else
		{
			string sub;
			sub.reserve(len);
			for (size_t i = pos; i < pos + len; i++)
			{
				sub += _str[i];
			}
			return sub;
		}
	}


	bool string::operator<(string& s)const
	{
		return strcmp(_str, s._str) < 0;
	}

	bool string::operator>(string& s)const
	{
		return !(*this <= s);
	}

	bool string::operator<=(string& s)const
	{
		return *this < s || *this == s;
	}
	bool string::operator>=(string& s)const
	{
		return !(*this < s);
	}

	bool string::operator==(string& s)const
	{
		return strcmp(_str, s._str) == 0;
	}

	bool string::operator!=(string& s)const
	{
		return !(*this == s);
	}


	void string::clear()
	{
		_str[0] = '\0';
		_size = 0;
	}

	void swap(hjx::string& s1, hjx::string& s2)
	{
		s1.swap(s2);
	}

	ostream& operator<<(ostream& out, const string& s)
	{
		for (int i = 0; i < s.size(); i++)
		{
			out << s[i];
		}
		return out;
	}

	//istream& operator>>(istream& in, string& s)
	//{
	//	s.clear();

	//	char buff[128];
	//	int i = 0;
	//	char ch = in.get();
	//	while (ch != ' ' && ch != '\0')
	//	{
	//		buff[i++] = ch;
	//		//s += ch;

	//		if (i == 127)
	//		{
	//			buff[i] = '\0';
	//			s += buff;
	//			i = 0;
	//		}

	//		ch = in.get();
	//	}

	//	if (i != 0)
	//	{
	//		buff[i] = '\0';
	//		s += buff;
	//	}

	//	return in;
	//}


	istream& operator>>(istream& in, string& str)
	{
		str.clear();
		char buff[128];
		char ch = in.get();
		int i = 0;

		while (ch != ' ' && ch != '\0')
		{
			buff[i++] = ch;

			if (i == 127)
			{
				buff[i] = '\0';
				str += buff;
				i = 0;
			}

			ch = in.get();
		}
		if (i != 0)
		{
			buff[i] = '\0';
			str += buff;
		}
		return in;
	}


}





