#include<stdio.h>
#include<stdlib.h>
#include<assert.h>

//静态顺序表
//#define N 100
//struct SeqList
//{
//	SLDataType a[N];
//	int size; 
//};


//动态顺序表
typedef int SLDataType;

typedef struct SeqList
{
	SLDataType* arr;
	int capacity;
	int size;
}SL;

//初始化和销毁
void SLInit(SL* ps);
void SLDestroy(SL* ps);
void SLPrint(SL* ps);//保持接口一致性

//顺序表的头部/尾部插入
void SLPushBack(SL* ps, SLDataType x);
void SLPushFront(SL* ps, SLDataType x);

//顺序表的头部/尾部删除
void SLPopBack(SL* ps);
void SLPopFront(SL* ps);

//指定位置插入数据
//指定位置删除数据
void SLInsert(SL* ps, int pos, SLDataType x);
void SLErase(SL* ps, int pos);
