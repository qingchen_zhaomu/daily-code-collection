#include<iostream>
using namespace std;
#include<vector>
#include<assert.h>

//全特化
//模板
template<class T>
bool myless(const T& x1, const T& x2)
{
    return x1 < x2;
}
//函数模板的特化(不推荐)
template<>
bool myless<int*>(int* const & x1, int* const & x2)
{
    return *x1 < *x2;
}
//直接另写一个函数(推荐)
bool myless(int* const & x1, int* const & x2)
{
    return *x1 < *x2;
}

//类模板
template<class T1, class T2>
class Date
{
    T1 _t1;
    T2 _t2;
public:
    Date() { cout << "Date<class T1, class T2>" << endl; }
};
//类模板——全特化
template<>
class Date<int, char>
{
    int _t1;
    char _t2;
public:
    Date() { cout << "Date<int, char>" << endl; }
};
//类模板——偏特化
template<class T1>
class Date<T1, int>
{
    T1 _t1;
    int _t2;
public:
    Date() { cout << "Date<T1, int>" << endl; }
};
//类模板——进一步偏特化
template<typename T1, typename T2>
class Date<T1*, T2*>
{
    T1 _t1;//如果传过来的是int*的类型，那么此时T就是int类型
    T2 _t2;
public:
    Date() { cout << "Date<T1*, T2*>" << endl; }
};

//class Solution {
//public:
//    int hammingWeight(int n)
//    {
//        int m = n, count = 0;
//        while (m)
//        {
//            //一次干掉一个1，直到该数变为0，循环结束
//            m &= (m - 1);
//            count++;
//        }
//        return count;
//    }
//};

//class Solution {
//public:
//    vector<int> singleNumber(vector<int>& nums)
//    {
//        int ornum = 0;
//        for (auto e : nums)ornum ^= e;
//        int tmp = (ornum == INT_MIN ? ornum : ornum & (-ornum));
//
//        int ty1 = 0, ty2 = 0;
//        for (auto e : nums)
//        {
//            if (e & tmp)ty1 ^= e;
//            else ty2 ^= e;
//        }
//        return { ty1, ty2 };
//    }
//};

//class Solution {
//public:
//    int singleNumber(vector<int>& nums)
//    {
//        int ret = 0, sum = 0;
//        for (int i = 0; i < 32; i++)
//        {
//            for (int j = 0; j < nums.size(); j++)
//            {
//                //将每一个的第i位全部相加
//                if ((nums[j] >> i) & 1)sum++;
//            }
//            if (sum % 3)
//            {
//                sum %= 3;
//                ret |= (1 << i);
//            }
//            sum = 0;
//        }
//        return ret;
//    }
//};

class Solution {
public:
    vector<vector<int>> matrixBlockSum(vector<vector<int>>& mat, int k)
    {
        //建立一个前缀和数组
        int m = mat.size(), n = mat[0].size();
        vector<vector<int>> dp(m + 1, vector<int>(n + 1));

        for (int i = 1; i <= m; i++)
            for (int j = 1; j <= n; j++)
                dp[i][j] = dp[i - 1][j] + dp[i][j - 1] - dp[i - 1][j - 1] + mat[i - 1][j - 1];

        vector<vector<int>> ret(m, vector<int>(n));
        for (int i = 0; i < m; i++)
        {
            for (int j = 0; j < n; j++)
            {
                //边界 左上
                int x1 = max(0, i - k) + 1; int y1 = max(0, j - k) + 1;
                int x2 = min(m - 1, i + k) + 1; int y2 = min(n - 1, j + k) + 1;
                ret[i][j] = dp[x2][y2] - dp[x1 - 1][y2] - dp[x2][y1 - 1] + dp[x1 - 1][y1 - 1];
            }
        }
        return ret;
    }
};

template<class T, size_t N>
class hjx
{
    int _a[N];
    int _top;
};

int main()
{
	//vector<vector<int>> ret(3);
    int i = 0;
    //cin >> i;
    hjx<int, 3> s1;
	return 0;
}