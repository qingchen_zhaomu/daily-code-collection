#define _CRT_SECURE_NO_WARNINGS 1

#include"SList.h"

//void SListTest01()
//{
//	//一般不会这样子去创建链表，这里只是为了展示链表的打印
//	SLTNode* node1 = (SLTNode*)malloc(sizeof(SLTNode));
//	node1->data = 1;
//	SLTNode* node2 = (SLTNode*)malloc(sizeof(SLTNode));
//	node2->data = 2;
//	SLTNode* node3 = (SLTNode*)malloc(sizeof(SLTNode));
//	node3->data = 3;
//	SLTNode* node4 = (SLTNode*)malloc(sizeof(SLTNode));
//	node4->data = 4;
//
//	node1->next = node2;
//	node2->next = node3;
//	node3->next = node4;
//	node4->next = NULL;
//
//	SLTNode* plist = node1;
//	SLTPrint(plist);
//}
void SListTest02()
{
	SLTNode* plist = NULL;
	SLTPushBack(&plist, 1);
	SLTPushBack(&plist, 2);
	SLTPushBack(&plist, 3);
	SLTPushBack(&plist, 4);
	SLTPrint(plist);
	SLTPushFrond(&plist, 100);
	SLTPushFrond(&plist, 200);
	SLTPrint(plist);
	SLTNode* newnode = SLTFind(&plist, 100);
	SLTInsert(&plist, newnode, 6);
	SLTPrint(plist);

	SLTInsertAfter(newnode, 7);
	SLTPrint(plist);

	SLTEraseAfter(newnode);
	SLTPrint(plist);
	SLTErase(&plist, newnode);
	SLTPrint(plist);
	SListDesTroy(&plist);
	SLTPrint(plist);
}

int main()
{
	//SListTest01();
	SListTest02();
	return 0;
}