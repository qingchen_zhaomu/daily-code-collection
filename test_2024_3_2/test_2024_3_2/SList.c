//#define _CRT_SECURE_NO_WARNINGS 1
//
//#include"SList.h"
//
////打印
//void SLTPrint(SLTNode* phead)
//{
//	SLTNode* pcur = phead;
//	while (pcur)
//	{
//		printf("%d->", pcur->data);
//		pcur = pcur->next;
//	}
//	printf("NULL\n");
//}
//
//
////链表的头插、尾插
//
//
//SLTNode* SLTBuyNode(SLTDataType x)
//{
//	SLTNode* newnode = (SLTNode*)malloc(sizeof(SLTNode));
//	newnode->data = x;
//	newnode->next = NULL;
//
//	return newnode;
//}
//
//
////尾插
//void SLTPushBack(SLTNode* pphead, SLTDataType x)
//{
//	//断言
//	assert(pphead);
//
//	SLTNode* newnode = SLTBuyNode(x);
//
//	//链表为空，新节点作为phead
//	if (pphead == NULL)//phead是否为空指针
//	{
//		pphead = newnode;
//		return;
//	}
//	//链表不为空，找尾节点
//	SLTNode* ptail = pphead;
//	while (ptail->next)
//	{
//		ptail = ptail->next;
//	}
//	//ptail就是尾节点
//	//又,nownode为NULL
//	ptail->next = newnode;
//}
//
//
////头插
//void SLTPushFrond(SLTNode** pphead, SLTDataType x)
//{
//	assert(pphead);
//
//	//开辟空间
//	SLTNode* newnode = SLTBuyNode(x);
//
//	//newnode   phead   newnode->next
//	newnode->next = *pphead;
//	*pphead = newnode;
//
//}
//
//
////链表的头删、尾删
//
////尾删
//
//void SLTPopBack(SLTNode** pphead)
//{
//	//链表不为空
//	assert(pphead && *pphead);
//
//	//链表只有一个节点
//	if ((*pphead)->next == NULL)
//	{
//		free(*pphead);
//		*pphead = NULL;
//		return;
//	}
//	//链表有多个节点
//	SLTNode* ptail = *pphead;
//	SLTNode* prev = NULL;
//	while (ptail->next)
//	{
//		prev = ptail;
//		ptail = ptail->next;
//	}
//	//此处ptail指向尾节点，prev指向尾节点的前一个节点
//	prev->next = NULL;
//	free(ptail);
//	ptail = NULL;
//}
//
////头删
//
//void SLTPopFrond(SLTNode** pphead)
//{
//	//不能传空链表
//	assert(pphead && *pphead);
//
//	SLTNode* next = (*pphead)->next;
//
//	free(*pphead);
//	*pphead = next;
//}
//
////查找
//SLTNode* SLTFind(SLTNode** pphead, SLTDataType x)
//{
//	assert(pphead && *pphead);
//
//	SLTNode* pcur = *pphead;
//	while (pcur)
//	{
//		if (pcur->data == x)
//		{
//			return pcur;
//		}
//		pcur = pcur->next;
//	}
//	//找不到数据x
//	return NULL;
//}
//
//
////在指定位置之前插入数据
//void SLTInsert(SLTNode** pphead, SLTNode* pos, SLTDataType x)
//{
//	assert(pphead && *pphead && pos);
//
//	if (pos == *pphead)
//	{
//		SLTPushFrond(pphead, x);
//		return;
//	}
//
//	SLTNode* newnode = SLTBuyNode(x);
//	SLTNode* prev = *pphead;
//	while (prev->next != pos)
//	{
//		prev = prev->next;
//	}
//	//prev指向pos的前一个节点
//	newnode->next = pos;
//	prev->next = newnode;
//}
//
////在指定位置之后插入数据
//void SLTInsertAfter(SLTNode* pos, SLTDataType x)
//{
//	assert(pos);
//
//	SLTNode* newnode = SLTBuyNode(x);
//
//	newnode->next = pos->next;
//	pos->next = newnode;
//}
//
////删除pos节点
//void SLTErase(SLTNode** pphead, SLTNode* pos)
//{
//	assert(pphead && *pphead && pos);
//
//	//判断一下pos是否为头节点
//	if (pos == *pphead)
//	{
//		//头删
//		SLTPopFrond(pphead);
//		return;
//	}
//
//	SLTNode* prev = *pphead;
//	while (prev->next != pos)
//	{
//		prev = prev->next;
//	}
//	prev->next = pos->next;
//	free(pos);
//	pos = NULL;
//}
//
////删除pos之后的节点
//void SLTEraseAfter(SLTNode* pos)
//{
//	assert(pos && pos->next);
//
//	SLTNode* del = pos->next;
//	pos->next = pos->next->next;
//	free(del);
//	del = NULL;
//}
//
////销毁链表
//void SListDesTroy(SLTNode** pphead)
//{
//	assert(pphead && *pphead);
//
//	SLTNode* pcur = *pphead;
//
//	while (pcur)
//	{
//		(*pphead) = (*pphead)->next;
//		free(pcur);
//		pcur = *pphead;
//	}
//
//}