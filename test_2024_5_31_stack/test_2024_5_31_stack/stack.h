#pragma once
#include<iostream>
using namespace std;
#include<assert.h>
#include<vector>
#include<list>

namespace hjx
{
	template<class T, class Container = vector<T>>
	class stack
	{
	public:

		iterator begin()
		{
			return _con.begin();
		}
		iterator end()
		{
			return _con.end();
		}

		void push(const T& x)
		{
			_con.push_back(x);
		}
		void pop()
		{
			_con.pop_back();
		}

		const T& top()
		{
			return _con.back();
		}
		bool empty()
		{
			return _con.empty();
		}

		size_t size()
		{
			return _con.size();
		}
	private:
		Container _con;
	};
}