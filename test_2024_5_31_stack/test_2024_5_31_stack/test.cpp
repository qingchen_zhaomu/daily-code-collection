#include"stack.h"

namespace hjx
{
	template<class T>
	void Print(const stack<T>& s1)
	{
		for (auto e : s1)cout << e << " ";
		cout << endl;
	}

	void test_mystack1()
	{
		stack<int> s1;
		s1.push(1);
		s1.push(2);
		s1.push(3);
		s1.push(4);
		s1.push(5);

		Print(s1);

		s1.pop();
		s1.pop();
		Print(s1);


	}

}

int main()
{
	hjx::test_mystack1();
	return 0;
}