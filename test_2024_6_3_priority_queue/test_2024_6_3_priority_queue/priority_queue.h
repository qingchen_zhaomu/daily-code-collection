#pragma once
#include<iostream>
using namespace std;
#include<assert.h>
#include<vector>

namespace hjx
{
	template<class T>
	class mygreater
	{
	public:
		bool operator()(const T& s1, const T& s2)
		{
			return s1 > s2;
		}
	};

	template<class T>
	class myless
	{
	public:

		bool operator()(const T& s1, const T& s2)
		{
			return s1 < s2;
		}
	};

	template<class T, class Container = vector<T>, class Compare = myless<T>>
	class priority_queue
	{
	public:
		priority_queue() = default;//默认构造函数的强制生成

		//template <class InputIterator>
		//priority_queue(InputIterator first, InputIterator last)
		//{
		//	while (first != last)
		//	{
		//		_con.push_back(*first);
		//		++first;
		//	}

		//	// 
		//	for (int i = (_con.size() - 1 - 1) / 2; i >= 0; i--)
		//	{
		//		adjust_down(i);
		//	}
		//}
		template <class InputIterator>
		priority_queue(InputIterator first, InputIterator last)
		{
			while (first != last)
			{
				push(*first);
				first++;
			}
		}

		void adjust_up(size_t child)
		{
			Compare comp;
			size_t parent = (child - 1) / 2;
			while (child > 0)
			{
				if (comp(_con[parent], _con[child]))
				{
					swap(_con[parent], _con[child]);
					child = parent;
					parent = (parent - 1) / 2;
				}
				else break;
			}
		}

		void adjust_down(size_t parent)
		{
			Compare comp;
			size_t child = parent * 2 + 1;

			while (child < _con.size())
			{
				if (child + 1 < _con.size() && comp(_con[child], _con[child + 1]))child++;

				if (comp(_con[parent], _con[child]))
				{
					swap(_con[parent], _con[child]);
					parent = child;
					child = child * 2 + 1;
				}
				else break;
			}
		}

		priority_queue(initializer_list<T> il)//初始化列表
		{
			for (auto e : il)push(e);
		}

		void push(const T& x)
		{
			_con.push_back(x);
			adjust_up(_con.size() - 1);
		}
		void pop()
		{
			swap(_con[0], _con[size() - 1]);
			_con.pop_back();
			adjust_down(0);
		}

		const T& top()
		{
			return _con[0];
		}
		size_t size()
		{
			return _con.size();
		}
		bool empty()
		{
			return _con.empty();
		}

	private:
		Container _con;
	};

}