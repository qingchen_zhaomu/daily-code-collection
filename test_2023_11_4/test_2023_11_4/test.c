#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<assert.h>
#include<math.h>
#include<windows.h>

void Print(char* a)
{
	printf("%s\n", a);
}
int main()
{
	void(*pf)(char*) = &Print;
	pf("hello world");
	return 0;
}

//int main()
//{
//	int i, j;
//	int arr[3][4] = { 0 };
//	for (i = 0; i < 3; i++)
//	{
//		for (j = 0; j < 4; j++)
//		{
//			scanf("%d", arr[i][j]);
//		}
//	}
//
//	return 0;
//}

//int main()
//{
//	int count = 0;
//	int arr[10] = { 1,2,3,4,-5,-6,7,8,9,10 };
//	//int* p = arr;
//	//int sz = sizeof(arr) / sizeof(arr[0]);
//	for (int i = 0; i < 10; i++)
//	{
//		if (arr[i] < 0)
//		{
//			count++;
//		}
//		//p++;
//	}
//	printf("%d\n", count);
//	return 0;
//}

//int main()
//{
//	int i = 0;
//	int count = 0;
//	int f = 0;
//	int salary;
//	float sum = 0.0;
//	scanf("%d", &f);
//	for (i = 0; i < f; i++)
//	{
//		scanf("%d", &salary);
//		if (salary > 20000)
//		{
//			count++;
//		}
//		sum += (float)salary;
//	}
//	printf("%f\n%d", sum / f, count);
//	return 0;
//}

//int f(int i)
//{
//	if (i > 1)
//	{
//		printf("%d*", i);
//		return i * f(i - 1);
//	}
//	else
//		printf("1");
//}
//
//int main()
//{
//	int i = 0;
//	scanf("%d", &i);
//	int c = f(i);
//	printf("=%d", c);
//	return 0;
//}

//int main()
//{
//	int i = 0;
//	int count = 0;
//	for (i = 101; i <= 200; i += 2)
//	{
//		int flag = 1;
//		int j = 0;
//		for (j = 2; j <= sqrt(i); j++)
//		{
//			if (i % j == 0)
//			{
//				flag = 0;
//				break;
//			}
//		}
//		if (flag == 1)
//		{
//			count++;
//			printf("%d ", i);
//		}
//	}
//	return 0;
//}

//int main()
//{
//	int i, count = 0;
//	for (i = 1000; i <= 2000; i++)
//	{
//		if ((i % 4==0 && i % 100 != 0) || i % 400 == 0)
//		{
//			printf("%d ", i);
//			count++;
//		}
//	}
//	printf("\n");
//	printf("%d\n", count);
//
//	return 0;
//}

//int Add(int a, int b)
//{
//	int z = 0;
//	z = a + b;
//	return z;
//}
//
//int main()
//{
//	int a = 10;
//	int b = 20;
//	int c = 0;
//	c = Add(a, b);
//	printf("%d\n", c);
//	return 0;
//}

//int main()
//{
//	int i;
//	int arr1[6] = { 1,2,1,2,1,2 };
//	int arr2[6] = { 0 };
//	int sz = sizeof(arr1) / sizeof(arr1[0]);
//	int* p = arr2;
//	for (i = 0; i < sz; i++)
//	{
//		if (arr1[i] % 2 == 1)
//		{
//			*p = arr1[i];
//			p++;
//		}
//	}
//	for (i = 0; i < sz; i++)
//	{
//		if (arr1[i] % 2 == 0)
//		{
//			*p = arr1[i];
//			p++;
//		}
//	}
//	for (i = 0; i < sz; i++)
//	{
//		printf("%d ", arr2[i]);
//	}
//	return 0;
//}

//int main()
//{
//	int arr[5] = { 1,2,3,4,5 };
//	int* p = arr;
//	int sz = sizeof(arr) / sizeof(arr[0]);
//	for (int i = 0; i < sz; i++)
//	{
//		printf("%d ", *(p + i));
//	}
//	printf("\n");
//	return 0;
//}

//void f(int i)
//{
//	if (i > 9)
//	{
//		f(i / 10);
//	}
//	printf("%d ", i % 10);
//}
//
//int main()
//{
//	int i = 0;
//	scanf("%d", &i);
//	f(i);
//	return 0;
//}

//int f(int i)
//{
//	if (i == 0)
//	{
//		return 1;
//	}
//	else
//	{
//		return i * f(i - 1);
//	}
//}
//
//int main()
//{
//	int i = 0;
//	scanf("%d", &i);
//	printf("%d\n", f(i));
//	return 0;
//}

//int Add_num(int i)
//{
//	if (i > 9)
//	{
//		return (i % 10) + Add_num(i / 10);
//	}
//	else
//		return i;
//}
//
//int main()
//{
//	int i = 0;
//	scanf("%d", &i);
//	printf("%d\n", Add_num(i));
//	return 0;
//}

//int f(int n, int k)
//{
//	if (k!=0)
//	{
//		return n * f(n, k - 1);
//	}
//}
//int main()
//{
//	int n = 0, k = 0;
//	scanf("%d", &n);
//	scanf("%d", &k);
//	printf("%d\n", f(n, k));
//	return 0;
//}

//int main()
//{
//	int i = 0;
//	scanf("%d", &i);
//	int a = 1, b = 1, sum = 0;
//	if (i == 1 || i == 2)
//	{
//		printf(" 1 ");
//	}
//	else
//	{
//		for (int j = 3; j <= i; j++)
//		{
//			sum = a + b;
//			a = b;
//			b = sum;
//		}
//		printf("%d\n", sum);
//	}
//	return 0;
//}

//int fbnq(int i)
//{
//	if (i == 1 || i == 2)
//		return 1;
//	else
//		return fbnq(i - 1) + fbnq(i - 2);
//}
//
//int main()
//{
//	int i = 0;
//	scanf("%d", &i);
//	printf("%d\n", fbnq(i));
//	return 0;
//}

//int main()
//{
//	int i = 0;
//	for (i = 1; i <= 100000; i++)
//	{		
//		int count = 1;
//		int ad = i;
//		while (ad/10)
//		{
//			count++;
//			ad = ad / 10;
//		}
//		ad = i;
//		int sum = 0;
//		int a = count;
//		while (ad)
//		{
//			count = a;
//			/*sum += pow(ad % 10, count);
//			ad = ad / 10;*/
//			int n = ad % 10;
//			int ret = 1;
//			while (count)
//			{
//				ret = n * ret;
//				count--;
//			}
//			sum += ret;
//			ad = ad / 10;
//		}
//		if (sum == i)
//		{
//			printf("%d ", i);
//		}
//	}
//	system("pause");
//	return 0;
//}

//int main()
//{
//	int i = 0, j = 0;
//	int k = 7;
//	for (i = 1; i <= 7; i++)
//	{
//		for (j = 0; j < i + 6; j++)
//		{
//			if (j < k - i)
//			{
//				printf(" ");
//			}
//			else
//			{
//				printf("*");
//			}
//		}
//		printf("\n");
//	}
//	k = 5;
//	for (i = 1; i <= 6; i++)
//	{
//		for (j = 1; j <= 7 + k; j++)
//		{
//			if (j <= i)
//			{
//				printf(" ");
//			}
//			else
//			{
//				printf("*");
//			}
//		}
//		k--;
//		printf("\n");
//	}
//	return 0;
//}

//int main()
//{
//	int can = 0, money = 20,i;
//	for (i = 0; i < 20; i++)
//	{
//		can++;
//	}
//	printf("%d\n", can/2 + i);
//	return 0;
//}

//int main()
//{
//	int i = 0;
//	do
//	{
//		scanf("%d", &i);
//		int n = i * i * i;
//		printf("%d\n", n);
//	} while (i);
//	return 0;
//}

//int Add(int x, int y)
//{
//	int z = 0;
//	z = x + y;
//	return z;
//}
//int main()
//{
//	int a = 10;
//	int b = 20;
//	int c = 0;
//	c = Add(a, b);
//	printf("%d\n", c);
//	return 0;
//}

//struct T
//{
//	double weight;
//	int age;
//};
//
//struct S
//{
//	int a;
//	char c;
//	struct T wa;
//	double h;
//	char arr[20];
//};
//int main()
//{
//	struct S s = { 100,'r',{48.5,18},3.14,"hehe" };
//	printf("%d %lf %d", s.a, s.h, s.wa.age);
//	return 0;
//}

////声明一个结构体类型
////此处假设声明一个学生
//
//struct stu
//{
//	//里面放的是成员变量
//	char name[20];//名字
//	char tele[12];//电话
//	char sex[10];//性别
//	int age;
//};
//int main()
//{
//	int i;//int是i的类型，说明i是一个整形
//	char c;//char是c的类型，说明c是一个字符
//	struct stu s1;//struct stu是s1的类型，说明s1是一个结构体
//
//	return 0;
//}

//void* my_memcpy(void* dest, const void* src, size_t num)
//{
//	assert(dest && src);
//	//断言,确保指针的有效性
//	void* ret = dest;
//	while (num--)//当num--到0的时候，条件为假，退出循环
//	{
//		*(char*)dest = *(char*)src;
//		++(char*)dest;
//		++(char*)src;
//	}
//	return ret;
//}
//int main()
//{
//	int arr1[] = { 1,2,3,4,5 };
//	int arr2[5] = { 0 };
//	void* p = my_memcpy(arr2, arr1, sizeof(arr1));
//	//(int*)p;
//	int sz = sizeof(arr1) / sizeof(arr1[0]);
//	for (int i = 0; i < sz; i++)
//	{
//		printf("%d ", *((int*)p+i));
//	}
//	return 0;
//}
