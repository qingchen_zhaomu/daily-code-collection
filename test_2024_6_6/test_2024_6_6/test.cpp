#include<iostream>
using namespace std;
#include<vector>

class Solution {
public:
    string convert(string s, int numRows)
    {
        if (numRows == 1)return s;
        string ret;
        int len = s.size();
        int d = 2 * numRows - 2;
        //第一行
        for (int i = 0; i < len; i += d)
            ret += s[i];
        //第二行到最后一行的前一行
        for (int k = 1; k < numRows - 1; k++)
        {
            for (int i = k, j = d - i; i < len || j < len; i += d, j += d)
            {
                if (i < len)ret += s[i];
                if (j < len)ret += s[j];
            }
        }
        //最后一行
        for (int i = numRows - 1; i < len; i += d)
        {
            ret += s[i];
        }

        return ret;
    }
};

//class Solution {
//public:
//    int findPoisonedDuration(vector<int>& nums, int duration)
//    {
//        int ret = 0;
//        for (int i = 0; i < nums.size() - 1; i++)
//        {
//            ret += min(nums[i + 1] - nums[i], duration);
//            // if(nums[i]+duration >= nums[i+1])
//            // {
//            //     ret += nums[i+1] - nums[i];
//            // }
//            // else 
//            // {
//            //     ret += duration;
//            // }
//        }
//        return ret + duration;
//    }
//};

//class Solution {
//public:
//    string modifyString(string str)
//    {
//        for (int i = 0; i < str.size(); i++)
//        {
//            if (str[i] == '?')
//            {
//                for (char ch = 'a'; ch <= 'z'; ch++)
//                {
//                    if ((i == 0 || ch != str[i - 1]) && (i == str.size() - 1 || ch != str[i + 1]))
//                    {
//                        str[i] = ch;
//                        break;
//                    }
//                }
//            }
//        }
//        return str;
//    }
//};

class solution {
public:
    vector<int> missingTwo(vector<int>& nums)
    {
        int x = 0;
        for (int i = 1; i <= nums.size() + 2; i++)x ^= i;
        for (auto e : nums)x ^= e;
        //现在ret存储的就是消失的两个数字^后的结果
        int ty1 = 0, ty2 = 0;
        int ornum = (x == INT_MIN ? x : x & (-x));
        for (int i = 1; i <= nums.size() + 2; i++)
        {
            if (i & ornum)ty1 ^= i;
            else ty2 ^= i;
        }
        for (auto e : nums)
        {
            if (e & ornum)ty1 ^= e;
            else ty2 ^= e;
        }
        return { ty1, ty2 };
    }
};

int main()
{

	return 0;
}