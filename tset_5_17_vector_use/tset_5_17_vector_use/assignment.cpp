#include<iostream>
using namespace std;
#include<string>
#include<algorithm>

void reserve_string(string& s1)
{
	size_t Start = 0;
	size_t End = s1.size() - 1;
	
	while (Start <= End)
		swap(s1[Start++], s1[End--]);
}

int main()
{
	string s1;
	getline(cin, s1);
	cout << "The string before reverse: " << s1 << endl;

	reserve_string(s1);
	cout << "The string after reverse:  " << s1 << endl;
	return 0;
}



//void Swap_two_number(int* n1, int* n2)
//{
//	int tmp = *n1;
//	*n1 = *n2;
//	*n2 = tmp;
//}
//
//int main()
//{
//	int num1, num2;
//	cout << "Please enter num1:";
//	cin >> num1;
//	cout << "Please enter num2:";
//	cin >> num2;
//
//	cout << "Two numbers before swap: " << num1 << " " << num2 << endl;
//
//	Swap_two_number(&num1, &num2);
//	cout << "Two numbers after swap : " << num1 << " " << num2 << endl;
//
//	return 0;
//}



