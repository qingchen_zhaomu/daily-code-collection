#include"Snake.h"

void SetPos(int x, int y)
{
	//获得句柄
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	COORD pos = { x,y };
	SetConsoleCursorPosition(handle, pos);
}


void WelcomeToGame()
{
	SetPos(35, 12);
	printf("欢迎来到贪吃蛇小游戏\n");
	SetPos(38, 20);
	system("pause");
	system("cls");

	SetPos(28, 10);
	printf("用↑↓←→来控制蛇的移动，A键是加速，D键是减速");
	SetPos(28, 11);
	printf("加速能得到更高的分数");
	SetPos(28, 12);
   	printf("按空格键可以暂停,按Esc键可以退出游戏");
	SetPos(38, 20);
	system("pause");
	system("cls");
}

void CreateMap()
{
	int i = 0;
	//上
	SetPos(0, 0);
	for (i = 0; i <= 56; i += 2)
	{
		wprintf(L"%lc", WALL);
	}
	//下
	SetPos(0, 26);
	for (i = 0; i <= 56; i += 2)
	{
		wprintf(L"%lc", WALL);
	}
	//左
	for (i = 1; i <= 25; i++)
	{
		SetPos(0, i);
		wprintf(L"%lc", WALL);
	}
	//右
	for (i = 1; i <= 25; i++)
	{
		SetPos(56, i);
		wprintf(L"%lc", WALL);
	}
}

void InitSnake(pSnake ps)
{
	int i = 0;
	pSnakeNode cur = NULL;
	for (i = 0; i < 5; i++)
	{
		cur = (pSnakeNode)malloc(sizeof(SnakeNode));
		if (cur == NULL)
		{
			perror("malloc fail!");
			return;
		}
		cur->x = POS_X + 2 * i;
		cur->y = POS_Y;
		cur->next = NULL;
		
		//头插
		if (ps->pSnake == NULL)
		{
			ps->pSnake = cur;
		}
		else
		{
			cur->next = ps->pSnake;
			ps->pSnake = cur;
		}
	}
	//打印蛇身
	
	cur = ps->pSnake;
	while (cur)
	{
		SetPos(cur->x, cur->y);
		wprintf(L"%lc", BODY);
		cur = cur->next;
	}

	ps->dir = RIGHT;
	ps->FoodWeight = 10;
	ps->pFood = NULL;
	ps->score = 0;
	ps->SleepTime = 200;
	ps->status = OK;
}

void CreateFood(pSnake ps)
{

	int x = 0, y = 0;
again:
	do
	{
		x = rand() % 53 + 2;
		y = rand() % 25 + 1;
	} while (x % 2 != 0);

	pSnakeNode cur = ps->pSnake;
	while (cur)
	{
		if (cur->x == x && cur->y == y)
		{
			goto again;
		}
		cur = cur->next;
	}
	
	pSnakeNode pFood = (pSnakeNode)malloc(sizeof(SnakeNode));
	if (pFood == NULL)
	{
		perror("malloc fail!");
		return;
	}
	pFood->x = x;
	pFood->y = y;

	ps->pFood = pFood;
	SetPos(x, y);
	wprintf(L"%lc", FOOD);
}

void GameStart(pSnake ps)
{
	system("mode con cols=100 lines=30");
	system("title 贪吃蛇");
	//隐藏光标
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_CURSOR_INFO CursorInfo;
	GetConsoleCursorInfo(handle, &CursorInfo);
	CursorInfo.bVisible = false;
	SetConsoleCursorInfo(handle, &CursorInfo);

	WelcomeToGame();

	CreateMap();

	InitSnake(ps);

	CreateFood(ps);

}

void PrintHelpInfo()
{
	SetPos(60, 12);
	printf("1.不能穿墙，不能咬到自己");
	SetPos(60, 14);
	printf("2.用 ↑.↓.←.→ 来控制蛇的移动");
	SetPos(60, 16);
	printf("3.A键是加速，D键是减速");

	SetPos(60, 18);
	printf("4.按空格键可以暂停,按Esc键可以退出游戏");
	SetPos(60, 20);
	printf("嘉鑫版");
}


int NextIsFood(pSnake ps, pSnakeNode pNext)
{
	if (pNext->x == ps->pFood->x && pNext->y == ps->pFood->y)
		return 1;
	else
		return 0;
}

void EatFood(pSnake ps, pSnakeNode pNext)
{
	pNext->next = ps->pSnake;
	ps->pSnake = pNext;

	pSnakeNode cur = ps->pSnake;

	while (cur)
	{
		SetPos(cur->x, cur->y);
		wprintf(L"%lc", BODY);
		cur = cur->next;
	}

	free(ps->pFood);

	CreateFood(ps);

}


void NotEatFood(pSnake ps, pSnakeNode pNext)
{
	pNext->next = ps->pSnake;
	ps->pSnake = pNext;



}


void SnakeMove(pSnake ps)
{
	pSnakeNode pNext = (pSnakeNode)malloc(sizeof(SnakeNode));
	if (pNext)
	{
		perror("malloc fail!");
		return;
	}
	pNext->next = NULL;
	switch (ps->dir)
	{
	case UP:
		pNext->x = ps->pSnake->x;
		pNext->y = ps->pSnake->y - 1;
		break;
	case DOWN:
		pNext->x = ps->pSnake->x;
		pNext->y = ps->pSnake->y + 1;
		break;
	case LEFT:
		pNext->x = ps->pSnake->x - 2;
		pNext->y = ps->pSnake->y;
		break;
	case RIGHT:
		pNext->x = ps->pSnake->x + 2;
		pNext->y = ps->pSnake->y;
		break;
	}
	if (NextIsFood(ps, pNext))
	{
		EatFood(ps, pNext);
	}
	else
	{
		NotEatFood(ps, pNext);
	}

	//判断撞墙
	//KillByWall(ps);

	//判断撞自己
	//KillBySelf(ps);

}


void pause()
{
	while (1)
	{
		Sleep(1000);
		if (KEY_PRESS(VK_SPACE))
		{
			break;
		}
	}
}

void GameRun(pSnake ps)
{
	PrintHelpInfo();

	do
	{
		SetPos(62, 9);
		printf("总分:%5d\n", ps->score);
		SetPos(62, 10);
		printf("食物的分值:%02d\n", ps->FoodWeight);

		if (KEY_PRESS(VK_UP) && ps->dir != DOWN)
		{
			ps->dir = UP;
		}
		else if (KEY_PRESS(VK_DOWN) && ps->dir != UP)
		{
			ps->dir = DOWN;
		}
		else if (KEY_PRESS(VK_LEFT) && ps->dir != RIGHT)
		{
			ps->dir = LEFT;
		}
		else if (KEY_PRESS(VK_RIGHT) && ps->dir != LEFT)
		{
			ps->dir = RIGHT;
		}
		else if (KEY_PRESS(VK_ESCAPE))
		{
			ps->status = ESC;
			break;
		}
		else if (KEY_PRESS(VK_SPACE))
		{
			pause();
		}
		else if (KEY_PRESS(0X41))
		{
			if (ps->SleepTime >= 80)
			{
				ps->SleepTime -= 30;
				ps->FoodWeight += 2;
			}
		}
		else if (KEY_PRESS(0X44))
		{
			if (ps->FoodWeight > 2)
			{
				ps->SleepTime += 30;
				ps->FoodWeight -= 2;
			}
		}

		//走一步
		SnakeMove(ps);

		//睡眠一下
		Sleep(ps->SleepTime);

	} while (ps->status==OK);

}



