#pragma once
#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>
#include<stdlib.h>
#include<windows.h>
#include<time.h>
#include<stdbool.h>
#include<locale.h>

#define WALL L'��'
#define BODY L'��'
#define FOOD L'��'

#define POS_X 24
#define POS_Y 5

#define KEY_PRESS(VK) ( GetAsyncKeyState(VK)&0x1 ? 1 : 0 )

enum GAME_STATUS
{
	OK=1,
	ESC,
	KILL_BY_SELF,
	KILL_BY_WALL
};

enum DIRECTION
{
	UP=1,
	DOWN,
	LEFT,
	RIGHT
};

typedef struct SnakeNode
{
	int x;
	int y;
	struct SnakeNode* next;
}SnakeNode, * pSnakeNode;

typedef struct Snake
{
	pSnakeNode pSnake;
	pSnakeNode pFood;
	int score;
	int FoodWeight;
	int SleepTime;
	enum DIRECTION dir;
	enum GAME_STATUS status;
}Snake,*pSnake;


void SetPos(int x,int y);

void GameStart(pSnake ps);

void InitSnake(pSnake ps);

void WelcomeToGame();

void CreateMap();

void CreateFood(pSnake ps);

void GameRun(pSnake ps);

void SnakeMove(pSnake ps);

int NextIsFood(pSnake ps, pSnakeNode pNext);

void EatFood(pSnake ps, pSnakeNode pNext);

void NotEatFood(pSnake ps, pSnakeNode pNext);

//void KillByWall(pSnake ps);

//void KillBySelf(pSnake ps);

//void GameEnd(pSnake ps);



