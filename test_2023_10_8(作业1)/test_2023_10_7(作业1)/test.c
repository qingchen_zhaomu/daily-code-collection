#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<string.h>

//int main()
//{
//	int a, b;
//	printf("请输入直角三角形的两直角边:");
//	scanf("%d %d", &a, &b);
//	printf("直角三角形的面积为:%d", a * b / 2);
//}
void menu()
{
	printf("#### 1.平均数   2.总分 ####\n");
}
float average(float a, float b, float c)
{
	return (a + b + c) / 3;
}
float sum(float a, float b, float c)
{
	return a + b + c;
}
int main()
{
	float a, b, c;
	int input = 0;
	float Average = 0;
	float Sum = 0;
	while (input != 1 && input != 2)
	{
		menu();
		printf("请选择要计算平均分还是总分:>");
		scanf("%d", &input);
		if (input == 1)
		{
			printf("请输入三科的分数:");
			scanf("%f %f %f", &a, &b, &c);
			Average = average(a, b, c);
			printf("平均分为:%f\n", Average);
		}
		else if (input == 2)
		{
			printf("请输入三科的分数:");
			scanf("%f %f %f", &a, &b, &c);
			Sum = sum(a, b, c);
			printf("总分为:%f\n", Sum);
		}
		else
		{
			printf("输入错误,请重新输入:\n");
		}
	}
	return 0;
}
//int main()
//{
//	float a = 0;
//	float b = 0;
//	float c = 0;
//	printf("请输入三个科目各自的分数:>");
//	scanf("%f %f %f", &a, &b, &c);
//	printf("平均分为:%f\n 总分为:%f\n", (a + b + c) / 3, a + b + c);
//	return 0;
//}