#include"vector.h"

namespace hjx
{
	void test_vector1()
	{
		vector<int> v1{ 5,3,7,3,7,2 };
		//v1.push_back(1000);
		//v1.pop_back();
		//v1.insert(v1.begin() + 6, 331);
		v1.erase(v1.begin() + 5);
		for (auto e : v1)cout << e << " ";
		cout << endl;
	}
}

int main()
{
	hjx::test_vector1();
	return 0;
}