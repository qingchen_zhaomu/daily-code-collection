#pragma once
#include<iostream>
using namespace std;
#include<assert.h>

namespace hjx
{
	template<class T>
	class vector
	{
	public:
		//迭代器
		typedef T* iterator;
		typedef const T* const_iterator;

		iterator begin()
		{
			return _start;
		}

		iterator end()
		{
			return _finish;
		}

		const_iterator begin()const
		{
			return _start;
		}

		const_iterator end()const
		{
			return _finish;
		}

		//编译器强制生成构造函数
		vector() = default;

		//析构
		~vector()
		{
			if (_start)
			{
				delete[] _start;
				_start = _finish = _end_of_storage = nullptr;
			}
		}

		void swap(vector<T>& v)
		{
			std::swap(_start, v._start);
			std::swap(_finish, v._finish);
			std::swap(_end_of_storage, v._end_of_storage);
		}

		//赋值重载
		vector<T>& operator=(vector<T> v)
		{
			swap(v);
			return *this;
		}

		//拷贝构造
		vector(vector<T>& v)
		{
			reserve(v.capacity());
			for (auto e : v)
			{
				push_back(e);
			}
		}

		//一段迭代器区间初始化
		template<class InputIterator>
		vector(InputIterator first, InputIterator last)
		{
			while (first != last)
			{
				push_back(*first);
				++first;
			}
		}

		vector(initializer_list<T> il)
		{
			reserve(il.size());
			for (auto e : il)
			{
				push_back(e);
			}
		}

		vector(size_t n, const T& val = T())
		{
			reserve(n);
			for (size_t i = 0; i < n; i++)
			{
				push_back(val);
			}
		}

		vector(int n, const T& val = T())
		{
			reserve(n);
			for (size_t i = 0; i < n; i++)
			{
				push_back(val);
			}
		}

		void reserve(size_t n)
		{
			if (n > capacity())
			{
				//提前记录，后面会出现迭代器失效的情况
				size_t oldsize = size();
				T* tmp = new T[n];

				if (_start)
				{
					for (size_t i = 0; i < oldsize; i++)
					{
						tmp[i] = _start[i];
					}
					delete[]_start;
				}

				_start = tmp;
				_finish = _start + oldsize;
				_end_of_storage = _start + n;
			}
		}

		size_t capacity()const
		{
			return _end_of_storage - _start;
		}

		size_t size()const
		{
			return _finish - _start;
		}

		//opertaor[]
		T& operator[](size_t i)
		{
			return _start[i];
		}

		const T& operator[](size_t i)const
		{
			return _start[i];
		}

		//insert
		iterator insert(iterator pos, const T& x)
		{
			assert(pos <= _finish);
			assert(pos >= _start);

			//判断扩容
			if (_finish == _end_of_storage)
			{
				size_t len = pos - _start;

				size_t newcapacity = capacity() == 0 ? 4 : 2 * capacity();
				reserve(newcapacity);

				pos = _start + len;
			}
			//插入数据
			iterator end = _finish;
			while (end > pos)
			{
				*end = *(end - 1);
				--end;
			}
			*pos = x;
			_finish++;

			return pos;
		}

		//erase
		iterator erase(iterator pos)
		{
			assert(pos < _finish);
			assert(pos >= _start);

			iterator it = pos + 1;
			while (it != _finish)
			{
				*(it - 1) = *it;
				it++;
			}
			--_finish;

			return pos;
		}

		void push_back(const T& x)
		{
			insert(end(), x);
		}

		void pop_back()
		{
			assert(size() > 0);
			--_finish;
		}



	private:
		iterator _start = nullptr;
		iterator _finish = nullptr;
		iterator _end_of_storage = nullptr;
	};
}


