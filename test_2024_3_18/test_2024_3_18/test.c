#define _CRT_SECURE_NO_WARNINGS 1

#include"Sort.h"

int main()
{
	int a[] = { 4,6,3,7,5,8,1,9,2 };
	PrintArray(a, sizeof(a) / sizeof(int));
	InsertSort(a, sizeof(a) / sizeof(int));
	PrintArray(a, sizeof(a) / sizeof(int));
	return 0;
}