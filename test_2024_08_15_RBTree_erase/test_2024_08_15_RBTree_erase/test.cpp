#include"RBTree.h"
#include<string>

void testrbtree1()
{
	RBTree<int, int> rb1;
	rb1.Insert({ 1, 1 });
	rb1.Insert({ 2, 2 });
	rb1.Insert({ 3, 3 });

	//rb1.InOrder();

	rb1.erase(2);
	rb1.InOrder();
	cout << endl;

	rb1.erase(1);
	rb1.InOrder();
	cout << endl;

	rb1.erase(3);
	rb1.InOrder();
	cout << endl;

}

void testrbtree2()
{
	RBTree<string, string> rb1;
	rb1.Insert({ "left", "���"});
	rb1.Insert({ "right", "�ұ�"});
	rb1.Insert({ "love", "����"});

	rb1.InOrder();
	cout << endl;

	rb1.erase("left");
	rb1.InOrder();
	cout << endl;

	rb1.erase("right");
	rb1.InOrder();
	cout << endl;

	rb1.erase("love");
	rb1.InOrder();
	cout << endl;

}

int main()
{
	//testrbtree1();
	testrbtree2();
	return 0;
}