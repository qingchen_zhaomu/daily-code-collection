#pragma once
#include<iostream>
using namespace std;

#define LL 1
#define RR 2
#define LR 3
#define RL 4



enum Colour
{
	RED,
	BLACK
};

template<class k, class v>
struct RBTreeNode
{
	RBTreeNode<k, v>* _left;
	RBTreeNode<k, v>* _right;
	RBTreeNode<k, v>* _parent;
	Colour _col;
	pair<k, v> _data;

	RBTreeNode(const pair<k, v>& data)
		:_left(nullptr)
		, _right(nullptr)
		, _parent(nullptr)
		, _data(data)
	{}
};



template<class K, class V>
class RBTree
{
	typedef RBTreeNode<K, V> Node;

public:


	RBTree() = default;

	~RBTree()
	{
		Destroy(_root);
		_root = nullptr;
	}

	bool Insert(const pair<K, V>& data)
	{
		if (_root == nullptr)
		{
			_root = new Node(data);
			_root->_col = BLACK;
			return true;
		}

		Node* cur = _root;
		Node* parent = nullptr;

		while (cur)
		{
			if (cur->_data.first < data.first)
			{
				parent = cur;
				cur = cur->_right;
			}
			else if (cur->_data.first > data.first)
			{
				parent = cur;
				cur = cur->_left;
			}
			else
				return false;
		}

		cur = new Node(data);
		cur->_col = RED;

		if (cur->_data.first < parent->_data.first)
			parent->_left = cur;
		else
			parent->_right = cur;
		cur->_parent = parent;


		Node* newone = cur;// 提前记录值以便返回

		while (parent && parent->_col == RED)
		{
			Node* grandpa = parent->_parent;

			if (grandpa->_left == parent)
			{
				// parent 在爷爷节点左边
				Node* uncle = grandpa->_right;

				if (uncle && uncle->_col == RED)
				{
					uncle->_col = parent->_col = BLACK;
					grandpa->_col = RED;

					cur = grandpa;
					parent = cur->_parent;
				}
				else
				{
					if (cur == parent->_left)
					{
						// 单旋的逻辑
						RotateR(grandpa);
						parent->_col = BLACK;
						grandpa->_col = RED;
					}
					else
					{
						// 双旋的逻辑
						RotateL(parent);
						RotateR(grandpa);
						cur->_col = BLACK;
						grandpa->_col = RED;
					}
					break;
				}

			}
			else
			{
				// parent 在爷爷节点右边
				Node* uncle = grandpa->_left;
				if (uncle && uncle->_col == RED)
				{
					grandpa->_col = RED;
					uncle->_col = parent->_col = BLACK;

					cur = grandpa;
					parent = cur->_parent;
				}
				else
				{
					if (cur == parent->_right)
					{
						RotateL(grandpa);
						grandpa->_col = RED;
						parent->_col = BLACK;
					}
					else
					{
						RotateR(parent);
						RotateL(grandpa);
						grandpa->_col = RED;
						cur->_col = BLACK;
					}
					break;
				}
			}
		}

		_root->_col = BLACK;
		return true;

	}



	bool erase(const K& data)
	{
		if (!_root)return false;


		Node* del = _root;
		while (del)
		{
			if (data < del->_data.first)
				del = del->_left;
			else if (data > del->_data.first)
				del = del->_right;
			else
				break;
		}

		if (del == nullptr)
			return false;
		
		if (_root == del && del->_left == nullptr && del->_right == nullptr)
		{
			delete _root;
			_root = nullptr;
			return true;
		}


		////////////////// 有两个孩子的情况
		if (del->_left && del->_right)
		{
			Node* rightMin = del->_right;
			while (rightMin->_left)
			{
				rightMin = rightMin->_left;
			}
			del->_data = rightMin->_data;

			// 将问题转换成叶子节点或者一个节点的删除
			del = rightMin;
		}

		////////////////// 有一个孩子的情况
		if ((del->_left && !del->_right) || (!del->_left && del->_right))
		{
			// 一个孩子的情况只能是父黑子红
			// 不然不满足每条路径都只有一个黑色节点

			Node* child = del->_left == nullptr ? del->_right : del->_left;
			del->_data = child->_data;
			del = child;
		}

		///////////////// 没有孩子的情况分为红色和黑色两种
		///////////////// 红色直接删，黑色需要细分情况
		if (del->_col == BLACK)
		{
			adjustRBTreeBalance(del);
		}

		Node* parent = del->_parent;
		if (parent->_left == del)
			parent->_left = nullptr;
		else
			parent->_right = nullptr;

		delete del;
		return true;

	}



	void adjustRBTreeBalance(Node* del)
	{
		// 前面已经判断过了，但是这里是后面代码的递归出口
		// 因为后面写到兄弟为黑并没有子节点，并且父亲为黑的情况的时候，就需要递归处理
		if (_root == nullptr)
			return;

		Node* parent = del->_parent;
		Node* uncle = del == parent->_left ? parent->_right : parent->_left;

		if (uncle->_col == BLACK)
		{
			int type = whichtype(uncle);
			switch (type)
			{
			case LL:
				uncle->_col = parent->_col;
				parent->_col = uncle->_right->_col = BLACK;
				RotateL(parent);
				break;
			case RR:
				uncle->_col = parent->_col;
				parent->_col = uncle->_left->_col = BLACK;
				RotateR(parent);
				break;
			case LR:
				parent->_col = BLACK;
				uncle->_right->_col = parent->_col;
				RotateL(uncle);
				RotateR(parent);
				break;
			case RL:
				parent->_col = BLACK;
				uncle->_left->_col = parent->_col;
				RotateR(uncle);
				RotateL(parent);
				break;
			default:
				// 没有红色子节点
				// 需要判断父节点是什么颜色
				// 如果父节点为红色，交换颜色即可
				// 如果父节点为黑色， 则uncle节点变红后递归处理
				if (parent->_col == RED)
				{
					parent->_col = BLACK;
					uncle->_col = RED;
				}
				else
				{
					uncle->_col = RED;
					adjustRBTreeBalance(parent);
				}

				break;
			}
		}
		else
		{
			// uncle节点的颜色为红
			if (parent->_left == uncle)
			{
				uncle->_col = BLACK;
				uncle->_right->_col = RED;
				RotateR(parent);
			}
			else
			{
				uncle->_col = BLACK;
				uncle->_left->_col = RED;
				RotateL(parent);
			}
		}

	}

	int whichtype(Node* uncle)
	{
		Node* parent = uncle->_parent;

		if (uncle == parent->_left)
		{
			if (uncle->_left != nullptr)
			{
				return RR;
			}
			else if (uncle->_right != nullptr)
			{
				return LR;
			}
		}
		else
		{
			if (uncle->_right != nullptr)
			{
				return	LL;
			}
			else if (uncle->_left != nullptr)
			{
				return RL;
			}
		}

		// 无红色节点的情况
		return 0;
	}




	void InOrder()
	{
		_InOrder(_root);
	}




private:

	//左单旋
	void RotateL(Node* parent)
	{
		Node* curR = parent->_right;
		Node* curRL = curR->_left;
		Node* Parentparent = parent->_parent;

		// 旋转主逻辑
		parent->_right = curRL;
		curR->_left = parent;

		// 处理父节点parent
		if (curRL)curRL->_parent = parent;
		parent->_parent = curR;

		if (Parentparent == nullptr)
		{
			_root = curR;
			curR->_parent = nullptr;
		}
		else
		{
			if (Parentparent->_left == parent)
				Parentparent->_left = curR;
			else
				Parentparent->_right = curR;

			curR->_parent = Parentparent;
		}

	}


	// 右单旋
	void  RotateR(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;

		parent->_left = subLR;
		if (subLR)
			subLR->_parent = parent;

		Node* parentParent = parent->_parent;

		subL->_right = parent;
		parent->_parent = subL;

		if (parentParent == nullptr)
		{
			_root = subL;
			subL->_parent = nullptr;
		}
		else
		{
			if (parent == parentParent->_left)
			{
				parentParent->_left = subL;
			}
			else
			{
				parentParent->_right = subL;
			}

			subL->_parent = parentParent;
		}
	}

	int _size(Node* root)
	{
		if (root == nullptr)return 0;

		return _size(root->_left) + _size(root->_right) + 1;
	}

	int _Height(Node* root)
	{
		if (root == nullptr)return 0;

		int right = _Height(root->_right);
		int left = _Height(root->_left);

		return left > right ? left + 1 : right + 1;
	}

	void Destroy(Node* root)
	{
		if (root == nullptr)return;

		Destroy(root->_left);
		Destroy(root->_right);
		delete root;
	}

	void _InOrder(Node* root)
	{
		if (!root)return;

		_InOrder(root->_left);
		cout << root->_data.first << ":" << root->_data.second << endl;
		_InOrder(root->_right);
	}

private:
	Node* _root;
};



