#include"list.h"

namespace hjx
{
	template<class T>
	void Print(const list<T>& s1)
	{
		for (auto e : s1)
			cout << e << " ";
		cout << endl;
	}

	void test_list1()
	{
		list<int> s1;
		s1.push_back(1);
		s1.push_back(2);
		s1.push_back(3);
		s1.push_back(4);

		Print(s1);

		list<int> s2 = s1;
		Print(s2);

		list<int> s3(s1);
		Print(s3);

		list<int> s4 = { 1,2,3,4,5,6,7 };
		Print(s4);
	}

}


int main()
{
	hjx::test_list1();
	return 0;
}
