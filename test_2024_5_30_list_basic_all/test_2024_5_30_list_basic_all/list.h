#pragma once
#include<iostream>
using namespace std;
#include<assert.h>

namespace hjx
{
	//节点的类
	template<class T>
	struct ListNode
	{
		ListNode* _next;
		ListNode* _prev;
		T _data;

		ListNode(const T& data = T())
			:_next(nullptr)
			, _prev(nullptr)
			, _data(data)
		{}
	};

	//迭代器的类 const / 非 const
	template<class T, class Ref, class Ptr>
	struct ListIterator
	{
		typedef ListNode<T> Node;
		typedef ListIterator<T, Ref, Ptr> Self;

		Node* _node;

		ListIterator(Node* node)
			:_node(node)
		{}

		Self& operator++()//后置
		{
			_node = _node->_next;
			return *this;
		}
		Self& operator--()
		{
			_node = _node->_prev;
			return*this;
		}
		Self operator++(int)//前置
		{
			Self tmp(*this);
			_node = _node->_next;
			return tmp;
		}
		Self operator--(int)
		{
			Self tmp(*this);
			_node = _node->_prev;
			return tmp;
		}

		Ref operator*()
		{
			return _node->_data;
		}
		Ptr operator->()
		{
			return &_node->_data;
			// -> 的优先级 > &
		}

		bool operator==(const Self& it)
		{
			return _node == it._node;
		}
		bool operator!=(const Self& it)
		{
			return _node != it._node;
		}
	};

	//控制链表的类
	template<class T>
	class list
	{
	public:
		typedef ListNode<T> Node;
		typedef ListIterator<T, T&, T*> iterator;
		typedef ListIterator<T, const T&, const T*> const_iterator;

		void empty_list()
		{
			_head = new Node();
			_head->_next = _head;
			_head->_prev = _head;
		}

		list()//哨兵位
		{
			empty_list();
		}

		list(const list<T>& s1)
		{
			empty_list();
			for (auto e : s1)
				push_back(e);
		}

		list<T>& operator=(list<T> s1)
		{
			swap(_head, s1._head);
			return *this;
		}

		list(initializer_list<T> il)
		{
			empty_list();//需要带上头节点
			for (const auto& e : il)
				push_back(e);
		}

		~list()
		{
			clear();
			delete _head;
			_head = nullptr;
		}

		void clear()
		{
			auto it = begin();
			while (it != end())
			{
				it = erase(it);
			}
		}

		void push_back(const T& x)
		{
			/*Node* newnode = new Node();
			Node* prev = _head -> prev;

			newnode->next = _head;
			newnode->prev = prev;
			prev->next = newnode;
			_head->prev = newnode;*/

			insert(end(), x);
		}

		iterator begin()
		{
			return iterator(_head->_next);
		}
		const_iterator begin()const
		{
			return const_iterator(_head->_next);
		}
		iterator end()
		{
			return iterator(_head);
		}
		const_iterator end()const
		{
			return const_iterator(_head);
		}

		iterator insert(iterator pos, const T& x)
		{
			Node* cur = pos._node;
			Node* newnode = new Node();
			Node* prev = cur->_prev;
			newnode->_data = x;

			newnode->_next = cur;
			newnode->_prev = prev;
			cur->_prev = newnode;
			prev->_next = newnode;

			return iterator(newnode);
		}

		iterator erase(iterator pos)
		{
			assert(pos != end());

			Node* cur = pos._node;
			Node* next = cur->_next;
			Node* prev = cur->_prev;

			prev->_next = cur->_next;
			next->_prev = cur->_prev;
			delete cur;
			return iterator(next);
		}

		void pop_back()
		{
			erase(--end());
		}
		void push_front(const T& x)
		{
			insert(begin(), x);
		}
		void pop_front()
		{
			erase(begin());
		}

	private:
		Node* _head;
	};
}
