#include"Queue.h"

void QueueInit(Queue* pq)
{
	assert(pq);
	pq->phead = NULL;
	pq->ptail = NULL;
	pq->size = 0;
}

void QueueDestroy(Queue* pq)
{
	assert(pq);
	QNode* cur = pq->phead;
	QNode* next = NULL;
	while (cur)
	{
		next = cur->next;
		free(cur);
		cur = next;
	}
	pq->phead = pq->ptail = NULL;
	pq->size = 0;
}

//入队列
//尾插
void QueuePush(Queue* pq, QDataType x)
{
	QNode* newnode = (QNode*)malloc(sizeof(QNode));
	if (!newnode)
	{
		perror("malloc fail!");
		return;
	}
	newnode->val = x;
	newnode->next = NULL;
	if (!pq->ptail)
		pq->phead = pq->ptail = newnode;
	else
	{
		//让尾节点的下一个节点指向新节点
		pq->ptail->next = newnode;
		//新的尾节点
		pq->ptail = newnode;
	}
	pq->size++;
}

//出队列
//头删
void QueuePop(Queue* pq)
{
	assert(pq);
	assert(pq->phead);

	//只有一个节点
	if (!pq->phead->next)
	{
		free(pq->phead);
		pq->phead = pq->ptail = NULL;
	}
	else//多节点
	{
		QNode* next = pq->phead->next;
		free(pq->phead);
		pq->phead = next;
	}
	pq->size--;
}

QDataType QueueFront(Queue* pq)
{
	assert(pq);
	assert(pq->phead);
	return pq->phead->val;
}

QDataType QueueBack(Queue* pq)
{
	assert(pq);
	assert(pq->ptail!=NULL);
	return pq->ptail->val;
}

bool QueueEmpty(Queue* pq)
{
	assert(pq);
	return pq->size == 0;
}

int QueueSize(Queue* pq)
{
	assert(pq);
	return pq->size;
}
