#include"Queue.h"

int main()
{
	Queue tmp;
	QueueInit(&tmp);
	QueuePush(&tmp, 1);
	QueuePush(&tmp, 2);
	QueuePush(&tmp, 3);
	QueuePush(&tmp, 4);
	
	while (!QueueEmpty(&tmp))
	{
		printf("%d\n", QueueFront(&tmp));
		QueuePop(&tmp);
	}
	QueueDestroy(&tmp);

	return 0;
}
