#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<assert.h>



//int main()
//{
//	int a = 2;
//	int b = a++;
//	printf("%d %d", a, b);
//	return 0;
//}

//int main()
//{
//	int a = 0;
//	do
//	{
//		printf("请输入要转换的分钟数:");
//		scanf("%d", &a);
//		int b = a / 60;
//		int c = a % 60;
//		if (a >= 0)
//		{
//			if (b == 1)
//				printf("%d hour %d minutes", b, c);
//			else
//				printf("%d hours %d minutes", b, c);
//			return 0;
//		}
//		else
//			printf("输入非法，请重新输入!!\n");
//	} while (a < 0);
//}

//int main()
//{
//	char c1, c2;
//	c1 = 127;
//	c2 = 128;
//	printf("%c %c\n", c1, c2);
//	printf("%d %d", c1, c2);
//	return 0;
//}

//int main()
//{
//	int a = 3, b = 5;
//	printf("a is %3d\nb is %3d\na+b is %3d\na-b is %3d\n", a,b,a+b,a-b);
//	return 0;
//}

//int main()
//{
//	int a = 1, b = 1;
//	int c = a++;
//	int d = ++b;
//	printf("a c b d \n");
//	printf("%d %d %d %d", a, b, c, d);
//	return 0;
//}

//char* my_strcpy(char* dest, const char* src)
//{
//	char* ret = dest;
//	assert(dest != NULL);
//	assert(src != NULL);
//	while ( *dest++ = *src++ )
//	{
//		;
//	}
//	return ret;//此处不能return dest，因为dest已经改变
//}
//int main()
//{
//	char arr1[] = "############";
//	char arr2[] = "bit";
//	printf("%s\n", my_strcpy(arr1, arr2));
//	return 0;
//}

// 1,1,1.000000 ,1.25

//int main()
//{
//	int x = 0;
//	int y = 10;
//	unsigned int z = 3;
//	char ch = '!';
//	float pi = 3.14;
//	return 0;
//}

//char* my_strcpy(char* des, const char* src)
//{
//	assert(des != '\0');//断言函数，如果不符合括号中的内容，则会报错
//	assert(src != '\0');//如果符合，则无事发生
//	while ( *des++ = *src++ )
//	{
//		;
//	}
//}
//int main()
//{
//	char arr1[] = "################";
//	char arr2[] = "bit";
//	my_strcpy(arr1, arr2);
//	printf("%s\n", arr1);
//	return 0;
//}

//void my_strcpy(char* des, char* src)
//{
//	while (*src != '\0')
//	{
//		*des = *src;
//		des++;
//		src++;
//	}
//	*des = *src;
//}

//int main()
//{
//	int i = 0;
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	for (i = 0; i <= 9; i++)
//	{
//		printf("hehe\n");
//		arr[i] = 0;
//	}
//	return 0;
//}

//int main()
//{
//	int i = 0;
//	int j = 0;
//	int n = 0;
//	int sum = 0;
//	scanf("%d", &n);
//	for (i = 1; i <= n; i++)
//	{//此处计算的是有多少个数字的阶乘要算
//		int ret = 1;
//		//此处将ret定义在这里是为了将ret的生命周期限定在外层for循环内
//		for (j = 1; j <= i; j++)
//		{
//			ret *= j;//此处计算的是阶乘
//		}
//		sum += ret;
//	}
//	printf("%d", sum);
//	return 0;
//}

//typedef struct Stu
//{
//	char arr[20];
//	int a;
//}S;
//typedef struct end
//{
//	char ch[20];
//	S s;
//}T;
//void Print(T* tmp)
//{
//	printf("%s\n", tmp->s.arr);
//}
//int main()
//{
//	T t = { "hehe",{"haha",1} };
//	printf("%s\n", t.ch);
//	printf("%s\n", t.s.arr);
//	Print(&t);
//	return 0;
//}

//typedef struct Stu
//{
//	char arr[20];
//	int a;
//	double b;
//}S;
//typedef struct T
//{
//	int a;
//	S s;
//}A;
//int main()
//{
//	A t = { 1,{"hehe",2,3.14} };
//	printf("%s\n", t.s.arr);
//	return 0;
//}