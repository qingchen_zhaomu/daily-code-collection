#include<iostream>
using namespace std;
#include<vector>
#include<string>
#include<queue>



//class Solution {
//public:
//    int minCostClimbingStairs(vector<int>& cost)
//    {
//        int n = cost.size();
//        vector<int> dp(n);
//        dp[n - 1] = cost[n - 1], dp[n - 2] = cost[n - 2];
//        for (int i = n - 3; i >= 0; i--)
//            dp[i] = cost[i] + min(dp[i + 1], dp[i + 2]);
//        return min(dp[0], dp[1]);
//    }
//};

//class Solution {
//public:
//    int minCostClimbingStairs(vector<int>& cost)
//    {
//        int n = cost.size();
//        vector<int> dp(n + 1, 0);
//        for (int i = 2; i <= n; i++)
//            dp[i] = min(dp[i - 1] + cost[i - 1], dp[i - 2] + cost[i - 2]);
//        return dp[n];
//    }
//};

//class Solution {
//public:
//    int n;
//
//    int dfs(vector<int>& cost, int stage, vector<int>& vis)
//    {
//        if (vis[stage] != 0)return vis[stage];
//
//        if (stage > n - 1)return 0;
//
//        int count1 = cost[stage] + dfs(cost, stage + 1, vis);
//        int count2 = cost[stage] + dfs(cost, stage + 2, vis);
//
//        vis[stage] = min(count1, count2);
//        return vis[stage];
//    }
//
//    int minCostClimbingStairs(vector<int>& cost)
//    {
//        n = cost.size();
//        vector<int> vis(n + 2, 0);
//        int ret1 = dfs(cost, 0, vis);
//        int ret2 = dfs(cost, 1, vis);
//        int ret = ret1 < ret2 ? ret1 : ret2;
//        return ret;
//    }
//};

//class Solution {
//public:
//    int waysToStep(int n)
//    {
//        int MOD = 1e9 + 7;
//        //创建dp表
//        vector<int> dp(n + 1);
//        //初始化
//        if (n == 1 || n == 2) return n;
//        if (n == 3) return 4;
//        dp[1] = 1, dp[2] = 2, dp[3] = 4;
//        //填表
//        for (int i = 4; i <= n; i++)
//            dp[i] = ((dp[i - 1] + dp[i - 2]) % MOD + dp[i - 3]) % MOD;
//        //返回值
//        return dp[n];
//    }
//};

//class Solution {
//public:
//    int tribonacci(int n)
//    {
//        //1.创建一个dp表
//        vector<int> dp(n + 1);
//        //2.初始化dp表
//        if (n == 0)return 0;
//        else if (n == 1 || n == 2) return 1;
//        dp[0] = 0, dp[1] = dp[2] = 1;
//        //3.填表
//        for (int i = 3; i <= n; i++)
//            dp[i] = dp[i - 1] + dp[i - 2] + dp[i - 3];
//        //4.返回结果
//        return dp[n];
//    }
//};

int main()
{

    return 0;
}
