#pragma once
#include<iostream>
using namespace std;
#include<assert.h>

namespace hjx
{
	template<class T>
	struct ListNode
	{
		ListNode* _next;
		ListNode* _prev;
		T _data;

		ListNode(const T& data = T())
			:_prev(nullptr)
			, _next(nullptr)
			, _data(data)
		{}
	};

	template<class T, class Ref, class Ptr>
	struct ListIterator
	{
		typedef ListNode<T> Node;
		typedef ListIterator<T, Ref, Ptr> Self;

		Node* _node;

		ListIterator(Node* node)
			:_node(node)
		{}

		Self& operator++()
		{
			_node = _node->_next;
			return *this;
		}
		Self& operator--()
		{
			_node = _node->_prev;
			return *this;
		}
		Self& operator++(int)
		{
			Self tmp(*this);
			_node = _node->_next;
			return *this;
		}
		Self& operator--(int)
		{
			Self tmp(*this);
			_node = _node->_prev;
			return *this;
		}

		Ref operator*()
		{
			return _node->_data;
		}
		Ptr operator->()
		{
			return &_node->_data;
		}

		bool operator!=(const Self& s1)
		{
			return s1._node != _node;
		}
		bool operator==(const Self& s1)
		{
			return s1._node == _node;
		}
	};

	template<class T>
	class list
	{
	public:
		typedef ListNode<T> Node;
		typedef ListIterator<T, T&, T*> iterator;
		typedef ListIterator<T, const T&, const T*> const_iterator;

		void empty_list()
		{
			_head = new Node();
			_head->_next = _head;
			_head->_prev = _head;
		}

		list()
		{
			empty_list();
		}

		list(const list<T>& s1)
		{
			empty_list();
			for (auto e : s1)
				push_back(e);
		}

		list<T>& operator=(list<T> s1)
		{
			swap(_head, s1._head);
			return *this;
		}

		list(initializer_list<T> il)
		{
			empty_list();
			for (const auto& e : il)
			{
				push_back(e);
			}
		}

		~list()
		{
			clear();
			delete _head;
			_head = nullptr;
		}

		void clear()
		{
			iterator it = begin();
			while (it != end())
			{
				it = erase(it);
			}
		}

		iterator begin()
		{
			return iterator(_head->_next);
		}
		iterator end()
		{
			return iterator(_head);
		}
		const_iterator begin()const
		{
			return const_iterator(_head->_next);
		}
		const_iterator end()const
		{
			return const_iterator(_head);
		}

		void push_back(const T& data)
		{
			/*Node* newnode = new Node();
			newnode->_data = data;
			Node* prev = _head->_prev;

			newnode->_next = _head;
			newnode->_prev = prev;
			_head->_prev = newnode;
			prev->_next = newnode;*/

			insert(end(), data);
		}

		iterator insert(iterator pos, const T& data)
		{
			Node* newnode = new Node();
			Node* cur = pos._node;
			Node* prev = cur->_prev;

			newnode->_next = cur;
			newnode->_prev = prev;
			cur->_prev = newnode;
			prev->_next = newnode;

			newnode->_data = data;

			return iterator(newnode);
		}

		iterator erase(iterator pos)
		{
			assert(pos != end());

			Node* cur = pos._node;
			Node* next = cur->_next;
			Node* prev = cur->_prev;

			prev->_next = next;
			next->_prev = prev;

			delete cur;
			cur = nullptr;

			return iterator(next);
		}

		void pop_back()
		{
			erase(--end());
		}
		void push_front(const T& x)
		{
			insert(begin(), x);
		}
		void pop_front()
		{
			erase(begin());
		}

	private:
		Node* _head;
	};
}








	template<class T>
	struct ListNode
	{
		ListNode* _prev;
		ListNode* _next;
		T _data;

		ListNode(const T& data)
			:_prev(nullptr)
			,_next(nullptr)
			,_data(data)
		{}
	};

	template<class T, class Ref, class Ptr>
	struct ListIterator
	{
		typedef ListIterator<T, Ref, Ptr> self;
		typedef ListNode<T> Node;

		Node* _node;

		ListIterator(Node* node)
			:_node(node)
		{}

		self& operator--()//����
		{
			_node = _node->prev;
			return *this;
		}
		self& operator++()
		{
			_node = _node->_next;
			return *this;
		}
		self& operator--(int)//ǰ��
		{
			self tmp(*this);
			_node = _node->_prev;
			return tmp;
		}
		self& operator++(int)
		{
			self tmp(*this);
			_node = _node->_next;
			return tmp;
		}

		bool operator==(const self& s1)
		{
			return s1._node == _node;
		}
		bool operator!=(const self& s1)
		{
			return s1._node != _node;
		}

		Ref operator*()
		{
			return _node->_data;
		}
		Ptr operator->()
		{
			return &_node->_data;
		}

	};

	template<class T>
	class list
	{
	public:
		typedef ListNode Node;
		typedef ListIterator<T, T&, T*> iterator;
		typedef ListIterator<T, const_T&, const_T*> const_iterator;

		/*void empty_node()
		{
			
		}*/

		list()
		{
			_head = new Node();
			_head->next = _head;
			_head->prev = _head;
		}

		iterator begin()
		{
			return iterator(_head->_next);
		}
		const_iterator begin()const
		{
			return iterator(_head->_next);
		}
		iterator end()
		{
			return iterator(_head);
		}
		const_iterator end()const
		{
			return iterator(_head->_next);
		}

	private:
		Node* _head;
	};


}

