//#include"list.h"
//#include<algorithm>
#include"priority_queue.h"

namespace hjx
{
	//template<class T>
	//void Print(list<T> s1)
	//{
	//	for (auto e : s1)
	//		cout << e << " ";
	//	cout << endl;
	//}
	//
	//void test_mylist1()
	//{
	//	list<int> s1;
	//	s1.push_back(1);
	//	s1.push_back(2);
	//	s1.push_back(3);
	//	s1.push_back(4);
	//	s1.push_back(5);
	//	Print(s1);
	//	/*s1.pop_back();
	//	s1.pop_back();
	//	Print(s1);*/

	//	//auto it = find(s1.begin(), s1.end(), 3);
	//	s1.insert(s1.begin()++, 331);
	//	Print(s1);
	//}


	
	void test_mypriority_queue1()
	{
		//vector<int> v1 = { 6,4,7,8,3,6,9,2,5,7,3 };
		////for (auto e : v1)cout << e << " "; cout << endl;
		//priority_queue<int, vector<int>, myless<int>> pq1(v1.begin(), v1.end());
		//while (!pq1.empty())
		//{
		//	cout << pq1.top() << " ";
		//	pq1.pop();
		//}
		//cout << endl;
		
		priority_queue<int, vector<int>, myless<int>> pq1;
		pq1.push(6);
		pq1.push(4);
		pq1.push(7);
		pq1.push(2);
		pq1.push(8);
		pq1.push(5);
		pq1.push(10);
		while (!pq1.empty())
		{
			cout << pq1.top() << " ";
			pq1.pop();
		}
		cout << endl;

		priority_queue<int, vector<int>, myless<int>> pq2{1,4,6,3,7,2,6,9,2,4};
		while (!pq2.empty())
		{
			cout << pq2.top() << " ";
			pq2.pop();
		}
		cout << endl;

	}

}

int main()
{
	//hjx::test_mylist1();
	hjx::test_mypriority_queue1();
	return 0;
}


//#include <iostream>
//using namespace std;
//#include<vector>
//
//int main()
//{
//    int n, m, q;
//    cin >> n >> m >> q;
//    vector<vector<long long>> vv1;
//    vector<vector<long long>> vv2;
//    for (int i = 0; i < n + 1; i++)
//        vv1.push_back(vector<long long>(m + 1, 0));
//    for (int i = 0; i < n + 1; i++)
//        vv2.push_back(vector<long long>(m + 1, 0));
//
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = 1; j <= m; j++)
//            cin >> vv1[i][j];
//    }
//
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = 1; j <= m; j++)
//            vv2[i][j] = vv2[i - 1][j] + vv2[i][j - 1] - vv2[i - 1][j - 1] + vv1[i][j];
//    }
//
//    while (q--)
//    {
//        int a, b, c, d;
//        cin >> a >> b >> c >> d;
//        cout << vv2[c][d] - vv2[a - 1][d] - vv2[c][b - 1] + vv2[a - 1][b - 1] << endl;
//    }
//
//    return 0;
//}

//#include <iostream>
//using namespace std;
//#include<vector>
//
//int main()
//{
//    int n, m, q;
//    cin >> n >> m >> q;
//    vector<vector<int>> vv1;
//    vector<vector<int>> vv2;
//    for (int i = 0; i < n + 1; i++)
//        vv1.push_back(vector<int>(m + 1, 0));
//    for (int i = 0; i < n + 1; i++)
//        vv2.push_back(vector<int>(m + 1, 0));
//    
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = 1; j <= m; j++)
//            cin >> vv1[i][j];
//    }
//
//    for (int i = 1; i <= n; i++)
//    {
//        for (int j = 1; j <= m; j++)
//            vv2[i][j] = vv2[i - 1][j] + vv2[i][j - 1] - vv2[i - 1][j - 1] + vv1[i][j];
//    }
//
//    while (q--)
//    {
//        int a, b, c, d;
//        cin >> a >> b >> c >> d;
//        cout << vv1[c][d] - vv1[a - 1][d] - vv1[c][b - 1] + vv1[a - 1][b - 1];
//    }
//
//    return 0;
//}