#pragma once
#include"RBTree.h"

namespace hjx
{
	template<class K, class V>
	class map
	{
		struct MapKeyofT
		{
			const K& operator()(const pair<K, V>& kv)
			{
				return kv.first;
			}
		};
	public:
		typedef typename RBTree<K, pair<const K, V>, MapKeyofT>::Iterator iterator;
		typedef typename RBTree<K, pair<const K, V>, MapKeyofT>::ConstIterator const_iterator;

		iterator begin()
		{
			return _t.Begin();
		}

		iterator end()
		{
			return _t.End();
		}

		const_iterator end()const
		{
			return _t.End();
		}

		const_iterator begin()const
		{
			return _t.Begin();
		}

		pair<iterator, bool> insert(const pair<K, V>& kv)
		{
			return _t.Insert(kv);
		}

		V& operator[](const K& key)
		{
			pair<iterator, bool> ret = insert({ key, V() });
			return ret.first->second;
		}

	private:
		RBTree<K, pair<const K, V>, MapKeyofT> _t;
	};


	void test_map()
	{
		map<string, string> dict;
		dict.insert({ "sort", "����" });
		dict.insert({ "left", "���" });
		dict.insert({ "right", "�ұ�" });

		dict["left"] = "��ߣ�ʣ��";
		dict["insert"] = "����";
		dict["string"];

		map<string, string>::iterator it = dict.begin();
		while (it != dict.end())
		{
			// �����޸�first�������޸�second
			//it->first += 'x';
			it->second += 'x';

			cout << it->first << ":" << it->second << endl;
			++it;
		}
		cout << endl;
	}
}
