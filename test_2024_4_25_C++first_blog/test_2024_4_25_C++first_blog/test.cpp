#include<iostream>
using namespace std;

void test(int a = 1, int b = 2)
{
	cout << 1;
}

void test(int a)
{
	cout << 1;
}

int main()
{
	test();
	return 0;
}

////参数个数不同
//int B(int a, int b)
//{
//	cout << a << b << endl;
//}
//
//int B(int a)
//{
//	cout << a << endl;
//}
//
////函数参数类型不同
//int Add(int a, int b)
//{
//	return a + b;
//}
//
//double Add(double a, double b)
//{
//	return a + b;
//}
//
////参数顺序不同
//void A(int a, char b)
//{
//	cout << a << b << endl;
//}
//
//void A(char b, int a)
//{
//	cout << a << b << endl;
//}
//
//int main()
//{
//
//	return 0;
//}

//void Func(int a, int b = 1, int c)
//{
//	cout << a << " " << b << " " << c << endl;
//}
//
//int main()
//{
//	Func(5, ,1);
//	return 0;
//}

//void Func(int a = 1, int b = 2, int c = 3)
//{
//	cout << a << " " << b << " " << c << endl;
//}
//
//int main()
//{
//	Func(5);
//	Func(5, 6);
//	Func(7, 8, 9);
//	return 0;
//}

//#include<stdio.h>

//namespace Boy
//{
//	int y = 0;
//	int n = 3;
//}
//using Boy::n;
//
//int main()
//{
//	int a = n;
//	printf("%d\n", a);
//	return 0;
//}

//namespace Boy
//{
//	namespace A
//	{
//		int rand = 3;
//	}
//}
//namespace Girl
//{
//	namespace B
//	{
//		int rand = 5;
//	}
//}
//
//int main()
//{
//	printf("%p\n", rand);
//	
//	return 0;
//}


