#include<iostream>
using namespace std;
#include<vector>
#include<string>
#include<queue>
#include<functional>



//class Solution {
//public:
//    int dx[8] = { 0,0,1,-1,-1,-1,1,1 };
//    int dy[8] = { 1,-1,0,0,1,-1,1,-1 };
//    int n, m;
//    vector<vector<char>> updateBoard(vector<vector<char>>& board, vector<int>& c)
//    {
//        n = board.size(), m = board[0].size();
//        if (board[c[0]][c[1]] == 'M')
//        {
//            board[c[0]][c[1]] = 'X';
//            return board;
//        }
//        dfs(board, c[0], c[1]);
//        return board;
//    }
//    int mnum(vector<vector<char>>& board, int i, int j)
//    {
//        int ret = 0;
//        for (int k = 0; k < 8; k++)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < n && y >= 0 && y < m && board[x][y] == 'M')ret++;
//        }
//        return ret;
//    }
//    void dfs(vector<vector<char>>& board, int i, int j)
//    {
//        int ret = mnum(board, i, j);
//        if (ret)board[i][j] = ret + '0';
//        else
//        {
//            board[i][j] = 'B';
//            for (int k = 0; k < 8; k++)
//            {
//                int x = i + dx[k], y = j + dy[k];
//                if (x >= 0 && x < n && y >= 0 && y < m && board[x][y] == 'E')
//                {
//                    dfs(board, x, y);
//                }
//            }
//        }
//    }
//};

//class Solution {
//public:
//    int dx[4] = { 0,0,-1,1 };
//    int dy[4] = { 1,-1,0,0 };
//    int n, m;
//    vector<vector<int>> ret;
//    vector<vector<int>> pacificAtlantic(vector<vector<int>>& nums)
//    {
//        n = nums.size(), m = nums[0].size();
//        vector<vector<bool>> visp(n, vector<bool>(m));
//        vector<vector<bool>> visa(n, vector<bool>(m));
//        for (int i = 0; i < m; i++)
//        {
//            if (visp[0][i] == false)dfs(nums, 0, i, visp);
//            if (visa[n - 1][i] == false)dfs(nums, n - 1, i, visa);
//        }
//        for (int i = 0; i < n; i++)
//        {
//            if (visp[i][0] == false)dfs(nums, i, 0, visp);
//            if (visa[i][m - 1] == false)dfs(nums, i, m - 1, visa);
//        }
//        for (int i = 0; i < n; i++)
//            for (int j = 0; j < m; j++)
//                if (visp[i][j] && visa[i][j])ret.push_back({ i, j });
//        return ret;
//    }
//    void dfs(vector<vector<int>>& nums, int i, int j, vector<vector<bool>>& tmp)
//    {
//        tmp[i][j] = true;
//        for (int k = 0; k < 4; k++)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < n && y >= 0 && y < m && nums[x][y] >= nums[i][j] && !tmp[x][y])
//            {
//                dfs(nums, x, y, tmp);
//            }
//        }
//    }
//};

//class Solution {
//public:
//    typedef pair<int, int> PII;
//    bool vis[201][201] = { false };
//    int dx[4] = { 0, 0, 1, -1 };
//    int dy[4] = { 1, -1, 0, 0 };
//    int m, n;
//
//    void solve(vector<vector<char>>& nums)
//    {
//        m = nums.size(), n = nums[0].size();
//        // 先处理边界情况
//        for (int i = 0; i < n; i++)
//        {
//            if (nums[0][i] == 'O')dfs(nums, 0, i);
//            if (nums[m - 1][i] == 'O')dfs(nums, m - 1, i);
//        }
//        for (int i = 0; i < m; i++)
//        {
//            if (nums[i][0] == 'O')dfs(nums, i, 0);
//            if (nums[i][n - 1] == 'O')dfs(nums, i, n - 1);
//        }
//        for (int i = 1; i < m - 1; i++)
//        {
//            for (int j = 1; j < n - 1; j++)
//            {
//                if (nums[i][j] == 'O' && vis[i][j] == false)
//                    nums[i][j] = 'X';
//            }
//        }
//    }
//    void dfs(vector<vector<char>>& nums, int i, int j)
//    {
//        vis[i][j] = true;
//        for (int k = 0; k < 4; k++)
//        {
//            int x = i + dx[k], y = j + dy[k];
//            if (x >= 0 && x < m && y >= 0 && y < n && !vis[x][y] && nums[x][y] == 'O')
//                dfs(nums, x, y);
//        }
//    }
//};

//class Solution {
//public:
//    bool vis[51][51];
//    int dx[4] = { 0, 0, 1, -1 };
//    int dy[4] = { 1, -1, 0, 0 };
//    int n, m;
//    int ret, count;
//    int maxAreaOfIsland(vector<vector<int>>& nums)
//    {
//        n = nums.size(), m = nums[0].size();
//        for (int i = 0; i < n; i++)
//            for (int j = 0; j < m; j++)
//                if (!vis[i][j] && nums[i][j] == 1)
//                {
//                    count = 0;
//                    dfs(nums, i, j);
//                    ret = max(ret, count);
//                }
//        return ret;
//    }
//    void dfs(vector<vector<int>>& nums, int nx, int ny)
//    {
//        count++;
//        vis[nx][ny] = true;
//        for (int i = 0; i < 4; i++)
//        {
//            int x = nx + dx[i], y = ny + dy[i];
//            if (x >= 0 && x < n && y >= 0 && y < m && !vis[x][y] && nums[x][y] == 1)
//                dfs(nums, x, y);
//        }
//    }
//};

//class Solution {
//public:
//    int dx[4] = { 0, 0, 1, -1 };
//    int dy[4] = { 1, -1, 0, 0 };
//    bool vis[301][301] = { false };
//    int n, m, ret;
//    int numIslands(vector<vector<char>>& nums)
//    {
//        n = nums.size(), m = nums[0].size();
//        for (int i = 0; i < n; i++)
//            for (int j = 0; j < m; j++)
//                if (nums[i][j] - '0' == 1 && vis[i][j] == false)
//                {
//                    ret++;
//                    dfs(nums, i, j);
//                }
//        return ret;
//    }
//    void dfs(vector<vector<char>>& nums, int nx, int ny)
//    {
//        vis[nx][ny] = true;
//        for (int i = 0; i < 4; i++)
//        {
//            int x = nx + dx[i], y = ny + dy[i];
//            if (x >= 0 && x < n && y >= 0 && y < m && !vis[x][y] && nums[x][y] - '0' == 1)
//            {
//                dfs(nums, x, y);
//            }
//        }
//    }
//};

//class Solution {
//public:
//    bool check[51][51];
//    int dx[4] = { 0,0,1,-1 };
//    int dy[4] = { 1,-1,0,0 };
//    int color;
//    int n, m, tmp;
//    vector<vector<int>> floodFill(vector<vector<int>>& nums, int sr, int sc, int _color)
//    {
//        n = nums.size(), m = nums[0].size();
//        tmp = nums[sr][sc];
//        color = _color;
//        nums[sr][sc] = color;
//        check[sr][sc] = true;
//        dfs(nums, sr, sc);
//        return nums;
//    }
//    void dfs(vector<vector<int>>& nums, int nx, int ny)
//    {
//        for (int i = 0; i < 4; i++)
//        {
//            int x = nx + dx[i], y = ny + dy[i];
//            if (x >= 0 && x < n && y >= 0 && y < m && !check[x][y] && nums[x][y] == tmp)
//            {
//                check[x][y] = true;
//                nums[x][y] = color;
//                dfs(nums, x, y);
//            }
//        }
//    }
//};

//class Solution {
//public:
//    bool check[21][21];
//    int dx[4] = { 0,0,1,-1 };
//    int dy[4] = { 1,-1,0,0 };
//    int ret;
//    int n, m;
//    int zn;
//    int uniquePathsIII(vector<vector<int>>& g)
//    {
//        n = g.size(), m = g[0].size();
//        int x, y;
//        for (int i = 0; i < n; i++)
//            for (int j = 0; j < m; j++)
//            {
//                if (g[i][j] == 0)zn++;
//                else if (g[i][j] == 1)
//                    x = i, y = j, check[i][j] = true;
//                else if (g[i][j] == -1)check[i][j] = true;
//            }
//        dfs(g, x, y, 0);
//        return ret;
//    }
//    void dfs(vector<vector<int>>& g, int nx, int ny, int pos)
//    {
//        if (g[nx][ny] == 2)
//        {
//            if (pos - 1 == zn)ret++;
//            return;
//        }
//        for (int i = 0; i < 4; i++)
//        {
//            int x = nx + dx[i], y = ny + dy[i];
//            if (x >= 0 && x < n && y >= 0 && y < m && !check[x][y])
//            {
//                if (g[x][y] == 0)
//                {
//                    check[x][y] = true;
//                    dfs(g, x, y, pos + 1);
//                    check[x][y] = false;
//                }
//                else
//                    if (pos == zn)ret++;
//            }
//        }
//    }
//};

//class Solution {
//public:
//    bool check[16][16];
//    int dx[4] = { 0,0,-1,1 };
//    int dy[4] = { -1,1,0,0 };
//    int n, m;
//    int ret;
//    int getMaximumGold(vector<vector<int>>& nums)
//    {
//        n = nums.size(), m = nums[0].size();
//        for (int i = 0; i < n; i++)
//            for (int j = 0; j < m; j++)
//                if (nums[i][j])
//                {
//                    check[i][j] = true;
//                    dfs(nums, i, j, nums[i][j]);
//                    check[i][j] = false;
//                }
//        return ret;
//    }
//    void dfs(vector<vector<int>>& nums, int nx, int ny, int sum)
//    {
//        ret = max(ret, sum);
//        for (int i = 0; i < 4; i++)
//        {
//            int x = nx + dx[i], y = ny + dy[i];
//            if (x >= 0 && x < n && y >= 0 && y < m && !check[x][y] && nums[x][y])
//            {
//                check[x][y] = true;
//                dfs(nums, x, y, sum + nums[x][y]);
//                check[x][y] = false;
//            }
//        }
//    }
//};

//class Solution {
//public:
//    bool check[7][7];
//    int dx[4] = { 1,-1,0,0 };
//    int dy[4] = { 0,0,1,-1 };
//    int n, m;
//    bool exist(vector<vector<char>>& nums, string word)
//    {
//        n = nums.size(), m = nums[0].size();
//        for (int i = 0; i < n; i++)
//            for (int j = 0; j < m; j++)
//                if (nums[i][j] == word[0] && !check[i][j])
//                {
//                    check[i][j] = true;
//                    if (dfs(nums, word, i, j, 1))return true;
//                    check[i][j] = false;
//                }
//        return false;
//    }
//    bool dfs(vector<vector<char>>& nums, string word, int nx, int ny, int pos)
//    {
//        if (pos == word.size())return true;
//        for (int k = 0; k < 4; k++)
//        {
//            int x = nx + dx[k], y = ny + dy[k];
//            if (x >= 0 && x < n && y >= 0 && y < m && !check[x][y] && nums[x][y] == word[pos])
//            {
//                if (nums[x][y] == word[pos])
//                {
//                    check[x][y] = true;
//                    if (dfs(nums, word, x, y, pos + 1))return true;
//                    check[x][y] = false;
//                }
//            }
//        }
//        return false;
//    }
//
//};

int main()
{

	return 0;
}