#include<iostream>
using namespace std;

class A
{
public:
    void PrintA()
    {
        cout << _a << endl;
    }
private:
    int _a;
};
int main()
{
    A* p = nullptr;
    p->PrintA();
    return 0;
}

//class A
//{
//public:
//	void Print()
//	{
//		cout << "Print()" << endl;
//	}
//private:
//	int _a;
//};
//int main()
//{
//	A* p = nullptr;
//	p->Print();
//	return 0;
//}

//class Date
//{
//public:
//	void Init(int year, int month, int day)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//int main()
//{
//	Date s1;
//	s1.Init(2024, 1, 13);
//	return 0;
//}

//class Date
//{
//public:
//	void Init(int year, int month, int day)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	/*void Print(int* this)
//	{
//		cout << this->_year << " " << this->_month << " " << this->_day << endl;
//	}*/
//	void Print()
//	{
//		cout << _year << " " << _month << " " << _day << endl;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//int main()
//{
//	Date s1, s2;
//	s1.Init(2024, 1, 13);
//	s2.Init(2023, 11, 18);
//
//	//s1.Print(&s1);
//	s1.Print();
//	//s2.Print(&s2);
//	s2.Print();
//	return 0;
//}

//class Date
//{
//public:
//	void Init(int year, int month, int day)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	/*void Print(int* this)
//	{
//		cout << this->_year << " " << this->_month << " " << this->_day << endl;
//	}*/
//	void Print()
//	{
//		cout << _year << " " << _month << " " << _day << endl;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//int main()
//{
//	Date s1, s2;
//	s1.Init(2024, 1, 13);
//	s2.Init(2023, 11, 18);
//
//	//s1.Print(&s1);
//	s1.Print();
//	//s2.Print(&s2);
//	s2.Print();
//	return 0;
//}

//class Date
//{};
//
//class Book
//{
//public:
//	void func()
//	{}
//};
//
//int main()
//{
//	Date s1;
//	Book s2;
//	cout << sizeof(s1) << endl;
//	cout << sizeof(s2) << endl;
//}

//class Date
//{
//public:
//	void Init(int year)
//	{
//		_year = year;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//int main()
//{
//	Date s1;
//	cout << sizeof(s1) << endl;
//	Date s2;
//	cout << sizeof(s2) << endl;
//	return 0;
//}

//class Date
//{
//	int _year;
//	int _month;
//	int _day;
//};
//
//int main()
//{
//	Date s1;
//	Date s2;
//	return 0;
//}

//class Date
//{
//public:
//	void Init()
//	{
//		;
//	}
//
//	int _year;
//	int _month;
//	int _day;
//};
//
//int main()
//{
//	Date s1;
//	s1.Init();
//	return 0;
//}

////.h文件内
//struct Date
//{
//	void Init();
//
//	int _year;
//	int _month;
//	int _day;
//};
////.cpp文件内
//void Init()
//{
//	;
//}
//
//int main()
//{
//
//	return 0;
//}

//struct Date
//{
//	void Init(int year = 2024, int month = 3, int day = 31)
//	{
//		year = year;
//		month = month;
//		day = day;
//	}
//
//	int year;
//	int month;
//	int day;
//};
//
//int main()
//{
//	Date s1;
//	s1.Init();
//	return 0;
//}

//struct Stack
//{
//	void Init(int n = 4)
//	{
//		_a = (int*)malloc(sizeof(int) * n);
//		if (_a == nullptr)
//		{
//			perror("malloc fail");
//			return;
//		}
//		_capacity = 0;
//		_top = 0;
//	}
//
//	int* _a;
//	int _capacity;
//	int _top;
//};
//
//int main()
//{
//	Stack s1;
//	s1.Init(10);
//	return 0;
//}

//struct Book//类
//{
//	int _a;
//	int _b;
//	int _c;
//};
//
//int main()
//{
//	//类创建出的两个对象
//	struct Book s1;
//	struct Book s2;
//	return 0;
//}