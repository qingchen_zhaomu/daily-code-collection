#define _CRT_SECURE_NO_WARNINGS 1
#include"tree.h"


TN* BuyBTNode(int val)
{
	TN* newnode = (TN*)malloc(sizeof(TN));
	if (newnode == NULL)
	{
		perror("malloc fail");
		return NULL;
	}
	newnode->val = val;
	newnode->left = NULL;
	newnode->right = NULL;
	return newnode;
}

// 手搓一棵树
TN* CreateTree()
{
	TN* n1 = BuyBTNode(1);
	TN* n2 = BuyBTNode(2);
	TN* n3 = BuyBTNode(3);
	TN* n4 = BuyBTNode(4);
	TN* n5 = BuyBTNode(5);
	TN* n6 = BuyBTNode(6);
	TN* n7 = BuyBTNode(7);
	TN* n8 = BuyBTNode(8);


	n1->left = n2;
	n1->right = n4;
	n2->left = n3;
	n4->left = n5;
	n4->right = n6;
	n5->left = n7;
	n5->right = n8;

	return n1;
}


//前序遍历
void PreOrder(TN* root)
{
	if (root == NULL)
	{
		printf("N ");
		return;
	}
	printf("%d ", root->val);
	PreOrder(root->left);
	PreOrder(root->right);
}

//中序遍历
void InOrder(TN* root)
{
	if (root == NULL)
	{
		printf("N ");
		return;
	}
	PreOrder(root->left);
	printf("%d ", root->val);
	PreOrder(root->right);
}

//后序遍历
void PostOrder(TN* root)
{
	if (root == NULL)
	{
		printf("N ");
		return;
	}
	PreOrder(root->left);
	PreOrder(root->right);
	printf("%d ", root->val);
}

//节点个数
int TreeSize(TN* root)
{
	return root == NULL ? 0 : TreeSize(root->left) + TreeSize(root->right) + 1;
}


int BinaryTreeLeafSize(TN* root)
{
	if (root == NULL)return 0;
	if (root->left == NULL && root->right == NULL)return 1;

	int a1 = BinaryTreeLeafSize(root->left);
	int a2 = BinaryTreeLeafSize(root->right);

	return a1 + a2;
}

//树的高度
int TreeHight(TN* root)
{
	if (root == NULL)return 0;
	int leftnum = TreeHight(root->left);
	int rightnum = TreeHight(root->right);
	return leftnum > rightnum ? leftnum + 1 : rightnum + 1;
}

int TreeKLevel(TN* root, int k)
{
	if (root == NULL)return 0;
	if (k == 1)return 1;
	return TreeKLevel(root->left, k - 1) + TreeKLevel(root->right, k - 1);
}    

TN* TreeFind(TN* root, int x)
{
	if (root == NULL)return NULL;
	if (root->val == x)return root;

	TN* NodeLeft = TreeFind(root->left, x);
	if (NodeLeft)return NodeLeft;

	TN* NodeRight = TreeFind(root->right, x);
	if (NodeRight)return NodeRight;

	return NULL;
}

int main()
{
	TN* root = CreateTree();
	PreOrder(root);
	printf("\n");

	printf("%d\n", TreeSize(root));

	TN* tmp = TreeFind(root, 5);
	printf("%d\n", tmp->val);

	printf("%d\n", TreeKLevel(root, 3));

	printf("%d\n", BinaryTreeLeafSize(root));

	return 0;
}