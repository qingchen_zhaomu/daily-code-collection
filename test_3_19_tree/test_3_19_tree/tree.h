#pragma once
#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
#include<stdlib.h>

typedef struct TreeNode
{
	struct TreeNode* left;
	struct TreeNode* right;
	int val;
}TN;

void PreOrder(TN* root);
void InOrder(TN* root);
void PostOrder(TN* root);

int TreeSize(TN* root);
int BinaryTreeLeafSize(TN* root);
int TreeHight(TN* root);
int TreeKLevel(TN* root, int k);
TN* TreeFind(TN* root, int x);