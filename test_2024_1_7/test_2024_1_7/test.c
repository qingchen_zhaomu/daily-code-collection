#define _CRT_SECURE_NO_WARNINGS 1

#include<stdio.h>

int main()
{
	int num[5] = { 0,2,1,4,3 };
	int* p = num;
	*(p + 1) = 1;
	printf("%d%d%d", *p, num[1], (*p)++);
	return 0;
}

//int main()
//{
//	int a, b;
//	int i = 0;
//	scanf("%d", &i);
//	for (a = 1; a <= i; a++)
//	{
//		for (b = 1; b <= a; b++)
//		{
//			printf("%d", b);
//		}
//		printf("\n");
//	}
//	return 0;
//}