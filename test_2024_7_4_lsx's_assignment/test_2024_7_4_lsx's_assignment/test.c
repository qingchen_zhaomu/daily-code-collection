#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>

typedef struct student
{
	int num; //学号
	char name[10]; //名字
	int c, oop, db; //三门课成绩
	float avg; //平均成绩
}stu;

void PrintInfo(stu* s1)
{
	printf("\n学号      姓名      C语言   OOP     数据库   平均成绩\n");
	printf("%-10d%-10s%-8d%-8d%-8d%-6.1f\n", s1->num, s1->name, s1->c, s1->oop, s1->db, s1->avg);
}

int main()
{
	int i;
	stu s[5];
	
	//初始化
	for (i = 0; i < 5; i++)
	{
		printf("请输入第%d个学生的信息：\n", i+1);
		scanf("%d%s%d%d%d", &s[i].num, &s[i].name, &s[i].c, &s[i].oop, &s[i].db);

		s[i].avg = (float)(s[i].c + s[i].oop + s[i].db) / 3;
	}
	for (int j = 0; j < 5; j++){ PrintInfo(&s[j]); }
	return 0;
}
