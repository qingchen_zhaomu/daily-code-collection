#pragma once
#include<iostream>
using namespace std;
#include<vector>
#include<string>
#include<assert.h>

template<class k>
struct HashFunc
{
	size_t operator()(const k& key)
	{
		return (size_t)key;
	}
};

template<>
struct HashFunc<string>
{
	size_t operator()(const string& key)
	{
		size_t hashi = 0;
		for (const auto& e : key)
		{
			hashi *= 31;
			hashi += e;
		}
		return hashi;
	}
};



template<class T>
struct hashNode
{
	hashNode<T>* _next;
	T _data;

	hashNode(const T& data)
		:_data(data)
		, _next(nullptr)
	{}
};

// 前置声明，因为后续的operator++需要用到hashtables
// 但是我们的hashtables也要用到迭代器
// 这就造成了一个先有鸡还是先有蛋的问题
// 所以我们在这里的解决方法是前置声明
template<class k, class T, class KeyOfT, class Hash>
class HashTable;

// 迭代器
template<class k, class T, class KeyOfT, class Hash, class Ptr, class Ref>
struct HTIterator
{
	typedef hashNode<T> Node;
	typedef HTIterator<k, T, KeyOfT, Hash, Ptr, Ref> Self;

	const HashTable<k, T, KeyOfT, Hash>* _ht;
	Node* _node;

	HTIterator(Node* node, const HashTable<k, T, KeyOfT, Hash>* ht)
		:_node(node)
		, _ht(ht)
	{}

	Ref operator*()
	{
		return _node->_data;
	}

	Ptr operator->()
	{
		return &_node->_data;
	}

	bool operator!=(const Self& s)
	{
		return _node != s._node;
	}

	bool operator==(const Self& s)
	{
		return _node == s._node;
	}

	Self& operator++()
	{
		if (_node->_next)
		{
			_node = _node->_next;
		}
		else
		{
			Hash hs;
			KeyOfT kot;

			size_t hashi = hs(kot(_node->_data)) % _ht->_tables.size();
			++hashi;
			while (hashi < _ht->_tables.size())
			{
				if (_ht->_tables[hashi]) { break; }
				++hashi;
			}
			if (_ht->_tables.size() == hashi)
				_node = nullptr;
			else
				_node = _ht->_tables[hashi];
		}
		return *this;
	}

};




template<class k, class T, class KeyOfT, class Hash>
class HashTable
{
	typedef hashNode<T> Node;

	template<class k, class T, class KeyOfT, class Hash, class Ptr, class Ref>
	friend struct HTIterator;
public:

	typedef HTIterator<k, T, KeyOfT, Hash, T*, T&> Iterator;
	typedef HTIterator<k, T, KeyOfT, Hash, const T*, const T&> ConstIterator;

	Iterator Begin()
	{
		if (_n == 0)return End();

		for (size_t i = 0; i < _tables.size(); i++)
		{
			if (_tables[i])return Iterator(_tables[i], this);
		}
		return End();
	}

	Iterator End()
	{
		return Iterator(nullptr, this);
	}

	ConstIterator Begin()const
	{
		if (_n == 0)return End();

		for (size_t i = 0; i < _tables.size(); i++)
		{
			if (_tables[i])return ConstIterator(_tables[i], this);
		}
	}

	ConstIterator End()const
	{
		return ConstIterator(nullptr, this);
	}


	HashTable()
	{
		_tables.resize(50, nullptr);
	}

	~HashTable()
	{
		for (size_t i = 0; i < _tables.size(); i++)
		{
			Node* cur = _tables[i];
			while (cur)
			{
				Node* next = cur->_next;
				delete cur;
				cur = next;
			}
			_tables[i] = nullptr;
		}
	}

	pair<Iterator, bool> Insert(const T& data)
	{
		Hash hs;
		KeyOfT kot;
		size_t hashi = hs(kot(data)) % _tables.size();

		// 用find找这个节点有没有出现过
		Iterator it = Find(kot(data));
		if (it != End())return { it, false };

		// 判断扩容
		if (_n == _tables.size())
		{
			// 进入判断逻辑
			vector<Node*> newtab(_tables.size() * 2, nullptr);
			// 拷贝数据
			for (size_t i = 0; i < _tables.size(); i++)
			{
				Node* cur = _tables[i];
				while (cur)
				{
					Node* next = cur->_next;
					size_t hasii = hs(kot(cur->_data)) % newtab.size();

					cur->_next = newtab[hasii];
					newtab[hasii] = cur;

					cur = next;
				}
				_tables[i] = nullptr;
			}
			_tables.swap(newtab);
		}

		// 插入逻辑，头插
		Node* newnode = new Node(data);
		newnode->_next = _tables[hashi];
		_tables[hashi] = newnode;
		++_n;

		return { Iterator(newnode, this), true };;
	}

	Iterator Find(const k& key)
	{
		Hash hs;
		KeyOfT kot;
		size_t hashi = hs(key) % _tables.size();
		Node* cur = _tables[hashi];

		while (cur)
		{
			if (kot(cur->_data) == key)
			{
				return Iterator(cur, this);
			}

			cur = cur->_next;
		}
		return End();
	}

	bool Erase(const k& key)
	{
		Hash hs;
		KeyOfT kot;

		size_t hashi = hs(key) % _tables.size();
		Node* cur = _tables[hashi];
		Node* prev = nullptr;

		while (cur)
		{
			if (kot(cur->_data) == key)
			{
				if (!prev)
				{
					_tables[hashi] = cur->_next;
				}
				else
				{
					prev->_next = cur->_next;
				}
				delete cur;
				--_n;
				return true;
			}
			prev = cur;
			cur = cur->_next;
		}
		return false;
	}



private:
	vector<Node*> _tables;
	size_t _n = 0;
};








