#pragma once
#include<iostream>
using namespace std;
#include<vector>
#include<string>
#include<assert.h>

template<class k>
struct HashFunc
{
	size_t operator()(const k& key)
	{
		return (size_t)key;
	}
};

template<>
struct HashFunc<string>
{
	size_t operator()(const string& key)
	{
		size_t hash = 0;
		for (auto e : key)
		{
			hash *= 31;
			hash += e;
		}
		return hash;
	}
};


namespace hash_bucket
{
	template<class T>
	struct hashNode
	{
		hashNode<T>* _next;
		T _data;

		hashNode(const T& data)
			:_data(data)
			,_next(nullptr)
		{}
	};


	template<class k, class T, class KeyOfT, class Hash>
	class HashTable;

	template<class k, class T, class KeyOfT, class Hash, class Ptr, class Ref>
	struct HTIterator
	{
		typedef hashNode<T> Node;
		typedef HTIterator<k, T, KeyOfT, Hash, Ptr, Ref> Self;

		Node* _node;
		const HashTable<k, T, KeyOfT, Hash>* _ht;

		HTIterator(Node* node, const HashTable<k, T, KeyOfT, Hash>* ht)
			:_node(node)
			,_ht(ht)
		{}

		Ref operator* ()
		{
			return _node->_data;
		}

		Ptr operator->()
		{
			return &_node->_data;
		}

		bool operator!=(const Self& s)
		{
			return _node != s._node;
		}

		bool operator==(const Self& s)
		{
			return _node == s._node;
		}

		Self& operator++()
		{
			if (_node->_next)
			{
				_node = _node->_next;
			}
			else
			{
				Hash hs;
				KeyOfT kot;
				size_t hashi = hs(kot(_node->_data)) % _ht->_tables.size();
				++hashi;

				while (hashi < _ht->_tables.size())
				{
					Node* cur = _ht->_tables[hashi];
					if (cur)break;
					hashi++;
				}

				if (hashi == _ht->_tables.size())
					_node = nullptr;
				else
					_node = _ht->_tables[hashi];
			}
			return *this;
		}

	};



	template<class k, class T, class KeyOfT, class Hash>
	class HashTable
	{
		template<class k, class T, class KeyOfT, class Hash, class Ptr, class Ref>
		friend struct HTIterator;

		typedef hashNode<T> Node;
	public:
		typedef HTIterator<k, T, KeyOfT, Hash, T*, T&> Iterator;
		typedef HTIterator<k, T, KeyOfT, Hash, const T*, const T&> ConstIterator;

		Iterator Begin()
		{
			if (_n == 0)return End();

			for (size_t i = 0; i < _tables.size(); i++)
			{
				if (_tables[i])return Iterator(_tables[i], this);
			}
			return End();
		}

		Iterator End()
		{
			return Iterator(nullptr, this);
		}

		ConstIterator Begin()const
		{
			if (_n == 0)return End();

			for (size_t i = 0; i < _tables.size(); i++)
			{
				if (_tables[i])return ConstIterator(_tables[i], this);
			}
		}

		ConstIterator End()const
		{
			return ConstIterator(nullptr, this);
		}


		HashTable()
		{
			_tables.resize(50, nullptr);
		}

		~HashTable()
		{
			for (size_t i = 0; i < _tables.size(); i++)
			{
				Node* cur = _tables[i];
				while (cur)
				{
					Node* next = cur->_next;
					delete cur;
					cur = next;
				}
				_tables[i] = nullptr;
			}
		}

		pair<Iterator, bool> Insert(const T& data)
		{
			Hash hs;
			KeyOfT kot;

			// 先找这个数据在不在
			Iterator it = Find(kot(data));
			if (it != End())return { it, false };

			size_t hashi = hs(kot(data)) % _tables.size();

			// 判断是否需要扩容
			if (_n == _tables.size())
			{
				vector<Node*> newht(_tables.size() * 2, nullptr);
				for (size_t i = 0; i < _tables.size(); i++)
				{
					Node* cur = _tables[i];
					while (cur)
					{
						Node* next = cur->_next;

						size_t hasii = hs(kot(cur->_data)) % _tables.size();
						// 头插
						cur->_next = newht[hasii];
						newht[hasii] = cur;

						cur = next;
					}
					_tables[i] = nullptr;
				}
				_tables.swap(newht);
			}

			Node* newnode = new Node(data);
			newnode->_next = _tables[hashi];
			_tables[hashi] = newnode;
			++_n;

			return { Iterator(newnode, this), true };
		}


		Iterator Find(const k& key)
		{
			Hash hs;
			KeyOfT kot;
			size_t hashi = hs(key) % _tables.size();
			Node* cur = _tables[hashi];

			while (cur)
			{
				if (kot(cur->_data) == key)
				{
					return Iterator(cur, this);
				}

				cur = cur->_next;
			}
			return End();
		}

		bool Erase(const k& key)
		{
			KeyOfT kot;
			Hash hs;

			size_t hashi = hs(key) % _tables.size();
			Node* prev = nullptr;
			Node* cur = _tables[hashi];

			while (cur)
			{
				if (kot(cur->_data) == key)
				{
					if (!prev)
					{
						_tables[hashi] = cur->_next;
					}
					else
					{
						prev->_next = cur->_next;
					}
					delete cur;
					--_n;
					return true;
				}
				prev = cur;
				cur = cur->_next;
			}
			return false;
		}


	private:
		vector<Node*> _tables;
		size_t _n = 0;
	};



}









