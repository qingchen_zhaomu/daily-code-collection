#include"List.h"

void ListTest01()
{
	LTNode* plist = LTInit();

	LTPushBack(plist, 1);
	LTPushBack(plist, 2);
	LTPushBack(plist, 3);
	LTPushBack(plist, 4);
	LTPrint(plist);

	LTPushFront(plist, 5);
	LTPrint(plist);

	LTPopBack(plist);
	LTPopFront(plist);
	LTPrint(plist);

	LTDesTroy(plist);
	//LTPrint(plist);
	plist = NULL;
}

int main()
{
	ListTest01();
	return 0;
}