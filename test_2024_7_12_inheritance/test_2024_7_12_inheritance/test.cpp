#include<iostream>
using namespace std;
#include<vector>
#include<string>

//class Person
//{
//public:
//	void Print()
//	{
//		cout << _age << " " << _name << endl;
//		cout << _tel << endl;
//	}
//protected:
//	int _age;
//	string _name;
//private:
//	int _tel;
//};
//
//class student : public Person
//{
//	//父类的私有在子类中不可使用，表示不能被继承
//	// 但是在子类中存在，如果要调用的话，可以在父类中设置可以调用的函数
//	//_val = 1;
//protected:
//	int _stuid;
//};
//class teacher : public Person
//{
//protected:
//	int _jobid;
//};


/// ////////////////////////////////////////////////////
// Student的_num和Person的_num构成隐藏关系，可以看出这样代码虽然能跑，但是非常容易混淆
//class Person
//{
//public:
////protected:
//	void Print()
//	{
//		cout << "hahaha" << endl;
//	}
//	string _name = "小李子"; // 姓名
//	int _num = 111;
//	// 身份证号
//};
//class Student : public Person
//{
//public:
//	void Print(int i)
//	{
//		cout << "eeeeeeeeee" << endl;
//		/*cout << " 姓名:" << _name << endl;
//		cout << " 身份证号:" << Person::_num << endl;
//		cout << " 学号:" << _num << endl;*/
//	}
////protected:
//	int _num = 999; // 学号
//};
//
//int main()
//{
//	Student s;
//	//cout << s._num << endl;
//	//s.Print();
//	return 0;
//}



/// ////////////////////////////////////////////////
class Person
{
public:
	Person(const char* name = "peter")
		: _name(name)
	{
		cout << "Person()" << endl;
	}
	Person(const Person& p)
		: _name(p._name)
	{
		cout << "Person(const Person& p)" << endl;
	}
	Person& operator=(const Person& p)
	{
		cout << "Person operator=(const Person& p)" << endl;
		if (this != &p)
			_name = p._name;
		return *this;
	}
	~Person()
	{
		cout << "~Person()" << endl;
	}
protected:
	string _name; // 姓名
};

class Student : public Person
{
public:
	//构造
	Student(const char* name = "", int num = 0)
		:Person(name)
		,_num(num)
	{}

	//拷贝构造
	Student(const Student& st)
		:Person(st)
	{
		_num = st._num;
	}
	Student& operator=(Student& st)
	{
		if (this != &st)
		{
			Person::operator=(st);
			_num = st._num;
		}
		return *this;
	}
	~Student()
	{
		cout << "~Student()" << endl;
	}
protected:
	int _num; //学号
};



/// //////////////////////////////////////////////////
class A
{
	int _a = 0;
public:
	~A()
	{
		cout << "~A" << endl;
	}
};

class B
{
	A c;
public:
	~B()
	{
		cout << "~B" << endl;
	}
};

int main()
{
	//Student s;
	B b;
	return 0;
}
