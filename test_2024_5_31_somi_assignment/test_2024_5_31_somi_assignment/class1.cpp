#include<iostream>
using namespace std;
#include<vector>
#include<string>

class Student {
public:
    Student(string name = "", int rollNumber = 0, int subject_size = 1)
        :_name(name)
        , _rollNumber(rollNumber)
    {
        _marks.reserve(subject_size);
    }

    int calculateAverage(const Student& student) 
    {
        int sum = 0;
        for (auto e : student._marks)
        {
            sum += e;
        }
        return sum /= student._marks.size();
    }

    void Enter_stu_info(int n)
    {
        cout << "\nEnter details for student " << n + 1 << ":" << endl;
        cout << "Name: ";
        getchar();
        getline(cin, _name);
        //getchar();
        cout << "Roll Number: ";
        cin >> _rollNumber;
        cout << "How many subjects you wants to enter:";
        int subject_num; 
        cin >> subject_num;
        cout << "Marks in " << subject_num << " subjects: ";
        _marks.reserve(subject_num + 1);
        for (int j = 0; j < subject_num; ++j)
        {
            int i; cin >> i;
            _marks.push_back(i);
        }
    }

private:
    string _name;
    int _rollNumber;
    vector<int> _marks;
};



int main() {
    int numStudents;
    cout << "Enter the number of students: ";
    cin >> numStudents;

    vector<Student> student(numStudents);

    //Enter details for student
    for (int i = 0; i < numStudents; i++)
        student[i].Enter_stu_info(i);

    // Display average marks of each student
    for (int i = 0; i < numStudents; i++)
    {
        cout << "Student " << i + 1 << " :" 
        << student[i].calculateAverage(student[i]) << endl;
    }

    // Finding student with highest average marks
    int num = 0, high_score = 0;
    for (int i = 0; i < numStudents; i++)
    {
        int n = student[i].calculateAverage(student[i]);
        if (high_score < n) num = i + 1, high_score = n;
    }
    cout << "The highest student is student " << num << ", the score is :" << high_score << endl;

    return 0;
}





//class Rectangle
//{
//public:
//	Rectangle(size_t length = 0, size_t width = 0)
//		:_length(length)
//		,_width(width)
//	{}
//	size_t area() { return _length * _width; }
//	size_t perimeter() { return 2 * (_length + _width); }
//
//protected:
//	size_t _length;
//	size_t _width;
//};
//
//class Square : public Rectangle
//{
//public:
//	Square(size_t len = 0)
//	{
//		_length = len;
//		_width = len;
//	}
//	size_t area(){ return _length * _width; }
//};
//
//int main()
//{
//	Square s1(3);
//	cout << s1.area() << endl;
//	return 0;
//}
