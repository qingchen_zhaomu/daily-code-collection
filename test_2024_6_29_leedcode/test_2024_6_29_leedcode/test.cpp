#include<iostream>
using namespace std;
#include<vector>
#include<string>
#include<queue>
#include<functional>

//class Solution {
//public:
//    int dx[4] = { 0, 0, 1, -1 };
//    int dy[4] = { 1, -1, 0, 0 };
//    typedef pair<int, int> PII;
//    int m, n;
//
//    int bfs(vector<vector<int>>& f, int bx, int by, int target)
//    {
//        m = f.size(), n = f[0].size();
//        bool vis[51][51] = { false };
//        queue<PII> q;
//        q.push({ bx, by });
//        vis[bx][by] = true;
//        int ret = 0;
//        while (q.size())
//        {
//            ret++;
//            int sz = q.size();
//            while (sz--)
//            {
//                auto [a, b] = q.front();
//                q.pop();
//                for (int i = 0; i < 4; i++)
//                {
//                    int x = a + dx[i];
//                    int y = b + dy[i];
//                    if (x >= 0 && x < m && y >= 0 && y < n && f[x][y] > 0 && vis[x][y] == false)
//                    {
//                        if (f[x][y] == target)
//                        {
//                            f[x][y] = 1;
//                            return ret;
//                        }
//                        vis[x][y] = true;
//                        q.push({ x, y });
//                    }
//                }
//            }
//        }
//        return -1;
//    }
//
//    int cutOffTree(vector<vector<int>>& f)
//    {
//        m = f.size(), n = f[0].size();
//        vector<PII> nums;
//        for (int i = 0; i < m; i++)
//            for (int j = 0; j < n; j++)
//                if (f[i][j] > 1) nums.push_back({ i, j });
//        sort(nums.begin(), nums.end(), [&](const pair<int, int>& p1, const pair<int, int>& p2)
//            {
//                return f[p1.first][p1.second] < f[p2.first][p2.second];
//            });
//
//        int ret = 0;//用于记录总的走的步数
//        int bx = 0, by = 0;
//        //砍树主逻辑
//        for (auto& [a, b] : nums)
//        {
//            int time = 0;//用于记录每一次找下一个节点走的步数
//            time = bfs(f, bx, by, f[a][b]);
//            if (bx == a && by == b)time = 0;
//            if (time == -1)return -1;
//            ret += time;
//            bx = a, by = b;
//        }
//        return ret;
//    }
//};

int main()
{
	vector<int> v1 = { 4,7,22,6,9,3,6,3,8,1,9 };
	sort(v1.begin(), v1.end());
	for (auto e : v1)cout << e << " ";
	return 0;
}

//class Solution {
//public:
//    int ladderLength(string beginWord, string endWord, vector<string>& wordList)
//    {
//        int n = beginWord.size();
//        unordered_set<string> vis(wordList.begin(), wordList.end());//存储字典中存在的单词
//        unordered_set<string> hash;//存储遍历之后存在过的string
//        string library = "abcdefghijklmnopqrstuvwxyz";
//
//        queue<string> q;
//        q.push(beginWord);
//        hash.insert(beginWord);
//        int ret = 1;
//
//        while (q.size())
//        {
//            int sz = q.size();
//            //最外层循环代表的是每一层的遍历
//            ret++;
//            while (sz--)
//            {
//                string s = q.front();
//                q.pop();
//                for (int i = 0; i < n; i++)
//                {
//                    string tmp = s;
//                    for (int j = 0; j < 26; j++)
//                    {
//                        tmp[i] = library[j];
//                        if (vis.count(tmp) && !hash.count(tmp))
//                        {
//                            if (tmp == endWord) return ret;
//                            hash.insert(tmp);
//                            q.push(tmp);
//                        }
//                    }
//                }
//            }
//        }
//        return 0;
//    }
//};

//class Solution {
//public:
//    int minMutation(string startGene, string endGene, vector<string>& bank)
//    {
//        unordered_set<string> vis(bank.begin(), bank.end());//存储基因库中的数据
//        unordered_set<string> hash;
//        string Genetype = "ACGT";
//        if (startGene == endGene) return 0;
//        if (vis.count(endGene) == 0) return -1;
//        queue<string> q;
//        q.push(startGene);
//        hash.insert(startGene);
//        int ret = 0;
//        while (q.size())
//        {
//            int sz = q.size();
//            ret++;
//            while (sz--)
//            {
//                string t = q.front();
//                q.pop();
//                for (int i = 0; i < 8; i++)
//                {
//                    string tmp = t;
//                    for (int j = 0; j < 4; j++)
//                    {
//                        tmp[i] = Genetype[j];
//                        if (vis.count(tmp) && !hash.count(tmp))
//                        {
//                            if (tmp == endGene) return ret;
//                            q.push(tmp);
//                            hash.insert(tmp);
//                        }
//                    }
//                }
//            }
//        }
//        return -1;
//    }
//};

//class Solution {
//public:
//    typedef pair<int, int> PII;
//    bool vis[101][101] = { false };
//    int dx[4] = { 0, 0, 1, -1 };
//    int dy[4] = { 1, -1, 0, 0 };
//    int m, n;
//
//    int nearestExit(vector<vector<char>>& maze, vector<int>& entrance)
//    {
//        m = maze.size(), n = maze[0].size();
//        // 从 entrance 位置出发开始向外一层一层遍历
//        queue<PII> q;
//        q.push({ entrance[0], entrance[1] });
//        vis[entrance[0]][entrance[1]] = true;
//        int time = 0;
//        while (q.size())
//        {
//            int sz = q.size();
//            time++;
//            for (int i = 0; i < sz; i++)
//            {
//                // 将该层的元素同时向外扩一层
//                auto [a, b] = q.front();
//                q.pop();
//                for (int j = 0; j < 4; j++)
//                {
//                    int x = a + dx[j];
//                    int y = b + dy[j];
//                    if (x >= 0 && x < m && y >= 0 && y < n &&
//                        maze[x][y] == '.' && vis[x][y] == false)
//                    {
//                        if (x == 0 || x == m - 1 || y == 0 || y == n - 1) return time;
//                        q.push({ x, y });
//                        vis[x][y] = true;
//                    }
//                }
//            }
//        }
//        return -1;
//    }
//};

//class Solution {
//public:
//    typedef pair<int, int> PII;
//    bool vis[201][201] = { false };
//    int dx[4] = { 0, 0, 1, -1 };
//    int dy[4] = { 1, -1, 0, 0 };
//    int m, n;
//
//    void bfs(vector<vector<char>>& nums, int i, int j)
//    {
//        queue<PII> q;
//        q.push({ i, j });
//        if (!(i == 0 || i == nums.size() - 1 || j == 0 || j == nums[0].size() - 1))
//            nums[i][j] = 'X';
//        while (q.size())
//        {
//            auto [a, b] = q.front();
//            q.pop();
//            for (int k = 0; k < 4; k++)
//            {
//                int x = a + dx[k];
//                int y = b + dy[k];
//                if (x >= 0 && x < m && y >= 0 && y < n && vis[x][y] != true && nums[x][y] == 'O')
//                {
//                    q.push({ x, y });
//                    vis[x][y] = true;
//                    if (!(i == 0 || i == nums.size() - 1 || j == 0 || j == nums[0].size() - 1))
//                        nums[x][y] = 'X';
//                }
//            }
//        }
//    }
//
//    void solve(vector<vector<char>>& nums)
//    {
//        m = nums.size(), n = nums[0].size();
//        // 先处理边界情况
//        for (int i = 0; i < n; i++)
//        {
//            if (nums[0][i] == 'O')bfs(nums, 0, i);
//            if (nums[m - 1][i] == 'O')bfs(nums, m - 1, i);
//        }
//        for (int i = 0; i < m; i++)
//        {
//            if (nums[i][0] == 'O')bfs(nums, i, 0);
//            if (nums[i][n - 1] == 'O')bfs(nums, i, n - 1);
//        }
//        for (int i = 1; i < m - 1; i++)
//        {
//            for (int j = 1; j < n - 1; j++)
//            {
//                if (nums[i][j] == 'O' && vis[i][j] == false) nums[i][j] = 'X';//bfs(nums, i, j);
//            }
//        }
//    }
//};

//class Solution {
//public:
//    typedef pair<int, int> PII;
//    bool vis[51][51] = { false };
//    int dx[4] = { 0, 0, 1, -1 };
//    int dy[4] = { 1, -1, 0, 0 };
//    int m, n;
//
//    int maxAreaOfIsland(vector<vector<int>>& grid)
//    {
//        m = grid.size(), n = grid[0].size();
//        int ret = 0, tmp = 0;
//        if (m == 1 && n == 1 && grid[0][0] == 1) return 1;
//        else if (m == 1 && n == 1 && grid[0][0] == 0) return 0;
//        for (int i = 0; i < m; i++)
//        {
//            for (int j = 0; j < n; j++)
//            {
//                queue<PII> q;
//                if (grid[i][j] == 1 && vis[i][j] == false)
//                {
//                    tmp = 0;
//                    q.push({ i, j });
//                    vis[i][j] = true;
//                    tmp++;
//                    while (q.size())
//                    {
//                        auto [a, b] = q.front();
//                        q.pop();
//                        for (int k = 0; k < 4; k++)
//                        {
//                            int x = a + dx[k];
//                            int y = b + dy[k];
//                            if (x >= 0 && x < m && y >= 0 && y < n
//                                && vis[x][y] != true && grid[x][y] == 1)
//                            {
//                                q.push({ x, y });
//                                vis[x][y] = true;
//                                tmp++;
//                            }
//                        }
//                    }
//                    ret = max(ret, tmp);
//                }
//            }
//        }
//        return ret;
//    }
//};

//int main()
//{
//
//	return 0;
//}
